<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

if( ! is_array($_SESSION['cart']) or ! $_SESSION['cart'] ){
    hle('cart') ;
}

if ($_SESSION['test']) {
    $stripe_publishable_key = $config['stripe']['test_publishable_key'];
} else {
    $stripe_publishable_key = $config['stripe']['live_publishable_key'];
}


$r2 = sql_fetch_by_key($db, 'pages', 'id', $config['site_sections']['ece']) ;//fetch page primary directory page for breadcrumbs
$page['breadcrumbs'] = '<a href="/">Home</a> › <a href="/'.$r2['keyword'].'">'.$r2['title'].'</a> › Cart';
$page['nav_chain_keys'][] = $config['site_sections']['ece'];


$page['head_extra'] .= '';
$cc = form_load_clear('checkout_payment') ;
$r = $_SESSION['customer'] ;


ob_start();
cart_display($db, array(
    'cart'=>$_SESSION['cart'],
    'discounts'=>$_SESSION['discounts'],
    'customer'=>$_SESSION['customer'],
    'show_tax'=>true,
    'show_controls'=>false,
));
$cart = ob_get_clean();

ob_start();
write_months('cc_month',$cc['cc_month'],'','class="card-expiry-month required" style="width:auto;margin-right:5px;"') ;
write_years('cc_year',$cc['cc_year'],'',date('y'),date('y')+10,'class="card-expiry-year required" style="width:auto"') ;
$ccdate = ob_get_clean();


if($_SESSION['paymentamount']){
    $amount = $_SESSION['paymentamount'];
}else{
    $amount = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
}


$content = $twigpanel->render('cart_payment.twig', array(
    'r' => $r,
    'c' => $_SESSION['cart'],
    'cc' => $cc,
    'amount' => $amount,
    'campers' => $_SESSION['campers'],
    'cid' => key($_SESSION['campers']),
    'stripe_publishable_key' => $stripe_publishable_key,
    'carthtml' => $cart,
    'config' => $config,
    'ccdate' => $ccdate
));


//$page['head'] = '<script type="text/javascript" src="/lib/jquery.validate.min.js"></script>';
$page['head'] .= '<script type="text/javascript" src="https://js.stripe.com/v1/"></script>';

$page['title'] = 'Shopping Cart: Payment';
$page['page_vars']['content'] = $content;
