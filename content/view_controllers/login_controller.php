<?
if ($page['page_vars']['file_path']) {
    include('templates/include/' . $page['page_vars']['file_path']);
}
$page['page_vars']['heading'] = 'Member Login';

$currenturl = strtok($_SERVER["REQUEST_URI"],'?');

ob_start();
?>
    <select name="state" id="state" required>
        <?
        foreach($config['us_states'] as $k => $v){
            $s = '';
            if($k == $r['state']){
                $s = 'selected="selected"';
            }
            ?>
            <option value="<?=$k ?>" <?=$s ?>><?=ucwords(strtolower($v)) ?></option>
            <?
        }
        ?>
    </select>
<?
$state_select = ob_get_clean();

switch ($_GET['a']) {
    case 'forgot_pw':
        $page['page_vars']['content'] = '
            <div class="form-contain">
                <div class="form-password">
                    <h3>Retrieve Password</h3>
                    <form action="/a.php?a=forgot_pass" method="post">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-element">
                                <input id="ret-email" type="email" name="user_email" />
                                <label for="ret-email">Email</label>
                            </div>
                            <input type="submit" value="Retrieve Password" name="retrieve" />
                            <hr />
                             <a href="'.$currenturl.'">Back to Login</a>
                        </div>
                    </form>
                </div>
            </div>
            ';
        break;
    case 'register':

        $page['page_vars']['content'] = '
            <div class="form-contain">
                <div class="form-register">
                    <h3>Register</h3>
                    <form action="/a.php?a=register" method="post" id="regform">
                        <input type="hidden" name="account_type" value="1" />
                        <div class="row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-element">
                                <input id="reg-displayname" type="text" name="display_name" required />
                                <label for="reg-displayname">Your Name*</label>
                            </div>
                            <div class="form-element">
                                <input id="reg-email" type="email" name="user_email" required />
                                <label for="reg-email">Email*</label>
                            </div>
                            <div class="form-element">
                                <input id="reg-pass" type="password" name="password" required />
                                <label for="reg-pass">Password*</label>
                            </div>
                            <div class="form-element">
                                <input id="reg-pass2" type="password" name="password2" required />
                                <label for="reg-pass2">Re-Type Password*</label>
                            </div>
                        </div>
                        </div>

                        <input type="submit" value="Register" name="register" />
                        <hr />
                        <a href="'.$currenturl.'">Back to Login</a>
                    </form>
                </div>
            </div>
            ';
        break;
    case 'login':
    default:
        //see mod_esn for login_form()
        $page['page_vars']['content'] = '<div class="form-contain"><div class="form-login"><div class="col-md-4 col-md-offset-4"><br>'.
            login_form($currenturl).
            '</div></div></div>';
        break;
}
ob_start();
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#regform').validate({
            errorPlacement: function (error, element) {
                error.insertBefore(element);
            }
        });
    });
</script>
<?
$page['page_vars']['content'] .= ob_get_clean();
?>