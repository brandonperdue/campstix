<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$meta_title = 'Register - Enter Camper Info' ;
$current_nav = 'cart' ;

// make sure cart isn't empty
if( ! is_array($_SESSION['cart']) or ! $_SESSION['cart'] ){
    hle('/cart') ;
}


$form_attempt = form_load_clear('cart-campers') ;
d('form_attempt') ;
d($form_attempt) ;
if( $form_attempt ){
    $r = $form_attempt ;
}else{
    $r = $_SESSION['customer'] ;
}

if($_GET['c']){
    $campertitle = 'Edit';

    //set id in array instead of just as the key (it is used in the form)
    $_SESSION['campers'][$_GET['c']]['id'] = $_GET['c'];

    //get product details
    $p = sql_fetch_by_key($db, 'products', 'id', $_SESSION['campers'][$_GET['c']]['pid']);

    //new camper
    $content = $twigpanel->render('cart_campers.twig', array(
        'r' => $r,
        'p' => $p,
        'cart' => $_SESSION['cart'],
        'camper' => $_SESSION['campers'][$_GET['c']],
        'title' => $campertitle,
        'config' => $config
    ));
}else{
    $cart_count = get_camper_count($_SESSION['cart']);
    $camper_count = count($_SESSION['campers']);

    //get next camper to register (we are only allowing 1 at a time, but this is here in case we want to expand)
    $pid = get_next_cart_reg($_SESSION['cart'],$_SESSION['campers']);

    if($pid){
        $campertitle = ($camper_count+1).' of '.$cart_count;

        $p = sql_fetch_by_key($db, 'products', 'id', $pid);

        //new camper
        $content = $twigpanel->render('cart_campers.twig', array(
            'r' => $r,
            'p' => $p,
            'cart' => $_SESSION['cart'],
            'camper' => array('pid'=>$pid),
            'title' => $campertitle,
            'config' => $config
        ));
    }else{
        //all campers done
        $mindue = 0;
        $maxdue = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);

        foreach($_SESSION['campers'] as $cid => $c){
            $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
            $c = sql_fetch_by_key($db, 'campers', 'id', $c['id']);
            $mindue += $p['minimumdown'];
        }
        $content = $twigpanel->render('cart_campers_done.twig', array(
            'r' => $r,
            'p' => $p,
            'c' => $c,
            'cart' => $_SESSION['cart'],
            'min' => $mindue,
            'max' => $maxdue,
            'campers' => $_SESSION['campers']
        ));
    }
}
$page['meta_title'] = 'Registration: Campers';
$page['page_vars']['content'] = $content;


