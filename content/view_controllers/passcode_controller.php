<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$cookie_name = $page['id'].'_passcode';

if($_POST['passcode']){
    if($_POST['passcode']==$page['page_vars']['passcode']){
        m('Passcode Accepted.  Thank You.');
        setcookie($cookie_name, $_POST['passcode'], time() + (2592000), "/"); // 2592000 = 30 days
        $_COOKIE[$cookie_name] = $_POST['passcode'];
    }else{
        e('Passcode Incorrect.  Please contact us for details or try your password again.');
    }
}


if($_COOKIE[$cookie_name]!=$page['page_vars']['passcode']) {

    ob_start();
    ?>
    <h2>Passcode Required</h2>
    <p>Please enter your passcode below to access this page.</p>
    <div class="form-contain">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form action="" method="post">

                    <div class="form-element">
                        <input id="ret-passcode" type="text" name="passcode" />
                        <label for="ret-passcode">Passcode</label>
                    </div>

                    <input type="submit" value="Submit Passcode" name="submit" />
                </form>
            </div>
        </div>
    </div>


    <?
    $content = ob_get_clean();

    $page['page_vars']['content'] = $content;
    $page['page_vars']['subsections'] = '';
}