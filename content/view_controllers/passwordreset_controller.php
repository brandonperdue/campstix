<?
if($page['page_vars']['file_path']){
    include('templates/include/'.$page['page_vars']['file_path']);
}

$page['page_vars']['heading'] = 'Password Reset';
if($_GET['code']) {
    $query = $db->prepare("SELECT * FROM users WHERE resetcode=? AND resettime >= NOW()");
    $query->execute(array($_GET['code']));
    $rr = $query->fetch();

    if($rr) {
        $page['page_vars']['content'] = '
            <div class="form-contain">
                <h3>Enter New Password</h3>
                <form action="/a.php?a=passwordreset" method="post">
                    <input type="hidden" name="code" value="' . $_GET['code'] . '">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-element">
                            <input id="reg-email" type="email" name="user_email" />
                            <label for="reg-email">Email Address</label>
                        </div>
                        <div class="form-element">
                            <input id="reg-pass" type="password" name="password" />
                            <label for="reg-pass">New Password</label>
                        </div>
                        <div class="form-element">
                            <input id="reg-pass2" type="password" name="password2" />
                            <label for="reg-pass2">Re-Type New Password</label>
                        </div>
                        <input type="submit" value="Save Password" name="submit" />
                    </div>
                </form>
            </div>
        ';
    }else{
        $page['page_vars']['content'] = 'Reset Code Not Found.  Please <a href="'. $config['site_address'].'/account?a=forgot_pw">request another</a> password reset code if you are still unable to login';
    }
}