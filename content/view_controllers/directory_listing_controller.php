<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']);

//get lat/long for current location
if ($_POST['location_latitude'] && $_POST['location_longitude']) {
    //geolocation was used to set the lat/long and zip

    $_SESSION['latitude'] = $_POST['location_latitude'];
    $_SESSION['longitude'] = $_POST['location_longitude'];
    if ($_POST['zip']) {
        $_SESSION['zip'] = $_POST['zip'];
    }
} elseif ($_POST['zip']) {
    //zip code was manually entered check database for lat/long
    $z = sql_fetch_by_key($db, 'postalcodes', 'postal', $_POST['zip']);
    if ($z) {
        $_SESSION['latitude'] = $z['latitude'];
        $_SESSION['longitude'] = $z['longitude'];
        $_SESSION['zip'] = $_POST['zip'];
    } else {
        $coords = get_coords($_POST['zip']);
        if ($coords != 'error') {
            $_SESSION['latitude'] = $coords['lat'];
            $_SESSION['longitude'] = $coords['lng'];
            $_SESSION['zip'] = $_POST['zip'];

            $fields = array('postal', 'latitude', 'longitude');
            $values = array('postal' => $_SESSION['zip'], 'latitude' => $_SESSION['latitude'], 'longitude' => $_SESSION['longitude']);
            $id = sql_upsert($db, 'postalcodes', $fields, $values, 'id', '');

        } else {
            e('Zip Code not found in database');
        }
    }

} elseif (!$_SESSION['latitude'] && !$_SESSION['longitude']) {
    //no data set so default to Inland Coffee location
    //$_SESSION['latitude'] = 47.681769;
    //$_SESSION['longitude'] = -117.279268;
}

if (!$_POST['distance']) {
    $_POST['distance'] = 120;
}
//removed || $_SESSION['zip'] to make default page go to blank
if ($_POST['search'] || $_POST['state'] || $_POST['city'] || $_POST['zip']) {
    if (!$_POST['search'] && !$_POST['state'] && !$_POST['city'] && !$_POST['zip']) {
        //$_POST['zip'] = $_SESSION['zip'];
    }

    $locations = '';
    $location_results = array();


    $params = array();

    $placeholders = '';

//$querystring = "SELECT d.* FROM directory d
//    INNER JOIN users u ON d.user = u.id
//    WHERE u.user_status";
    $querystring = "SELECT d.* FROM directory d WHERE d.status ORDER BY d.name";

    if ($_POST['search'] || $_POST['state'] || $_POST['city']) {

        if ($_POST['search']) {

            $placeholders .= ' AND (';

            $placeholders .= 'd.name like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.description like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.url like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.phone like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.email like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.address like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.address_2 like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.city LIKE ? OR ';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= 'd.state like ? OR ';
            $params[] = "%" . $_POST['search'] . "%";

            $placeholders .= 'd.zip like ?';
            $params[] = "%" . $_POST['search'] . "%";
            $placeholders .= ')';

//    $querystring = "SELECT d.* FROM directory d
//    INNER JOIN users u ON d.user = u.id
//    WHERE $placeholders AND u.user_status";

        }
        if ($_POST['state']) {
            $placeholders .= 'AND d.state = ? ';
            $params[] = $_POST['state'];
        }
        if ($_POST['city']) {
            $placeholders .= 'AND d.city = ? ';
            $params[] = $_POST['city'];
        }

        $querystring = "SELECT d.* FROM directory d WHERE d.status $placeholders ORDER BY d.name";
    }

    $locations_array = array();

    $query = $db->prepare($querystring);
//$i=0;
//foreach($params as $p){
//    $i++;
//    $query->bindParam($i,$p, PDO::PARAM_STR);
//}
    $query->execute($params);
    $rr = $query->fetchAll();
    if ($rr) {
        foreach ($rr as $r) {
            if (!$_POST['zip']) {
                $location_results[$r['id']] = $r;
            } elseif ($r['coordinates']) {
                list($c1, $c2) = explode(',', $r['coordinates']);
                $loc_distance = distance($c1, $c2, $_SESSION['latitude'], $_SESSION['longitude']);
                if ($loc_distance <= $_POST['distance']) {
                    $location_results[$r['id']] = $r;
                    $location_results[$r['id']]['distance'] = $loc_distance;
                    /*
                    #set content for map markers (no map for now)
                    $content = $r['address'].'<br /><a href="#" onclick="scrollToAnchor(\'loc_'.$r['id'].'\')" class="scrollto">More Details</a><br />';

                    #add item to array using coords as key to fix duplicate marker issue
                    $locations_array[$r['coordinates']] = array('loc'=>urlencode('<strong style="font-size:12pt;">'.$r['name'].'</strong><br />'),
                                                                'content' => $locations_array[$r['coordinates']]['content'].urlencode($content),'c1'=>$c1,'c2'=>$c2 );
                    */
                }
            }
        }
    }

    //sort based on distance
    usort($location_results, function ($a, $b) {
        if ($a['distance'] == $b['distance']) {
            return 0;
        }

        return ($a['distance'] < $b['distance']) ? -1 : 1;
    });

    foreach ($locations_array as $la) {
        $locations .= '["' . $la['loc'] . $la['content'] . '",' . $la['c1'] . ',' . $la['c2'] . '],';
    }
    $locations = rtrim($locations, ',');


    $content = $twigpanel->render('directory-listing.twig', array(
        'location_results' => $location_results,
        'locations'        => $locations,
        'session'          => $_SESSION,
        'f'                => $account,
        'post'             => $_POST,
        'types'            => $types,
        'config'           => $config
    ));

    //get regional ads
    $page['adverts'] = getAds();
} else {
    ob_start();
    ?>
    <h3>Find providers near your location.</h3>
    <form action="/espresso-service-directory" method="post">
        <input type="hidden" value="true" name="form_submit"/>
        <input type="hidden" value="" name="location_latitude" id="location_latitude"/>
        <input type="hidden" value="" name="location_longitude" id="location_longitude"/>
        <input type="text" name="zip" value="" placeholder="Zip Code" class="search_txt" style="min-height: 34px;vertical-align: top;">
        <button title="Filter Results" class="prm-btn small-btn search_btn">Search <i class="fa fa-search"></i></button>
        <a title="Filter Results" id="find_location" class="prm-btn small-btn search_btn">Use Your Location <i class="fa fa-crosshairs"></i></a>
    </form><br><br>

    <?php
    if (!$account['stripe_id'] || $account['expires'] < date('Y-m-d H:i:s')) {
        echo '<br><hr>';
        echo $twigpanel->render('_premium_account_text.twig', array(
            'type' => $account['account_type']
        ));
    }


    $content = ob_get_clean();

    //get national ads
    $page['adverts'] = getAds(false);

}

$states = getStates();

$query = $db->prepare("SELECT state FROM directory GROUP BY state");
$query->execute();
$ss = $query->fetchAll(PDO::FETCH_COLUMN, 0);

$states = array_intersect_key($states, array_flip($ss));

$cities = array();
$query = $db->prepare("SELECT UPPER(state) as state, city FROM directory WHERE city != '' GROUP BY city, state ORDER BY city");
$query->execute();
$cc = $query->fetchAll();

$sidebar = $twigpanel->render('directory-nav.twig', array(
    'states' => $states,
    'cc'     => $cc,
    'f'      => $account,
    'p'      => $_POST,
    's'      => $_SESSION
));

$page['page_vars']['sidebar'] .= $sidebar;
$page['page_vars']['content'] .= $content;
