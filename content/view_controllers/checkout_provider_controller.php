<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);


$r2 = sql_fetch_by_key($db, 'pages', 'id', $config['site_sections']['ece']) ;//fetch page primary directory page for breadcrumbs
$page['breadcrumbs'] = '<a href="/">Home</a> › <a href="/'.$r2['keyword'].'">'.$r2['title'].'</a> › Cart';
$page['nav_chain_keys'][] = $config['site_sections']['ece'];

$meta_title = 'Checkout - Select a Service Provider' ;
$current_nav = 'cart' ;

// make sure cart isn't empty
if( ! is_array($_SESSION['cart']) or ! $_SESSION['cart'] ){
    hle('/cart') ;
}

// retrieve customer info from cookie to session, if found
if( $_COOKIE['customer'] ){
    $customer_array = unserialize(stripslashes($_COOKIE['customer'])) ;
    d('Customer Data') ;
    d($_COOKIE['customer']) ;
    d($customer_array) ;
    d('/Customer Data') ;
    if( is_array($customer_array) ){
        foreach( $customer_array as $k=>$v ){
            $_SESSION['customer'][$k] = $v ;
        }
    }
}

$form_attempt = form_load_clear('cart-provider') ;
d('form_attempt') ;
d($form_attempt) ;
if( $form_attempt ){
    $r = $form_attempt ;
}else{
    $r = $_SESSION['customer'] ;
}

$zipsearch = array();

//if zip search not entered, use shipping address
if(!$_POST['zip']){
    $_POST['zip'] = $_SESSION['customer']['si_zip'];
}

$z = sql_fetch_by_key($db, 'postalcodes', 'postal', $_POST['zip']) ;
if($z){
    $zipsearch['latitude'] = $z['latitude'];
    $zipsearch['longitude'] = $z['longitude'];
    $zipsearch['zip'] = $_POST['zip'];
    $_SESSION['latitude'] = $zipsearch['latitude'];
    $_SESSION['longitude'] = $zipsearch['longitude'];
}else{
    $coords = get_coords($_POST['zip']);
    if($coords != 'error'){
        $zipsearch['latitude'] = $coords['lat'];
        $zipsearch['longitude'] = $coords['lng'];
        $zipsearch['zip'] = $_POST['zip'];
        $_SESSION['latitude'] = $zipsearch['latitude'];
        $_SESSION['longitude'] = $zipsearch['longitude'];

        $fields = array('postal','latitude','longitude');
        $values = array('postal' => $_SESSION['zip'], 'latitude' => $_SESSION['latitude'], 'longitude' => $_SESSION['longitude']);
        sql_upsert($db, 'postalcodes', $fields, $values, 'id', '');

    }else{
        e('Zip Code not found in database');
    }
}


$location_results = array();

//search all directories
$querystring = "SELECT d.* FROM directory d WHERE d.status AND d.coordinates ORDER BY d.name";
$query = $db->prepare($querystring);
$query->execute();
$dd = $query->fetchAll();
if ($dd) {
    foreach ($dd as $d) {
        list($c1, $c2) = explode(',', $d['coordinates']);
        $loc_distance = distance($c1, $c2, $_SESSION['latitude'], $_SESSION['longitude']);
        if ($loc_distance <= 120) {
            $location_results[$d['id']] = $d;
            $location_results[$d['id']]['distance'] = $loc_distance;
        }
    }
}

//sort based on distance
usort($location_results, function($a, $b) {
    if(  $a['distance'] ==  $b['distance'] ){ return 0 ; }
    return ($a['distance'] < $b['distance']) ? -1 : 1;
});

//if a provider is already selected bump it to the front of the array
if($r['provider']){
    $selected = $location_results[$r['provider']];
    unset($location_results[$r['provider']]);
    $location_results = array($r['provider']=>$selected)+$location_results;
}




$content = $twigpanel->render('cart_provider.twig', array(
    'r' => $r,
    'provider' => $location_results,
    'zipsearch' => $zipsearch,
    'config' => $config
));


$page['title'] = 'Shopping Cart: Provider';
$page['page_vars']['content'] = $content;
