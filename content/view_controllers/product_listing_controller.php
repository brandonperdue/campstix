<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);


$template_array = array('page' => $page);

$sort = '';
switch($_POST['sort']){
    case 1:
        $sort = 'title DESC';
        break;
    case 2:
        $sort = 'price ASC';
        break;
    case 3:
        $sort = 'price DESC';
        break;
    default:
        $sort = 'priority ASC';
        break;
}

$results = array();
if($_REQUEST['c']){
    $search = '';
    $search_array = array();

    if ($_REQUEST['c']) {
        $search .= ' AND id_categories IN(?)';
        $search_array[] = $_REQUEST['c'];
    }

    $query = $db->prepare("SELECT * FROM products WHERE showdate <= NOW() AND display $search ORDER BY $sort");
    $query->execute($search_array);
    $pp = $query->fetchall();

}else{

    $query = $db->prepare("SELECT * FROM products WHERE showdate <= NOW() AND display ORDER BY $sort");
    $query->execute(array());
    $pp = $query->fetchall();

}


$content = $twigpanel->render('product-listing.twig', array('products'=>$pp));


$page['page_vars']['content'] .= $content;

