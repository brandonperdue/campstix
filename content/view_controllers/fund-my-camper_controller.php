<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$meta_title = 'Fund My Camper' ;

$funding = sql_fetch_by_key($db, 'funding', 'keyword', $_GET['c']);
$camper = sql_fetch_by_key($db, 'campers', 'id', $funding['cid']);
$product = sql_fetch_by_key($db, 'products', 'id', $camper['pid']);
$scholarship = sql_fetch_by_key($db, 'scholarship', 'cid', $funding['cid']);

$content = $twigpanel->render('fund-my-camper.twig', array(
    'c' => $camper,
    'p' => $product,
    's' => $scholarship,
    'f' => $funding,
    'test_mode'   => $_SESSION['test'],
    'config' => $config,
	'current_path' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
));

$page['meta_title'] = 'Fund '.$camper['firstname'].' '.$camper['lastname'].'\'s Camp';
$page['page_vars']['content'] = $content;

