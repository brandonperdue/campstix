<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$twigpanel->addFilter(new Twig_SimpleFilter('fmt_price', 'fmt_price'));


//remove end slash from product keyword
$_GET['camp'] = trim($_GET['camp'], '/');

$query = $db->prepare("SELECT * FROM products WHERE (id=? OR keyword=?) ORDER BY priority ASC");
$query->execute(array($_GET['camp'],$_GET['camp']));
$pp = $query->fetch();


$cartform = $twigpanel->render('product_form.twig', array(
    'p' => $pp
));
$content = $twigpanel->render('product.twig', array(
    'returnlink' => $returnlink,
    'p' => $pp,
    'cartform' => $cartform
));


$page['meta_title'] = $pp['title'];
$page['page_vars']['content'] = $content;
