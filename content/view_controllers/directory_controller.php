<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

//add additional filters or extensions
$twigpanel->addFilter(new Twig_SimpleFilter('addhttp', 'addhttp'));
$twigpanel->addFilter(new Twig_SimpleFilter('starRateDisplay', 'starRateDisplay'));
$twigpanel->addFunction(new Twig_SimpleFunction('admin_logged_in', 'admin_logged_in'));
$twigpanel->addFunction(new Twig_SimpleFunction('getUser', 'getUser'));



//$page = get_page($db, array('keyword' => 'espresso-services-directory'), $_GET['cmsversion']);



$query = $db->prepare("SELECT * FROM directory WHERE (id=? OR keyword=?)");
$query->execute(array($_GET['directory'],rtrim($_GET['directory'],'/')));
$r = $query->fetch();

$query = $db->prepare("SELECT * FROM review WHERE user_id = ? AND directory_id = ?");
$query->execute(array($_SESSION['log']['id'],$r['id']));
$userreviews = $query->fetchAll();

$user = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']);//get user info
$duser = sql_fetch_by_key($db, 'users', 'id', $r['id_user']);//fetch page primary directory page for breadcrumbs
if($r['id']) {

    $reviews = getReviews($r['id']);
    $average = getReviewAverage($r['id']);

    $r2 = sql_fetch_by_key($db, 'pages', 'id', $config['site_sections']['esd']);//fetch page primary directory page for breadcrumbs
    $page['breadcrumbs'] = '<a href="/">Home</a> › <a href="/' . $r2['keyword'] . '">' . $r2['title'] . '</a> › ' . $r['name'];
    $page['nav_chain_keys'][] = $config['site_sections']['esd'];

    $content = $twigpanel->render('directory.twig', array(
        'l'              => $r,
        'reviews'        => $reviews,
        'review_average' => $average,
        'user'           => $_SESSION['log'],
        'hidereviewbtn'  => count($userreviews)
    ));


    $r3 = sql_fetch_by_key($db, 'pages', 'id', $config['site_sections']['ece']);//fetch page primary directory page for breadcrumbs



    $sidebar = $twigpanel->render('directory-sidebar.twig', array(
        'l' => $r,
        'esd' => $r2,
        'ece' => $r3,
        'u' => $duser
    ));

    $page['page_vars']['sidebar'] .= $sidebar;

    $page['page_vars']['heading'] = $r['name'];
    $page['title'] = $r['name'];
    $page['meta_title'] = $r['name'];
    $page['page_vars']['content'] .= $content;


    $page['adverts'] = getAds(true, true);
//hide top advert on these pages
    $page['adverts'][1] = false;
}