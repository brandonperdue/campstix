<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$blogsettings = sql_fetch_by_key($db, 'blogs', 'id', $page['page_vars']['id_blogs']);
$page['page_vars']['heading'] = $blogsettings['title'];

$tt = $db->query("SELECT * FROM blog_tags ORDER BY title ASC");
$blogstags = array();
foreach($tt as $t){

    $query = $db->prepare("SELECT t.id FROM blog_entry_tags t, blogs_entries e WHERE NOT e.hidden AND e.date_posted <= CURDATE() AND e.id=t.bid AND t.tid = ? AND e.id_blogs = ?");
    $query->execute(array($t['id'],$blogsettings['id']));
    $rr = $query->fetchall();
    $t['count'] = count($rr);
    if($t['count']) {
        $blogstags[] = $t;
    }
}


$sidebar = $twigpanel->render('blog_nav.twig', array(
    'page' => $page,
    'blogstags' => $blogstags,
    'current' => array('cat'=>$_GET['category'],'startd'=>$_GET['startd'],'endd'=>$_GET['endd'])
));

$where = 'AND e.date_posted <= CURDATE() AND e.id_blogs = ?';

if(isset($_GET['post'])){
    $blog = $db->prepare("SELECT e.* FROM blogs_entries e WHERE NOT e.hidden AND e.keyword = ? $where");
    $blog->execute(array($_GET['post'],$blogsettings['id']));
    $blog = $blog->fetchall();

    $content = $twigpanel->render('blog-post.twig', array(
        'page' => $page,
        'blog' => $blog[0]
    ));
}else {
    $where_vars = array($page['page_vars']['id_blogs']);
    if($_GET['startd'] && $_GET['endd']){
        $where .= ' AND e.date_posted >= ? AND e.date_posted <= ? ';
        $where_vars[] = $_GET['startd'];
        $where_vars[] = $_GET['endd'];
    }

    if(isset($_GET['category'])) {
        $where_vars[] = $_GET['category'];

        array_unshift($where_vars , $blogsettings['id']);
        $blogs = $db->prepare("SELECT e.* FROM blogs_entries e, blog_entry_tags t WHERE NOT e.hidden AND e.id = t.bid AND e.id_blogs=? $where AND t.tid=? ORDER BY date_posted DESC");
        $blogs->execute($where_vars);
        $blogs = $blogs->fetchall();
        $r = sql_fetch_by_key($db, 'blog_tags', 'id', $_GET['category']);
        $page['page_vars']['heading'] .= ': '.$r['title'];

    }else{

        array_unshift($where_vars , $blogsettings['id']);
        $blogs = $db->prepare("SELECT e.* FROM blogs_entries e WHERE NOT e.hidden AND e.id_blogs=? $where ORDER BY date_posted DESC");
        $blogs->execute($where_vars);
        $blogs = $blogs->fetchall();
    }
    $content = $twigpanel->render('blogs.twig', array(
        'page' => $page,
        'blogs' => $blogs
    ));
}


$page['page_vars']['sidebar'] .= $sidebar;
$page['page_vars']['content'] .= $content;

