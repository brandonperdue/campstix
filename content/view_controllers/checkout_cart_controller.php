<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$r2 = sql_fetch_by_key($db, 'pages', 'id', $config['site_sections']['ece']) ;//fetch page primary directory page for breadcrumbs
$page['breadcrumbs'] = '<a href="/">Home</a> › <a href="/'.$r2['keyword'].'">'.$r2['title'].'</a> › Cart';
$page['nav_chain_keys'][] = $config['site_sections']['ece'];

$product_list = '';
foreach ($_SESSION['cart'] as $key => $opts) {
    $product_list .= $opts['id'].', ';
}
$tax = cart_get_tax($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);

ob_start();
cart_display($db, array(
    'cart' => $_SESSION['cart'],
    'discounts' => $_SESSION['discounts'],
    'customer' => $_SESSION['customer'],
    'show_controls' => true,
    'show_tax' => $tax > 0 ? true : false,
));
$cart = ob_get_clean();

$content = $twigpanel->render('cart.twig', array(
    'c' => $_SESSION['cart'],
    'carthtml' => $cart,
    'xval' => session_id(),
    'related' => $related,
    'config' => $config
));


$page['title'] = 'Shopping Cart';
$page['page_vars']['content'] = $content;
