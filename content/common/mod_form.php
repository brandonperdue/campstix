<?
function form_text($label,$name,$value=null,$attributes=''){
	global $form_mode ;
	ob_start();
	?>
	<span class="form-element">
		<label>
			<?
			if ($label) {
				?>
				<div class="label"><?= htmlspecialchars($label) ?></div>
				<?
			}
			if( 'output'==$form_mode ){
				?><div style="background-color: #eaeaea ;"><?= htmlspecialchars($value) ?> </div><?
			}else{
				?><input id="form-<?= $name ?>" type="text" name="<?= $name ?>" value="<?= htmlspecialchars($value) ?>" <?= $attributes ?>/><?
			}
			?>
		</label>
	</span>
	<?
	return ob_get_clean();
}
function form_checkbox($label, $name, $value=null, $attributes=''){
	global $form_mode;
	ob_start();
	if ($form_mode == 'output') {
		?>
		<?= $value ? '&#10003;' : '&#9633;' ?>
		<?= htmlspecialchars($label) ?>
		<?
	} else {
		?>
		<span class="form-element">
			<label>
				<input id="form-<?= $name ?>" type="checkbox" name="<?= $name ?>" <?= $value ? 'checked="checked"' : '' ?>/>
				<?= htmlspecialchars($label) ?>
			</label>
		</span>
		<?
	}
	return ob_get_clean();
}
function form_save($key,$post_data){
	$_SESSION['forms'][$key] = $post_data ;
}
function form_load_clear($key, $defaults = null){
	if( isset($_SESSION['forms'][$key]) ){
		if( get_magic_quotes_gpc() ){
	$form_data = array_modify_recursive($_SESSION['forms'][$key],'stripslashes') ;
		}else{
			$form_data = $_SESSION['forms'][$key] ;
		}
	unset($_SESSION['forms'][$key]) ;
	return $form_data ;
	}elseif( $defaults ){
		return $defaults ;
	}else{
		return array() ;
	}
}
// write a select dropdown
function write_select($o){
	global $form_mode;

	if ($form_mode) {
		echo $o['current'];
		return;
	}
	// $o: sql,value,label,rows,name,current,default,display_length,use_values_as_keys,attribs
	if( ! $o['current'] ){
		$o['current'] = $o['default'] ;
	}
	if( ! is_array($o['rows']) and ! ($o['rows'] instanceof Traversable) ){
		exit('write_select: "rows" must be array or Traversable') ;
		}
	if( $o['label'] and $o['value'] ){
		$rows_remapped = array();
		foreach( $o['rows'] as $r ){
			$rows_remapped[$r[$o['value']]] = $r[$o['label']] ;
	}
		$o['rows'] = $rows_remapped ;
	}
	if( $o['use_values_as_keys'] ){
		$o['rows'] = array_combine($o['rows'],$o['rows']) ;
	}

	?><select id="form-<?= $name ?>" name="<?= $o['name'] ?>" <?= $o['attribs'] ?>>
		<?
		if (!$o['disable_blank_option']) {
			?>
			<option value=""><?= $o['blank_display_text'] ?></option>
			<?
		}
		foreach( $o['rows'] as $k=>$v ){
			if( $o['display_length'] ){
				$v = fmt_ellipsis($v,$o['display_length']) ;
			}
			?><option value="<?= $k ?>" <?= $k==$o['current'] ? 'selected="selected"':'' ?>><?= $v ?></option><?
		}
	?></select><?
}
function form_radio($o){
	// $o: sql,rows,name,current,default,display_length,use_values_as_keys,attribs
	if( ! $o['current'] ){
		$o['current'] = $o['default'] ;
	}
	if( $o['sql'] ){ // sql is optional
		$rows_db = sql($o['sql']) ;
		foreach( $rows_db as $r ){
			$o['rows'][$r[$o['value']]] = $r[$o['label']] ;
		}
	}
	if( ! is_array($o['rows']) ){
		exit('write_select: "rows" must be an array') ;
	}
	if( $o['use_values_as_keys'] ){
		$o['rows'] = array_combine($o['rows'],$o['rows']) ;
	}
	d($o['rows']) ;
	
	$i = 0 ;
	foreach( $o['rows'] as $k=>$v ){
		$i++ ;
		if( $o['display_length'] ){
			$v = fmt_ellipsis($v,$o['display_length']) ;
		}
		?><input type="radio" name="<?= $o['name'] ?>" <?= $k==$o['current'] ? 'checked="checked"':'' ?> <?= $o['attribs'] ?> value="<?= $k ?>" id="radio_<?= $o['name'].'_'.$i ?>" />
		<label for="radio_<?= $o['name'].'_'.$i ?>"><?= $v ?></label><br/><?
	}
}
function write_months($name,$current,$default='',$attribs=''){
	foreach( range(1,12) as $i ){
        $temp = date('m',mktime(0,0,0,$i,1)) ;
		$rows[$temp] = $temp;
	}
	write_select(array('rows'=>$rows,'name'=>$name,'current'=>$current,'default'=>$default,'attribs'=>$attribs)) ;
}
function write_days($name,$current,$default='',$attribs=''){
	$rows = array_combine(range(1,31),range(1,31)) ;
	write_select(array('rows'=>$rows,'name'=>$name,'current'=>$current,'default'=>$default,'attribs'=>$attribs)) ;
}
function write_hours($name,$current,$default=''){
	$rows = array(
		'24'=>'12 am','01'=>'1 am','02'=>'2 am','03'=>'3 am','04'=>'4 am','05'=>'5 am','06'=>'6 am','07'=>'7 am','08'=>'8 am','09'=>'9 am','10'=>'10 am','11'=>'11 am',
		'12'=>'12 pm','13'=>'1 pm','14'=>'2 pm','15'=>'3 pm','16'=>'4 pm','17'=>'5 pm','18'=>'6 pm','19'=>'7 pm','20'=>'8 pm','21'=>'9 pm','22'=>'10 pm','23'=>'11 pm') ;
	write_select(array('rows'=>$rows,'name'=>$name,'current'=>$current,'default'=>$default)) ;
}
function write_minutes($name,$current,$default=''){
	$rows = array() ;
	foreach( range(0,59) as $i ){
		$minute_str = str_pad($i, 2, '0', STR_PAD_LEFT) ;
		$rows[$minute_str] = $minute_str ;
	}
	write_select(array('rows'=>$rows,'name'=>$name,'current'=>$current,'default'=>$default)) ;
}
function write_years($name,$current,$default='',$start_year='',$end_year='',$attribs=''){
	if( ! $start_year ){ $start_year = date('y') ; }
	if( ! $end_year ){ $end_year = date('y')+10 ; }
	$years = range($start_year,$end_year) ;
	$rows = array_combine($years,$years) ;
	write_select(array('rows'=>$rows,'name'=>$name,'current'=>$current,'default'=>$default,'attribs'=>$attribs)) ;
}
function write_select_date($o){
	// prefix , current
	// assumes that current is in the format of a mysql datetime
	if( $o['current'] ){
		list($y,$m,$d,$h,$i) = preg_split('@[^\d]+@', $o['current']) ;
	}elseif( $o['default'] ){
		list($y,$m,$d,$h,$i) = preg_split('@[^\d]+@', $o['default']) ;
	}else{
		list($y,$m,$d,$h,$i) = explode('-',date('Y-m-d-H-i')) ;
	}
	if( ! $o['included_parts'] ){
		$o['included_parts'] = array('year', 'month', 'day') ;
	}
	if( in_array('month', $o['included_parts']) ){
		write_months($o['name'].'-m', $m) ;
	}
	if( in_array('day', $o['included_parts']) ){
		write_days(  $o['name'].'-d', $d) ;
	}
	if( in_array('year', $o['included_parts']) ){
		write_years( $o['name'].'-y', $y, '', $o['start_year'], $o['end_year']) ;
	}
	if( in_array('hour', $o['included_parts']) ){
		write_hours(  $o['name'].'-h', $h) ;
	}
	if( in_array('minute', $o['included_parts']) ){
		write_minutes(  $o['name'].'-i', $i) ;
	}
}
// put together a mysql datetime string by combining array values whose keys end in -y, -m, and -d
function date_compose($prefix,$values){
	d($values) ;
	return
		str_pad($values["$prefix-y"],4,'0',STR_PAD_LEFT).'-'.
		str_pad($values["$prefix-m"],2,'0',STR_PAD_LEFT).'-'.
		str_pad($values["$prefix-d"],2,'0',STR_PAD_LEFT).' '.
		str_pad($values["$prefix-h"],2,'0',STR_PAD_LEFT).':'.
		str_pad($values["$prefix-i"],2,'0',STR_PAD_LEFT).':'.
		str_pad($values["$prefix-s"],2,'0',STR_PAD_LEFT)
		;
}
// put together a mysql hh:mm:ss string by combining array values whose keys end in -y, -m, and -d
function time_compose($prefix,$values){
	d($values) ;
	return
		str_pad($values["$prefix-h"],2,'0',STR_PAD_LEFT).':'.
		str_pad($values["$prefix-i"],2,'0',STR_PAD_LEFT).':'.
		str_pad($values["$prefix-s"],2,'0',STR_PAD_LEFT)
		;
}
function write_states( $name , $current='' , $attributes = '' ){
	// list of states and provinces
	$states = array('AB'=>'Alberta','AL'=>'Alabama','AK'=>'Alaska','AZ'=>'Arizona','AR'=>'Arkansas','BC'=>'British Columbia','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FL'=>'Florida','GA'=>'Georgia','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MB'=>'Manitoba','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','MT'=>'Montana','NE'=>'Nebraska','NV'=>'Nevada','NB'=>'New Brunswick','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NF'=>'Newfoundland','NC'=>'North Carolina','ND'=>'North Dakota','NT'=>'Northwest Territories','NS'=>'Nova Scotia','NU'=>'Nunavut','OH'=>'Ohio','OK'=>'Oklahoma','ON'=>'Ontario','OR'=>'Oregon','PA'=>'Pennsylvania','PE'=>'Prince Edward Island','PR'=>'Puerto Rico','QC'=>'Quebec','RI'=>'Rhode Island','SK'=>'Saskatchewan','SC'=>'South Carolina','SD'=>'South Dakota','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming','YT'=>'Yukon Territory',) ;
	$states = array_combine(array_keys($states),array_keys($states)) ;
	write_select(array(
		'rows'=>$states,
		'name'=>$name,
		'current'=>$current,
		'attribs'=>$attributes,
		)) ;
}
function write_states_checkout($name, $current = ''){
	global $db;
	// list of states and provinces
	write_select(array(
		'rows' => $db->query("SELECT * FROM shipping_locations WHERE display ORDER BY province ASC, name ASC"),
		'value' => 'abbreviation',
		'label' => 'name',
		'name' => $name,
		'current' => $current,
		'display_length' => 35,
	));
}
function write_countries( $name , $current='' , $class='' , $type = '' ){
	if( !$current ) $current = 'US' ;
	$countries = array('US'=>'United States','CA'=>'Canada') ;
	write_select(array(
		'rows'=>$countries,
		'name'=>$name,
		'current'=>$current,
		)) ;
}
function form_safe_filename($filename) {
	$replace_char = '_';
	$filename = str_replace('php', 'zzz', $filename);
	return trim(preg_replace('@[^\w.-]+@', $replace_char, $filename), $replace_char);
}
