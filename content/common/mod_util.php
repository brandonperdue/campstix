<?
/******** site utilities ********/
function smtp_mail($from, $to, $subject, $email_body){
    global $config;
    $smtp_conf = $config['smtp_conf'];
    $fromaddress = $smtp_conf['fromaddress'];

    // Create the Transport
    $transport = (new Swift_SmtpTransport($smtp_conf['host'], $smtp_conf['port']))
        ->setUsername($smtp_conf['username'])
        ->setPassword($smtp_conf['password'])
    ;


    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

    // Create a message
    $message = (new Swift_Message($subject))
        ->setFrom(array( $fromaddress => $config['company_name']))
        ->setReplyTo($from)
        ->setTo($to)
        ->setBody($email_body)
        ->setContentType("text/html")
    ;

    // Send the message
    $result = $mailer->send($message);
}


function array_modify_recursive($arr, $function)
{
    if (is_array($arr)) {
        foreach ($arr as $k => $v) {
            $arr[$k] = array_modify_recursive($v, $function);
        }

        return $arr;
    } else {
        return $function($arr);
    }
}

function e($message)
{
    $_SESSION['e'][] = $message;
}

function e_multiple(array $messages)
{
    foreach ($messages as $message) {
        e($message);
    }
}

function m($message)
{
    $_SESSION['m'][] = $message;
}

// print neat debugging info
function d($str)
{
    global $debug, $debug_buffer;
    if ($debug) {
        $debug_buffer .= '<pre>' . print_r($str, true) . '</pre>';
    }
}

function hle($url)
{
    header("Location: $url");
    exit;
}

function settings_load($db)
{
    // load settings from db
    $settings = array();
    $query = $db->prepare("SELECT * FROM settings");
    $query->execute();
    while ($row = $query->fetch()) {
        $settings[$row['name']] = $row['value'];
    }

    return $settings;
}


function getAge($dob,$condate = ''){
    if(!$condate){
        $condate = date("Y-m-d");
    }
    $birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $dob))))));
    $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $condate))))));
    $age = $birthdate->diff($today)->y;

    if($dob == "0000-00-00"){
        $age = 'None Given';
    }

    return $age;
}

/*********************************************************************************************************
 * DESCRIPTION: addhttp() replaces http on urls that are missing http, https, ftp, etc.
 * PARAMETERS:
 *    $url
 *        - the url to check for http
 *********************************************************************************************************/
function addhttp($url)
{
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }

    return $url;
}

/*********************************************************************************************************
 * DESCRIPTION: phonemat() Takes in a phone number and formats it in a (509)555-5555 format
 * PARAMETERS:
 *    $input
 *        - the phone number being formatted
 *********************************************************************************************************/
function phonemat($input)
{
    $result = preg_replace("/[^0-9]/", "", $input);
    $area = substr($result, 0, 3);
    $node1 = substr($result, 3, 3);
    $node2 = substr($result, 6, 4);
    $number = "(" . $area . ")" . $node1 . "-" . $node2;
    if (strlen($result) > 10) {
        $node3 = substr($result, 10, 4);
        $number .= " x" . $node3;
    }

    return $number;
}

/*********************************************************************************************************
 * DESCRIPTION: snippet() Returns a subsection of a string for use as an abstract/content preview without
 *               cutting the string in the middle of words.
 * PARAMETERS:
 *    $text
 *        - The text to
 *    $length
 *        - The amount of letters to cut the string off at. Defaults to 64
 *    $tail
 *        - The text that is added on the end of the string if it has been cut. Defaults to "..."
 *********************************************************************************************************/
function snippet($text, $length = 64, $tail = "...")
{
    $text = trim($text);
    $txtl = strlen($text);
    if ($txtl > $length) {
        for ($i = 1; $text[$length - $i] != " "; $i++) {
            if ($i == $length) {
                return trim(substr($text, 0, $length)) . $tail;
            }
        }
        $text = trim(substr($text, 0, $length - $i + 1)) . $tail;
    }

    return $text;
}

/*********************************************************************************************************
 * DESCRIPTION: randstring() a specified length and returns a random string at that length
 * PARAMETERS:
 *    $len
 *        - the length of the requested string
 *********************************************************************************************************/
function randstring($len)
{
    $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charslen = strlen($chars);
    $str = '';
    for ($i = 0; $i < $len; $i++) {
        $str .= $chars[rand(0, $charslen - 1)];
    }

    return $str;
}


/*********************************************************************************************************
 * DESCRIPTION: toAscii() creates a url safe version of a string
 *********************************************************************************************************/
function toAscii($str, $replace = array(), $delimiter = '-')
{
    if (!empty($replace)) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}


function logged_in()
{
    global $_SESSION;
    if (isset($_SESSION['log']['name']) && isset($_SESSION['log']['id'])) {
        return true;
    } else {
        return false;
    }
}

function admin_logged_in()
{
    global $_SESSION;
    if (isset($_SESSION['admin'])) {
        return true;
    } else {
        return false;
    }
}