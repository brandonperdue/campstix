<?

function db_row_copy($db, $table, $oldid, $idkey = 'id', $passthrough = array())
{
    $row = sql_fetch_by_key($db, $table, $idkey, $oldid);
    $c = array();
    $v = array();
    foreach ($row as $key => $value) {
        if ($key != $idkey) {
            $c[] = $key;
            if (isset($passthrough[$key])) {
                $v[$key] = $passthrough[$key];
            } else {
                $v[$key] = $value;
            }
        }
    }

    return sql_upsert($db, $table, $c, $v);
}


function cms_deep_page_copy($db, $id_source, $id_parent)
{
    static $level = 0;

    $page_columns = array('id_parent', 'title', 'meta_description', 'content', 'keyword', 'priority', 'navhide', 'template', 'hide_breadcrumbs');
    $page_vars_columns = array('id_pages', 'title', 'value', 'date_created', 'date_updated', 'id_admins_created', 'id_admins_updated');
    $page_sections_columns = array('id_pages', 'image', 'header', 'subheader', 'content', 'gallery', 'priority');

    $page = sql_fetch_by_key($db, 'pages', 'id', $id_source);

    // copy the page itself
    $page['id_parent'] = $id_parent;
    if ($level == 0) {
        $page['title'] .= ' - Copy';
    }
    $new_page_id = sql_upsert($db, 'pages', $page_columns, $page);

    // copy page's pages_vars
    $query = $db->prepare("SELECT * FROM pages_vars WHERE id_pages = :id_pages");
    $query->execute(array(':id_pages' => $id_source));
    while ($page_var = $query->fetch()) {
        $page_var['id_pages'] = $new_page_id;
        sql_upsert($db, 'pages_vars', $page_vars_columns, $page_var);
    }

    // copy page's pages_sections
    $query = $db->prepare("SELECT * FROM pages_sections WHERE id_pages = :id_pages");
    $query->execute(array(':id_pages' => $id_source));
    while ($page_var = $query->fetch()) {
        $page_var['id_pages'] = $new_page_id;
        sql_upsert($db, 'pages_sections', $page_sections_columns, $page_var);
    }

    $query = $db->prepare("SELECT * FROM pages WHERE id_parent = :id_parent");
    $query->execute(array(':id_parent' => $id_source));
    // copy page's children
    while ($sub_page = $query->fetch()) {
        $level++;
        cms_deep_page_copy($db, $sub_page['id'], $new_page_id);
        $level--;
    }

    return $new_page_id;
}

function cms_render_page(PDO $db, &$page, $settings = array(), $config = array(), $import_vars = array())
{
    $forcelogin = array('checkout_campers','checkout_address');
    if (($page['secure'] || in_array($page['template'],$forcelogin)) && !logged_in()) {
        $page['template'] = 'login';
    }
    $template_vars = array_merge($import_vars, [
        'page'     => $page,
        'vars'     => $page['page_vars'],
        'settings' => $settings,
        'config'   => $config,
    ]);
    $controller_path = 'view_controllers/' . $page['template'] . '_controller.php';
    if (is_file($controller_path)) {
        $controller_vars = include $controller_path;
        if (is_array($controller_vars)) {
            $page = array_merge($page, $controller_vars);
        }
    }

    return $page;
}

function cms_photo_selector($label, $input_name, $current_value, $basedir_thumb, $basedir_preview)
{
    ob_start();
    ?>
    <b><?= htmlspecialchars($label) ?></b><br/>
    <?
    if ($current_value) {
        ?>
        <a style="display: block;" title="Current image" href=""
           onclick="window.open('<?= $basedir_preview . "/" . $current_value ?>','','width=800,height=600');return false;" target="_blank">
            <img style="display: block; width: 150px;" src="<?= $basedir_thumb . "/" . $current_value ?>"/>
        </a>
        <?
    }
    ?>
    <input type="file" name="<?= $input_name ?>"/>
    <?
    $html = ob_get_clean();

    return $html;
}

function cms_get_nav(PDO $db, $id_parent, $current_nav_chain, $max_depth = 3, $absolute_paths = false, $current_li_class = 'current')
{
    global $_SESSION;
    static $depth = 1;

    if ($depth > $max_depth) {
        return;
    }

//    Hide "Service Provider Only" pages in nav
    $additionalflag = '';
    if($_SESSION['log']['id']){
        $u = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']);
        if($u['account_type'] != 2){
            $additionalflag = ' AND secure_provider != 1 ';
        }
    }

    $query = $db->prepare("SELECT * FROM pages WHERE id_parent = ? AND navhide != 1 $additionalflag ORDER BY priority ASC");
    $query->execute(array($id_parent));
    $results = $query->fetchAll();

    if ($query->rowCount()) {
        foreach ($results as $index => $sub) {
            $sub = get_page($db, $sub);
            $link_class = '';
            if ($current_nav_chain[$sub['id']]) {
                $link_class = $current_li_class;
            }
            // get subnav before generating, so we can optionally display dropdown marker
            $depth++;
            $sub['subs'] = cms_get_nav($db, $sub['id'], $current_nav_chain, $max_depth, $absolute_paths, $current_li_class);
            $depth--;
            if ($sub['keyword'] == 'home') {
                $link = '';
            } else {
                $link = $sub['keyword'];
            }
            if ($absolute_paths) {
                $link = "/$link";
            } else {
                $link = "./$link";
            }
            $sub['depth'] = $depth;
            $sub['link_class'] = $link_class;
            $sub['link'] = $link;
            $subs[] = $sub;
        }
    }

    return $subs;
}

function get_page(PDO $db, array $a, $version = '')
{
    static $cache = array();
    if ($a['id']) {
        $search_field = 'id';
        $search_value = $a['id'];
    }
    if ($a['keyword']) {
        $search_field = 'keyword';
        $search_value = $a['keyword'];
    }
    $cache_key = $search_field . '-' . $search_value;
    if (isset($cache[$cache_key])) {
        return $cache[$cache_key];
    }
    if (isset($search_field) and isset($search_value)) {
        $query = $db->prepare("SELECT * FROM pages WHERE `$search_field` = ?");
        $query->execute(array($search_value));
        $page = $query->fetch();
        if ($page) {
            $page['page_vars'] = get_page_vars($db, $page['id'], $version);
        }
        $cache[$cache_key] = $page;

        return $page;
    }

    return false;
}

function get_page_vars(PDO $db, $id, $version = '')
{
    if (!$version) {
        $version = '';
    }
    $query = $db->prepare("SELECT * FROM pages_vars WHERE id_pages = ? AND version = ?");
    $query->execute(array($id, $version));
    $page_vars_rows = $query->fetchAll();

    $page_vars = array();
    foreach ($page_vars_rows as $page_var_row) {
        $page_vars[$page_var_row['title']] = $page_var_row['value'];
    }

    $query = $db->prepare("SELECT * FROM pages_sections WHERE id_pages = ? AND version = ? ORDER BY priority ASC");
    $query->execute(array($id, $version));
    $page_sections = $query->fetchAll();
    $page_vars['subsections'] = $page_sections;

    return $page_vars;
}

function admin_perm_check(PDO $db, $user_record, $table, $id = 0)
{
    $permissions = $user_record['permissions'];
    if (!is_array($permissions)) {
        return false;
    }
    foreach ($permissions as $permission) {
        // if this user has a permission row with item_type "super", they can do anything
        if ('super' == $permission['item_type']) {
            return true;
        }
        if ($permission['table'] == $table and $permission['id_items'] == $id) {
            return true;
        }
        // if this permission is a tree permission in the same table that the item we're checking is in, check if the sub tree of this permissions's item contains the item in question
        if ($permission['tree'] and $permission['table'] == $table) {
            $descendant_rows = cms_get_sub_items_list($db, $table, $permission['id_items']);
            foreach ($descendant_rows as $descendant_row) {
                if ($descendant_row['id'] == $id) {
                    return true;
                }
            }
        }
    }

    return false;
}

function admin_login(PDO $db, $username, $password)
{
    // collect admin record
    $query = $db->prepare("SELECT * FROM admins WHERE username = ?");
    $query->execute(array($username));
    $admin_record = $query->fetch();

    // if not found, send back to login page
    if (!$admin_record) {
        return false;
    }

    // check password hash
    $hasher = new Hautelook\Phpass\PasswordHash(8, false);
    if (!$hasher->CheckPassword($password, $admin_record['hashed_password'])) {
        return false;
    }

    $query = $db->prepare("SELECT * FROM permissions WHERE id_admins = ?");
    $query->execute(array($admin_record['id']));
    $permissions = $query->fetchAll();

    if (!$permissions) {
        return false;
    }
    // set up the session with admin and permission details
    $admin = $admin_record;
    $admin['permissions'] = array();
    foreach ($permissions as $permission) {
        $permission_type = sql_fetch_by_key($db, 'permissions_item_types', 'keyword', $permission['item_type']);
        $p = $permission;
        $p['table'] = $permission_type['table'];
        $p['tree'] = $permission_type['tree'];
        $admin['permissions'][] = $p;
    }
    if (admin_perm_check($db, $admin, 'super')) {
        $admin['type'] = 'super';
    } else {
        $admin['type'] = 'normal';
    }
    asort($admin['permissions']);

    return $admin;
}

function cms_get_sub_items_list(PDO $db, $table_name, $id_parent, $list = array(), $hierarchical = false, $orderby = 'priority')
{
    static $depth = 0;
    if ($hierarchical) {
        $list = array();
    }
    $query = $db->prepare("SELECT * FROM `$table_name` WHERE id_parent = ? ORDER BY $orderby");
    $query->execute(array($id_parent));
    while ($row = $query->fetch()) {
        $row['depth'] = $depth;
        $depth++;
        if ($hierarchical) {
            $row['children'] = cms_get_sub_items_list($db, $table_name, $row['id'], $list, $hierarchical);
        }
        $list[$row['id']] = $row;
        if (!$hierarchical) {
            $list = cms_get_sub_items_list($db, $table_name, $row['id'], $list, $hierarchical);
        }
        $depth--;
    }

    return $list;
}

function button($type, $url, $text = '', $title = '')
{
    $types = array(
        'details'   => array(
            'class'  => 'btn btn-default',
            'title'  => 'Details',
            'append' => '<i class="fa fa-fw fa-info-circle"></i> '),
        'add'       => array(
            'class'  => 'btn btn-success',
            'title'  => 'Add',
            'append' => '<i class="fa fa-fw fa-plus"></i> '),
        'back'      => array(
            'class'  => 'btn btn-default',
            'title'  => 'Back',
            'append' => '<i class="fa fa-fw fa-arrow-left"></i> '),
        'add_small' => array(
            'class'  => 'button_add_small',
            'append' => ''),
    );
    $t = $types[$type];
    $title = $title ? $title : $t['title'];
    ?><a href="<?= $url ?>" class="<?= $t['class'] ?>" title="<?= $title ?>"><?= $t['append'] . $text ?></a><?
}

function edit_button($url)
{
    ?><a href="<?= $url ?>" class="btn btn-default btn-xs" title="Edit"><span class="fa fa-pencil"></a><?
}

function add_button($url)
{
    ?><a href="<?= $url ?>" class="btn btn-success btn-xs" title="add"><span class="fa fa-plus"></a><?
}

function duplicate_button($table, $id, $custom_url = null)
{
    if ($custom_url) {
        $dup_url = $custom_url;
    } else {
        $dup_prefix = "a.php?a=copy&amp;t=$table&amp;return_url=" . urlencode($_SERVER['REQUEST_URI']) . "&amp;id=";
        $dup_url = $dup_prefix . $id;
    }
    ?>
    <a href="<?= $dup_url ?>" class="btn btn-default btn-xs" title="Copy page, sub-pages, and attributes"><span class="fa fa-copy"></span></a>
    <?
}

function delete_button($table, $id, $custom_url = null)
{
    if ($custom_url) {
        $delete_url = $custom_url;
    } else {
        $delete_prefix = "a.php?a=delete&amp;t=$table&amp;return_url=" . urlencode($_SERVER['REQUEST_URI']) . "&amp;id=";
        $delete_url = $delete_prefix . $id;
    }
    ?>
    <span class="delete_container">
		<button title="Delete" onclick="if(window.confirm('Really delete?')){ window.location='<?= $delete_url ?>'; }"
                class="delete_first btn btn-danger btn-xs">
            <span class="fa fa-trash-o"></span>
        </button>
	</span>
    <?
}

function print_controls($o = 0)
{
    ?>
    <div class="controls">
    <?
    if (!$o['no_save']) {
        ?>
        <button type="submit" class="btn btn-primary" name="submit" value="save"><i class="fa fa-fw fa-floppy-o"></i> Save</button><?
    }
    ?>
    </div><?
}

function print_rearrange_controls($show_cancel = true)
{
    ?>
    <button class="btn btn-primary rearrange-finished rearrange-tabs" style="display: none;"><i
            class="fa fa-fw fa-check"></i> Save Positions
    </button>
    <?
    if ($show_cancel) {
        ?>
        <button class="btn btn-cancel rearrange-cancel rearrange-tabs" style="display: none;"><i
                class="fa fa-fw fa-times"></i> Cancel
        </button>
        <?
    }
}

function show_nothing()
{
    ?><p class="dead">(Nothing here yet)</p><?
}

// find the navigation chain, based on db pages structure, for a given page
function find_nav_chain($db, $r)
{
    $chain = array();
    $chain[$r['id']] = $r; // first one's free - passed as argument
    while (0 != $r['id_parent']) { // continue until we've reached the top
        $r = get_page($db, array('id' => $r['id_parent']));
        // avoid self-reference
        if ($chain[$r['id']]) {
            break;
        }
        $chain[$r['id']] = $r; // add id to list
    }

    return array_reverse($chain, true);
}

function get_breadcrumbs($nav_chain, $separator = '&gt;', $use_links = true, $use_link_for_last = false)
{
    $current_page = end($nav_chain);
    $nav_keyword = $current_page['keyword'];
    ob_start();
    // show breadcrumbs if not on homepage
    if ('home' == $nav_keyword) {
        // nothing
    } else {
        $i = 0;
        $l = count($nav_chain);
        ?><a href="/">Home</a> <?= $separator ?> <?
        foreach ($nav_chain as $id => $page) {
            $i++;
            if($page['keyword']!='home') {
                if (isset($page['hide_in_breadcrumbs']) and $page['hide_in_breadcrumbs']) {
                    continue;
                }
                if ($i == $l and !$use_link_for_last) {
                    echo $page['title'];
                } elseif ($use_links) {
                    ?><a href="/<?= $page['keyword'] ?>"><?= $page['title'] ?></a><?
                } else {
                    ?><?= $page['title'] ?><?
                }
                if ($i != $l) {
                    echo ' ' . $separator . ' ';
                }
            }
        }
    }
    $breadcrumbs = ob_get_clean();

    return $breadcrumbs;
}

function cms_show_subpages(PDO $db, $id, $parent_ids = array(), $allow_drag_drop = true)
{
    static $depth = 0;
    $disable_hide_children = true;

    $query = $db->prepare("SELECT p.*, pv.version
		FROM pages p LEFT OUTER JOIN pages_vars pv
		ON pv.id_pages = p.id AND pv.version != ''
		WHERE id_parent = ?
		GROUP BY p.id
		ORDER BY priority ASC");
    $query->execute(array($id));
    $rr = $query->fetchAll();
    ?>
    <ol class="list treeview <?= ($allow_drag_drop and $depth == 0) ? 'root' : '' ?>" cmsTable="pages" cmsHierarchyMode="nested"
        rootElementId="<?= $id ?>"><?
    foreach ($rr as $r) {
        ?>
        <li class="<?= $class ?>" id="priorityitems-<?= $r['id'] ?>">
        <div class="row" title="edit">
            <div class="buttons" title="">
                <?
                if ($r['version']) {
                    ?>
                    <div class="cell">
                        <a href="pages_.php?id=<?= $r['id'] ?>&version=<?= $r['version'] ?>">Draft</a>
                    </div>
                    <?
                }
                ?>
                <!--<div class="cell"><a href="pages.php?root=<?= htmlspecialchars($r['id']) ?>" class="btn btn-xs" title="Open subpages view"><span class="fa fa-sitemap fa-rotate-270"></span></a></div>-->
                <div class="cell"><a href="pages_config_.php?id=<?= $r['id'] ?>" title="Config" class="btn btn-xs"><span
                            class="fa fa-gear"></span></a></div>
                <div class="cell">
                    <a href="<?= "a.php?a=pages-copy&amp;id={$r['id']}" ?>" class="btn btn-default btn-xs"
                       title="Copy page, sub-pages, and attributes"><span class="fa fa-copy"></span></a>
                </div>
                <div class="cell">
                    <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=secure&amp;t=pages&amp;id=<?=$r['id'] ?>" title="Click to <?=$r['secure']?'remove login requirement':'require login' ?> to see page">
                        <?
                        if ($r['secure']) {
                            ?><span class="fa fa-lock fa-fw"></span><?
                        } else {
                            ?><span class="fa fa-unlock fa-fw"></span><?
                        }
                        ?>
                    </a>
                </div>
                <div class="cell">
                    <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=navhide&amp;t=pages&amp;id=<?= $r['id'] ?>"
                       title="Click to <?= $r['navhide'] ? 'show' : 'hide' ?> in nav">
                        <?
                        if ($r['navhide']) {
                            ?><span class="fa fa-eye-slash"></span><?
                        } else {
                            ?><span class="fa fa-eye"></span><?
                        }
                        ?>
                    </a>
                </div>
                <div class="cell"><?
                    if (!$r['disable_delete']) {
                        delete_button('pages', $r['id']);
                    }
                    ?></div>
                <?
                if (!$disable_hide_children) {
                    ?>
                    <div class="cell">
                        <a class="hide_children">H</a>
                    </div>
                    <?
                }
                ?>
            </div>
            <a class="cell edit-link <?= $r['navhide'] ? ' dead' : '' ?>"
               href="pages_.php?id=<?= $r['id'] ?>">
                <?= htmlspecialchars($r['title']) ?> <?= $r['navhide'] ? '<span class="fa fa-eye-slash fa-fw" title="Page is hidden from navigation"></span> ' : '' ?>
                <?= $r['type'] == 'link' ? '<a href="#" title="This item is a link only" style="text-decoration: none ; font-size: 80% ;">Link</a>' : '' ?>
                <?= $r['secure'] ? ' <span class="fa fa-lock fa-fw" title="Page requires login to see"></span>' : '' ?>
            </a>
        </div>
        <?
        $depth++;
        $parent_ids_next_level = $parent_ids;
        $parent_ids_next_level[] = $r['id'];
        cms_show_subpages($db, $r['id'], $parent_ids_next_level);
        $depth--;
        ?>
        </li>
        <?
    }
    ?></ol><?
}

function cms_show_sub_items(PDO $db, $table_name, $id, $parent_ids = array(), $allow_drag_drop = true, $disable_children = true)
{
    static $depth = 0;
    $disable_hide_children = true;

    $query = $db->prepare("SELECT p.*
		FROM $table_name p
		WHERE id_parent = ?
		GROUP BY p.id
		ORDER BY priority ASC");
    $query->execute(array($id));
    $rr = $query->fetchAll();
    ?>
    <ol class="list treeview  <?= ($depth == 0 and $allow_drag_drop) ? 'root' : '' ?>" cmsTable="<?= $table_name ?>" cmsHierarchyMode="nested"
        rootElementId="<?= $id ?>"><?
    foreach ($rr as $r) {
        ?>
        <li class="<?= $class ?>" id="priorityitems-<?= $r['id'] ?>">
        <div class="row" title="edit">
            <div class="buttons">
                <div class="cell"><?
                    if (!$disable_children) {
                        ?>
                        <a href="<?= $table_name . "_.php?id_parent={$r['id']}" ?>" class="btn btn-success btn-xs" title="Add Sub Item"><span
                                class="fa fa-plus"></span></a>
                        <?
                    }
                    ?></div>
                <div class="cell"><?
                    if (!$r['disable_delete']) {
                        delete_button($table_name, $r['id']);
                    }
                    ?></div>
                <?
                if (!$disable_hide_children) {
                    ?>
                    <div class="cell">
                        <a class="hide_children">H</a>
                    </div>
                    <?
                }
                ?>
            </div>
            <a class="cell edit-link <?= $r['navhide'] ? ' dead' : '' ?>"
               href="<?= $table_name ?>_.php?id=<?= $r['id'] ?>"><?= htmlspecialchars($r['title']) ?> <?= $r['navhide'] ? '<span class="fa fa-eye-slash" title="Item is hidden"></span> ' : '' ?><?= $r['type'] == 'link' ? '<a href="#" title="This item is a link only" style="text-decoration: none ; font-size: 80% ;">Link</a>' : '' ?></a>
        </div>
        <?
        $depth++;
        $parent_ids_next_level = $parent_ids;
        $parent_ids_next_level[] = $r['id'];
        cms_show_sub_items($db, $table_name, $r['id'], $parent_ids_next_level);
        $depth--;
        ?>
        </li>
        <?
    }
    ?></ol><?
}


function get_paginator($o)
{
    // $o: window, num_tabs, baselink, count, cur_page
    $baselink = $o['baselink'];
    $pg_tabs_num = $o['num_tabs'];
    $pg_window = $o['window'];
    $count = $o['count'];
    $link_suffix = isset($o['link_suffix']) ? $o['link_suffix'] : '';

    $pg_curpage = isset($o['cur_page']) ? $o['cur_page'] : 1;
    $pg_mid_num = floor($pg_tabs_num / 2);
    $pg_start = ($pg_curpage - 1) * $pg_window;
    if ($pg_start <= 0) $pg_start = 0;

    $pg_count = ceil($count / $pg_window);
    if ($pg_count < 1) $pg_count = 1;
    $pg_last = $pg_start + $pg_window > $count ? $count : $pg_start + $pg_window;
    // redefining starting points
    $pg_begin = $pg_curpage - $pg_mid_num;
    if ($pg_begin <= 0) $pg_begin = 1;
    $pg_end = $pg_begin + $pg_tabs_num - 1;
    if ($pg_end > $pg_count) $pg_end = $pg_count;
    if ($pg_begin <= 0) $pg_begin = 1;
    $prev_page = $pg_curpage - 1 > 0 ? ($pg_curpage - 1) : 1;
    $next_page = $pg_curpage + 1 > $pg_count ? $pg_count : ($pg_curpage + 1);

    if ($count > $pg_window) {
        ob_start();
        if ($o['show_arrows']) {
            ?><a href="<?= $baselink . $prev_page . $link_suffix ?>">&#9664;</a><?
        }
        foreach (range($pg_begin, $pg_end) as $i) {
            if ($i == $pg_curpage) {
                ?><a class="current"><?= $i ?></a><?
            } else {
                ?><a href="<?= $baselink . $i . $link_suffix ?>"><?= $i ?></a><?
            }
        }
        if ($o['show_arrows']) {
            ?><a href="<?= $baselink . $next_page . $link_suffix ?>">&#9654;</a><?
        }
        $paginator = ob_get_clean();
    } else {
        $paginator = '';
    }

    return $paginator;
}