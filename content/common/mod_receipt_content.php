<html>
<body>
<table align="center" width="590">
    <tr>
        <td>

            <table width="100%" style="border-spacing:0px; border-collapse:collapse;">
                <tr>
                    <td style="font-family:Arial;font-size:90%;">
                        <b style="font-size: 120%"><?= $config['company_name'] ?></b><br/>
                        Order Number: <?= $o['ordernumber'] ?><br/>
                        <?= $config['email_contact'] ?><br/>
                        <a href="<?= $config['site_address'] ?>"><?= $config['site_address'] ?></a>
                    </td>
                </tr>
            </table>
            <?php

            $c = sql_fetch_by_key($db, 'campers', 'id', $o['camper']['id']);
            $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);

            ?>
            <h3><?=$p['title'] ?></h3>
            <?=$p['reciept_text'] ?>
            <hr/>

            <p><b style="font-size: 120% ;">Items Purchased</b></b>

            <table cellspacing="3" style="width:100%">
                <?
                if($_SESSION['paymentamount']){
                    ?>
                    <tr>
                        <th style="text-align: left ;">Camper</th>
                        <th style="text-align: left ;">Price</th>
                        <th colspan="2"></th>
                    </tr>
                    <tr>
                        <td style="font-size:80%;"><?=$o['camper']['firstname'] ?> <?=$o['camper']['lastname'] ?></td>
                        <td style="text-align: left ;"><?= number_format($_SESSION['paymentamount'],2) ?></td>
                        <td colspan="2"></td>
                    </tr>
                    <?
                }else{

                cart_display($db, array(
                    'cart'                        => $o['cart'],
                    'discounts'                   => $o['discounts'],
                    'customer'                    => $o['customer'],
                    'show_tax'                    => true,
                    'show_controls'               => false,
                    'tax_override_amount'         => $o['tax_override_amount'],
                    'grand_total_override_amount' => $o['grand_total_override_amount'],
                ));
                }
                ?>
            </table>

            <hr/>

            <p><b style="font-size: 120% ;">Billing Info</b></p>
            <table>
                <tr>
                    <td style="vertical-align: top ;">
                        <table class="chart">
                            <tr>
                                <th colspan="2" style="text-align: left ;">Billing</th>
                            </tr>
                            <tr>
                                <td><b>First Name</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_firstname']) ?></b></td>
                            </tr>
                            <tr>
                                <td><b>Last Name</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_lastname']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Country</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_country']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Address</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_address']) ?></td>
                            </tr>
                            <tr>
                                <td><b>City</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_city']) ?></td>
                            </tr>
                            <tr>
                                <td><b>State/Province</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_state']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Zip/Postal Code</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_zip']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Email</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_email']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Phone</b></td>
                                <td><?= htmlspecialchars($o['customer']['bi_phone']) ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>