<?

//addhttp function is located in mod_util

function getGallery($id){
    global $db;

    $query = $db->prepare("SELECT * FROM photos WHERE id_parent = ? ORDER BY priority");
    $query->execute(array($id));
    $pics = $query->fetchAll();

    return $pics;
}
function boom(){
    return "test";
}
function getContentGallery($content){
    global $db,$settings;
    $basedir_photos = 'upload/photos';
    return preg_replace_callback(
        '@\[gallery-(\d+)\]@',
        function ($matches) use ($db, $basedir_photos, $settings) {
            return '<div class="gallery">' . gallery_display(
                $db,
                $basedir_photos,
                $matches[1]
            ) . '</div>';
        },
        $content
    );
}