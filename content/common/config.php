<?
error_reporting(0);
ini_set("display_errors", 0);

require_once(__DIR__."/../../vendor/autoload.php");
require_once(__DIR__."/mod_cms.php");
require_once(__DIR__."/mod_form.php");
require_once(__DIR__."/mod_format.php");
require_once(__DIR__."/mod_sql.php");
require_once(__DIR__."/mod_util.php");
require_once(__DIR__."/mod_galleries.php");
require_once(__DIR__."/mod_blog.php");
require_once(__DIR__."/mod_cart.php");
require_once(__DIR__."/mod_campstix.php");
require_once(__DIR__."/mod_twig_ext.php");


error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);


// config settings
date_default_timezone_set('America/Los_Angeles');

// import config file and merge with local config
$config = json_decode(file_get_contents(__DIR__.'/../../config/config.json'), true);

if (file_exists(__DIR__.'/../../config/config-local.json')) {
    $local_config = json_decode(file_get_contents(__DIR__.'/../../config/config-local.json'), true);
    $config = array_replace_recursive($config, $local_config);
}

mb_internal_encoding("utf-8");
session_start();


// database connection
try {
    $db = new PDO("mysql:dbname=" . $config['db']['name'] . ";host=" . $config['db']['host'], $config['db']['username'], $config['db']['password']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->query("SET sql_mode=''");
} catch (PDOException $e) {
    die("Problem connecting to database. ");
}

// twig environment setup
$twig = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"].'/templates'), [
    'debug'=>true,
    'cache'=>__DIR__.'/../../twig_cache'
]);

$twig->addExtension(new Twig_Extension_Debug());
$twig->addExtension(new Aptoma\Twig\Extension\MarkdownExtension(new Aptoma\Twig\Extension\MarkdownEngine\MichelfMarkdownEngine()));
$twig->addFunction(new Twig_SimpleFunction('getGallery', 'getGallery'));
$twig->addFunction(new Twig_SimpleFunction('getContentGallery', 'getContentGallery'));

// prep for Stripe transactions
//require_once('lib/stripe-php/lib/Stripe.php');

if ($_SESSION['test']) {
    $stripe_secret_key = $config['stripe']['test_secret_key'];
} else {
    $stripe_secret_key = $config['stripe']['live_secret_key'];
}
\Stripe\Stripe::setApiKey($stripe_secret_key);

// mailer
$mailer_transport = Swift_SmtpTransport::newInstance(
    $config['smtp']['host'],
    $config['smtp']['port'],
    'ssl'
);
$mailer_transport->setUsername($config['smtp']['username']);
$mailer_transport->setPassword($config['smtp']['password']);
$mailer = Swift_Mailer::newInstance($mailer_transport);

if ($config['log_swiftmailer']) {
    $swiftmailer_logger = new Swift_Plugins_Loggers_ArrayLogger();
    $mailer->registerPlugin(new Swift_Plugins_LoggerPlugin($swiftmailer_logger));
}

// logging
$logger = new Monolog\Logger('applog');
$logger->pushHandler(new Monolog\Handler\StreamHandler(__DIR__.'/../../app.log', Monolog\Logger::DEBUG));

if ($_SESSION['admin']) {
    $config['admin_conf'] = $config['admin_types'][$_SESSION['admin']['type']];
}

// set up test mode
if (isset($_GET['test']) and $_SESSION['admin']) {
    $_SESSION['test'] = $_GET['test'];
}


//Settings from DB
$settings = settings_load($db);
if($settings['receipt_contact']) {
    $config['receipt_contact'] = $settings['receipt_contact'];
}
$volunteerfieldsbasic = array(
    'uid',
    'pid',
    'firstname',
    'lastname'
);
$volunteerfieldstwigs = array(
    'uid',
    'pid',
    'firstname',
    'lastname',
    'bdate',
    'gender',
    'tshirt',
    'newtocamp',
    'availableallweek',
    'available_start',
    'available_departure',
    'campname_return',
    'last_volunteer_team',
    'last_volunteer_year',
    'previouscamper',
    'heardfrom',
    'campname_new',
    'contact_email',
    'confirm_contact_email',
    'contact_phone',
    'can_we_text_you',
    'contact_address',
    'contact_city',
    'contact_zip',
    'contact_state',
    'mailing_address',
    'mailing_city',
    'mailing_state',
    'mailing_zip',
    'emergencycontact_fname',
    'emergencycontact_lname',
    'emergencycontact_relation',
    'emergencycontact_phone',
    'hasdiabetes',
    'allergies',
    'allergy_bees',
    'allergy_latex',
    'allergy_penicillin',
    'allergy_seasonal',
    'allergy_med_reactions',
    'allergies_more',
    'allergy_epipen',
    'allergies_food',
    'allergy_peanuts',
    'allergy_shellfish',
    'allergy_dairy',
    'allergy_eggs',
    'allergy_soy',
    'allergy_gluten',
    'allergy_nuts',
    'allergy_fruit',
    'allergy_food_reactions',
    'allergy_food_epipen',
    'allergies_food_more',
    'medical_diet',
    'gluten_free',
    'dairy_free',
    'medical_diet_more',
    'otherdiataryneeds',
    'otherdiataryneeds_more',
    'otherdisabilities',
    'otherdisabilities_more',
    'limitedsleep',
    'limitedsleep_more',
    'otherconditions',
    'otherconditions_more',
    'highestdegree',
    'yearsvolunteered',
    'preferred_camper_age',
    'mash',
    'mash_title',
    'mash_license',
    'mash_licenserevoked',
    'mash_licenserevoked_more',
    'mash_malpractice',
    'mash_malpractice_more',
    'mash_expectation',
    'mash_toughestchallenges',
    'mash_sleepchallenges',
    'mash_growth_goals',
    'mash_improveexperience',
    'mash_hobbies',
    'mash_return_motivation',
    'program',
    'program_position',
    'consider_another_program',
    'program_publicspeaking',
    'program_othercontributions',
    'program_certifications',
    'program_expectations',
    'program_elevateleader',
    'program_toughestchallenges',
    'program_improveexperience',
    'program_standout',
    'program_under18_camper',
    'support',
    'support_position',
    'support_positionother',
    'support_consider_another',
    'support_consider_another_more',
    'support_contributions',
    'support_expectations',
    'support_improve',
    'support_standout',
    'support_experience',
    'departmentpreference_1',
    'departmentpreference_2',
    'olderthan18',
    'guardian_of_camper',
    'read_COV_policy',
    'children_fullnames',
    'release_agree',
    'camp_waiver_agree',
    'parent_vol_phone',
    'parentname',
    'parentemail',
    'parentphone',
    'dresscode_agree',
    'code_agree'
);

$volunteerfieldsstix = array_merge(
    $volunteerfieldstwigs,
    array(
        'dish',
        'dish_current_foodscard',
        'dish_skills',
        'dish_management_skills',
        'dish_dietary_credentials',
        'dish_specialty_diet_skills',
        'dish_job_preference',
        'dish_expectations',
        'dish_best_quality',
        'dish_best_utilize',
        'dish_any_considerations',
        'departmentpreference_3',
        'release_biblecamp',
        'release_hba1c',
        'release_code',
        'release_code_of_ethics',
        'release_dresscode',
        'release_images',
        'release_transport'
    )
);

$camperfieldsbasic = array(
    'uid',
    'pid',
    'firstname',
    'lastname'
);
$camperfieldstwigs = array(
    'uid',
    'pid',
    'firstname',
    'lastname',
    'bdate',
    'gender',
    'gender2',
    'tshirt',
    'new_to_camp',
    'heard_about_from',
    'has_diabetes',
    'age_of_diagnosis',
    'parent_status',
    'legal_custody',
    'family_history',
    'ethnicity',
    'insurance_group',
    'policy_number',
    'diabetes_provider',
    'provider_phone',
    'primary_care_name',
    'primary_provider_phone',
    'care_opportunities',
    'other_challenges',
    'other_challenges_more',
    'other_activities',
    'other_activities_more',
    'allergies',
    'allergy_bees',
    'allergy_latex',
    'allergy_penicillin',
    'allergy_seasonal',
    'allergy_med_reactions',
    'allergy_epipen',
    'allergies_more',
    'allergies_food',
    'allergy_peanuts',
    'allergy_shellfish',
    'allergy_dairy',
    'allergy_eggs',
    'allergy_soy',
    'allergy_gluten',
    'allergy_nuts',
    'allergy_fruit',
    'allergy_food_reactions',
    'allergy_food_epipen',
    'allergies_food_more',
    'medical_diet',
    'gluten_free',
    'dairy_free',
    'medical_diet_more',
    'other_allergyinfo',
    'other_allergyinfo_more',
    'asthma',
    'asthma_more',
    'important_medical_info',
    'important_medical_info_more',
    'immunizations_up_to_date',
    'immunizations_date',
    'immunizations_up_to_date_more',
    'parent_fname',
    'parent_lname',
    'guardian_relation',
    'parent_email',
    'parent_alt_email',
    'parent_alt_name',
    'newsletter_signup',
    'parent_phone',
    'can_we_text_you',
    'parent_alt_phone',
    'parent_address',
    'parent_city',
    'parent_state',
    'parent_zip',
    'emergencycontact_fname',
    'emergencycontact_lname',
    'emergencycontact_relation',
    'emergencycontact_phone',
    'emergencycontact_alt_phone'
);
$camperfieldsstix = array_merge(
    $camperfieldstwigs,
    array(
        'within_500_miles',
        'preferred_name',
        'swiming_level',
        'first_time_away_from_home',
        'bedwetter',
        'bedwetter_more',
        'parent_employer',
        'release_transport',
        'release_images',
        'release_dresscode',
        'release_expectations',
        'release_code',
        'release_biblecamp',
        'chip_participate',
        'signature_full_legal_name',
        'signature_date',
    )
);




// log in for admin
if( false!==strpos($_SERVER['PHP_SELF'],'backroom/') ){
    if( 'admin_login' == $_POST['action'] ){
        $_SESSION['admin'] = admin_login($db, $_POST['username'], $_POST['password']) ;
        if( $_SESSION['admin'] ){
            hle($config['admin_types'][ $_SESSION['admin']['type'] ]['home_url']) ;
        }
    }
    if( ! $_SESSION['admin'] and 'index.php'!=basename($_SERVER['PHP_SELF']) ){
        hle('./') ;
    }
}
