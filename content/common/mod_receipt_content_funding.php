<html>
<body>
<table align="center" width="590">
    <tr>
        <td>
            <table width="100%" style="border-spacing:0px; border-collapse:collapse;">
                <tr>
                    <td style="font-family:Arial;font-size:90%;">
                        <b style="font-size: 120%"><?= $config['company_name'] ?></b><br/>
                        Order Number: <?= $o['ordernumber'] ?><br/>
                        <?= $config['email_contact'] ?><br/>
                        <a href="<?= $config['site_address'] ?>"><?= $config['site_address'] ?></a>
                    </td>
                </tr>
            </table>
            <hr/>
            <p><b style="font-size: 120% ;">Fund My Camp Payment</b></b>
            <table style="width:100%" cellspacing="3">
                <tbody><tr>
                    <th style="text-align: left ;">Camper</th>
                    <th style="text-align: left ;">Price</th>
                </tr>
                <tr>
                    <td style="font-size:80%;"><?=$o['camper']['firstname'] ?> <?=$o['camper']['lastname'] ?></td>
                    <td>$<?=$o['values']['order_grand_total'] ?></td>
                </tr>
                </tbody>
            </table>
            <hr/>
            <p><b style="font-size: 120% ;">Billing Info</b></p>
            <table>
                <tr>
                    <td style="vertical-align: top ;">
                        <table class="chart">
                            <tr>
                                <th colspan="2" style="text-align: left ;">Billing</th>
                            </tr>
                            <tr>
                                <td><b>Name</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_firstname']) ?></b></td>
                            </tr>
                            <tr>
                                <td><b>Address</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_address']) ?></td>
                            </tr>
                            <tr>
                                <td><b>City</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_city']) ?></td>
                            </tr>
                            <tr>
                                <td><b>State/Province</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_state']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Zip/Postal Code</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_zip']) ?></td>
                            </tr>
                            <tr>
                                <td><b>Email</b></td>
                                <td><?= htmlspecialchars($o['values']['bi_email']) ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>