<?
function add_subsections($values, $id = 0){
    global $db, $_SESSION;

    $fields = array('id_pages','header','subheader','content','gallery','priority');

    $basedir = '../upload/page_images';
    // resize image if provided

    if ($values['image']['name']) {
        list($image) = relocateImage($values['image']['tmp_name'], array(
            array('width' => 100, 'name' => $values['image']['name'], 'dir' => "$basedir/thumb"),
            array('width' => 1000, 'name' => $values['image']['name'], 'dir' => "$basedir/1000"),
            array('width' => 2000, 'name' => $values['image']['name'], 'dir' => "$basedir/2000"),
            array('name' => $values['image']['name'], 'dir' => "$basedir/orig"),
        ));
        m('image added '.$image);
        $fields[] = 'image';
        $values['image'] = $image;
    }

//    $values['id_pages'] = $id_pages;

    return sql_upsert($db, 'pages_sections', $fields, $values, 'id', $id);
}

function get_camper_count($cart){
    $cart_count = 0;
    foreach($cart as $item){
        if($item['regtype']){
            $cart_count += $item['qty'];
        }
    }
    return $cart_count;
}

function get_funding_keyword($keyword){
    global $db;
    $r = sql_fetch_by_key($db, 'funding', 'keyword', $keyword);

    if($r) {
        return get_funding_keyword(randstring(20));
    }
    return $keyword;
}

function get_next_cart_reg($cart, $campers){
    foreach($cart as $p){
        $pid = $p['id'];
        $qty = $p['qty'];
        $camper_qty = 0;
        foreach($campers as $c){
            if($c['pid'] ==  $pid){
                $camper_qty++;
            }
        }
        if($camper_qty<$qty){
            return $pid;
        }
    }
    return false;
}

function save_camper(){

}

function login_form($address){
    ob_start();
    ?>
        <form action="/a.php?a=login" method="post">
            <div class="form-element">
                <input id="log-user_email" type="text" name="user_email" />
                <label for="log-user_email">Email Address</label>
            </div>
            <div class="form-element">
                <input id="log-pass" type="password" name="password" />
                <label for="log-pass">Password</label>
            </div>
            <input type="submit" value="login" name="login" class="full-width" />
            <hr>
            <a href="<?=$address ?>?a=forgot_pw">Forgot Password?</a><br>
            Don't have an account? <a href="<?=$address ?>?a=register">Create an Account</a>
        </form>
    <?
    return ob_get_clean();
}


function mc_get_paginator($o)
{
    // $o: window, num_tabs, baselink, count, cur_page
    $baselink = $o['baselink'];
    $pg_tabs_num = $o['num_tabs'];
    $pg_window = $o['window'];
    $count = $o['count'];
    $link_suffix = isset($o['link_suffix']) ? $o['link_suffix'] : '';

    $pg_curpage = isset($o['cur_page']) ? $o['cur_page'] : 1;
    $pg_mid_num = floor($pg_tabs_num / 2);
    $pg_start = ($pg_curpage - 1) * $pg_window;
    if ($pg_start <= 0) $pg_start = 0;

    $pg_count = ceil($count / $pg_window);
    if ($pg_count < 1) $pg_count = 1;
    $pg_last = $pg_start + $pg_window > $count ? $count : $pg_start + $pg_window;
    // redefining starting points
    $pg_begin = $pg_curpage - $pg_mid_num;
    if ($pg_begin <= 0) $pg_begin = 1;
    $pg_end = $pg_begin + $pg_tabs_num - 1;
    if ($pg_end > $pg_count) $pg_end = $pg_count;
    if ($pg_begin <= 0) $pg_begin = 1;
    $prev_page = $pg_curpage - 1 > 0 ? ($pg_curpage - 1) : 1;
    $next_page = $pg_curpage + 1 > $pg_count ? $pg_count : ($pg_curpage + 1);

    if ($count > $pg_window) {
        ob_start();
        // left arrow
        if ($pg_curpage > 1) {
            ?><a href="<?= $baselink . ($pg_curpage - 1) . $link_suffix ?>">&lt;</a><?
        } else {
            ?><a class="current">&lt;</a><?
        }
        // numbered buttons
        foreach (range($pg_begin, $pg_end) as $i) {
            if ($i == $pg_curpage) {
                ?><a class="current"><?= $i ?></a><?
            } else {
                ?><a href="<?= $baselink . $i . $link_suffix ?>"><?= $i ?></a><?
            }
        }
        // right arrow
        if ($pg_curpage < $pg_end) {
            ?><a href="<?= $baselink . ($pg_curpage + 1) . $link_suffix ?>">&gt;</a><?
        } else {
            ?><a class="current">&gt;</a><?
        }
        $paginator = ob_get_clean();
    } else {
        $paginator = '';
    }

    return $paginator;
}

function get_coords($zip)
{
    global $config;

    $coordinateurl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($zip) . '&key=' . $config['googleapi']['serverkey'];

    $coordinateinfo = file_get_contents($coordinateurl);

    $coordinateinfo = json_decode($coordinateinfo, true);
    if ($coordinateinfo['results'][0]['geometry']['location']['lat']) {
        return array(
            'lat' => $coordinateinfo['results'][0]['geometry']['location']['lat'],
            'lng' => $coordinateinfo['results'][0]['geometry']['location']['lng']
        );
    } else {
        return 'error';
    }
}

function distance($lat1, $lon1, $lat2, $lon2)
{
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;

    return $miles;
}

function IsPostcode($postcode)
{
    $postcode = strtoupper(str_replace(' ', '', $postcode));
    if (preg_match("/\b\d{5}(-\d{4})?\b/", $postcode))
        return true;
    else
        return false;
}


function aasort(&$array, $key)
{
    $sorter = array();
    $ret = array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii] = $va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii] = $array[$ii];
    }
    $array = $ret;
}

function convert_to_1d($rr, $column)
{
    $temp = array();
    foreach ($rr as $r) {
        $temp[] = $r[$column];
    }

    return $temp;
}



function getUser($id)
{
    global $db;

    $u = sql_fetch_by_key($db, 'users', 'id', $id);


    return $u;
//    return $u['display_name'];
}


function testKeyword($table, $keyword, $id, $i = 0)
{
    global $db;

    //test for approved tables
    switch ($table) {
        case 'directory':
        case 'options':
        case 'page':
            break;
        default:
            $table = '';
            break;
    }

    $query = $db->prepare("SELECT * FROM $table WHERE keyword = ? AND id != ?");
    $query->execute(array($keyword . ($i ? $i : ''), $id));
    $r = $query->fetchall();

    if (count($r)) {
        return testKeyword($table, $keyword, $id, $i + 1);
    } else {
        return $keyword . ($i ? $i : '');
    }

}

/*
 * Gets a keyword based off of a table and name.  ID field is for updating existing table rows.
 */
function getKeyword($table, $name, $id = 0)
{
    $keyword = strtolower(preg_replace('/[^\w]/', '-', $name));
    $keyword = testKeyword($table, $keyword, $id);

    return $keyword;
}


function getStates($type = 'both')
{
    global $db, $config;
    switch ($type) {
        case 'abr':
            return array_keys($config['us_states']);
            break;
        case 'full':
            return array_values($config['us_states']);
            break;
        case 'both':
        default:
            return $config['us_states'];
    }
}

function convertState($state, $abbr = false)
{
    global $config;

    foreach ($config['us_states'] as $k => $v) {
        if (trim(strtoupper($state)) == $k || trim(strtoupper($state)) == $v) {
            if ($abbr) {
                return $v;
            } else {
                return $k;
            }
        }
    }

    //If not found something has gone wrong.  Return the original state
    return $state;
}

function getPriceRange($id, $sale = true)
{
    global $db;
    $p = sql_fetch_by_key($db, 'products', 'id', $id);

    $price = $p['price'];
    $high = $price;
    $low = $price;

    //get all options for this product
    $query = $db->prepare("SELECT * FROM product_options WHERE pid = ? ORDER BY addprice DESC");
    $query->execute(array($id));
    $options = $query->fetchall();
    $revoptions = array_reverse($options);

    //loop through for high price
    foreach ($options as $o) {
        $r = sql_fetch_by_key($db, 'options', 'id', $o['oid']);
        //if this is a product specific option it is a "Model" not just a feature
        if ($r['id_product']) {
            $high += $o['addprice'];
            break;
        }
    }
    //loop through for high price
    foreach ($revoptions as $o) {
        $r = sql_fetch_by_key($db, 'options', 'id', $o['oid']);
        //if this is a product specific option it is a "Model" not just a feature
        if ($r['id_product']) {
            $low += $o['addprice'];
            break;
        }
    }
    if($sale){
        if($p['saleoff']){
            $low = $low - $p['saleoff'];
            $high = $high - $p['saleoff'];
        }
        if($p['salepercent']){
            $percentage = 1 - ($p['salepercent']/100);
            $low = $low * $percentage;
            $high = $high * $percentage;
        }
    }

    if ($low != $high) {
        return fmt_price($low) . ' - ' . fmt_price($high);
    } else {
        return fmt_price($low);
    }

}


/*********************************************************************************************************
 * DESCRIPTION: formattedemail() Places an email message into a formatted peice of HTML code
 * PARAMETERS:
 *    $content
 *        - The content for the email.
 *********************************************************************************************************/
function formattedemail($content)
{
    global $config;
    $sitecolor = '#286987';

    $message = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    </head>
    <body bgcolor="#eeeeee" style="background:#eeeeee"><br />
        <center>
           <img src="' . $config['site_address'] . '/images/logos/main-flat-nods.png" alt="" style="width:180px;" />
           <br>
           <br>
        <table width="100%" bgcolor="#ffffff" style="max-width:650px; margin:auto; background:#ffffff; border-color:#eeeeee">
            <tr>
                <td bgcolor="' . $sitecolor . '" style="padding:5px; background:' . $sitecolor . '">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding:15px;">
                    ' . $content . '
                </td>
            </tr>
            <tr>
                <td bgcolor="' . $sitecolor . '" style="padding:5px; background:' . $sitecolor . ';">&nbsp;</td>
            </tr>
        </table>
        </center><br /><br />
    </body>
    </html>';

    return $message;
}


?>