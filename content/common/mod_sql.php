<?
function sql_fetch_by_key(PDO $db, $safe_tablename, $safe_column_name, $unsafe_column_value, $use_cache=false){
	static $cache;
	$cache_key = $safe_tablename . '-' . $safe_column_name . '-' . $unsafe_column_value;
	if ($use_cache and $cache[$cache_key]) {
		return $cache[$cache_key];
	}
	$query = $db->prepare("SELECT * FROM `$safe_tablename` WHERE `$safe_column_name` = ?") ;
	$query->execute(array($unsafe_column_value)) ;
	$row = $query->fetch();
	$cache[$cache_key] = $row;
	return $row ;
}
function sql_upsert(PDO $db, $table_name, array $fields, array $unsafe_values, $key_field_name=null, $unsafe_key_value=null, $insert_only_setlist=null){
	$set_parts = array() ;
	$replacement_values = array() ;
	foreach( $fields as $field_name ){
		if( isset($unsafe_values[$field_name]) ){
			$set_parts[] = "$field_name = :$field_name" ;
			$replacement_values[":$field_name"] = $unsafe_values[$field_name] ;
		}
	}

	if( isset($key_field_name) and isset($unsafe_key_value) and $unsafe_key_value ){
		$replacement_values[":$key_field_name"] = $unsafe_key_value ;
		$set_string = implode(', ', $set_parts) ;
		$sql = "UPDATE `$table_name` SET $set_string WHERE $key_field_name = :$key_field_name" ;
	}else{
		if( isset($insert_only_setlist) ){
			$set_parts[] = trim($insert_only_setlist, ', ') ;
		}
		$set_string = implode(', ', $set_parts) ;
		$sql = "INSERT INTO `$table_name` SET $set_string" ;
	}
	
	$query = $db->prepare($sql) ;
	$query->execute($replacement_values) ;
	$error = $query->errorInfo() ;
	
	d($sql) ;
	d($replacement_values) ;
	d($error[2]) ;

	return (isset($unsafe_key_value) and $unsafe_key_value) ? $unsafe_key_value : $db->lastInsertId() ;
}
function sql_organize_values(array $fields, array $input_values, array $checkbox_fields=null ){
	// fill in values
	$output_values = array() ;
	foreach( $fields as $field_name ){
		$output_values[$field_name] = isset($input_values[$field_name]) ? $input_values[$field_name] : '' ;
	}
	if( is_array($checkbox_fields) ){
		// set checkbox fields to int
		foreach( $checkbox_fields as $field_name ){
			$output_values[$field_name] = $output_values[$field_name] ? 1 : 0 ;
		}
	}
	return $output_values ;
}
function sql_add_csv_params(array $params, array $to_add, $param_prefix = 'param'){
	$i = 0;
	$param_names = array();
	foreach ($to_add as $value) {
		$i++;
		$param_name = ':'.$param_prefix.'_'.$i;
		$params[$param_name] = $value;
		$param_names[] = $param_name;
	}
	$csv_param_string = implode(', ', $param_names);
	return array($params, $csv_param_string);
}
