<?
function gallery_get_main_photo($db, $gallery_id)
{
    $query = $db->prepare("SELECT * FROM photos WHERE featured = 1 AND type = 'galleries' AND id_parent = ?");
    $query->execute([$gallery_id]);
    return $query->fetch();
}
function gallery_display($db, $basedir_photos, $id_parent, $max = 100)
{
    $basedir = "$basedir_photos";
    $query = $db->prepare("SELECT *
		FROM photos
		WHERE id_parent = :id_parent
		ORDER BY priority ASC
		");
    $query->execute(array(
        ':id_parent' => $id_parent,
    ));
    $photos = $query->fetchAll();
    if ($query->rowCount()) {
        ob_start();
        $rel = "$type-$id_parent-gallery";
        foreach ($photos as $photo) {



            $imgpath = '/' . $basedir . '/400/' . $photo['image'];
            $bigimgpath = '/' . $basedir . '/800/' . $photo['image'];
            $link = '/' . $basedir . '/800/' . $photo['image'];
                $title = trim($photo['title']);
            $title_html = $title ? "title=\"$title\"" : "";
            list($width, $height) = getimagesize($bigimgpath);
            ?>
            <a rel="<?= $rel ?>" href="<?= $link ?>" class="pic_container" itemprop="image" <?= $title_html ?>>
                <span class="pic" style="background-image: url('<?= htmlspecialchars(addslashes($imgpath)) ?>') ;"></span>
                <span class="big-pic" style="background-image: url('<?= htmlspecialchars(addslashes($imgpath)) ?>') ;"></span>
                <span class="title"><?= $photo['title'] ?></span>
                <span class="description"><?= $photo['content'] ?></span>
                <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                    <img itemprop="image" class="post_preview" alt="<?= $title_html ?>" style="display:none;" src="https://chas.org/<?=$imgpath ?>" />
                    <meta itemprop="url" content="https://chas.org/<?=$imgpath ?>">
                    <meta itemprop="width" content="<?=$width ?>">
                    <meta itemprop="height" content="<?=$height ?>">
                </div>
            </a>
            <?
        }
        $html = ob_get_clean();

        return $html;
    }
}
/*
function gallery_display($db, $basedir_photos, $id_parent, $max = 100)
{

    $query = $db->prepare("SELECT * FROM galleries WHERE id=?");
    $query->execute(array($id_parent));
    $gallery = $query->fetch();

    $basedir = "$basedir_photos";
    $query = $db->prepare("SELECT *
		FROM photos
		WHERE NOT hidden AND id_parent = :id_parent
		ORDER BY priority ASC
		");
    $query->execute(array(
        ':id_parent' => $id_parent,
    ));
    $photos = $query->fetchAll();
    if ($query->rowCount()) {
        ob_start();
        $rel = "$type-$id_parent-gallery";
        ?>
        <div class="gallery ">
                <div class="slick-img">
                    <?
                    $i = 0;
                    foreach ($photos as $photo) {
                        $imgpath = '/' . $basedir . '/thumb/' . $photo['image'];
                        $bigimgpath = '/' . $basedir . '/800/' . $photo['image'];
                        $zoomedimgpath = '/' . $basedir . '/1600/' . $photo['image'];
                        $link = '/' . $basedir . '/800/' . $photo['image'];
                        $i++;
                        ?>
                        <div class="slick-img-item">
                            <a href="<?=$zoomedimgpath ?>" class="pic_container">
                                <img alt="<?= $photo['alt'] ?>" <? if($i==1) echo 'itemprop="image"'; ?> src="<?= htmlspecialchars(addslashes($bigimgpath)) ?>"/>
                            </a>
                            <div class="caption"><?= $photo['title'] ?></div>
                        </div>
                    <?
                    }
                    ?>
                </div>
                <? if(count($photos)>1){ ?>
                <div class="slick-nav <?=(count($photos)==5) ? 'slick-five-fix':'' ?>">
                    <?
                    $i = 0;
                    foreach ($photos as $photo) {
                        $imgpath = '/' . $basedir . '/thumb/' . $photo['image'];
                        $bigimgpath = '/' . $basedir . '/800/' . $photo['image'];
                        $link = '/' . $basedir . '/800/' . $photo['image'];
                        ?>
                        <div class="slick-nav-item <?= !$i?'slick-active':''; ?>" data-slick-index="<?=$i ?>">
                            <img alt="<?= $photo['alt'] ?>" src="<?= htmlspecialchars(addslashes($imgpath)) ?>"/>
                        </div>
                    <?
                        $i++;
                    }
                    ?>
                </div>
                <? } ?>
            </div>
        <script>
            $(document).ready(function () {
                $('.slick-gallery').each(function( index ) {
                    var s_gallery = $(this);
                    var s_nav = $(this).find('.slick-nav');
                    var s_imgs = $(this).find('.slick-img');

                        s_imgs.slick({
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            fade: false,
                            infinite: false
                        });

                        $('.slick-nav-item').click(function() {
                            s_imgs.slick("slickGoTo", $(this).attr('data-slick-index'));
                            $('.slick-nav-item').removeClass('slick-active');
                            $(this).addClass('slick-active');
                        });
                        s_imgs.on('beforeChange', function(event, slick, currentSlide, nextSlide){

                            $('.slick-nav-item').removeClass('slick-active');
                            $('.slick-nav-item[data-slick-index="'+nextSlide+'"]').addClass('slick-active');
                        });

                });
            });
        </script>
        <?
        $html = ob_get_clean();
        return $html;
    }
}

?>