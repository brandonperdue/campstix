
$(document).ready(function() {
    $('.dropdown-toggle').dropdown();
    $('.dropdown').hover(  function() {
        $( this ).addClass('open');
    }, function() {
        $( this ).removeClass('open');
    });

    $('.accordion-list-group').each(function(){
        //"click" first item to open it
       $(this).find('.collapsible-item-title-link:first').click();
    });


    $("input.numberonly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".read-more").click(function () {

        var closetextattr = $(this).attr('closetxt');
        if (typeof closetextattr !== typeof undefined && closetextattr !== false) {
            var closetext = closetextattr;
        }else{
            var closetext = "Close";
        }

        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $(this).html($(this).attr('currenttext'));

            $(this).attr('currenttext',closetext);

            $('#'+$(this).attr('target')).slideUp();
        }else{
            $(this).addClass('active');
            $(this).attr('currenttext',$(this).html());
            $(this).html(closetext);

            $('#'+$(this).attr('target')).slideDown();
        }
    });

    $(".btn-navbar").click(function () {
        $(this).toggleClass("open");
        $('#fixed-menu').toggleClass("open");
        $('.header-narrow').toggleClass("open");
    });

    $("input").blur(function() { checkFormValue(this); });
    $("textarea").blur(function() { checkFormValue(this); });
    $("select").blur(function(){ checkFormValue(this); });

    //check for values on page refresh
    $("input").each(function(){ checkFormValue(this); });
    $("textarea").each(function(){ checkFormValue(this); });
    $("select").each(function(){ checkFormValue(this); });

    $('.pic_container').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    $('.ajax_popup').magnificPopup({
        type: 'ajax'
    });

    $('.inline_popup').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });
    $('.text-popup').magnificPopup({
        type:'inline',
        midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
    });

    window.sr = ScrollReveal();
    var revealDown = {
        delay    : 300,
        origin   : 'top',
        distance : '100px',
        rotate   : { z : 10 },
        easing   : 'ease-in-out',
        scale    : 1
    };
    sr.reveal('.reveal-down', revealDown, 50);

    var revealUpx2 = {
        delay    : 50,
        origin   : 'bottom',
        distance : '50px',
        easing   : 'ease-in-out',
        scale    : 1
    };
    sr.reveal('.reveal-up2x', revealUpx2, 50);

    var revealUp = {
        delay    : 300,
        origin   : 'bottom',
        distance : '100px',
        easing   : 'ease-in-out',
        scale    : 1
    };
    sr.reveal('.reveal-up', revealUp, 50);

    var revealRight = {
        delay    : 200,
        origin   : 'right',
        distance : '100px',
        rotate   : { z : 10 },
        easing   : 'ease-in-out',
        scale    : 1
    };
    sr.reveal('.reveal-right', revealRight, 100);

    var revealLeft = {
        delay    : 300,
        origin   : 'left',
        distance : '0px',
        easing   : 'ease-in-out',
        rotate   : { z : -60 },
        scale    : 1
    };
    sr.reveal('.reveal-left', revealLeft);
    var revealLeft2 = {
        delay    : 600,
        origin   : 'left',
        distance : '0px',
        easing   : 'ease-in-out',
        scale    : 1
    };
    sr.reveal('.reveal-left2', revealLeft2);


    $("select.fancyselect").selectBoxIt();

    /* responsive table stuff */
    $( "table th:first-child").each(function() {
        var parent = $( this ).parent().parent().parent();
        var parentTag = parent.get( 0 ).tagName;

        if(parentTag.toUpperCase() == 'TABLE'){
            parent.addClass('rwd-table');
            count = parent.find('th').length;
            for(var i = 1; i<=count; i++){
                var column_title = parent.find( "tr th:nth-child("+i+")" ).html();
                parent.find( "tr td:nth-child("+i+")" ).attr('data-th',column_title);
            }
        }
    });

    $('.ajform').each(function() {
        $(this).validate({
            onkeyup: false
        });
        $(this).ajaxForm({
            type: 'post',
            success: showResponse
        });
    });
});

function addSpaces(initial){
    initial = initial.replace(/\D/g,'');
    initial = initial.replace(/(\d{3})(\d{3})(\d{4})/, "$1 $2 $3");
    return initial;
}

function checkFormValue(item){
    if($(item).val().length === 0 || $(item).val() == '') {
        $(item).removeClass('hasvalue');
    }else{
        $(item).addClass('hasvalue');
    }
}

// post-submit callback
function showResponse(responseText, statusText, xhr, jqForm)  {
    if(responseText != 'error') {
        jqForm.html(responseText);
        location.reload();
    }else{
        $('.formerror',jqForm).html('An error occured');
        $('input[type="submit"]',jqForm).removeAttr('disabled');
    }
}



function parallax(safari) {
    $(window).scroll(function () {
        parallax_all(safari);
    });
}

function parallax_all(safari){
    var top = $(window).scrollTop();

    parallax_banner(safari,top);
    parallax_gallery('.gallery-shift',top);
    //parallax_horz_shift('.background-shift',top);
    //parallax_vert_shift('.background-shift-vert',top);
}

function parallax_banner(safari,top){
    if ($('#parallax-banner').length) {
        $('#parallax-banner .banner-layer').each(function () {
            if(isScrolledIntoView($(this))) {
                var rate = $(this).attr('data-rate');
                var dir = $(this).attr('data-dir');

                if(safari){
                    //slow down safari to minimize jitters
                    rate = rate * .4;
                }
                var yPosChange = top * rate;

                if(dir == 'horizontal'){
                    //1-rate is to reverse the rate so that the further back items scale slower
                    //var scale = 1 + (top / headerHeight) * (1 - rate) / (4);
                    $(this).css({
                        "transform": "translate3d(" + yPosChange + "px,0px, 0px)"
                    });
                }else if(dir == 'both'){
                    //1-rate is to reverse the rate so that the further back items scale slower
                    //var scale = 1 + (top / headerHeight) * (1 - rate) / (4);
                    $(this).css({
                        "transform": "translate3d(" + yPosChange + "px," + yPosChange + "px, 0px)"
                    });
                }else{
                    //1-rate is to reverse the rate so that the further back items scale slower
                    //var scale = 1 + (top / headerHeight) * (1 - rate) / (4);
                    $(this).css({
                        "transform": "translate3d(0px," + yPosChange + "px, 0px)"
                    });
                }
            }
        });
    }
}
function parallax_gallery(c, top){
    if ($(c).length) {
        $(c).each(function () {
            if(isScrolledIntoView($(this))) {
                rate = -.1;
                //start at +60
                var elemTop = $(this).offset().top;

                var PosChange = (top * rate) - (elemTop * rate) - 60;
                //1-rate is to reverse the rate so that the further back items scale slower
                //var scale = 1 + (top / headerHeight) * (1 - rate) / (4);
                $(this).css({
                    "transform": "translate3d(0px," + PosChange + "px, 0px)"
                });
            }
        });
    }
}
function parallax_horz_shift(c, top){
    if ($(c).length) {
        $(c).each(function () {
            if(isScrolledIntoView($(this))) {
                rate = .4;
                var PosChange = top * rate;

                $(this).css("background-position", PosChange + "px 0");
            }
        });
    }
}
function parallax_vert_shift(c, top){
    if ($(c).length) {
        $(c).each(function () {
            if(isScrolledIntoView($(this))) {
                rate = .2;

                var PosChange = top * rate -300;

                $(this).css("background-position", "center bottom " + PosChange + "px");
            }
        });
    }
}


function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top - 60;
    var elemBottom = elemTop + $(elem).height() + 60;

    //console.log(elemBottom +' >= '+ docViewTop +' || '+ elemTop +' <= '+ docViewBottom);
    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

function parallaxInit() {

    var platform = navigator.platform.toLowerCase();
    var isiPad = navigator.userAgent.match(/iPad/i) != null;
    var isiPhone = navigator.userAgent.match(/iPhone/i) != null;
    var isAndroid = navigator.userAgent.match("/Android/i") != null;

    var ismsie = false;

    var ua = window.navigator.userAgent;

    var ismsie = ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0;
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

    if (isiPad || isiPhone || isAndroid) {
        $("#parallax-banner .banner-layer").hide();
        $("#parallax-banner #ground-layer").hide();
        return;
    }
    if ((platform.indexOf('win32') != -1 || platform.indexOf('linux') != -1)
        && (isSafari || ismsie)) {
        smoothscroll();
    }


    parallax_all(isSafari);

    parallax(isSafari);
}

function smoothscroll(){
    $.srSmoothscroll({
        step: 40,
        speed: 300,
        target: $('body'),
        container: $(window)
    });
}

function sticky_nav(){
    var distanceY = $(document).scrollTop();
    var headerwrap = $("#headerwrap");
    var header = $("header");
    var headertop = headerwrap.offset().top + 5;

    if (headertop <= distanceY) {
        header.addClass("sticky");
    } else {
        header.removeClass("sticky");
    }
}


function change_product_price(el){
    var qty = $('[name="qty"]', el).val();
    var id = $('[name="id"]', el).val();
    var price_contain = $('.price', el);

    price_contain.html('<i class="fa fa-cog fa-spin"></i>');

    $.ajax({
        type: 'POST',
        url: "/a_ajax.php?_=" + jQuery.now(),
        cache : false,
        data: {
            'a': 'get_price',
            'qty': qty,
            'id': id
        }
    }).done(function (content) {
        price_contain.html(content);
    });
}



function checkformore(i){
    if($(i).attr('show')){
        showon = $(i).attr('show');
    }else{
        showon = 'Yes';
    }

    if($(i).attr('hide')){
        hide = $(i).attr('hide');
    }else{
        hide = false;
    }
    if($(i).attr('alt')){
        alt = $(i).attr('alt');
    }else{
        alt = false;
    }

    if($(i).val() == showon || (alt && $(i).val() == alt)){
        $('#'+$(i).attr('rel')).show(400);
        if($('#'+$(i).attr('rel')).attr('rel') == 'req'){
            $('#'+$(i).attr('rel')+' textarea:not(.neverreq)').prop('required',true);
            $('#'+$(i).attr('rel')+' select:not(.neverreq)').prop('required',true);
            $('#'+$(i).attr('rel')+' input:not(.neverreq)').prop('required',true);
        }
//            if we a hide attribute is set hide it
        if(hide){
            $('#'+hide).hide();
            $('#'+hide+' textarea:not(.neverreq)').prop('required',false);
            $('#'+hide+' select:not(.neverreq)').prop('required',false);
            $('#'+hide+' input:not(.neverreq)').prop('required',false);
        }
    }else if($(i).val() == ''){
        //if blank hide everything
        $('#'+$(i).attr('rel')).hide();
        $('#'+$(i).attr('rel')+' textarea:not(.neverreq)').prop('required',false);
        $('#'+$(i).attr('rel')+' select:not(.neverreq)').prop('required',false);
        $('#'+$(i).attr('rel')+' input:not(.neverreq)').prop('required',false);
        if(hide){
            $('#'+hide).hide();
            $('#'+hide+' textarea:not(.neverreq)').prop('required',false);
            $('#'+hide+' select:not(.neverreq)').prop('required',false);
            $('#'+hide+' input:not(.neverreq)').prop('required',false);
        }
    }else{
        $('#'+$(i).attr('rel')).hide();
        $('#'+$(i).attr('rel')+' textarea:not(.neverreq)').prop('required',false);
        $('#'+$(i).attr('rel')+' select:not(.neverreq)').prop('required',false);
        $('#'+$(i).attr('rel')+' input:not(.neverreq)').prop('required',false);
//            if we a hide attribute is set show it
        if(hide){
            $('#'+hide).show(400);
            if($('#'+hide).attr('rel') == 'req'){
                $('#'+hide+' textarea:not(.neverreq)').prop('required',true);
                $('#'+hide+' select:not(.neverreq)').prop('required',true);
                $('#'+hide+' input:not(.neverreq)').prop('required',true);
            }
        }
    }
}

$(document).ready(function () {
    window.addEventListener('scroll', sticky_nav);
    sticky_nav();
    parallaxInit();
});