/*
  This will create a responsive nave that is hidden/shown by pressing a button with the "toggleMenu" class
*/

$(document).ready(function(){
	$(".toggleMenu").click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$("nav").toggle();
	});
	adjustMenu();
});
				

var ww = $(window).width();
$(window).bind('resize orientationchange', function() {
	ww = $(window).width();
	adjustMenu();
});

var adjustMenu = function() {
	if (ww < 600) {
		$(".toggleMenu").css("display", "inline-block");
		if (!$(".toggleMenu").hasClass("active")) {
			$("nav").hide();
		} else {
			$("nav").show();
		}
		$("nav ul li").unbind('mouseenter mouseleave');
		$("nav ul li a.parent").unbind('click').bind('click', function(e) {
			// must be attached to anchor element to prevent bubbling
			//var html = $(this).append($('#top').clone()).remove().html();
			$('.newnav').remove();
			$(this).parent("li").find('ul').prepend('<li class="newnav"><a href="'+$(this).attr('href')+'">'+$(this).clone().html()+'</a></li>');
			e.preventDefault();
			$(this).parent("li").toggleClass("hover");
		});
	} 
	else if (ww >= 600) {
		$('.newnav').remove();
		$(".toggleMenu").css("display", "none");
		$("nav").show();
		$("nav ul li").removeClass("hover");
		$("nav ul li a").unbind('click');
		$("nav ul li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
			// must be attached to li so that mouseleave is not triggered when hover over submenu
			$(this).toggleClass('hover');
		});
	}
}