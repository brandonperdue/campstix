<?
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

require 'common/config.php';
include 'backroom/common/mod_upload_w.php';

if (!isset($_GET['a'])) {
    exit("No action specified.");
}

switch (trim($_GET['a'],'/')) {
    case 'tentorrv':
        $v = sql_fetch_by_key($db, 'volunteers', 'id', $_POST['vid']);
        $u = sql_fetch_by_key($db, 'users', 'id', $v['uid']);
        $fields = array(
            'vid',
            'lodging',
            'lodging_other',
            'owner',
            'owner_other',
            'stayingwith',
            'rvhookups',
            'rvhookups_other'
        );

        $id = sql_upsert($db, 'tentorrv', $fields, $_POST, 'id',$_POST['id']);

        $email_body =
            "Lodging Type: ".$_POST['lodging']."<br>".
            ($_POST['lodging_other']?"Other: ".$_POST['lodging_other']."<br>":'').
            "Lodging Owner: ".$_POST['owner']."<br>".
            ($_POST['owner_other']?"Other: ".$_POST['owner_other']."<br>":'').
            "Staying With: ".$_POST['stayingwith']."<br>".
            "Hookups Needed: ".$_POST['rvhookups']."<br>".
            ($_POST['rvhookups_other']?"Other: ".$_POST['rvhookups_other']."<br>":'')
        ;


        if($_POST['id']){
            $subject = "Camp Stix - Volunteer Tent or RV Info Update";
            $email_body = "The Tent or RV Info for ".$c['firstname']." ".$c['lastname']." has been updated.<br><br>".$email_body;
        }else{
            $subject = "Camp Stix - Volunteer Tent or RV Info";
            $email_body = "Tent or RV Info has been submitted on behalf of ".$c['firstname']." ".$c['lastname']."<br><br>".$email_body;
        }
//        $config['email_contact'] = "brandon@designspike.com";
        smtp_mail($u['user_email'], 'campstix@campstix.org', $subject, formattedemail($email_body));

        m('Your request has been submitted.');
        hle('/account');

        break;
    case 'cabinmate':
        $c = sql_fetch_by_key($db, 'campers', 'id', $_POST['cid']);
        $u = sql_fetch_by_key($db, 'users', 'id', $c['uid']);

        $fields = array(
            'cid',
            'firstname1',
            'lastname1',
            'age1',
            'gender1',
            'firstname2',
            'lastname2',
            'age2',
            'gender2'
        );



        $id = sql_upsert($db, 'cabinmate', $fields, $_POST, 'id',$_POST['id']);

        $email_body =
            "<b>First Cabin-Mate</b><br>".
            "Name: ".$_POST['firstname1']." ".$_POST['lastname1']."<br>".
            "Age: ".$_POST['age1']."<br>".
            "Gender: ".$_POST['gender1']."<br><br>".
            "<b>Second Cabin-Mate</b><br>".
            "Name: ".$_POST['firstname2']." ".$_POST['lastname2']."<br>".
            "Age: ".$_POST['age2']."<br>".
            "Gender: ".$_POST['gender2']."<br><br>"
        ;


        if($_POST['id']){
            $subject = "Camp Stix - Cabin-Mate Request Update";
            $email_body = "The Cabin-Mate request for ".$c['firstname']." ".$c['lastname']." has been updated.<br><br>".$email_body;
        }else{
            $subject = "Camp Stix - Cabin-Mate Request";
            $email_body = "A Cabin-Mate request has been made on behalf of ".$c['firstname']." ".$c['lastname']."<br><br>".$email_body;
        }

        smtp_mail($u['user_email'], 'campstix@campstix.org', $subject, formattedemail($email_body));

        m('Your request has been submitted.');
        hle('/account');

        break;
    case 'scholarship':
        $c = sql_fetch_by_key($db, 'campers', 'id', $_POST['cid']);
        $u = sql_fetch_by_key($db, 'users', 'id', $c['uid']);

        $fields = array(
            'cid',
            'request',
            'parent_name',
            'parent_email',
            'parent_address',
            'parent_city',
            'parent_state',
            'parent_zip',
            'how_many_adults',
            'how_many_children',
            'ages_of_children',
            'school_lunch',
            'school_lunch_other',
            'annual_income',
            'content'
        );



        $id = sql_upsert($db, 'scholarship', $fields, $_POST, 'id',$_POST['id']);

        $email_body =
            "<b>Parent or Guardian's Contact Information</b><br>".
            "Name: ".$_POST['parent_name']."<br><br>".
            "Email: ".$_POST['parent_email']."<br><br>".
            "Address: ".$_POST['parent_address']."<br><br>".
            "City: ".$_POST['parent_city']."<br><br>".
            "State: ".$_POST['parent_state']."<br><br>".
            "Zip: ".$_POST['parent_zip']."<br><br>".
            "<b>Household Information</b><br>".
            "How Many Adults Are In Your Family: ".$_POST['how_many_adults']."<br><br>".
            "How Many Children Are In Your Family: ".$_POST['how_many_children']."<br><br>".
            "What are the ages of the children in your family?: ".$_POST['ages_of_children']."<br><br>".
            "<b>Household Information</b><br>".
            "School Lunch Eligibility: ".$_POST['school_lunch']."<br><br>".
            ($_POST['school_lunch_other']?"If Other: ".$_POST['school_lunch_other']."<br><br>":"").
            "What is your Annual Household Income: $".$_POST['annual_income']."<br><br>".
            "Requested Scholarship Amount: $".$_POST['request']."<br><br>".
            "<b>Additional Comments</b><br>".$_POST['content']."<br><br>"
        ;


        if($_POST['id']){
            $subject = "Camp Stix - Scholarship Request Update";
            $email_body = "The scholarship request for ".$c['firstname']." ".$c['lastname']." has been updated.<br><br>".$email_body;
        }else{
            $subject = "Camp Stix - Scholarship Request";
            $email_body = "A scholarship request has been made on behalf of ".$c['firstname']." ".$c['lastname']."<br><br>".$email_body;
        }
        smtp_mail($u['user_email'], $config['email_contact'], $subject, formattedemail($email_body));


        m('Your request has been submitted.');
        hle('/account');

        break;
    case 'funding':

        $c = sql_fetch_by_key($db, 'campers', 'id', $_POST['cid']);
//        $u = sql_fetch_by_key($db, 'users', 'id', $c['uid']);


        $fields = array(
            'cid',
            'goal',
            'content'
        );

        if(!$_POST['id']){

            $keyword = get_funding_keyword(randstring(20));
            $_POST['keyword'] = $keyword;

            $fields[] = 'keyword';
        }

        $basedir = 'upload/funding';
        // resize image if provided
        $fieldname = 'image';
        if ($_FILES[$fieldname]['name']) {
            list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
                array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
                array('width' => 400, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/400"),
                array('width' => 800, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/800"),
                array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
            ));
            $fields[] = $fieldname;
            $_POST[$fieldname] = $image;
        }


        $id = sql_upsert($db, 'funding', $fields, $_POST, 'id',$_POST['id']);

        m('Your funding page has been saved.');
        hle('/account');

        break;
    case 'passwordreset':
        $returnurl = $_SERVER['HTTP_REFERER'];
        $table_name = 'users';
        $fields = array('user_pass', 'resetcode', 'resettime');

        $query = $db->prepare("SELECT * FROM users WHERE LOWER(user_email)=?");
        $query->execute(array(strtolower($_POST['user_email'])));
        $rr = $query->fetch();

        if($rr['resetcode'] != $_POST['code'] || strlen($_POST['code'])<=6){
            e("That Code is invalid".$rr['resetcode']);
        }elseif((time() > strtotime($rr['resettime'])) || $rr['resettime']=='0000-00-00 00:00:00'){
            e("Your Password reset has expired");
        }elseif($_POST['password']!=$_POST['password2']){
            e("Passwords must match");
        }elseif(strlen($_POST['password'])<=6){
            e("Passwords must be at least 6 characters long");
        }else{
//            $_POST['password'] = md5($_POST['password']);

            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            $_POST['user_pass'] = $hasher->HashPassword($_POST['password']);

            $_POST['resetcode'] = "";
            $_POST['resettime'] = '0000-00-00 00:00:00';
            $raw_values = $_POST;
            // save event record
            $values = sql_organize_values($fields, $raw_values);
            $id = sql_upsert($db, 'users', $fields, $values, 'resetcode',$_POST['code']);
            $id = $rr['id'];
            if ($id) {
                $_SESSION['log']['logged'] = 1 ;
                $_SESSION['log']['username'] = $_POST['email'] ;
                $_SESSION['log']['id'] = $id;
                m("Your Account Has Been Updated.");
                $r = sql_fetch_by_key($db, 'pages', 'id', 442);
                $returnurl = '/account';
            } else {
                e("Problem saving account");
            }
        }

        hle($returnurl);
        break;
    case 'forgot_pass':

        $query = $db->prepare("SELECT * FROM users WHERE LOWER(user_email)=?");
        $query->execute(array(strtolower($_POST['user_email'])));
        $rr = $query->fetch();
        if( ! $rr ){
            $error = "That username doesn't exist";
            e("That email address does not have an account");
        }else{//the username was found in the DB
            $subject = "Camp STIX - Password Retrieval";
            $code = randstring(20);
            $codeaddress = $config['site_address'].'/?view=passwordreset&code='.$code;

            $fields = array('resetcode', 'resettime');
            $raw_values = array('resetcode' => $code, 'resettime' => $date = date('Y-m-d H:i:s', strtotime('+3 hour')));
            $values = sql_organize_values($fields, $raw_values);
            sql_upsert($db, 'users', $fields, $values, 'id', $rr['id']);

            $email_body = 'Please go to <a href="'.$codeaddress.'">'.$codeaddress.'</a> to update your password.  This link will expire in 1 hour';
            smtp_mail($config['email_sender'], $_POST['user_email'], $subject, formattedemail($email_body));
            m("Password retrieval instructions have been sent to ".$_POST['user_email'].".  These instructions will expire in 1 hour.");
            m("Please check your Spam/Junk box filter if message is not found in your inbox after 5 minutes.");
        }//end else
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'login':
        $query = $db->prepare("SELECT * FROM users WHERE LOWER(user_email)=?");
        $query->execute(array(strtolower($_POST['user_email'])));
        $rr = $query->fetch();
        if( ! $rr ) {
            $error = "That username doesn't exist";
            e($error);
        }elseif(!$rr['user_status']){
            $error = 'That user account is pending approval.  Please contact us for more information.';
            e($error);
        }elseif($rr['user_status']<0){
            $error = 'That user account has been denied access.  Please contact us for more information.';
            e($error);
        }else{//the username was found in the DB
            // check password hash
            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            if (!$hasher->CheckPassword($_POST['password'], $rr['user_pass'])) {
                $error = "Incorrect password";
                e($error);
            }else{
                $_SESSION['log']['logged'] = 1 ;
                $_SESSION['log']['name'] = $rr['display_name'] ;
                $_SESSION['log']['email'] = $rr['user_email'] ;
                $_SESSION['log']['id'] = $rr['id'] ;
                m("Login Successful.");
            }
        }//end else
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'login_as':
        if($_SESSION['admin']['id']) {
            $query = $db->prepare("SELECT * FROM users WHERE id=?");
            $query->execute(array(strtolower($_GET['uid'])));
            $rr = $query->fetch();
            if (!$rr) {
                $error = "That user doesn't exist";
                e($error);
            } else {//the user was found in the DB
                $_SESSION['log']['logged'] = 1;
                $_SESSION['log']['name'] = $rr['display_name'];
                $_SESSION['log']['email'] = $rr['user_email'];
                $_SESSION['log']['id'] = $rr['id'];
                m("Login Successful.");
            }//end else
        }else{
            e('You must be an admin to do that.');
        }
        hle("/account");
        break;
    case 'logout':
        $_SESSION['log'] = '';
        unset($_SESSION['log']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'register':
        $table_name = 'users';
        $fields = array('user_email', 'display_name');

        $success_message = "Your account Has Been Added.";
        $_POST['user_status'] = 1;
        $fields = array_merge($fields,array('user_status'));

        if($_POST['password_new']){
            $_POST['password'] = $_POST['password_new'];
        }

        if($_SESSION['log']['id'] && $_POST['password']==''){ #if user is logged in this is an edit and password may be blank(not changed)
            $pwflag = false;
        }else{
            $fields[]='user_pass';
            $pwflag = true;
        }

        $query = $db->prepare("SELECT * FROM users WHERE LOWER(user_email)=?");
        $query->execute(array(strtolower($_POST['user_email'])));
        $rr = $query->fetch();

        if($rr && strtolower($_POST['user_email'])!=strtolower($_SESSION['log']['email'])){
            e("There is already an account for that email address");
        }elseif($_POST['password']!=$_POST['password2'] && $pwflag){
            e("Passwords must match");
        }elseif(strlen($_POST['password'])<=6 && $pwflag){
            e("Passwords must be at least 6 characters long");
        }elseif(strpos($_POST['user_email'],'@') === false){
            e("A valid email address is required");
        }else{
            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            $_POST['user_pass'] = $hasher->HashPassword($_POST['password']);

            $raw_values = $_POST;
            // save event record
            $values = sql_organize_values($fields, $raw_values);
            $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_SESSION['log']['id']);

            if ($id) {
                if($_SESSION['log']['id'] == $id) {
                    $_SESSION['log']['name'] = $_POST['display_name'];
                    $_SESSION['log']['email'] = $_POST['user_email'];
                    m("Your Account Has Been Saved.");
                }else{
                    m($success_message);
                    //login individual/business accounts
                    if($_POST['account_type'] == 1) {
                        $_SESSION['log']['logged'] = 1 ;
                        $_SESSION['log']['name'] = $_POST['display_name'] ;
                        $_SESSION['log']['email'] = $_POST['user_email'] ;
                        $_SESSION['log']['id'] = $id;
                    }
                }
            } else {
                e("Problem saving account");
            }
        }

        hle($_SERVER['HTTP_REFERER']);
        break;
}