<?
include 'common/config.php';
switch ($_REQUEST['a']) {
    case 'get_price' :
        $options = '';

        $price = get_product_price($db, $_POST['id'],$options);
        $originalprice = get_product_price($db, $_POST['id'],$options, false);
        if($price == $originalprice){
            echo '$<span itemprop="price">'.number_format(round($price*$_POST['qty'],0),0).'</span>';
        }else{
            echo '<div class="nonsaleprice">Originally: $<span itemprop="price">'.number_format(round($originalprice*$_POST['qty'],0),0).'</span></div>';
            echo 'Sale: $<span itemprop="price">'.number_format(round($price*$_POST['qty'],0),0).'</span>';
        }

        break;
    case 'get_weight' :
        $options = '';
        parse_str($_POST['options'],$options);
        echo '<span class="note">weight:</span> '.ceil(get_product_weight($db, $_POST['id'],$options)*$_POST['qty']).'lbs';
        break;
    case 'get_manufacturers' :
        if($_POST['cat']){

            $query = $db->prepare("SELECT id FROM product_categories WHERE id_parent = ? ORDER BY priority");
            $query->execute(array($_POST['cat']));
            $subcats = $query->fetchall();
            if(!is_numeric($_POST['cat'])){
                $_POST['cat'] = 0;
            }
            if($subcats){
                foreach($subcats as $subcat){
                    $_POST['cat'] = $_POST['cat'].','.$subcat['id'];
                }
            }
            $query = $db->prepare("SELECT DISTINCT id_manufacturer FROM products WHERE display AND id_categories IN(".$_POST['cat'].")");
            $query->execute();
            $pp = $query->fetchall();


        }else{
            $query = $db->prepare("SELECT DISTINCT id_manufacturer FROM products WHERE display");
            $query->execute();
            $pp = $query->fetchall();
        }


        if($pp) {
            $manufacturer = '';
            foreach($pp as $p){
                $manufacturer .= ','.$p['id_manufacturer'];
            }
            $manufacturer = ltrim($manufacturer,',');


            $query = $db->prepare("SELECT * FROM manufacturer WHERE id IN(".$manufacturer.") ORDER BY title ASC");
            $query->execute();
            $mm = $query->fetchall();

            ob_start();
            if($mm) {
                ?>
                <b>Manufacturer</b>
                <div class="form-select">
                    <select name="manufacturer" class="fancyselect" id="">
                        <option value="">All Manufacturers</option>
                        <? foreach ($mm as $m) { ?>
                            <option
                                value="<?= $m['id'] ?>" <? if ($_POST['currentmanu'] == $m['id']) echo 'selected="selected"'; ?>><?= $m['title'] ?></option>
                        <? } ?>
                    </select>
                </div>
                <?
            }else{
                echo 'test<Br>';
            }
        }
        echo ob_get_clean();

        break;
    case 'add_rating' :
        $u = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']);
        $d = sql_fetch_by_key($db, 'directory', 'id', $_POST['directory_id']);

        $query = $db->prepare("SELECT * FROM review WHERE user_id = ? AND directory_id = ?");
        $query->execute(array($u['id'],$d['id']));
        $rr = $query->fetch();
        if($rr && $rr['id']!=$_POST['id'] && !$_POST['parent']){
            echo 'You can only leave one review per directory listing.';
        }else{
            $fields = array('name','title','content','parent','directory_id','product_id','rate','user_id');
            $values = sql_organize_values($fields, $_POST);
            $id = sql_upsert($db, 'review', $fields, $values, 'id', $_POST['id']);
            if($id){
                echo 'Your response has been submitted';
                if($config['email_dir_users']) {
                    $dir_body = 'You have just received a review on your Espresso Service Network profile. <a href="/directory/'.$d['keyword'].'/">Click here to read.</a>';
                    smtp_mail($config['email_sender'], $d['email'], "You've Received a Review - ESN", formattedemail($dir_body));
                }
                $subject = 'Verify your review on ESN';
                $codeaddress = $config['site_address'].'/?view=verifyreview&code='.$id.'-'.$_SESSION['log']['id'];
                $email_body = 'Thank you for taking the time to review your espresso service provider. Your comments and suggestions are an integral part of improving espresso services here in the United States.
                    <br><br>
                    Will you now take the time to verify your review? Although it is not necessary for you to do so, a verified review lets consumers know that your assessment is based on an actual experience or encounter with the service company.
                    <br><br>
                    Any information you provide to verify your review will not be shared.
                    <br><br>
                    Please go to <a href="'.$codeaddress.'">'.$codeaddress.'</a> to verify your review.';
                smtp_mail($config['email_sender'], $u['user_email'], $subject, formattedemail($email_body));
                if($_POST['parent'] && $_POST['notify-customer']){
                    $parent = sql_fetch_by_key($db, 'review', 'id', $_POST['parent']);
                    $customer = sql_fetch_by_key($db, 'users', 'id', $parent['user_id']);
                    $dir_body = 'You have just received a reply on your Espresso Service Network review of '.$d['title'].'. <a href="/directory/'.$d['keyword'].'/">Click here to read.</a>';
                    smtp_mail($config['email_sender'], $user['user_email'], "You've Received a Reply - ESN", formattedemail($dir_body));
                }
            }else{
                echo 'error';
            }
        }

        break;
    case 'report_review' :
        $fields = array('name','title','content','parent','directory_id','product_id','rate','report','user_id');
        $_POST['title'] = 'Review Report';
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, 'review', $fields, $values, 'id', $_POST['id']);

        if($id){
            echo 'Your report has been sent';
        }else{
            echo 'error';
        }

        break;
}

