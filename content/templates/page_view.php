<?
/* $page['header_extra'] = '<link rel="stylesheet" href="/lib/magnific-popup/magnific-popup.css"> 
<script src="/lib/magnific-popup/jquery.magnific-popup.js"></script>';
global $basedir_photos, $basedir_banners;
$page_vars['content'] = preg_replace_callback(
    '@\[gallery-(\d+)\]@',
    function($matches) use ($db, $basedir_photos, $settings) {
        return '<div class="gallery">'.gallery_display(
            $db,
            $basedir_photos,
            'galleries',
            $matches[1]
        ).'</div>';
    },
    $page_vars['content']
); */
?>

<div class="breadcrumbs">
</div>
<div class="container pad_section content">

    <?

    $subnav = cms_get_nav($db, key($page['nav_chain']), $page['nav_chain'], 3, true, 'current');
    ?>

    <div class="col-md-<?=$subnav?'8':'12' ?>">
    	<?
        	if($page_vars['image']){
				echo '<img src="/'.$basedir_banners.'/'.$page_vars['image'].'" alt="" />';
			}
		?>
        <h2><?= $page_vars['heading'] ?></h2>

        <? if (!$page['hide_breadcrumbs']) { ?>
            <div class="inner-breadcrumbs"><?= $page['breadcrumbs'] ?></div>
        <? } ?>


        <?= $page_vars['content'] ?><br />
        <?
		
        $query = $db->prepare("SELECT * FROM pages WHERE id_parent = ? ORDER BY priority") ;
        $query->execute(array($page['id'])) ;
        $nn = $query->fetchAll() ;
        if(count($nn)) {
            echo '<h4>Sections</h4><ul class="in-content-nav">';
            foreach ($nn as $n) {
                echo '<li><a href="/' . $n['keyword'] . '">' . $n['title'] . '</a></li>';
            }
            echo '</ul>';
        }
		
        ?>
    </div>
    	<?
        if($subnav) {
                ?>
    <div class="col-md-4">
                <div class="side_nav">
                    <?

                    $p = sql_fetch_by_key($db, 'pages', 'id', key($page['nav_chain']));
                    echo '<a href="/' . $p['keyword'] . '">' . $p['title'] . '</a>';


                    $nav_options = array(
                        'subnav_marker' => '<i class="fa fa-caret-down fa-fw"></i>',
                        'no_class' => 'true',
                    );
                    echo $twig->render('nav.html', array(
                        'options' => $nav_options,
                        'nav' => $subnav
                    ));
                    ?>
                </div><br/>
    </div>
            <?
            }
		?>
</div>