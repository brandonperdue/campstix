<?
$fields = array(
    'heading',
    'subheading',
    'content',
    'url',
    'label',
    'heading2',
    'subheading2',
    'content2',
    'url2',
    'label2',
    'heading3',
    'subheading3',
    'content3',
    'url3',
    'label3',
    'heading4',
    'subheading4',
    'content4',
    'url4',
    'label4',
    'heading5',
    'subheading5',
    'content5',
    'url5',
    'label5',

    'gallery',
) ;

$basedir = '../upload/page_images';
// resize image if provided
$fieldname = 'banner_image';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}

$fieldname = 'image2';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}

$fieldname = 'image5';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}


include '../templates/basic_template_save.php' ;

