<?
$fields = array(
    'banner_content',
    'intro_heading',
    'intro_subheading',
    'intro_content',
    'intro_url',
    'intro_label',
    'event_heading',
    'event_subheading',
    'reg_url',
    'reg_label',
    'donate_heading',
    'donate_subheading',
    'donate_url',
    'donate_label',
    'sponsor_heading',
    'sponsor_subheading',
    'sponsor_content',
    'testimonial_heading',
    'testimonial_content',
    'testimonial_sig'
) ;

$basedir = '../upload/page_images';
// resize image if provided
$fieldname = 'banner_image';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}
$fieldname = 'donate_image';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}
$fieldname = 'testimonial_image';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}


$basedir = '../upload/people';
$fieldname = 'testimonial_sig_image';
if($_POST[$fieldname.'_delete']){
    $fields[] = $fieldname;
    $_POST[$fieldname] = '';
}
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 110, 'height' => 110, 'function'=>'resizethumb', 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/100"),
        array('width' => 400, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/400"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}



include '../templates/basic_template_save.php' ;

