<table class="editor " id="type_normal">
	<tr>
		<td>
			<div class="title">Page Heading</div>
			<input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Page Content</div>
			<textarea name="content" class="rich_editor" style="width: 100% ; height: 500px ;"><?= $page_vars['content'] ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<div class="title">Path of PHP file to include (relative to site root)</div>
			<input type="text" name="file_path" value="<?= htmlspecialchars($page_vars['file_path']) ?>" style="width: 100% ;"/>
		</td>
	</tr>
</table>

