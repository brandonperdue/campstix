<div class="breadcrumbs">
</div>

<div class="container pad_section content">
	<?
		if(!$page['hide_sidenav']){
        	$subnav = cms_get_nav($db, key($page['nav_chain']), $page['nav_chain'], 3, true, 'current');
		}
        if($subnav){
    ?>
    		<div class="col-md-8">
	<? 
        }else{
    ?>
    		<div class="col-md-12">
    <?	} ?>
				<?
                if( is_file($page_vars['file_path']) ){
                    include($page_vars['file_path']) ;
                }else{
                    echo "File not found: {$page_vars['file_path']}" ;
                }
                ?>
                
            </div>
    <? if($subnav){ ?>
        <div class="col-md-4">
            <div class="side_nav">
                <?
                    
                    $p = sql_fetch_by_key($db, 'pages', 'id', key($page['nav_chain'])) ;
                    echo '<a href="/'.$p['keyword'].'">'.$p['title'].'</a>';
                    
        
                    $nav_options = array(
                        'subnav_marker' => '<i class="fa fa-caret-down fa-fw"></i>',
                        'no_class' => 'true',
                    );
                    echo $twig->render('nav.html', array(
                        'options' => $nav_options,
                        'nav' => $subnav
                    ));
                ?>
            </div>
        </div>
    <? } ?>
</div>