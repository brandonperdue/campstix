<?
$fields = array(
    'heading',
    'subheading',
    'content',
    'reglink'
) ;

$basedir = '../upload/page_images';
// resize image if provided
$fieldname = 'banner_image';
if ($_FILES[$fieldname]['name']) {
    list($image) = relocateImage($_FILES[$fieldname]['tmp_name'], array(
        array('width' => 100, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/thumb"),
        array('width' => 1000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/1000"),
        array('width' => 2000, 'name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/2000"),
        array('name' => $_FILES[$fieldname]['name'], 'dir' => "$basedir/orig"),
    ));
    $fields[] = $fieldname;
    $_POST[$fieldname] = $image;
}

$basedir_logos = '../upload/logos/';
// resize image if provided
if ($_FILES['logo']['name']) {
    list($warranty_file) = relocateFile($_FILES['logo']['tmp_name'], array(
        array('name' => $_FILES['logo']['name'], 'dir' => $basedir_logos),
    ));
    $fields[] = 'logo';
    $_POST['logo'] = $_FILES['logo']['name'];
} elseif ($_POST['logo_clear']) {
    $fields[] = 'logo';
    $_POST['logo'] = '';
}

include '../templates/basic_template_save.php' ;

