<?
global $basedir_photos, $basedir_banners;
$page_vars['content'] = preg_replace_callback(
    '@\[gallery-(\d+)\]@',
    function($matches) use ($db, $basedir_photos, $settings) {
        return '<div class="gallery">'.gallery_display(
            $db,
            $basedir_photos,
            'galleries',
            $matches[1]
        ).'</div>';
    },
    $page_vars['content']
);
?>

<div class="breadcrumbs">
	<? if (!$page['hide_breadcrumbs']) { ?>
        <div class="container"><div class="inner"><?= $page['breadcrumbs'] ?></div></div>
    <? } ?>
</div>

<div class="container pad_section content">
    <div class="col-md-8">
    	
    	<? 
			global $g_google_api_key;
			if($page_vars['address']){
		?>
    	<iframe
            width="100%"
            height="300"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/place?key=<?=$g_google_api_key ?>&q=<?=str_replace(array(PHP_EOL,' '), array('',"+"), $page_vars['address']) ?>">
        </iframe>
		<br />
        <? } ?>

        <h2><?= $page_vars['heading'] ?></h2>
        <?=$page_vars['address'] ?><br />
        <? if($page_vars['phone']) echo $page_vars['phone'].'<br />'; ?>
        <?= $page_vars['content'] ?><br />
    </div>
    <div class="col-md-4">
    	<?
			$subnav = cms_get_nav($db, key($page['nav_chain']), $page['nav_chain'], 3, true, 'current');
			if($subnav){
		?>
            <div class="side_nav">
                <?
                    
                    $p = sql_fetch_by_key($db, 'pages', 'id', key($page['nav_chain'])) ;
                    echo '<a href="/'.$p['keyword'].'">'.$p['title'].'</a>';
                    
        
                    $nav_options = array(
                        'subnav_marker' => '<i class="fa fa-caret-down fa-fw"></i>',
                        'no_class' => 'true',
                    );
                    echo $twig->render('nav.html', array(
                        'options' => $nav_options,
                        'nav' => $subnav
                    ));
                ?>
            </div><br />
        <? 
			}
		?>        
    	<div class="provider_search">
            <h5>Find a location near you.</h5>
            <form action="/providers" method="post" id="ziplookup">
                <input type="text" value="" placeholder="Zip Code" title="Zip Code" name="location_zip" id="location_zip" class="search_txt" />
                <input type="submit" value="&#xf002;" class="search_btn fa" />
            </form>
        </div>
		<?	
			if($page_vars['testimonial']){ 
		?>
            <div class="testimonial_white">
                <?=trim($page_vars['testimonial'],'"') ?>"
                <div class="text-right"><em>-<?=ltrim($page_vars['signature'],'-') ?></em></div>
            </div>
        <?
        	}
		
			global $basedir_callouts;
			foreach(array($page_vars['co_box1'],$page_vars['co_box2'],$page_vars['co_box3']) as $co_id){
				if($co_id){
				$co = sql_fetch_by_key($db, 'callouts', "id", $co_id) ;
		?>
                <div class="callout_box">
                    <? if($co['image']){ ?>
                    <img src="/<?= $basedir_callouts.'/'.$co['image'] ?>" alt="" />
                    <? } ?>
                    <div class="callout_txt">
                        <h5><? if($co['link']){ echo '<a href="'.$co['link'].'">'.$co['title'].'</a>'; }else{ echo $co['title']; } ?></h5>
                        <p><?=$co['subtitle'] ?></p>
                    </div>
                </div>
        <? 
				}
			}
		?>
    </div>
</div>