<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
$basedir = '../upload/page_images';
?>

<div class="row">
    <div class="col-md-6">
        <h2>Banner</h2>
        <div class="formfield">
            <?= cms_photo_selector(
                'Banner Image (2000 pixels wide)',
                'banner_image',
                $page_vars['banner_image'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>
        <h2>Content</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading" value="<?= $page_vars['subheading'] ?>" style="width: 100% ;"/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Content Alignment</b> <span class="note">All of the main content will be centered by default.  Left Alignment is useful for long simple pages like Privacy Policy or FAQ pages.</span><br/>
            <select name="alignment">
                <option value="">Center</option>
                <option value="left" <? if($page_vars['alignment']=="left"){ ?>selected="selected"<? } ?>>Left</option>
            </select>
        </div>
    </div>
</div>
<hr>
<h2>Sub Sections</h2>
<p>These are separate content sections that are listed below the main content</p>
<a href="#" id="addsubsection" class="btn btn-primary"><i class="fa fa-plus"></i> Add Sub Section</a>
<div id="subsections">
</div>
<script>
    var count = 1;
    $(document).ready(function () {
        $("#addsubsection").click(function (e) {
            e.preventDefault();
            getSection(0);
        })

        <?php
        $query = $db->prepare("SELECT * FROM pages_sections WHERE id_pages = ? ORDER BY priority ASC");
        $query->execute(array($_GET['id']));
        $rr = $query->fetchAll();
        foreach($rr as $r){
        ?>
        setTimeout(getSection(<?=$r['id'] ?>), <?=100*$i ?>);
    <?
        }
        ?>
    });

    function getSection(id) {
        $.ajax({
            type: 'POST',
            url: "/backroom/a_ajax.php?_=" + jQuery.now(),
            cache: false,
            data: {
                'a': 'edit_subsection',
                'count': count,
                'id': id
            }
        }).done(function (content) {
            $("#subsections").append('<div class="subsection" id="ss_' + count + '">' + content + '</div>');
            $('#ss_' + count + ' .rich_editor').each(function (index, element) {
                initialCKeditor(index, element);
            });
            count++;
        });
    }
</script>