<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);

$showform = true;

if($_SESSION['log']['id']){
    $uid = $_SESSION['log']['id'];
}else{
    $showform = false;
    $page['page_vars']['content'] .= '<p>To edit your account you need to <a href="/account">login</a> or <a href="/account?a=register">register</a></p>';
}

if($showform){

    //$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;
    $user = sql_fetch_by_key($db, 'users', 'id', $uid) ;

    $content = $twigpanel->render('edit-account.twig', array(
        'f' => $user,
        'session' => $_SESSION,
        'user' => $_SESSION['log'],
        'post' => $_POST,
        'config' => $config
    ));

    $page['page_vars']['content'] .= $content;

}