<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);

$showform = true;


if(!$_GET['c']){
    e('A camper must be selected');
    hle('/account');
}

if($_SESSION['log']['id']){
    $uid = $_SESSION['log']['id'];
}else{
    $showform = false;
    $page['page_vars']['content'] .= '<p>To edit a scholarship request you need to <a href="/account">login</a> or <a href="/account?a=register">register</a></p>';
}
unset($_SESSION['scholarship']);

if($showform){

    $user = sql_fetch_by_key($db, 'users', 'id', $uid);
    $camper = sql_fetch_by_key($db, 'campers', 'id', $_GET['c']);
    $scholarship = sql_fetch_by_key($db, 'scholarship', 'cid', $_GET['c']);

    $content = $twigpanel->render('scholarship.twig', array(
        'session' => $_SESSION,
        'c' => $camper,
        'u' => $user,
        's' => $scholarship,
        'post' => $_POST,
        'config' => $config
    ));

    $page['page_vars']['content'] .= $content;

}