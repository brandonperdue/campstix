<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);

$showform = true;


if(!$_GET['v']){
    e('A volunteer must be selected');
    hle('/account');
}

if($_SESSION['log']['id']){
    $uid = $_SESSION['log']['id'];
}else{
    $showform = false;
    $page['page_vars']['content'] .= '<p>To submit Tent or RV info you need to <a href="/account">login</a> or <a href="/account?a=register">register</a></p>';
}

if($showform){

    $user = sql_fetch_by_key($db, 'users', 'id', $uid);
    $volunteer = sql_fetch_by_key($db, 'volunteers', 'id', $_GET['v']);
    $tentorrv = sql_fetch_by_key($db, 'tentorrv', 'vid', $_GET['v']);

    $content = $twigpanel->render('tentorrv.twig', array(
        'session' => $_SESSION,
        'v' => $volunteer,
        'u' => $user,
        'tr' => $tentorrv,
        'post' => $_POST,
        'config' => $config
    ));

    $page['page_vars']['content'] .= $content;

}