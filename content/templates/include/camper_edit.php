<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);


$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;

$query = $db->prepare("SELECT * FROM campers WHERE id = ?") ;
$query->execute(array($_GET['c']));
$camper = $query->fetch() ;

$p = sql_fetch_by_key($db, 'products', 'id', $camper['pid']);
$camper['p'] = $p;

$content = $twigpanel->render('camper_edit.twig', array(
    'post' => $_POST,
    'camper' => $camper,
    'config' => $config
));

$page['page_vars']['content'] .= $content;