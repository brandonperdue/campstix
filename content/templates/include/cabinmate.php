<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);

$showform = true;


if(!$_GET['c']){
    e('A camper must be selected');
    hle('/account');
}

if($_SESSION['log']['id']){
    $uid = $_SESSION['log']['id'];
}else{
    $showform = false;
    $page['page_vars']['content'] .= '<p>To request a cabin-mate you need to <a href="/account">login</a> or <a href="/account?a=register">register</a></p>';
}

unset($_SESSION['cabin-mate']);

if($showform){

    $user = sql_fetch_by_key($db, 'users', 'id', $uid);
    $camper = sql_fetch_by_key($db, 'campers', 'id', $_GET['c']);
    $cabinmate = sql_fetch_by_key($db, 'cabinmate', 'cid', $_GET['c']);

    $content = $twigpanel->render('cabinmate.twig', array(
        'session' => $_SESSION,
        'c' => $camper,
        'u' => $user,
        'cm' => $cabinmate,
        'post' => $_POST,
        'config' => $config
    ));

    $page['page_vars']['content'] .= $content;

}