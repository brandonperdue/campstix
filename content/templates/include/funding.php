<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);

$showform = true;


if(!$_GET['c']){
    e('A camper must be selected');
    hle('/account');
}

if($_SESSION['log']['id']){
    $uid = $_SESSION['log']['id'];
}else{
    $showform = false;
    $page['page_vars']['content'] .= '<p>To edit a funding page you need to <a href="/account">login</a> or <a href="/account?a=register">register</a></p>';
}

if($showform){

    $user = sql_fetch_by_key($db, 'users', 'id', $uid);
    $camper = sql_fetch_by_key($db, 'campers', 'id', $_GET['c']);
    $product = sql_fetch_by_key($db, 'products', 'id', $camper['pid']);
    $scholarship = sql_fetch_by_key($db, 'scholarship', 'cid', $_GET['c']);
    $funding = sql_fetch_by_key($db, 'funding', 'cid', $_GET['c']);

    $basedir = '/upload/funding';
    $imagefield = cms_photo_selector(
        'Image of Camper',
        'image',
        $funding['image'],
        $basedir . '/thumb/',
        $basedir . '/800/'
    );


    $content = $twigpanel->render('funding.twig', array(
        'session' => $_SESSION,
        'c' => $camper,
        'u' => $user,
        's' => $scholarship,
        'p' => $product,
        'f' => $funding,
        'imagefield' => $imagefield,
        'post' => $_POST,
        'config' => $config
    ));

    $page['page_vars']['content'] .= $content;

}