<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);



$content = $twigpanel->render('donate.twig', array(
    'config' => $config,
    'error' => $error,
    'test_mode'   => $_SESSION['test'],
    'post' => $_POST
));

$page['page_vars']['subcontent']  = $content;

?>