<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

if($_SESSION['cabin-mate']){
    hle('cabin-mate-request-form?c='.$_SESSION['cabin-mate']);
}
if($_SESSION['scholarship']){
    hle('request-scholarship?c='.$_SESSION['scholarship']);
}

$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;

$query = $db->prepare("SELECT * FROM campers WHERE uid = ? AND status != 'Unpaid'") ;
$query->execute(array($_SESSION['log']['id']));
$campers = $query->fetchAll() ;

$query = $db->prepare("SELECT * FROM campers WHERE uid = ? AND status = 'Unpaid' && dateadded >= (CURDATE() + INTERVAL -7 DAY)") ;
$query->execute(array($_SESSION['log']['id']));
$partialcampers = $query->fetchAll() ;

$partial_w_product = array();
foreach($partialcampers as $c){
    $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
    $c['p'] = $p;
	$partial_w_product[] = $c;
}

$campers_w_product = array();
$openfunding = false;
foreach($campers as $c){
    $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
    $f = sql_fetch_by_key($db, 'funding', 'cid', $c['id']);
    $s = sql_fetch_by_key($db, 'scholarship', 'cid', $c['id']);
    $cm = sql_fetch_by_key($db, 'cabinmate', 'cid', $c['id']);
    $c['p'] = $p;
    $c['f'] = $f;
    if($p['startdate'] > date('Y-m-d') && $f){
    	$openfunding = true;
	}
    $c['s'] = $s;
    $c['cm'] = $cm;
    $campers_w_product[] = $c;
}
$query = $db->prepare("SELECT * FROM volunteers WHERE uid = ?") ;
$query->execute(array($_SESSION['log']['id']));
$volunteers = $query->fetchAll() ;
$volunteers_w_product = array();
foreach($volunteers as $v){
    $p = sql_fetch_by_key($db, 'products', 'id', $v['pid']);
    $tr = sql_fetch_by_key($db, 'tentorrv', 'vid', $v['id']);
    $v['p'] = $p;
    $v['tr'] = $tr;
    $volunteers_w_product[] = $v;
}

$content = $twigpanel->render('account.twig', array(
    'f' => $account,
    'session' => $_SESSION,
    'user' => $_SESSION['log'],
    'post' => $_POST,
    'campers' => $campers_w_product,
    'partialsignups' => $partial_w_product,
    'volunteers' => $volunteers_w_product,
    'config' => $config,
    'openfunding' => $openfunding
));

$page['page_vars']['content'] .= $content;