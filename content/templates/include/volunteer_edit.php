<?
$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../twig_cache'
]);

$account = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;

if($_GET['v']){
    $query = $db->prepare("SELECT * FROM volunteers WHERE id = ?") ;
    $query->execute(array($_GET['v']));
    $volunteer = $query->fetch() ;
    $product = sql_fetch_by_key($db, 'products', 'id', $volunteer['pid']);
    $title = ' - Edit';
    $volunteer['program_lead'] = explode("|",$volunteer['program_lead']);
}else{
    $product = sql_fetch_by_key($db, 'products', 'id', $_GET['c']);
    $title = '';
    $page['meta_title'] = "Volunteer Registration";
}
if(!$product['id']){
    e('You must select a camp to volunteer for before signing up.');
    hle('/volunteer-camp-selection/');
}

$content = $twigpanel->render('volunteer_edit.twig', array(
    'post' => $_POST,
    'title' => $title,
    'v' => $volunteer,
    'p' => $product,
    'config' => $config
));

if($volunteer['status'] ==  "Approved"){
    $content = 'Volunteer request has been approved.  Editing is disabled.';
}

$page['page_vars']['content'] .= $content;