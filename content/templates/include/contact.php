<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);


$error = '';
$show = true;
if(isset($_POST['submit'])){
    if( ! $_SESSION['captcha_solution']	or $_POST['captcha_answer'] != $_SESSION['captcha_solution']){
        $error = 'Your answer to the security question was incorrect.<br /><br />';
    }else{

        $to = $config['email_developer'];
        $to = $config['email_contact'];
        //$to = 'brandon@designspike.com';
        $subject = 'Contact Form - ESN';

        $message = '<strong>Your Name:</strong> '.$_POST['name'].'<br />';
        $message .= '<strong>Email Address:</strong> '.$_POST['email'].'<br />';
        $message .= '<strong>Phone Number:</strong> '.$_POST['phone'].'<br />';
        $message .= '<strong>Comments:</strong> <br />'.$_POST['comment'].'<br /><br />';

        smtp_mail($_POST['email'], $to, $subject, $message);
        $show = false;
    }
}



$a = rand(1,10) ;
$b = rand(1,10) ;
$_SESSION['captcha_solution'] = $a+$b ;

if($show) {
    $content = $twigpanel->render('contact.twig', array(
        'a' => $a,
        'b' => $b,
        'error' => $error,
        'post' => $_POST
    ));
}else{
    $content = '<div id="contact-form">
            <div class="row">
                <h2>Your message has been sent.<br />Thank You!</h2><br /><br /><br />
            </div>
    </div>';
}

$page['page_vars']['content']  .= $content;

?>