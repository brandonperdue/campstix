<?

$twigpanel = new Twig_Environment(new Twig_Loader_Filesystem($_SERVER["DOCUMENT_ROOT"] . '/templates/include'), [
    'debug' => true,
    'cache' => __DIR__ . '/../../../twig_cache'
]);


if($_GET['event']) {
    $query = $db->prepare("SELECT e.*, c.color AS color FROM events e, calendars c WHERE e.id = ? LIMIT 1");
    $query->execute(array($_GET['event']));
    $ee = $query->fetchAll();

    $events = array();
    foreach($ee as $e){
        if($e['startdate'] == '0000-00-00 00:00:00'){
            $e['startdate'] = $e['eventdate'];
        }

        $smonth = date('M',strtotime($e['startdate']));
        $emonth = date('M',strtotime($e['eventdate']));

        $sdays = date('j',strtotime($e['startdate']));
        $edays = date('j',strtotime($e['eventdate']));

        if($smonth == $emonth){
            $date = $emonth.'<br><span class="smalltext">'.($sdays==$edays?$edays:$sdays.'-'.$edays).'</span>';
        }else{
            //different months
            $month = $emonth;
            $date = '<div class="smalltext twomonths">'.$smonth.' '.$sdays.'<br>-<br>'.$emonth.' '.$edays.'</div>';
        }
        $e['date'] = $date;

        $events[] = $e;
    }


    $content = $twigpanel->render('event.twig', array(
        'events' => $events
    ));
}else{

//get events
    $query = $db->prepare("SELECT e.*, c.color AS color FROM events e, calendars c WHERE e.cid = c.id AND e.eventdate >= DATE(NOW()) AND e.eventdate < '2099-12-31' ORDER BY e.eventdate ASC");
    $query->execute();
    $ee = $query->fetchAll();
    $events = array();
    foreach($ee as $e){
        if($e['startdate'] == '0000-00-00 00:00:00'){
            $e['startdate'] = $e['eventdate'];
        }

        $smonth = date('M',strtotime($e['startdate']));
        $emonth = date('M',strtotime($e['eventdate']));

        $sdays = date('j',strtotime($e['startdate']));
        $edays = date('j',strtotime($e['eventdate']));

        if(!$e['synopsis']){
            $e['synopsis'] = snippet(strip_tags($e['content']));
        }else{
            $e['synopsis'] = strip_tags($e['synopsis']);
        }

        if($smonth == $emonth){
            $date = $emonth.'<br><span class="smalltext">'.($sdays==$edays?$edays:$sdays.'-'.$edays).'</span>';
        }else{
            //different months
            $month = $emonth;
            $date = '<div class="smalltext twomonths">'.$smonth.' '.$sdays.'<br>-<br>'.$emonth.' '.$edays.'</div>';
        }
        $e['date'] = $date;

        $events[] = $e;
    }


    $content = $twigpanel->render('events.twig', array(
        'events' => $events,
        'error' => $error,
        'post' => $_POST
    ));
}

$page['page_vars']['content']  .= $content;

?>