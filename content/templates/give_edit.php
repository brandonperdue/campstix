<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}
$basedir = '../upload/page_images';
?>

<div class="row">
    <div class="col-md-6">
        <h2>Section 1</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading" value="<?= $page_vars['subheading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Link URL</b><br/>
            <input type="text" name="url" value="<?= $page_vars['url'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Link Label</b><br/>
            <input type="text" name="label" value="<?= $page_vars['label'] ?>" style="width: 100% ;"/>
        </div>

        <h2>Section 2</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading2" value="<?= $page_vars['heading2'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading2" value="<?= $page_vars['subheading2'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content2" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content2'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Link URL</b><br/>
            <input type="text" name="url2" value="<?= $page_vars['url2'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Link Label</b><br/>
            <input type="text" name="label2" value="<?= $page_vars['label2'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Background Image (2000 pixels wide)',
                'image2',
                $page_vars['image2'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>

        <h2>Section 3</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading3" value="<?= $page_vars['heading3'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading3" value="<?= $page_vars['subheading3'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content3" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content3'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Link URL</b><br/>
            <input type="text" name="url3" value="<?= $page_vars['url3'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Link Label</b><br/>
            <input type="text" name="label3" value="<?= $page_vars['label3'] ?>" style="width: 100% ;"/>
        </div>
        <h2>Section 4</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading4" value="<?= $page_vars['heading4'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading4" value="<?= $page_vars['subheading4'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content4" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content4'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Link URL</b><br/>
            <input type="text" name="url4" value="<?= $page_vars['url4'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Link Label</b><br/>
            <input type="text" name="label4" value="<?= $page_vars['label4'] ?>" style="width: 100% ;"/>
        </div>
        <h2>Section 5</h2>
        <div class="formfield">
            <b>Heading</b><br/>
            <input type="text" name="heading5" value="<?= $page_vars['heading5'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="subheading5" value="<?= $page_vars['subheading5'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Text</b><br/>
            <textarea name="content5" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['content5'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Link URL</b><br/>
            <input type="text" name="url5" value="<?= $page_vars['url5'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Link Label</b><br/>
            <input type="text" name="label5" value="<?= $page_vars['label5'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Background Image (2000 pixels wide)',
                'image5',
                $page_vars['image5'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>
    </div>
    <div class="col-md-6">
        <h2>Banner</h2>
        <div class="formfield">
            <?= cms_photo_selector(
                'Banner Image (2000 pixels wide)',
                'banner_image',
                $page_vars['banner_image'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>

        <div class="formfield">
            <b>Image Gallery</b> <span class="note">Gallery used for image grid.  Should have 13 images.</span><br/>
            <select name="gallery">
                <option value="0">No Gallery</option>
                <?
                $galleries = $db->query("SELECT *, title AS formatted_title FROM galleries WHERE NOT projectid ORDER BY title ASC");
                foreach ($galleries as $g) {
                    $query = $db->prepare("SELECT * FROM photos WHERE id_parent = :id_parent ORDER BY priority");
                    $query->execute(array(
                        ':id_parent' => $g['id']
                    ));
                    $p = $query->fetchAll();
                    ?>
                    <option value="<?= $g['id'] ?>" <? if($g['id'] == $page_vars['gallery']){ ?>selected="selected"<? } ?>>
                        <?= $g['formatted_title'] ?> (<?=count($p) ?> images)
                    </option>
                    <?
                }
                ?>
            </select>
        </div>
    </div>
</div>