<div id="editor_holder"></div>

<input type="hidden" id="document-json" name="document" value="<?= htmlspecialchars($page_vars['document']) ?>">

<script src="/lib/bower_components/json-editor/dist/jsoneditor.js"></script>
<script>
    function popupWindow(url, title, w, h) {
        var left = (screen.width/2)-(w/2);
        var top = (screen.height/2)-(h/2);
        return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
    }

    JSONEditor.defaults.options.theme = 'bootstrap3';

    JSONEditor.defaults.resolvers.unshift(function(schema) {
        if (schema.type === 'string' && schema.format === "uploadedfile") {
            return "uploadedfile";
        }
    })

    // add the file picker
    JSONEditor.defaults.editors.uploadedfile = JSONEditor.defaults.editors.string.extend({
        getNumColumns: function() {
            return 4;
        },
        build: function() {
            var self = this;
            this.title = this.header = this.label = this.theme.getFormInputLabel(this.getTitle());

            // Input that holds the base64 string
            this.input = this.theme.getFormInputField('text');
            this.input.jsoneditorinput = this;
            this.input.className = this.input.className + " filepicker-input";
            this.container.appendChild(this.input);

            var description = this.schema.description;
            if (!description) description = '';

            this.preview = this.theme.getFormInputDescription(description);
            this.container.appendChild(this.preview);

            this.control = this.theme.getFormControl(this.label, this.uploader||this.input, this.preview);

            // this button opens the filepicker popup window
            var button = document.createElement("span");
            button.className = 'filepicker-button btn btn-default';
            button.innerHTML = 'Pick File';
            this.control.insertBefore(button, this.control.lastChild);

            // set the button to open the popup
            button.onclick = button.onkeydown = function() {
                var popup = popupWindow('../lib/elfinder/elfinder-filepicker.html', 'Select file', 1000, 800);
                // create a callback which the popup window will call when the file is selected
                popup.callback = function(file) {
                    self.setValue(file.url);
                    self.onChange(true);
                    popup.close();
                };
            };

            this.container.appendChild(this.control);

        }
    });

    // This is the starting value for the editor
    try {
        var starting_value = JSON.parse(document.getElementById('document-json').value);
    }
    catch (e) {
        var starting_value = {};
    }

      // Initialize the editor
    var editor = new JSONEditor(document.getElementById('editor_holder'),{
        // Enable fetching schemas via ajax
        ajax: true,
        
        // The schema for the editor
        schema: {
            type: "array",
            title: "Page sections",
            format: "tabs",
            items: {
                title: "Section",
                //headerTemplate: "{{self.template}}",
                oneOf: [
                    {
                        $ref: "schemas/food_menu.json",
                        title: "Food Menu"
                    },
                    {
                        $ref: "schemas/hero.json",
                        title: "Hero image"
                    },
                    {
                        $ref: "schemas/columns.json",
                        title: "Columns"
                    },
                    {
                        $ref: "schemas/mini_hero.json",
                        title: "Section header with gallery"
                    }
                ]
            }
        },

        // Seed the form with a starting value
        startval: starting_value,
        
        // Disable additional properties
        no_additional_properties: true,
        
        // Require all properties by default
        required_by_default: true,

        disable_edit_json: true,

        disable_properties: true
    });
      
    editor.on("change", function() {
        document.getElementById('document-json').value = JSON.stringify(editor.getValue());
    });
</script>
