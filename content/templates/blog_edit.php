<?
if( ! $page_vars['index_entries'] ){
	$page_vars['index_entries'] = $config['blog_index_entry_limit_default'] ;
}
?>
<table class="editor " id="type_normal">
	<tr>
		<td>
			Blog to Display<br/><?
			write_select(array(
				'rows'=>$db->query("SELECT * FROM blogs ORDER BY title ASC"),
				'value'=>'id',
				'label'=>'title',
				'name'=>'id_blogs',
				'current'=>$page_vars['id_blogs'],
				'display_length'=>99,
				)) ;
		?></td>
	</tr>
	<tr>
		<td>
			<span class="note">Edit the blog's configuration for more options.</span>
		</td>
	</tr>
</table>

