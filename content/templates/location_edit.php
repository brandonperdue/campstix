<?
if (!isset($page_vars['heading'])) {
	$page_vars['heading'] = $r['title'];
}
?>
<table class="editor " id="type_normal">
	<tr>
		<td colspan="3">
			<div class="title">Google Map Coordinates <span class="note">This can be found by right clicking on a google map and selecting "What's here?". (example: 47.663477, -117.412493)</span></div>
			<input type="text" name="coordinates" value="<?= $page_vars['coordinates'] ?>" style="width: 100% ;"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Address</div>
			<input type="text" name="address" value="<?= $page_vars['address'] ?>" style="width: 100% ;"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Primary Phone</div>
			<input type="text" name="phone" value="<?= $page_vars['phone'] ?>" style="width: 100% ;"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Page Heading</div>
			<input type="text" name="heading" value="<?= $page_vars['heading'] ?>" style="width: 100% ;"/>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Page Content</div>
			<textarea name="content" class="rich_editor" style="width: 100% ; height: 700px ;"><?= $page_vars['content'] ?></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Testimonial</div>
			<textarea name="testimonial" style="width: 100% ; height:60px;"><?= $page_vars['testimonial'] ?></textarea><br /><br />
			<strong>Quote by: </strong> <input type="text" name="signature" value="<?= $page_vars['signature'] ?>" style="width:200px;" />
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<div class="title">Call-Out Boxes</div>
		</td>
	</tr>
	<tr>
		<?
			$query = $db->prepare("SELECT * FROM callouts ORDER BY id DESC") ;
			$query->execute(array()) ;
			$rr = $query->fetchAll() ;
		?>
		<td>
			<strong>Box #1</strong><br />
            <select name="co_box1">
            	<option value=""></option>
            <?
            	foreach($rr as $r){
			?>
            	<option value="<?=$r['id'] ?>" <? if($page_vars['co_box1']==$r['id']){ echo 'selected="selected"'; } ?>><?=$r['title'] ?></option>
            <?	} ?>
            </select>
		</td>
		<td>
			<strong>Box #2</strong><br />
            <select name="co_box2">
            	<option value=""></option>
            <?
            	foreach($rr as $r){
			?>
            	<option value="<?=$r['id'] ?>" <? if($page_vars['co_box2']==$r['id']){ echo 'selected="selected"'; } ?>><?=$r['title'] ?></option>
            <?	} ?>
            </select>
		</td>
		<td>
			<strong>Box #3</strong><br />
            <select name="co_box3">
            	<option value=""></option>
            <?
            	foreach($rr as $r){
			?>
            	<option value="<?=$r['id'] ?>" <? if($page_vars['co_box3']==$r['id']){ echo 'selected="selected"'; } ?>><?=$r['title'] ?></option>
            <?	} ?>
            </select>
		</td>
	</tr>
</table>

