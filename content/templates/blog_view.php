<?

$blog = sql_fetch_by_key($db, 'blogs', "id", $page_vars['id_blogs']) ;
if( $blog['index_entries'] ){
	$limit = $blog['index_entries'] ;
}else{
	$limit = $g_blog_index_entry_limit_default ;
}

$query = $db->prepare("SELECT *
	FROM blogs_entries
	WHERE id_blogs = ? AND hidden = 0
	ORDER BY date_posted DESC
	LIMIT $limit") ;
$query->execute(array($blog['id'])) ;
$entries = $query->fetchAll() ;

$page['rss_url'] = get_blog_rss_url($blog) ;
$page['rss_title'] = $blog['title'] ;

?>
<div class="action_box subscribe_link">
	<a href="<?= get_blog_rss_url($blog) ?>">
		<img src="images/rss_icon.png" alt=""/>
		Subscribe
	</a>
</div>
<div class="blog_index">
	<h1><?= $blog['title'] ?></h1>
	<div class="blog_welcome_message"><?= $blog['description'] ?></div>
	<div class="entry_list">
		<?
		foreach( $entries as $entry ){
			$link = get_blog_entry_url($blog, $entry) ;
			?>
			<div class="entry">
				<h3><a href="/<?= $link ?>"><?= $entry['title'] ?></a></h3>
				<div class="date"><?= fmt_datetime($entry['date_posted']) ?></div>
				<div class="summary"><?= $entry['summary'] ?></div>
				<?
				if( isset($g_blog_comments_possible)
					and $g_blog_comments_possible
					and ! $entry['comments_disabled']
					){
					?><div class="comments_link"><a href="<?= $link ?>#disqus_thread">Comments</a></div><?
				}
				?>
			</div>
			<?
		}
		?>
	</div>
</div>
