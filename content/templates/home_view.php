<?
global $basedir_callouts, $basedir_hp, $basedir_news, $basedir_welcome;
?>
<div class="container pad_section">
    <?
    $highlightid = 0;
    $nn = $db->query("SELECT * FROM news WHERE sticky AND FROM_UNIXTIME(expirationdate) > NOW() AND (FROM_UNIXTIME(show_date) <= NOW() OR show_date=-1) AND `date` <= CURDATE() ORDER BY `date` DESC LIMIT 1");
    foreach ($nn as $n) {
        $c = sql_fetch_by_key($db, 'news_categories', "id", $n['category_id']) ;
        $link = '/news?category='.$c['id'].'&article='.$n['id'];
        $highlightid = $n['id'];
        ?>
    <div class="row">
        <div class="callout_box full">

            <? if (file_exists($basedir_news . '/large/' . $n['highlight_img'])) { ?>
                <img src="<?= $basedir_news . '/large/' . $n['highlight_img'] ?>" alt=""/>
            <? }elseif (file_exists($basedir_news . '/' . $n['image'])) { ?>
                <img src="<?= $basedir_news . '/large/' . $n['image'] ?>" alt=""/>
            <? }elseif (file_exists($basedir_news . '/' . $n['image'])) { ?>
                <img src="<?= $basedir_news . '/' . $n['image'] ?>" alt=""/>
            <? } ?>
            <div class="callout_txt co_<?=$n['highlight_color'] ?>">
                <h1><a href="<?= $link ?>"><?= $n['title'] ?></a></h1><br>
                <h6><?= date('M jS, Y', strtotime($n['date'])) ?></h6>
                <p><?= $n['summary'] ?></p><br>
                <div class="callout_note">Posted in:
                    <a href="<?='/news?category='.$c['id'] ?>"><?= $c['title'] ?>
                        <i class="fa fa-fw <?= $c['icon'] ?>"></i></a></div>
            </div>
        </div>
    </div>
    <?
    }
    ?>
    <div class="row">
        <div class="col-md-8">
            <h2>
                <a href="/news" class="view-all-lnk">View All <i class="fa fa-fw fa-external-link"></i></a>
                <a href="/news">News</a>
            </h2>
            <?
            $nn = $db->query("SELECT * FROM news WHERE id!='".$highlightid."' AND FROM_UNIXTIME(expirationdate) > NOW() AND (FROM_UNIXTIME(show_date) <= NOW() OR show_date=-1) AND `date` <= CURDATE() ORDER BY `date` DESC");
            foreach ($nn as $n) {
                $c = sql_fetch_by_key($db, 'news_categories', "id", $n['category_id']) ;
                $link = '/news?category='.$c['id'].'&article='.$n['id'];
                ?>
                <div class="row news-item">
                    <div class="col-md-10 location_result">
                        <div class="col-md-3">
                            <a href="<?= $link ?>">
                                <? if ($n['image']) { ?>
                                    <img src="<?= $basedir_news . '/' . $n['image'] ?>" alt=""/>
                                <? } else { ?>
                                    <img src="/images/logo.png" class="placeholder-img" alt=""/>
                                <? } ?>
                            </a>
                        </div>
                        <div class="col-md-9">
                            <h5><a href="<?= $link ?>"><?= $n['title'] ?></a></h5>
                            <i><?= date('M jS, Y', strtotime($n['date'])) ?></i><br>
                            <?= $n['summary'] ?>
                        </div>
                    </div>
                    <div class="col-md-2 news-category center">
                        <a href="<?='/news?category='.$c['id'] ?>"><?= $c['title'] ?><br>
                            <i class="fa fa-2x <?= $c['icon'] ?>"></i></a>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="col-md-4">
            <div class="calendar-hp">
                <h2><a href="/calendar">Calendar</a></h2>
                <?

                $month = date('n');
                $year = date('Y');

                #add 0 to string for months under 10
                if($month<10) $fmonth = '0'.$month; else $fmonth = $month;

                $styles = '';
                $events = array();
                $rr = $db->query("SELECT * FROM calendars");
                foreach( $rr as $r ){
                    if($r['google']){
                        $events = g_calendar_load($r['google'],"$year-$fmonth-01","$year-$fmonth-31",$r['id'], $events);
                    }else{
                        $events = loc_calendar_load($r['id'],"$year-$fmonth-01","$year-$fmonth-31", $events);
                    }

                    $styles .= '.cal_'.$r['id'].' a{ background:#'.$r['color'].' }';
                }

                #sort 2nd level of the array
                $newarray = array();
                foreach ($events as $key => $row) {
                    $newarray[$key] = msort($row, array('date'));
                }
                $events = $newarray;

                $num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                $datetime = strtotime("$year-$month-1");
                $start_day = date('w', $datetime); # 0=Sunday, 6=Saturday
                ?>
                <style type="text/css">
                    <?=$styles ?>
                </style>
                <!--  cal_list or cal_grid -->
                <script type="text/javascript">
                    $(document).ready(function(){
                        var month = <?=$month?>;
                        var year = <?=$year ?>;
                        load_cal(month,year);
                    });
                </script>
                <script type="text/javascript" src="/lib/ds_calendar/cal_display.js"></script>
                <div class="center"><?=date('F Y') ?></div>
                <table class="ds_calc cal_grid" style="height: 350px;">
                    <thead>
                    <tr>
                        <th>Sun</th><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    $i = 0-$start_day;
                    while($i<$num_days){
                        echo '<tr class="cal_row">';
                        for($d=0;$d<7;$d++){
                            $i++;
                            $events_html = '';
                            foreach($events[$year . "-" . ($month < 10 ? '0' : '') . $month . "-" . ($i < 10 ? '0' : '') . $i] as $e) {

                                $time = date('g:ia', strtotime($e['date']));
                               $displaydate = date('M jS, Y', strtotime($e['date'])) . ' ' . str_replace('12:00am', '', $time);

                                $events_html .= '<div class="dialog_content" id="dialog_' . $e['id'] . '" title="' . $e['title'] . '">
                                    <h5>' . $e['title'] . '</h5>
                                    <h6>' . $displaydate . '</h6><p>' . $e['content'] . '</p><br /><br /></div>';
                                $url = '/calendar?eventid=' . $e['_id'];

                                $events_html .= '<div class="event_wrap cal_' . $e['cal'] . '"><a href="' . $url . '">' . str_replace('12:00am', '', $time) . ' ' . $e['title'] . '</a></div>';
                            }

                            if($i>0&&$i<=$num_days){
                                $txt = $i;
                                $class = 'cal_active';
                                if(!$events_html){
                                    $class = 'cal_empty';
                                }
                            }else{
                                $txt = '';
                                $class = 'cal_null';
                            }
                            ?>
                            <td class="<?=$class ?>">
                                <div class="cal_wrap">
                                    <div class="cal_contain">
                                        <span class="cal_lbl"><?=$txt ?></span>
                                        <span class="cal_lbl_res"><?=$displaydate ?></span>
                                        <?=$events_html	?>
                                    </div>
                                </div>
                            </td>
                        <?
                        }
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
                <br>
            </div>
            <?=$page_vars['content'] ?>
            <br>
            <h2>Welcomes/Farewells:</h2>
            <?
            $rr = $db->query("SELECT * FROM plugin_hellogoodbye_employees WHERE type='1' AND FROM_UNIXTIME(expirationdate) > NOW() + INTERVAL 1 DAY ORDER BY id");
            if($rr->rowCount()){ echo '<strong>Welcome To:</strong>'; }
            foreach ($rr as $r) {
                ?>
                <div class="announce cf">
                    <div class="col-xs-4 right"><img src="<?= '/'.$basedir_welcome.'/'. $r['image'] ?>" alt="" /></div>
                    <div class="col-xs-8"><a href="/farewell-welcome?emp=<?= $r['id'] ?>"><?= $r['name'] ?></a>
                        <br><?= $r['position'] ?>
                        </div>
                </div>
            <? } ?>
            <?
            $rr = $db->query("SELECT * FROM plugin_hellogoodbye_employees WHERE type='2' AND FROM_UNIXTIME(expirationdate) > NOW() + INTERVAL 1 DAY ORDER BY id");
            if($rr->rowCount()){ echo '<strong>Farewell To:</strong>'; }
            foreach ($rr as $r) {
                ?>
                <div class="announce cf">
                    <div class="col-xs-4 right"><img src="<?= '/'.$basedir_welcome.'/'. $r['image'] ?>" alt="" /></div>
                    <div class="col-xs-8"><a href="/farewell-welcome?emp=<?= $r['id'] ?>"><?= $r['name'] ?></a>
                        <br><?= $r['position'] ?>
                    </div>
                </div>
            <? } ?>
            <br>
            <h2>Kudos</h2>

            <?

            $query = $db->prepare("SELECT * FROM kudo ORDER BY `entered` DESC LIMIT 2");
            $query->execute($attributes);
            $nn = $query->fetchAll();
            foreach ($nn as $n) {
                $c = sql_fetch_by_key($db, 'kudo_cat', "id", $n['cid']);
                ?>
                <div class="hp-kudo" style=" background: <?=$c['color'] ?>;">
                <div class="hp-kudo-cat"><i class="fa fa-fw <?= $c['icon'] ?>"></i> <?= $c['title'] ?></div>
                <div class="hp-kudo-content">
                    <h6 class="hp-kudo-name"><?=$n['person'] ?> <i style="font-size:60%;"><?= date('M jS, Y g:HA', strtotime($n['entered'])) ?></i><div class="leftarrow"></div></h6>
                    <div class="hp-kudo-message">
                        <? if($n['title']){ ?><b><?= $n['title'] ?></b><br><? } ?>
                        <i><?= nl2br(trim($n['content'])) ?></i><br>
                        <div style="text-align:right; font-size: 70%; line-height:normal;">
                            submitted by: <?=$n['name'] ?>
                        </div>
                    </div>
                </div>
                </div>
            <? } ?>
            <a href="/kudos">View More Kudos</a>
        </div>
    </div>
    <div class="row">
    </div>
</div>