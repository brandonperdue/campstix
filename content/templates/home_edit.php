<?
if (!isset($page_vars['heading'])) {
    $page_vars['heading'] = $r['title'];
}

$basedir = '../upload/page_images';
?>
<div class="row">
    <div class="col-md-6">
        <h2>Banner</h2>
        <div class="formfield">
            <b>Banner Text</b><br/>
            <textarea name="banner_content" class="rich_editor basic" style="width: 100% ; height: 100px ;"><?= $page_vars['banner_content'] ?></textarea>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Banner Image (2000 pixels wide)',
                'banner_image',
                $page_vars['banner_image'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>
        <h2>Intro</h2>
        <div class="formfield">
            <b>Intro Heading</b><br/>
            <input type="text" name="intro_heading" value="<?= $page_vars['intro_heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sub Heading</b><br/>
            <input type="text" name="intro_subheading" value="<?= $page_vars['intro_subheading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Intro Link URL</b><br/>
            <input type="text" name="intro_url" value="<?= $page_vars['intro_url'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Intro Link Label</b><br/>
            <input type="text" name="intro_label" value="<?= $page_vars['intro_label'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Intro Text</b><br/>
            <textarea name="intro_content" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['intro_content'] ?></textarea>
        </div>
    </div>
    <div class="col-md-6">
        <h2>Events</h2>
        <div class="formfield">
            <b>Event Heading</b><br/>
            <input type="text" name="event_heading" value="<?= $page_vars['event_heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Event Sub Heading</b><br/>
            <input type="text" name="event_subheading" value="<?= $page_vars['event_subheading'] ?>" style="width: 100% ;"/>
        </div>

        <h2>Registration</h2>
        <div class="formfield">
            <b>Registration Link URL</b><br/>
            <input type="text" name="reg_url" value="<?= $page_vars['reg_url'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Registration Link Label</b><br/>
            <input type="text" name="reg_label" value="<?= $page_vars['reg_label'] ?>" style="width: 100% ;"/>
        </div>

        <h2>Donate</h2>
        <div class="formfield">
            <b>Donation Heading</b><br/>
            <input type="text" name="donate_heading" value="<?= $page_vars['donate_heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Donation Sub Heading</b><br/>
            <input type="text" name="donate_subheading" value="<?= $page_vars['donate_subheading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Donation Content</b><br/>
            <textarea name="donate_content" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['donate_content'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Donation Link URL</b><br/>
            <input type="text" name="donate_url" value="<?= $page_vars['donate_url'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Donation Link Label</b><br/>
            <input type="text" name="donate_label" value="<?= $page_vars['donate_label'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Donate Background Image (2000 pixels wide)',
                'donate_image',
                $page_vars['donate_image'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>
        <h2>Sponsors</h2>

        <div class="formfield">
            <b>Sponsor Heading</b><br/>
            <input type="text" name="sponsor_heading" value="<?= $page_vars['sponsor_heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sponsor Sub Heading</b><br/>
            <input type="text" name="sponsor_subheading" value="<?= $page_vars['sponsor_subheading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Sponsor Content</b><br/>
            <textarea name="sponsor_content" class="rich_editor basic" style="width: 100% ; height: 200px ;"><?= $page_vars['sponsor_content'] ?></textarea>
        </div>
        <h2>Testimonials</h2>

        <div class="formfield">
            <b>Testimonial Heading</b><br/>
            <input type="text" name="testimonial_heading" value="<?= $page_vars['testimonial_heading'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <b>Testimonial Content</b><br/>
            <textarea name="testimonial_content"  style="width: 100% ; height: 200px ;"><?= $page_vars['testimonial_content'] ?></textarea>
        </div>
        <div class="formfield">
            <b>Testimonial Signature</b><br/>
            <input type="text" name="testimonial_sig" value="<?= $page_vars['testimonial_sig'] ?>" style="width: 100% ;"/>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Testimonail Signature Image (100 x 100)',
                'testimonial_sig_image',
                $page_vars['testimonial_sig_image'],
                '../upload/people/thumb/',
                '../upload/people/1000/'
            )
            ?>
            <? if($page_vars['testimonial_sig_image']){ ?>
                <div class="formfield">
                    <label><input type="checkbox" name="testimonial_sig_image_delete"> Remove image</label>
                </div>
            <? } ?>
        </div>
        <div class="formfield">
            <?= cms_photo_selector(
                'Testimonial Background Image (2000 pixels wide)',
                'testimonial_image',
                $page_vars['testimonial_image'],
                $basedir . '/thumb/',
                $basedir . '/1000/'
            )
            ?>
        </div>
    </div>
</div>

