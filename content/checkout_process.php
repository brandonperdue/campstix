<?

include 'common/config.php';

switch ($_GET['a']) {
    case 'couponcode':

        $discount = $db->prepare("SELECT * FROM discounts WHERE code=?");
        $discount->execute(array($_POST['code']));
        $discount = $discount->fetch();
        if ($discount) {
            if ($discount['expires'] > (date('Y-m-d') . ' 00:00:00')) {
                unset($_SESSION['discounts']);//only one code at a time
                $_SESSION['discounts'][] = $discount['id'];
            } else {
                e('Coupon Code "' . $_POST['code'] . '" Has Expired');
            }
        } else {
            e('Coupon Code "' . $_POST['code'] . '" Not Found');
        }

        hle('/cart');
        break;
    case 'cart-invoice-add':
        if (!$_SESSION['cart']) {
            $_SESSION['cart'] = array();
        }
        $_POST['invoice_amount'] = $_POST['invoice_amount'] + ($_POST['invoice_amount'] * .029);
        $_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], '999999', array(), $_POST['invoice_amount'], 'Invoice #' . $_POST['invoice_number']);
        hle('/cart');
        break;
    case 'cart-add':
        //We are only allowing one product at a time, so clear out old cart and campers
        $_SESSION['cart'] = array();
        $_SESSION['campers'] = array();
        $_SESSION['discounts'] = array();
        $options = array();
        $i = 0;


        $_REQUEST['qty'] = intval($_REQUEST['qty']);

        if ($_REQUEST['qty'] > 50) {
            $_REQUEST['qty'] = 50;
        } elseif ($_REQUEST['qty'] < 1) {
            $_REQUEST['qty'] = 1;
        }

        foreach ($_REQUEST['opt'] as $o) {
            $i++;
            $option_value = $db->prepare("SELECT o.title, c.id, c.title AS key_title FROM options o, option_categories c WHERE o.id_categories=c.id AND o.id=?");
            $option_value->execute(array($o));
            $option_value = $option_value->fetch();

            if(!$option_value){
                $option_value = $db->prepare("SELECT title FROM options WHERE id=?");
                $option_value->execute(array($o));
                $option_value = $option_value->fetch();
                $options["option_$i"] = $o . '|' . $option_value['title']. '| Model';
            }else{
                $options["option_$i"] = $o . '|' . $option_value['title'] . '|' . $option_value['key_title'];
            }

        }
        $_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], $_REQUEST['id'], $options, $_REQUEST['qty'], null, '', $_GET['partnumber_append']);

        //see if there are camp registration products in the cart
        $campers = get_camper_count($_SESSION['cart']);
        if($campers){
            hle('/cart-campers');
        }else{
            hle('/cart-address');
        }
        break;

    case 'cart-add-with-arbitrary-price':
        if (!$_SESSION['cart']) {
            $_SESSION['cart'] = array();
        }
        $_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], $_POST['id'], array(), $_POST['amount']);
        hle('/cart');
        break;

    case 'cart-add_multiple_products':
        $product_ids = explode(',', $_GET['products']);
        // check product_ids
        if (!$product_ids or !is_array($product_ids)) {
            exit('Incorrect product specification.');
        }
        if (!$_SESSION['cart']) {
            $_SESSION['cart'] = array();
        }
        $_SESSION['cart'] = cart_add_multiple_products($db, $_SESSION['cart'], $product_ids);
        hle('/cart');
        break;

    case 'cart-qty':
        $key = $_GET['key'];
        if ($_SESSION['cart'][$key] and $_GET['qty'] >= 0) {
            $_SESSION['cart'][$key]['qty'] = (int)$_GET['qty'];
            if ($_SESSION['cart'][$key]['qty'] <= 0) {
                unset($_SESSION['cart'][$key]);
            }
        }
        hle('/cart');
        break;

    case 'cart-remove':
        unset($_SESSION['cart'][$_GET['key']]);
        hle('/cart');
        break;
    case 'cart-camper-pay':
        //reset cart, camper and discount sessions
        $_SESSION['cart'] = array();
        $_SESSION['campers'] = array();
        $_SESSION['discounts'] = array();

        //get camper
        $c = sql_fetch_by_key($db, 'campers', 'id', $_GET['c']);
        $s = sql_fetch_by_key($db, 'scholarship', 'cid', $_GET['c']);
        if($c['uid']!=$_SESSION['log']['id']){
            e('Permission denied');
            hle($_SERVER['HTTP_REFERER']);
        }
        if($c['pledged'] || $s['amount']){
            //insert account credit as discount (use pre entered discount id #1 in db)
            $_SESSION['discounts'][] = array('id' => 1, 'camper' => $c['id']);
        }

        $_SESSION['campers'][$c['id']] = $c;

        $_SESSION['cart'] = cart_add_product($db, $_SESSION['cart'], $c['pid'], array(), 1, null, '', '');

        hle('/cart-campers');
        break;
    case 'registration-set-amount':

        $mindue = 0;
        $maxdue = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);

        foreach($_SESSION['campers'] as $cid => $c){
            $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
            $c = sql_fetch_by_key($db, 'campers', 'id', $cid);

            $mindue += $p['minimumdown'];

            //if a financial need code was entered check to see if it matches and ignore the min down if it does.
            if($_POST['financialcode']) {
                if($p['financialneedcode']!=$_POST['financialcode']) {
                    e('Financial Code Not Found');
                    hle('/cart-campers');
                }else {
                    //financial code found so redirect to completed registration
                    $id = sql_upsert($db, 'campers', array('status'), array('status'=>'Registered'), 'id',$cid);

                    $_SESSION['cart'] = array();
                    unset($_SESSION['paymentamount']);
                    $_SESSION['campers'] = array();
                    $_SESSION['discounts'] = array();
                    m('Registration Complete.  To see funding options see the camper table below.');
                    hle('/account');
                }
            }else {

            }
        }

        if($_POST['paymentamount'] < $mindue){
            e('You need to pay at least $'.$mindue);
            hle('/cart-campers');
        }else{
            $_SESSION['paymentamount'] = $_POST['paymentamount'];
            hle('/cart-address');
        }
        break;

    case 'edit-camper':
        //check camper account ownership
        if($_POST['id']){
            $c = sql_fetch_by_key($db, 'campers', 'id', $_POST['id']);
            if($c['uid']!=$_SESSION['log']['id']){
                e('Permission denied');
                hle($_SERVER['HTTP_REFERER']);
            }
        }
        //check for empty entries (backlink issues etc)
        if(!$_POST['firstname'] || !$_POST['lastname']){
            e('A name is required for a registration');
            hle($_SERVER['HTTP_REFERER']);
        }

        $p = sql_fetch_by_key($db, 'products', 'id', $_POST['pid']);

        if($p['signuptype'] == 1){
            //STIX
            $fields = $camperfieldsstix;
        }elseif($p['signuptype'] == 2){
            //TWIGS
            $fields = $camperfieldstwigs;
        }elseif($p['signuptype'] == 3){
            //BASIC
            $fields = $camperfieldsbasic;
        }else{
            $fields = $camperfieldsbasic;
        }

        $checkboxfields = array(
            'allergy_bees',
            'allergy_latex',
            'allergy_penicillin',
            'allergy_seasonal',
            'allergy_peanuts',
            'allergy_shellfish',
            'allergy_dairy',
            'allergy_eggs',
            'allergy_soy',
            'allergy_gluten',
            'allergy_nuts',
            'gluten_free',
            'dairy_free'
        );
        foreach($checkboxfields as $ch){
            if(!$_POST[$ch]){
                $_POST[$ch] = 'No';
            }
        }

        if($_POST['bdate']){
            $_POST['bdate'] = date('Y-m-d', strtotime($_POST['bdate']));
        }

        $values = $_POST;
        $values['uid'] = $_SESSION['log']['id'];

        $id = sql_upsert($db, 'campers', $fields, $values, 'id',$_POST['id']);

        //set session for camper in case of checkout
        $_SESSION['campers'][$id] = $values;
        m("Registration Saved");

        if($_POST['want_cabin_mate']){
            $_SESSION['cabin-mate'] = $id;
        }
        if($_POST['want_scholarship']){
            $_SESSION['scholarship'] = $id;
        }

		//back from whence you came
        if($_POST['redirect']){
            hle($_POST['redirect']);
        }else{
            hle(strtok($_SERVER['HTTP_REFERER'], '?'));
        }

        break;
    case 'edit-volunteer':
        //check camper account ownership
        if($_POST['id']){
            $v = sql_fetch_by_key($db, 'volunteers', 'id', $_POST['id']);
            if($v['uid']!=$_SESSION['log']['id']){
                e('Permission denied');
                hle($_SERVER['HTTP_REFERER']);
            }
        }
        //check for empty entries (backlink issues etc)
        if(!$_POST['firstname'] || !$_POST['lastname']){
            e('A name is required for a registration');
            hle($_SERVER['HTTP_REFERER']);
        }

        $p = sql_fetch_by_key($db, 'products', 'id', $_POST['pid']);

        if($p['signuptype'] == 1){
            //STIX
            $fields = $volunteerfieldsstix;
        }elseif($p['signuptype'] == 2){
            //TWIGS
//            $fields = $volunteerfieldstwigs;
            $fields = $volunteerfieldsstix;
        }elseif($p['signuptype'] == 3){
            //BASIC
            $fields = $volunteerfieldsbasic;
        }else{
            $fields = $volunteerfieldsbasic;
        }

        $checkboxfields = array(
            'allergy_bees',
            'allergy_latex',
            'allergy_penicillin',
            'allergy_seasonal',
            'allergy_peanuts',
            'allergy_shellfish',
            'allergy_dairy',
            'allergy_eggs',
            'allergy_soy',
            'allergy_gluten',
            'allergy_nuts',
            'gluten_free',
            'dairy_free'
        );
        foreach($checkboxfields as $ch){
            if(!$_POST[$ch]){
                $_POST[$ch] = 'No';
            }
        }


        if($_POST['bdate']){
            $_POST['bdate'] = date('Y-m-d', strtotime($_POST['bdate']));
        }
        if($_POST['program_lead']){
            $_POST['program_lead'] = implode('|',$_POST['program_lead']);
        }

        $values = $_POST;
        $values['uid'] = $_SESSION['log']['id'];

        $id = sql_upsert($db, 'volunteers', $fields, $values, 'id',$_POST['id']);

        //set session for camper in case of checkout
        //$_SESSION['campers'][$id] = $values;
        m("Registration Saved");

        //back from whence you came
        if($_POST['want_rv_tent']){
            hle('/tent-or-rv-form/?v='.$id);
        }elseif($_POST['redirect']){
            hle($_POST['redirect']);
        }else{
            hle(strtok($_SERVER['HTTP_REFERER'], '?'));
        }

        break;
    case 'cart-shipping':
        form_save('cart-shipping', $_POST);
        $fields_to_copy = array('firstname', 'lastname', 'address', 'city', 'state', 'zip', 'country');
        $required_fields = array('bi_country', 'bi_firstname', 'bi_lastname', 'bi_address', 'bi_city', 'bi_zip', 'bi_email', 'bi_phone');
        $other_fields_to_save = array('remember_me', 'bi_state');

        //force shipping info to match billing
        $_POST['si_same'] = true;
        if ($_POST['si_same']) {
            // copy billing to shipping
            foreach ($fields_to_copy as $field) {
                $tmp_customer['si_' . $field] = $_POST['bi_' . $field];
            }
        } else {
            // shipping fields must be checked
            $required_fields = array_merge($required_fields, array('si_country', 'si_firstname', 'si_lastname', 'si_address', 'si_city', 'si_zip'));
        }

        // check required fields; save customer data
        foreach ($required_fields as $field) {
            if (!$_POST[$field]) {
                $error = true;
            }
            $tmp_customer[$field] = $_POST[$field];
        }
        // save optional fields
        foreach ($other_fields_to_save as $field) {
            // don't overwrite if previously copied
            if (!isset($tmp_customer[$field])) {
                $tmp_customer[$field] = $_POST[$field];
            }
        }
        // USA and Canada only - check for state
        if (
            (!$tmp_customer['si_state'] and in_array($tmp_customer['si_country'], $g_state_required_countries)) or
            (!$tmp_customer['bi_state'] and in_array($tmp_customer['bi_country'], $g_state_required_countries))
        ) {
            $error = true;
            e('State is required for your selected country');
        }

        // save customer to session
        unset($_SESSION['customer']);
        $_SESSION['customer'] = $tmp_customer;

        // save customer to cookie if requested
        if ($_POST['remember_me'] and is_array($_SESSION['customer'])) {
            setcookie('customer', serialize(array_map('stripslashes', $_SESSION['customer'])), time() + 60 * 60 * 24 * 365);
        } else {
            setcookie('customer', '', time() - 3600);
        }

        if ($error) {
            e('All fields are required');
            hle('/cart-address');
        } else {
            hle('/cart-payment');
        }
        break;
    case 'fund-my-camper':
        //get all the data for the camper
        $camper = sql_fetch_by_key($db, 'campers', 'id', $_POST['cid']);
        $product = sql_fetch_by_key($db, 'products', 'id', $camper['pid']);
        $user = sql_fetch_by_key($db, 'users', 'id', $camper['uid']);
        $funding = sql_fetch_by_key($db, 'funding', 'cid', $_POST['cid']);
        $scholarship = sql_fetch_by_key($db, 'scholarship', 'cid', $_POST['cid']);

        $stripe = array(
            "secret_key"      => $config['stripe']['live_secret_key'],
            "publishable_key" => $config['stripe']['live_publishable_key']
        );

        if ($_SESSION['test']) {
            $stripe = array(
                "secret_key"      => $config['stripe']['test_secret_key'],
                "publishable_key" => $config['stripe']['test_publishable_key']
            );
        }

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $token  = $_POST['stripeToken'];
        $amount = $_POST['amount']*100;
        $email = $_POST['stripeEmail'];
        $desc = "Camp Funding: For ". $camper['firstname']." ".$camper['lastname'];

        if($token) {
            $customer = \Stripe\Customer::create(array(
                "description" => $_POST['stripeBillingName'],
                "email" => $email,
                'source' => $token
            ));

            //submit charge
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $amount,
                'description' => $desc,
                'currency' => 'usd'
            ));

            //update camper with new pledged amount
            $pledged_amount = $_POST['amount']+$camper['pledged'];
            $fields = array('pledged');
            $values = array('pledged'=>$pledged_amount);

            $totalpaid = $scholarship['amount'] + $pledged_amount;

            //if the full price has been paid we need to mark this camper as "Paid".
            if($totalpaid >= $product['price']){
                $fields[] = 'status';
                $values['status'] = 'Paid';
                $completemessage = "You're camp registration has been completed. <br><br>Thank You!";

                smtp_mail(array($config['receipt_contact']=>$config['company_name']), $user['user_email'], $desc." Complete", formattedemail($completemessage));
            }

            $id = sql_upsert($db, 'campers', $fields, $values, 'id',$camper['id']);

            //Add order to database
            $order_stripe_id = $charge['id'];
            $ordernumber = cart_new_order_number();

            $order_values = array(
                'id_camper' => $camper['id'],
                'order_grand_total' => $_POST['amount'],
                'purchase_type' => 'Stripe - Fund My Camp',
                'bi_firstname' => $_POST['stripeBillingName'],
                'bi_address' => $_POST['stripeBillingAddressLine1'],
                'bi_city' => $_POST['stripeBillingAddressCity'],
                'bi_state' => $_POST['stripeBillingAddressState'],
                'bi_zip' => $_POST['stripeBillingAddressZip'],
                'bi_email' => $email,
                'transaction_id' => $order_stripe_id,
                'ordernumber' => $ordernumber
            );
            $id_orders = sql_upsert($db, 'orders', array_keys($order_values), $order_values, null, null, "order_date = NOW()");


            // now that order is in db, create receipt - receipt needs order record id so it can link to downloads
            $receipt = cart_checkout_create_and_save_receipt($db, array(
                'receipt_template_path' => 'common/mod_receipt_content_funding.php',
                'id_orders' => $id_orders,
                'ordernumber' => $ordernumber,
                'camper' => $camper,
                'values' => $order_values,
                'checkout_code_customer_title' => $checkout_code_customer['title']
            ));

            m('Your Payment Has Been Processed.');

            //send message to account holder
            $message = $_POST['stripeBillingName']." has funded ".$camper['firstname']." ".$camper['lastname']."'s camp for $".number_format($_POST['amount'],2).
                "<br><br>
                $".number_format($pledged_amount,2)." of the $".number_format($funding['goal'],2)." goal has been paid for ".$camper['firstname']." so far.
                <br><br>";
            $subject = $desc;
            smtp_mail(array($config['receipt_contact']=>$config['company_name']), $user['user_email'], $subject, formattedemail($message));

            //send receipts
            if ($_SESSION['test']) {
                $order_email = $config['email_developer'];
            } else {
                if($product['contact_email']){
                    $order_email = $product['contact_email'];
                }else{
                    $order_email = $config['email_contact'];
                }
            }
            $emails_to_send_receipt_to = array($order_email, $_SESSION['customer']['bi_email']);

            foreach ($emails_to_send_receipt_to as $email) {
                try {
                    smtp_mail(array($config['receipt_contact']=>$config['company_name']), $email, "Order #$ordernumber", $receipt);
                } catch (Exception $e) {
                    e("The order went through, but there was an error sending the receipt email.");
                }
            }
        }else{
            m('Payment failed');
        }

        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'donation':

        $stripe = array(
            "secret_key"      => $config['stripe']['live_secret_key'],
            "publishable_key" => $config['stripe']['live_publishable_key']
        );

        if ($_SESSION['test']) {
            $stripe = array(
                "secret_key"      => $config['stripe']['test_secret_key'],
                "publishable_key" => $config['stripe']['test_publishable_key']
            );
        }

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $token  = $_POST['stripeToken'];
        $amount = $_POST['amount']*100;
        $email = $_POST['stripeEmail'];
        $comments = $_POST['comment'];

        if($token) {
            $customer = \Stripe\Customer::create(array(
                "description" => $_POST['stripeBillingName'],
                "email" => $email,
                'source' => $token
            ));

            $desc = "Donation: ".$_POST['stripeBillingName'];

            //submit charge
            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $amount,
                'description' => $desc,
                'currency' => 'usd'
            ));

            //Add order to database
            $order_stripe_id = $charge['id'];
            $ordernumber = cart_new_order_number();

            $order_values = array(
                'id_camper' => $camper['id'],
                'order_grand_total' => $_POST['amount'],
                'purchase_type' => 'Stripe - Donation',
                'bi_firstname' => $_POST['stripeBillingName'],
                'bi_address' => $_POST['stripeBillingAddressLine1'],
                'bi_city' => $_POST['stripeBillingAddressCity'],
                'bi_state' => $_POST['stripeBillingAddressState'],
                'bi_zip' => $_POST['stripeBillingAddressZip'],
                'bi_email' => $email,
                'transaction_id' => $order_stripe_id,
                'ordernumber' => $ordernumber
            );
            $id_orders = sql_upsert($db, 'orders', array_keys($order_values), $order_values, null, null, "order_date = NOW()");

            // now that order is in db, create receipt - receipt needs order record id so it can link to downloads
            $receipt = cart_checkout_create_and_save_receipt($db, array(
                'receipt_template_path' => 'common/mod_receipt_content_donation.php',
                'id_orders' => $id_orders,
                'ordernumber' => $ordernumber,
                'comments' => $comments,
                'values' => $order_values,
                'checkout_code_customer_title' => $checkout_code_customer['title']
            ));


            //send receipts
            if ($_SESSION['test']) {
                $order_email = $config['email_developer'];
            } else {
                $order_email = $config['email_contact'];
            }
            $emails_to_send_receipt_to = array($order_email, $_SESSION['customer']['bi_email']);

            foreach ($emails_to_send_receipt_to as $email) {
                try {
                    smtp_mail(array($config['receipt_contact']=>$config['company_name']), $email, "Order #$ordernumber", $receipt);
                } catch (Exception $e) {
                    e("The donation went through, but there was an error sending the receipt email.");
                }
            }
            m('Your Donation Has Been Processed.');
            hle('/thank-you-donation');
        }else{
            e('Payment failed');
        }

        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'cart-stripe-checkout':
        form_save('checkout_payment', $_POST);
        // validation
        if (!is_array($_SESSION['cart'])) {
            e('Your cart is empty');
            if ($_SESSION['purchase']['ordernumber']) {
                e("If you're trying to check out again, your order with number {$_SESSION['purchase']['ordernumber']} was already processed. Please check your email for a receipt, or contact us if you need help");
            }
        }

        //check to see if the number of campers in the cart match the number of campers stored
        $cart_count = get_camper_count($_SESSION['cart']);
        $camper_count = count($_SESSION['campers']);
        if($cart_count > $camper_count){
            e('You have empty registration products.  Please enter your registration info before checkout');
            hle('/cart-campers');
        }


        // handle purchase orders
        if ($_POST['checkout_code']) {
            // validate that code exists
            $query = $db->prepare("SELECT * FROM customers WHERE allow_purchase_orders = 1 AND LOWER(checkout_code) = LOWER(?) AND checkout_code != ''");
            $query->execute(array($_POST['checkout_code']));
            $checkout_code_customer = $query->fetch();
            if ($checkout_code_customer['id']) {
                $is_purchase_order = true;
            } else {
                e('Invalid checkout code');
            }
        } else {
            if (!$_POST['cc_number'])
                e('Card Number is required');
            if (!$_POST['cc_code'])
                e('Card Security Code is required');
            if (!$_POST['cc_month'] or !$_POST['cc_year'])
                e('Card Expiration is required');
        }
        // cancel if validation errors
        if ($_SESSION['e']) {
            hle('/cart-payment');
        }
        // attempt to process order
        $order_grand_total = cart_get_grand_total($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
        $order_tax = cart_get_tax($db, $_SESSION['cart'], $_SESSION['discounts'], $_SESSION['customer']);
        $order_shipping = cart_get_shipping($db, $_SESSION['cart'], $_SESSION['customer']);

        $fulldue = $order_grand_total;

        $cid = key($_SESSION['campers']);
        $c = sql_fetch_by_key($db, 'campers', 'id', $cid);
        $product = sql_fetch_by_key($db, 'products', 'id', $c['pid']);

        if($_SESSION['paymentamount']){
            $order_grand_total = $_SESSION['paymentamount'];
            $order_tax = 0;
            $order_shipping = 0;
        }



        $order_vars = array(
            'tax' => $order_tax,
            'shipping' => $order_shipping,
            'bi_firstname' => $_SESSION['customer']['bi_firstname'],
            'bi_lastname' => $_SESSION['customer']['bi_lastname'],
            'bi_country' => $_SESSION['customer']['bi_country'],
            'bi_address' => $_SESSION['customer']['bi_address'],
            'bi_city' => $_SESSION['customer']['bi_city'],
            'bi_state' => $_SESSION['customer']['bi_state'],
            'bi_zip' => $_SESSION['customer']['bi_zip'],
            'bi_phone' => $_SESSION['customer']['bi_phone'],
            'bi_email' => $_SESSION['customer']['bi_email'],
            'si_firstname' => $_SESSION['customer']['si_firstname'],
            'si_lastname' => $_SESSION['customer']['si_lastname'],
            'si_country' => $_SESSION['customer']['si_country'],
            'si_address' => $_SESSION['customer']['si_address'],
            'si_city' => $_SESSION['customer']['si_city'],
            'si_state' => $_SESSION['customer']['si_state'],
            'si_zip' => $_SESSION['customer']['si_zip'],
            'si_phone' => $_SESSION['customer']['si_phone'],
            'provider' => $_SESSION['customer']['provider'],
        );

        if ($is_purchase_order) {
            // no CC processing
            $purchase_type = 'Checkout Code';
        } else {
            // get the credit card details submitted by the form
            $token = $_POST['stripeToken'];

            try {

                $desc = $product['title'].' - '.$order_vars['bi_firstname'].' '.$order_vars['bi_lastname'];

                $customer = \Stripe\Customer::create(array(
                    "description" => $order_vars['bi_firstname'].' '.$order_vars['bi_lastname'],
                    "email" => $order_vars['bi_email'],
                    'source' => $token
                ));

                // create the charge on Stripe's servers - this will charge the user's card
                $charge = \Stripe\Charge::create(array(
                    'customer' => $customer->id,
                    "amount" => $order_grand_total * 100, // amount in cents, again
                    "currency" => "usd",
                    "description" => $desc
                ));
            } catch (Exception $e) {
                e("Error processing payment.");
                hle('/cart-payment');
            }
            $purchase_type = 'Stripe';
            $order_stripe_id = $charge['id'];
        }
        $ordernumber = cart_new_order_number();
        // was order successful?
        if ($ordernumber) {
            $order_vars['ordernumber'] = $ordernumber;
            // save to DB

            // order id is captured and copied to session so we can lookup download tickets and display on thankyou
            $id_orders = cart_checkout_create_order_db_records($db, array(
                'id_camper' => $c['id'],
                'referer' => $_SESSION['referer'],
                'purchase_type' => $purchase_type,
                'customer' => $_SESSION['customer'],
                'cart' => $_SESSION['cart'],
                'tax_data' => $_SESSION['tax_data'],
                'tax' => $order_tax,
                'shipping' => $order_shipping,
                'grand_total' => $order_grand_total,
                'transaction_id' => $order_stripe_id,
                'ordernumber' => $ordernumber,
                'id_customer' => $_SESSION['log']['id'],
                'checkout_code' => $_POST['checkout_code'],
                'id_checkout_code_customer' => $checkout_code_customer['id']
            ));

            //update campers to paid
            if($c['id']){
                if($fulldue <= $order_grand_total){
                    sql_upsert($db, 'campers', array('status'), array('status'=>'Paid'), 'id', $c['id']);
                }else{
                    sql_upsert($db, 'campers', array('status', 'pledged'), array('status'=>'Registered', 'pledged'=>$c['pledged']+$order_grand_total), 'id', $c['id']);
                }
            }

            // now that order is in db, create receipt - receipt needs order record id so it can link to downloads
            $receipt = cart_checkout_create_and_save_receipt($db, array(
                'id_camper' => $c['id'],
                'camper' => $c,
                'order' => $order_vars,
                'receipt_template_path' => 'common/mod_receipt_content.php',
                'cart' => $_SESSION['cart'],
                'discounts' => $_SESSION['discounts'],
                'customer' => $_SESSION['customer'],
                'id_orders' => $id_orders,
                'ordernumber' => $ordernumber,
                'checkout_code_customer_title' => $checkout_code_customer['title']
            ));

            $_SESSION['purchase']['ordernumber'] = $ordernumber;
            $_SESSION['purchase']['id_orders'] = $id_orders;
            $_SESSION['purchase']['receipt'] = $receipt; // for display on thank you page
            // send receipts

            if ($_SESSION['test']) {
                $order_email = $config['email_developer'];
            } else {
                if($product['contact_email']){
                    $order_email = $product['contact_email'];
                }else{
                    $order_email = $config['email_contact'];
                }
            }
            $emails_to_send_receipt_to = array($order_email, $_SESSION['customer']['bi_email']);


            foreach ($emails_to_send_receipt_to as $email) {
                try {
                    smtp_mail(array($config['receipt_contact']=>$config['company_name']), $email, "Order #$ordernumber", $receipt);
                } catch (Exception $e) {
                    e("The order went through, but there was an error sending the receipt email.");
                }
            }

            unset($_SESSION['paymentamount']);
            unset($_SESSION['campers']);
            unset($_SESSION['discounts']);
            unset($_SESSION['cart']);
            hle('/thank-you?p='.$c['pid']);
        } else {
            hle('/cart-payment');
        }
        break;

}

