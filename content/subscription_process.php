<?

include 'common/config.php';

switch ($_GET['a']) {

    case 'cancel_recurring_payment':
        $user = sql_fetch_by_key($db, 'users', 'id', $_SESSION['log']['id']) ;

        try {
            $customer = \Stripe\Customer::retrieve($user['stripe_id']);
            $subscription = $customer->subscriptions->retrieve($user['stripe_sub_id'])->cancel();
        } catch (Exception $e) {
            e("Error Canceling subscription.");
            hle($_SERVER['HTTP_REFERER']);
        }
        $values = array("stripe_id"=>'',"stripe_sub_id"=>'');
        $fields = array('stripe_id','stripe_sub_id');
        $values = sql_organize_values($fields, $values);
        sql_upsert($db, 'users', $fields, $values, 'id', $_SESSION['log']['id']);

        //sql_update('users', setlist(array('stripe_id','stripe_sub_id'),$values), 'id', $_SESSION['log']['id']);

        set_subscription_enddate();

        m('Automatic payments have been turned off');
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'stripe-webhook':
        $input = @file_get_contents("php://input");
        $event_json = json_decode($input);

        $type = $event_json->type;

        switch($type){
            case "invoice.payment_succeeded":
                //charge so update expiration date
                $sub_id = $event_json->data->object->lines->data[0]->id;
                $fields = array('expires');
                $post = array('expires'=>date('Y-m-d H:i:s', strtotime('+1 year')));
                $values = sql_organize_values($fields, $post);
                //temp code for ssl fix
                sql_upsert($db, 'users', $fields, $post, 'stripe_sub_id', $sub_id);
                break;
        }

        http_response_code(200);
        break;
    case 'subscription-stripe-checkout':

        // get the credit card details submitted by the form
        $token = $_POST['stripeToken'];
        $email = $_POST['stripeEmail'];

        $plan = 'esn_yearly';
        if($_GET['provider']){
            $plan = 'esp_yearly';
        }

        try {
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source'  => $token,
                'plan' => $plan
            ));

            $purchase_type = 'Stripe';
            $customer_stripe_id = $customer['id'];
            $customer_stripe_sub_id = $customer->subscriptions->data[0]->id;

        } catch (Exception $e) {
            e("Error processing payment.");
            hle('/subscribe');
        }


        if ($customer_stripe_id) {

            $values = array("stripe_id"=>$customer_stripe_id,"stripe_sub_id"=>$customer_stripe_sub_id);
            $fields = array('stripe_id','stripe_sub_id');

            sql_upsert($db, 'users', $fields, $values, 'id', $_SESSION['log']['id']);
            unset($_SESSION['subscription_tested']);

            hle('/thank-you-subscription');
        } else {
            e('An Error Occurred With Your Payment');
            hle('/subscribe');
        }
        break;

}

