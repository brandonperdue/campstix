<?
include 'common/config.php';

$settings['breadcrumb_separator'] = $config['breadcrumb_separator'];

if ($_GET['view'] and false === strpos($_GET['view'], '/')) {
    $page = [];
    $page['template'] = $_GET['view'];
    $page['meta_title'] = $page['title'] . ($settings['html_title_snippet'] ? " {$settings['html_title_snippet']}" : '');
    $page = cms_render_page($db, $page, $settings, $config, [
        'user' => $_SESSION['user'],
        'form' => form_load_clear($_GET['view']),
        'get'  => $_GET,
    ]);
    if (!$page['breadcrumbs'] and is_array($page['breadcrumbs_array'])) {
        $page['breadcrumbs'] = get_breadcrumbs($page['breadcrumbs_array'], $settings['breadcrumb_separator'], true, false);
    }
}

$page['id_nav_root_page'] = 0;
$page['nav_depth'] = 3;
if($page['meta_title']){
    $page['meta_title'] = $page['meta_title'] . ($settings['html_title_snippet'] ? " {$settings['html_title_snippet']}" : '');
}else{
    $page['meta_title'] = $page['title'] . ($settings['html_title_snippet'] ? " {$settings['html_title_snippet']}" : '');
}
$page['nav'] = cms_get_nav($db, 0, $page['nav_chain'], $page['nav_depth'], true, 'current');
$page['canonical_url'] = "http://" . $config['ssl_address'] . $_SERVER['REQUEST_URI'];
$page['requested_url'] = "http" . ($_SERVER['HTTPS'] ? 's' : '') . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];


$page['page_vars']['content'] = getContentGallery($page['page_vars']['content']);

$query = $db->prepare("SELECT * FROM pages WHERE footershow ORDER BY priority ASC");
$query->execute();
$fnav = $query->fetchall();

$page['loginform'] = login_form('/account');



// display the page
echo $twig->render($page['template'] . '.html.twig', array(
    'nav_options' => $nav_options,
    'nav'         => $page['nav'],
    'fnav'        => $fnav,
    'page'        => $page,
    'cart'        => $_SESSION['cart'],
    'messages'    => $_SESSION['m'],
    'errors'      => $_SESSION['e'],
    'user'        => $_SESSION['log'],
    'test_mode'   => $_SESSION['test'],
    'config'      => $config,
    'settings'    => $settings,
    'search_term' => $_GET['q']
));

// empty the errors and messages which should have been shown to the user
unset($_SESSION['e']);
unset($_SESSION['m']);
