<?
include '../common/config.php';
include 'common/header.php';
if ($_GET['pid']) {
    $p = sql_fetch_by_key($db, 'products', 'id', $_GET['pid']);
    ?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
<!--            --><?// button('add', 'volunteers_.php', 'Add Volunteer'); ?>
            <a href="volunteers.php" class="btn btn-default"><i class="fa fa-fw fa-tree"></i> Select Camp</a>
            <a href="a.php?a=export-volunteers&pid=<?= $_GET['pid'] ?>" class="btn btn-default"><i class="fa fa-fw fa-download"></i> Export All</a>
            <?
            if ($_GET['order'] == 'name') {
                $order = 'firstname';
                ?>
                <a href="volunteers.php?pid=<?= $_GET['pid'] ?>" class="btn btn-default"><i class="fa fa-fw fa-sort"></i> Order By Date</a>
                <?
            } else {
                $order = 'dateadded';
                ?>
                <a href="volunteers.php?pid=<?= $_GET['pid'] ?>&order=name" class="btn btn-default"><i class="fa fa-fw fa-sort"></i> Order By Name</a>
                <?
            }
            ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1><?= $p['title'] ?> Volunteers</h1>
            <a href="a.php?a=export-volunteers&pid=<?= $_GET['pid'] ?>&status=Approved" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Approved</a>
            <h2>Approved</h2>
            <?
            $where = 'status = "Approved" AND pid = ?';
            $wherearray = array($_GET['pid']);
            $query = $db->prepare("SELECT * FROM volunteers WHERE $where ORDER BY $order DESC");
            $query->execute($wherearray);
            $rr = $query->fetchAll();
            if (!$rr) {
                echo 'None Found';
            } else {
                ?>
                <ol class="treeview list root" cmsTable="volunteers" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="users_.php?id=<?= $u['id'] ?>"
                                   title="<?= $u['user_email'] ?> | Click to see User info">
                                    <span class="fa fa-user fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                                   title="Login as <?= $u['user_email'] ?>" target="_blank">
                                    <span class="fa fa-sign-in fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell"><? delete_button('volunteers', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="volunteers_.php?id=<?= $r['id'] ?>">
                            <b><?= htmlspecialchars($r['firstname'] . ' ' . $r['lastname']) ?></b>
                            <span class="rightcontent">Reg: <?= $u['user_email'] ?> | <?= date('M jS Y', strtotime($r['dateadded'])) ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
            <a href="a.php?a=export-volunteers&pid=<?= $_GET['pid'] ?>&status=Approved" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Approved</a>
            <br>
            <br>
            <br>
            <hr>
            <br>
            <a href="a.php?a=export-volunteers&pid=<?= $_GET['pid'] ?>&status=Pending" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Pending</a>
            <h2>Pending</h2>
            <?
            $where = 'status = "Pending" AND pid = ?';
            $wherearray = array($_GET['pid']);
            $query = $db->prepare("SELECT * FROM volunteers WHERE $where ORDER BY $order DESC");
            $query->execute($wherearray);
            $rr = $query->fetchAll();
            if (!$rr) {
                echo 'None Found';
            } else {
                ?>
                <ol class="treeview list root" cmsTable="volunteers" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="users_.php?id=<?= $u['id'] ?>"
                                   title="<?= $u['user_email'] ?> | Click to see User info">
                                    <span class="fa fa-user fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                                   title="Login as <?= $u['user_email'] ?>" target="_blank">
                                    <span class="fa fa-sign-in fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell"><? delete_button('volunteers', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="volunteers_.php?id=<?= $r['id'] ?>">
                            <b><?= htmlspecialchars($r['firstname'] . ' ' . $r['lastname']) ?></b>
                            <span class="rightcontent">Reg: <?= $u['user_email'] ?> | <?= date('M jS Y', strtotime($r['dateadded'])) ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
            <br>
            <a href="a.php?a=export-volunteers&pid=<?= $_GET['pid'] ?>&status=Pending" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Pending</a>
        </div>
    </div>
    <?

} else {
    $query = $db->prepare("SELECT * FROM products WHERE signuptype ORDER BY startdate DESC");
    $query->execute();
    $rr = $query->fetchAll();
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h1>Select Camp</h1>
            <ol class="treeview list root">
                <?php
                foreach ($rr as $r) {
                    $query = $db->prepare("SELECT * FROM volunteers WHERE pid = ? ORDER BY dateadded DESC");
                    $query->execute(array($r['id']));
                    $cc = $query->fetchAll();
                    ?>
                    <li class="no-nest">
                        <div class="row">
                            <a class="cell edit-link" href="volunteers.php?pid=<?= $r['id'] ?>">
                                <?= htmlspecialchars($r['title']) ?> <b>(<?= count($cc) ?> Volunteers)</b>
                            </a>
                        </div>
                    </li>
                <? } ?>
            </ol>
        </div>
    </div>
    <?
}
include 'common/footer.php';

