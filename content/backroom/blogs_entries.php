<?
include '../common/config.php' ;

$query = $db->prepare("SELECT * FROM blogs_entries WHERE id_blogs = ? ORDER BY date_posted DESC") ;
$query->execute(array($_GET['id_blogs'])) ;
$rr = $query->fetchAll() ;

$blog = sql_fetch_by_key($db, "blogs", "id", $_GET['id']) ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'blogs.'.$blog['id']) ){
	exit('Permission denied') ;
}

include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <? button('add', 'blogs_entries_.php?id_blogs='.$_GET['id_blogs'], 'Add Post') ; ?>
        <hr>
        <h4>Categories</h4>
        <? button('add', 'blog_tags_.php', 'Add Category'); ?>
        <? cms_show_sub_items($db, 'blog_tags', 0, [], false, true); ?>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1>Blog Posts</h1>

<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
        <ol class="treeview list root" cmsTable="products" cmsHierarchyMode="flat">
            <?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			if( $r['hidden'] ){
				$class .= ' dead' ;
			}
			?>
            <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                <div class="row">
                    <div class="buttons">
                        <div class="cell">
                            <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=hidden&amp;t=blogs_entries&amp;id=<?=$r['id'] ?>" title="Click to <?=$r['hidden']?'hide':'show' ?> in nav">
                                <?
                                if ($r['hidden']) {
                                    ?><span class="fa fa-eye-slash"></span><?
                                } else {
                                    ?><span class="fa fa-eye"></span><?
                                }
                                ?>
                            </a>
                        </div>
                        <div class="cell"><? delete_button('blogs_entries', $r['id']) ?></div>
                    </div>
                    <a class="cell edit-link <?= $r['hidden']?' dead':'' ?>" href="blogs_entries_.php?id_blogs=<?=$_GET['id_blogs'] ?>&id=<?= $r['id'] ?>">
                        <?= htmlspecialchars($r['title']) ?>
                        <span class="dead"><?= htmlspecialchars(date('F j g:i a', strtotime($r['date_posted']))) ?></span>
                    </a>
                </div>
			</li><?
		}
	?></ol><?
}
?>
    </div>
    </div>
<?
include 'common/footer.php' ;

