<?
include '../common/config.php';
include 'common/mod_upload_w.php';

// this page performs all actions available in the cms and redirects the brower to the appropriate url

$deletable = array('pages', 'permissions', 'admins', 'products', 'product_categories', 'users', 'directory', 'galleries', 'photos',
    'news', 'blogs', 'options', 'option_categories', 'blogs', 'blogs_entries', 'blog_tags', 'bkg_image','events','calendars','campers','orders');


// broad security check
if (!$_SESSION['admin']) {
    exit('Permission denied');
}

switch ($_GET['a']) {

    case 'logout' :
        unset($_SESSION['admin']);
        hle("./");
        break;

    case 'permissions-save' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        $table_name = 'permissions';
        $id = sql_upsert($db, $table_name, array('id_admins', 'id_items', 'item_type'), $_POST, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("admins_permissions_.php?id=$id");
        break;

    case 'flag-switch' :
        $whitelist = array(
            array('pages', 'navhide'),
            array('pages', 'secure'),
            array('adverts', 'display'),
            array('products', 'display'),
            array('photos', 'hidden'),
            array('options', 'display'),
            array('shipping_locations', 'display'),
            array('gallery_tags', 'display'),
            array('blogs_entries', 'hidden'),
        );
        $table = $_GET['t'];
        $flag = $_GET['f'];
        if (in_array(array($table, $flag), $whitelist)) {
            $query = $db->prepare("UPDATE `$table` SET `$flag` = NOT `$flag` WHERE id = ?");
            $query->execute(array($_GET['id']));
            m(ucwords($table) . ' updated');
        } else {
            e("Bad flag specification");
        }
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'admins-save' :
        $fields_to_update = array('username', 'notes');
        $values = $_POST;
        if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
            exit('Permission denied');
        }
        if (!$_POST['username']) {
            e("Username shouldn't be blank");
        }
        if ($_POST['password']) {
            $fields_to_update[] = 'hashed_password';
            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            $values['hashed_password'] = $hasher->HashPassword($_POST['password']);
        }


        $table_name = 'admins';
        $id = sql_upsert($db, $table_name, $fields_to_update, $values, 'id', $_POST['id']);

        //add normal permissions to bypass permission system
        if($id !=  1){
            sql_upsert($db, 'permissions', array('id_admins','item_type'), array('id_admins'=>$id,'item_type'=>'normal'), 'id_admins', $_POST['id']);
        }

        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'drafts-delete' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
            exit('Permission denied');
        }
        $id_pages = $_REQUEST['id'];
        $query = $db->prepare("DELETE FROM pages_vars WHERE id_pages = :id_pages AND version != ''");
        $query->execute(array(':id_pages' => $id_pages));
        m("Draft deleted");
        hle('pages_.php?id=' . $id_pages);
        break;

    case 'mass-delete' :
        $table = $_GET['t'];
        if (in_array($table, $deletable)) {
            foreach ($_POST['fields'] as $value) {
                $query = $db->prepare("DELETE FROM `$table` WHERE id = ?");
                $query->execute(array($value));
            }
            m(ucwords($table) . ' deleted');
            hle($_GET['return_url'] . ($_GET['past'] ? '?past=true' : ''));
        }
        break;
    case 'delete' :
        $table = $_GET['t'];
        if (!in_array($table, $deletable)) {
            exit('Bad delete specification');
        }
        // permission check
        switch ($table) {
            case "calendars":
                //delete events
                $query = $db->prepare("DELETE FROM `events` WHERE cid = ?");
                $query->execute(array($_GET['id']));
                break;
            case "orders":
                //delete orders
                $query = $db->prepare("DELETE FROM `order_items` WHERE id_orders = ?");
                $query->execute(array($_GET['id']));
                $query = $db->prepare("DELETE FROM `order_shipments` WHERE id_orders = ?");
                $query->execute(array($_GET['id']));
                break;
            case "campers":
                //delete events
                $query = $db->prepare("DELETE FROM `funding` WHERE cid = ?");
                $query->execute(array($_GET['id']));
                $query = $db->prepare("DELETE FROM `scholarship` WHERE cid = ?");
                $query->execute(array($_GET['id']));
                break;
            case 'pages':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM pages_vars WHERE id_pages = ?");
                $query->execute(array($_GET['id']));
                $query = $db->prepare("DELETE FROM pages_sections WHERE id_pages = ?");
                $query->execute(array($_GET['id']));
                break;
            case 'users':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM directory WHERE id_user = ?");
                $query->execute(array($_GET['id']));
                break;
            case 'options':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM product_options WHERE oid = ?");
                $query->execute(array($_GET['id']));
                break;

            case 'products':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                $query = $db->prepare("DELETE FROM product_options WHERE pid = ?");
                $query->execute(array($_GET['id']));
                $query = $db->prepare("DELETE FROM products_related WHERE pid = ? OR  pid_target = ?");
                $query->execute(array($_GET['id'],$_GET['id']));
                break;
            case 'bkg_image':
                $bkgdir = '../upload/background';
                $bkg_image = sql_fetch_by_key($db, 'bkg_image', "id", $_GET['id']);
                unlink($bkgdir.'/thumb/'.$bkg_image['image']);
                unlink($bkgdir.'/orig/'.$bkg_image['image']);
                unlink($bkgdir.'/2400/'.$bkg_image['image']);
                break;

            default:
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                break;
        }
        try {
            $query = $db->prepare("DELETE FROM `$table` WHERE id = ?");
            $query->execute(array($_GET['id']));
            m('Item deleted');
        } catch (Exception $e) {
            e("Couldn't delete item - make sure it doesn't have any dependencies.");
        }
        hle($_GET['return_url']);
        break;

    case 'copy' :
        $table = $_GET['t'];
        if (!in_array($table, $deletable)) {
            exit('Bad copy specification');
        }
        // permission check
        switch ($table) {
            case 'products':
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }

                try {
                    $newid = db_row_copy($db, $table, $_GET['id'], 'id', array('gallery_id' => ''));

                    $query = $db->prepare("SELECT * FROM product_options WHERE pid = ?");
                    $query->execute(array($_GET['id']));
                    $oo = $query->fetchAll();
                    foreach ($oo as $o) {
                        db_row_copy($db, "product_options", $o['id'], 'id', array('pid' => $newid));
                    }

                    $query = $db->prepare("SELECT * FROM options WHERE id_product = ?");
                    $query->execute(array($_GET['id']));
                    $oo = $query->fetchAll();
                    foreach ($oo as $o) {
                        db_row_copy($db, "options", $o['id'], 'id', array('id_product' => $newid));
                    }
                    m("Copy successful");

                } catch (Exception $e) {
                    e("Error occurred: " . $e->getMessage());
                }
                break;

            default:
                if (!admin_perm_check($db, $_SESSION['admin'], $table, $_GET['id'])) {
                    exit('Permission denied');
                }
                try {
                    db_row_copy($db, $table, $_GET['id']);
                    m("Copy successful");
                } catch (Exception $e) {
                    e("Error occurred: " . $e->getMessage());
                }
                break;
        }
        hle($_GET['return_url']);
        break;
    case 'account-password':
        if (strlen($_POST['new_password']) < 6) {
            e("New password must be at least 6 characters long.");
        }
        if (!$_SESSION['e']) {
            try {
                $hasher = new Hautelook\Phpass\PasswordHash(8, false);
                $pw = $hasher->HashPassword($_POST['new_password']);
                $query = $db->prepare("UPDATE admins SET hashed_password = :pw WHERE id = :admin_id");
                $query->execute(array(
                    ':pw' => $pw,
                    ':admin_id' => $_SESSION['admin']['id']
                ));
                m('Password updated');
            } catch (Exception $e) {
                e("Database error while updating password.");
            }
        }
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'blogs-save' :
        $table_name = 'blogs';
        $checkbox_fields = array();
        $fields = array('title', 'keyword', 'index_entries', 'description');

        // validation
        if (!$_POST['keyword']) {
            e("Location is required - the location keyword is used for the blog's address");
        }
        $values = sql_organize_values($fields, $_POST, $checkbox_fields);

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'blogs_entries-save' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'blogs.' . $_POST['id_blogs'])) {
            exit('Permission denied');
        }

        $table_name = 'blogs_entries';
        $basedir_blog_images = '../upload/blogs';
        $checkbox_fields = array('hidden');
        $fields = array('content', 'id_blogs', 'title', 'keyword', 'summary', 'date_posted', 'date_modified','hidden');
        $_POST['date_modified'] = date('Y-m-d H:i:s');
        $raw_values = $_POST;

        // resize image if provided
        if ($_FILES['image']['name']) {
            list($image) = relocateImage($_FILES['image']['tmp_name'], array(
                array('width' => 250, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/thumb"),
                array('width' => 400, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/400"),
                array('width' => 700, 'height' => 500, 'function' => 'resizeprop', 'name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/600"),
                array('name' => $_FILES['image']['name'], 'dir' => "$basedir_blog_images/orig"),
            ));
            $fields[] = 'image';
            $raw_values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $fields[] = 'image';
            $raw_values['image'] = '';
        }

        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id'], $insert_only);


        //edit product options
        $query = $db->prepare("DELETE FROM blog_entry_tags WHERE bid = ?");
        $query->execute(array($id));

        foreach ($_POST['option'] as $option) {
            sql_upsert($db, "blog_entry_tags", array('tid', 'bid'), array('tid' => $option, 'bid' => $id), 'id', '');
        }


        m("Saved at " . $config['date']);
        hle("$table_name" . "_.php?id_blogs=" . $_POST['id_blogs'] . "&id=$id");
        break;

    case 'blog_tags-save' :
        $table_name = 'blog_tags';
        $fields = array('title');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("blog_tags_.php?id=$id");
        break;

    case 'pages-copy' :
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
            exit('Permission denied');
        }
        $page = sql_fetch_by_key($db, 'pages', 'id', $_GET['id']);
        $db->beginTransaction();
        try {
            $new_page_id = cms_deep_page_copy($db, $page['id'], $page['id_parent']);
            $db->commit();
            m("Copy successful");
            $_SESSION['selectors_to_highlight'][] = "#pages-$new_page_id>div";
        } catch (Exception $e) {
            $db->rollback();
            e("Error occurred: " . $e->getMessage());
        }
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'pages-save' :
        if ($_POST['id'] and !admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id'])) {
            exit('Permission denied');
        }
        $id_pages = $_POST['id'];
        $page = get_page($db, array('id' => $_POST['id']));
        $template = $page['template'];
        $template_save_path = "../templates/$template" . "_save.php";
        if (!$page) {
            exit('Page with ID ' . $_POST['id'] . ' not found for saving');
        }
        if (!$config['templates'][$template]) {
            exit('Template "' . $template . '" not found');
        }
        if (!is_file($template_save_path)) {
            include "../templates/page_save.php";
        }
        $return_url = "pages_.php?id={$_POST['id']}";
        include $template_save_path;
        m("Saved at " . $config['date']);
        hle($return_url);
        break;

    case 'pages-config' :
        if ($_POST['id'] and !admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id'])) {
            exit('Permission denied');
        }
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['id_parent'])) {
            exit('Permission denied - can\'t move a page to be a child of a page you don\'t have permission to edit');
        }
        $table_name = 'pages';
        $checkbox_fields = array('secure_provider', 'navhide', 'hide_h1', 'hide_in_breadcrumbs', 'hide_breadcrumbs', 'show_subnav', 'comments_disabled', 'footershow');

        if($_POST['secure_provider']){
            $checkbox_fields[] = 'secure';
            $_POST['secure'] = 1;
        }

        $fields = array_merge($checkbox_fields, array('image', 'id_parent', 'keyword', 'title', 'type', 'frontend_include', 'link', 'template', 'meta_title', 'meta_description'));

        // validation
        if (!$_POST['keyword']) {
            e("Location is required - the location keyword is used for the page's address");
        }
        if (!$_POST['template']) {
            e("Pages can't be displayed without a template");
        }
        // check for duplicate keyword
        $query = $db->prepare("SELECT * FROM pages WHERE keyword = ? AND id != ?");
        $query->execute(array($_POST['keyword'], $_POST['id']));
        $page_with_same_keyword = $query->fetch();
        if ($page_with_same_keyword) {
            e("Another page uses the same location: <a href=\"pages_.php?id={$page_with_same_keyword['id']}\">{$page_with_same_keyword['title']}</a>");
        }

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("pages_config_.php?id=$id");
        break;

    case 'settings-save' :
        if ($_SESSION['test']) {
            $sql = "INSERT INTO settings SET
					name = :name,
					title = :title,
					description = :description,
					value = :value,
					date_updated = NOW(),
					id = :id
				ON DUPLICATE KEY UPDATE
					name = :name,
					title = :title,
					description = :description,
					value = :value,
					date_updated = NOW()";
            $params = array(
                ':name' => $_POST['name'],
                ':title' => $_POST['title'],
                ':description' => $_POST['description'],
                ':value' => trim($_POST['value']),
                ':id' => $_POST['id'],
            );
        } else {
            $sql = "UPDATE settings SET
					value = :value,
					date_updated = NOW()
				WHERE id = :id";
            $params = array(
                ':value' => trim($_POST['value']),
                ':id' => $_POST['id'],
            );
        }
        $query = $db->prepare($sql);
        $query->execute($params);
        m("Saved at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'prioritize' :
        // need to have permission to edit the root element of the tree which they're editing
        if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_POST['root_element_id'])) {
            exit('Permission denied');
        }
        $allowed_tables = array('products', 'product_categories', 'pages', 'photos', 'options', 'option_categories');

        $table_name = $_POST['table'];
        $hierarchy_mode = $_POST['mode'];

        if (!in_array($table_name, $allowed_tables)) {
            exit('Not allowed to sort that table.');
        }
        if (!in_array($hierarchy_mode, array('nested', 'flat'))) {
            exit('Heirarchy mode not allowed');
        }

        $positions_arr = $_POST['priorityitems'];

        if ($hierarchy_mode == 'nested') {
            $query = $db->prepare("UPDATE `$table_name` SET priority = :priority, id_parent = :id_parent WHERE id = :id");
        } else {
            $query = $db->prepare("UPDATE `$table_name` SET priority = :priority                         WHERE id = :id");
        }
        $i = 0;

        foreach ($positions_arr as $id => $id_parent) {
            $i++;
            if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $id)) {
                exit('Permission denied: trying to move #' . $id);
            }
            $params = array(
                ':priority' => $i,
                ':id' => $id,
            );

            if ($hierarchy_mode == 'nested') {
                if ($id_parent == 'root') {
                    $id_parent = $_POST['root_element_id'];
                }
                // need to have permission to edit both this element's newly-specified parent to add it as a sub-item
                if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $id_parent)) {
                    exit('Permission denied: trying to move an item under #' . $id_parent);
                }
                $params[':id_parent'] = $id_parent;
            }

            $query->execute($params);
            print_r($db->errorInfo(), true);
        }
        m("Positions saved.");
        break;

    case 'photos-save' :
        $table_name = 'photos';
        $basedir = "../upload/photos";
        $fields = array('id_parent', 'title', 'content', 'image','alt');
        $values = $_POST;

        // resize image if provided
        if ($_FILES['file']['name']) {
            list($image) = relocateImage($_FILES['file']['tmp_name'], array(
                array('width' => 100, 'height' => 100, 'function' => 'resizethumb', 'name' => $_FILES['file']['name'], 'dir' => "$basedir/thumb"),
                array('width' => 260, 'height' => 270, 'function' => 'resizethumb', 'name' => $_FILES['file']['name'], 'dir' => "$basedir/260"),
                array('width' => 460, 'height' => 485, 'function' => 'resizethumb', 'name' => $_FILES['file']['name'], 'dir' => "$basedir/460"),
                array('width' => 400, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/400"),
                array('width' => 800, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/800"),
                array('width' => 1600, 'name' => $_FILES['file']['name'], 'dir' => "$basedir/1600"),
                array('name' => $_FILES['file']['name'], 'dir' => "$basedir/orig"),
            ));
            $values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $values['image'] = $image;
        } else {
            unset($fields['image']);
        }
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);


        $urladdon = '';
        if ($_GET['pid']) {
            $urladdon = 'pid=' . $_GET['pid'] . '&';
        }

        #if submitted through form add save message and redirect, otherwise dropzone was used.
        if ($_POST['submit']) {
            m("Saved at " . $config['date']);
            if ($_POST['submit'] == 'Save and add another') {
                hle("$table_name" . "_.php?{$urladdon}id_parent={$_POST['id_parent']}");
            } else {
                hle("$table_name" . "_.php?{$urladdon}id={$id}");
            }
        }
        break;

    case 'galleries-set-main':
        $photo = sql_fetch_by_key($db, 'photos', 'id', $_GET['id']);
        if (!$photo) {
            exit('Photo not found.  ID provided: ' . $_GET['id']);
        }
        try {
            $query = $db->prepare("UPDATE photos SET featured = 0 WHERE id_parent = :id_parent AND type = :type");
            $query->execute(array(
                ':id_parent' => $photo['id_parent'],
                ':type' => $photo['type']
            ));
            $query = $db->prepare("UPDATE photos SET featured = 1 WHERE id = :id");
            $query->execute(array(
                ':id' => $_GET['id']
            ));
        } catch (Exception $e) {
            exit($e->getMessage());
        }
        echo "Main photo was updated.";
        break;

    case 'galleries-save':
        $table_name = 'galleries';
        $id = sql_upsert($db, $table_name, array('title','description'), $_POST, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'calendars-save' :
        $table_name = 'calendars';
        $fields = array('title', 'color');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'events-save' :
        $table_name = 'events';

        $checkbox_fields = array('allday');
        $fields = array('cid', 'title', 'synopsis', 'content', 'eventdate', 'startdate', 'timetext', 'allday');

        if(!$_POST['startdate']){
            $_POST['startdate'] = $_POST['eventdate'];
        }

        if(!$_POST['eventdate'] || $_POST['eventdate'] < $_POST['startdate']){
            $_POST['eventdate'] = $_POST['startdate'];
        }


        $_POST['startdate'] = date('Y-m-d H:i:s', strtotime($_POST['startdate'] . ' ' . $_POST['eventtime']));
        $_POST['eventdate'] = date('Y-m-d H:i:s', strtotime($_POST['eventdate'] . ' ' . $_POST['eventtime']));

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
//        $dates = $_POST['eventdate'];
//        $eid = 0;
//        foreach ($dates as $date) {
//            $_POST['eventdate'] = date('Y-m-d H:i:s', strtotime($date . ' ' . $_POST['eventtime']));
//
//            $values = sql_organize_values($fields, $_POST, $checkbox_fields);
//            $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
//            if (!$eid) {
//                $eid = $id;
//            }
//        }

        m("Saved at " . $config['date']);
        hle($table_name . "_.php?cal=" . $_POST['cid'] . "&id=$id");
        break;
    case 'product_categories-save' :
        $table_name = 'product_categories';
        $fields = array('title', 'id_parent');

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("product_categories_.php?id=$id");
        break;

    case 'products-save' :
        $product = sql_fetch_by_key($db, 'products', 'id', $_POST['id']);
        $table_name = 'products';
        $checkbox_fields = array('taxable');
        $fields = array('title','signuptype','contact_email','reciept_text','thankyou_text', 'showdate', 'showdate_volunteer', 'startdate', 'financialneedcode', 'keyword', 'price', 'minimumdown','salepercent','saleoff', 'shipping', 'description', 'id_categories', 'taxable');

        if(!$_POST['keyword']){
            e('Location is required');
            hle("products_.php?id=".$_POST['id']);
        }

        //strip non numeric characters from number values
        $_POST['price'] = preg_replace("/[^0-9.]/", '', $_POST['price']);
        $raw_values = $_POST;

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        //add all relationship entries
//        foreach ($_POST['option'] as $option) {
//            $k =  array('oid', 'pid', 'addprice');
//            $v = array('oid' => $option, 'pid' => $id, 'addprice' => $_POST['addprice'][$option]);
//            sql_upsert($db, "product_options", $k, $v, 'id', '');
//        }

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("products_.php?id=$id");
        break;

    case 'product_single-save' :
        $product = sql_fetch_by_key($db, 'products', 'id', $_POST['id']);
        $table_name = 'products';
        $checkbox_fields = array('taxable');
        $fields = array('title', 'price', 'minimumdown','description', 'id_categories', 'taxable');
        //strip non numeric characters from number values
        $_POST['price'] = preg_replace("/[^0-9.]/", '', $_POST['price']);
        $_POST['xsize'] = preg_replace("/[^0-9.]/", '', $_POST['xsize']);
        $_POST['ysize'] = preg_replace("/[^0-9.]/", '', $_POST['ysize']);
        $_POST['zsize'] = preg_replace("/[^0-9.]/", '', $_POST['zsize']);
        $_POST['weight'] = preg_replace("/[^0-9.]/", '', $_POST['weight']);

        $raw_values = $_POST;

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("product_single_.php?id=$id");
        break;
    case 'option_categories-save' :
        $table_name = 'option_categories';
        $checkbox_fields = array();
        $fields = array('title','option_id','description');

        $values = sql_organize_values($fields, $_POST, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle("option_categories_.php?id=$id");
        break;

    case 'options-save' :
        $options = sql_fetch_by_key($db, 'options', 'id', $_POST['id']);
        $table_name = 'options';
        $checkbox_fields = array();
        $fields = array('title', 'id_categories','content', 'keyword');
        $_POST['keyword'] = toAscii($_POST['title']);
        $raw_values = $_POST;

        // save product record
        $values = sql_organize_values($fields, $raw_values, $checkbox_fields);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        //edit product options
        $query = $db->prepare("DELETE FROM option_hide WHERE oid = ?");
        $query->execute(array($id));
        foreach ($_POST['option'] as $option) {
            sql_upsert($db, "option_hide", array('hide_oid', 'oid'), array('hide_oid' => $option, 'oid' => $id), 'id', '');
        }

        if ($id) {
            m("Saved at " . $config['date']);
        } else {
            e("Problem saving product");
        }
        hle("options_.php?id=$id");
        break;

    case 'campers-save' :
        $table_name = 'campers';

//        $fields = $camperfieldsstix;
        $fields = array();
        $fields[] = 'uid';
        $fields[] = 'pledged';
        $fields[] = 'status';

        $values = $_POST;

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id&pid=".$_POST['pid']);
        break;

    case 'volunteers-save' :
        $table_name = 'volunteers';

        $fields = array();
        $fields[] = 'uid';
        $fields[] = 'status';

        $values = $_POST;

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id&pid=".$_POST['pid']);
        break;


    case 'orders-save' :
        $table_name = 'orders';
        $fields = array('notes',);
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'get_permission_item_selector':
        $permission_item_type = sql_fetch_by_key($db, 'permissions_item_types', "keyword", $_GET['item_type']);
        $table = $permission_item_type['table'];
        if (!$table) {
            exit();
        } else {
            echo $permission_item_type['title'] . "<br/>";
            if ($permission_item_type['tree']) {
                $rows = cms_get_sub_items_list($db, $table, 0);
                foreach ($rows as $key => $row) {
                    $rows[$key]['title'] = str_repeat("&nbsp;", 3 * $row['depth']) . $row['title'];
                }
            } else {
                $rows = $db->query("SELECT * FROM `$table` ORDER BY title ASC");
            }

            write_select(array(
                'db' => $db,
                'rows' => $rows,
                'label' => 'title',
                'value' => 'id',
                'current' => $_GET['current'],
                'name' => 'id_items',
            ));
        }
        break;

    case 'util_page_keyword_find_replace':
        sql_modify_recursively($db, 'pages', 'id', 'id_parent', $_REQUEST['id_parent'], function (PDO $db, $row) {
            $new_keyword = str_replace($_REQUEST['find'], $_REQUEST['replace'], $row['keyword']);
            $query = $db->prepare("UPDATE pages SET keyword = ? WHERE id = ?");
            $query->execute(array($new_keyword, $row['id']));
        });
        m("Update complete");
        hle($_SERVER['HTTP_REFERER']);
        break;

    case 'news-save':
        $table_name = 'news';
        $fields = array('title', 'description', 'content', 'date_posted', 'homepage');
        $checkbox_fields = array('homepage');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'gallery_tag_cats-save':
        $table_name = 'gallery_tag_cats';
        $_POST['safename'] = safename($_POST['title']);
        $fields = array('title', 'safename');

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . ".php?id=$id");
        break;
    case 'gallery_tags-save':
        $table_name = 'gallery_tags';
        $_POST['safename'] = safename($_POST['title']);
        $fields = array('cat','title', 'safename');

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'discounts-save':
        $table_name = 'discounts';
        $fields = array('title', 'code', 'percent', 'minimum', 'dollaroff','expires');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'shipping_locations-save':
        $table_name = 'shipping_locations';
        $fields = array('price');
        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'orders-resend-receipt' :
        if( ! $_POST['email'] ){
            e('Email is required to resend receipt') ;
            hle("orders_.php?id={$_POST['id_orders']}") ;
        }
        $order = sql_fetch_by_key($db, 'orders', "id", $_POST['id_orders']);
        if( ! $order ){
            e('Order not found') ;
            hle("orders_.php?id={$_POST['id_orders']}") ;
        }
        smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $_POST['email'], "Order #$ordernumber", $order['receipt']);

        m("Receipt was sent to {$_POST['email']}") ;
        hle("orders_.php?id={$_POST['id_orders']}") ;
        break ;

    case 'orders-send-tracking-info' :
        // get some data about carrier & order
        $carrier = sql_fetch_by_key($db, 'shipping_carriers', "id", $_POST['id_shipping_carriers']);
        $order = sql_fetch_by_key($db, 'orders', "id", $_POST['id_orders']);
        $date_shipped = date_compose('date_shipped', $_POST) ;

        // prepare email
        $tracking_link = str_replace('%s', $_POST['tracking_number'], $carrier['link_format']) ;
        $email_to      = $_POST['email'] ;
        $email_subject = "Order #{$order['ordernumber']} Shipped" ;

        $email_message = "<p>Good news: all or part of your order was shipped on ".date('F j', strtotime($date_shipped)).".  Use this link to track your order: <a href=\"$tracking_link\">$tracking_link</a>.</p>" ;
        if( $_POST['notes'] ){
            $email_message .= "<p><b>Notes</b></p><p>".nl2br(htmlspecialchars(stripslashes($_POST['notes'])))."</p>" ;
        }
        $email_message .= "<p>For your reference, your original receipt is copied below.</p><div>{$order['receipt']}</div>" ;
        $email_headers = $config['smtp_conf'] ;

        // send email
        smtp_mail("\"{$config['company_name']}\"<{$config['receipt_contact']}>", $_POST['email_to'], $email_subject, $email_message);

        // save db record
        $fields = array('id_orders', 'id_shipping_carriers', 'carrier_title', 'email_to', 'notes', 'tracking_number',
            'tracking_link', 'date_shipped', 'email_content', 'date_email_sent');
        $_POST['carrier_title'] = $carrier['title'];
        $_POST['tracking_link'] = $tracking_link;
        $_POST['date_shipped'] = $date_shipped;
        $_POST['email_content'] = $email_message;
        $_POST['date_email_sent'] = date('Y-m-d H:i:s');

        $values = sql_organize_values($fields, $_POST);

        $id = sql_upsert($db, 'order_shipments', $fields, $values, 'id', $_POST['id']);

        // wrap up
        m("Shipping info saved and sent at $g_date") ;
        hle("orders_.php?id={$_POST['id_orders']}") ;
        break ;
    case 'directory-save':
        $table_name = 'directory';

        $fields = array(
            'name', 'keyword', 'description', 'url', 'phone', 'email', 'address', 'address_2',
            'city', 'state', 'zip', 'hours_location', 'hours_dispatch', 'id_user', 'coordinates','loc_address',
            'loc_city', 'loc_state', 'loc_zip','insured','additionalinfo','preferredmanufacturers');

        if(!$_POST['keyword']){
            $_POST['keyword'] = getKeyword($table_name, trim($_POST['name'].'-'.$_POST['city'].'-'.$_POST['state'],'-'), $_POST['id']);
        }else{
            $_POST['keyword'] = getKeyword($table_name, $_POST['keyword'], $_POST['id']);
        }

        $basedir_logo_images = '../upload/directory';

        $values = sql_organize_values($fields, $_POST);

        // resize image if provided
        if ($_FILES['image']['name']) {
            list($image) = relocateImage($_FILES['image']['tmp_name'], array(
                array('width' => 150, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_logo_images/thumb"),
                array('width' => 300, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_logo_images/300"),
                array('width' => 600, 'name' => $_FILES['image']['name'], 'dir' => "$basedir_logo_images/600"),
                array('name' => $_FILES['image']['name'], 'dir' => "$basedir_logo_images/orig"),
            ));
            $fields[] = 'image';
            $values['image'] = $image;
        } elseif ($_POST['image_clear']) {
            $fields[] = 'image';
            $values['image'] = '';
        }

        $basedir_warranty = '../upload/directory/file/';
        // resize image if provided
        if ($_FILES['warranty_file']['name']) {
            list($warranty_file) = relocateFile($_FILES['warranty_file']['tmp_name'], array(
                array('name' => $_FILES['warranty_file']['name'], 'dir' => $basedir_warranty),
            ));
            $fields[] = 'warranty_file';
            $values['warranty_file'] = $_FILES['warranty_file']['name'];
        } elseif ($_POST['warranty_clear']) {
            $fields[] = 'warranty_file';
            $values['warranty_file'] = '';
        }

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;
    case 'scholarship-save':

        $r = sql_fetch_by_key($db, 'scholarship', 'id', $_POST['id']);
        $c = sql_fetch_by_key($db, 'campers', 'id', $_POST['cid']);
        $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
        $u = sql_fetch_by_key($db, 'users', 'id', $c['uid']);

        $table_name = 'scholarship';

        $fields = array(
            'cid',
            'status',
            'thankyousent',
            'note',
            'amount',
            'request',
            'parent_name',
            'parent_email',
            'parent_address',
            'parent_city',
            'parent_state',
            'parent_zip',
            'how_many_adults',
            'how_many_children',
            'ages_of_children',
            'school_lunch',
            'school_lunch_other',
            'annual_income',
            'content');

        $values = sql_organize_values($fields, $_POST);

        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);

        if($_POST['emailcamper'] && $_POST['status']){
            $subject = 'Camp STIX scholarship request';
            if($_POST['status'] == 1){
                $message = 'Your scholarship request of $'.$r['request'].' for '.
                    $c['firstname'].' '.$c['lastname'].' has been approved for $'.$_POST['amount'].'.  <br>'.
                    'Please log into your account at the <a href="'.$config['site_address'].'/account">Camp STIX site</a> to pay any pending balance.  <br><br>';
            }elseif($_POST['status'] == 2){
                $message = 'Your scholarship request of $'.$r['request'].' for '.
                    $c['firstname'].' '.$c['lastname'].' has been denied.  <br>'.
                    'If you have any questions about this please <a href="'.$config['site_address'].'/contact">contact us</a> for more information.  <br><br>';
            }
            if($_POST['comments']){
                $message .= '<b>Additional Notes From Admin</b>'.$_POST['comments'];
            }
            if($u['user_email']){
                smtp_mail($config['email_sender'], $u['user_email'], $subject, formattedemail($message));
            }
            smtp_mail($config['email_sender'], $config['email_contact'], $subject, formattedemail($message));
            m("Account email sent ");

        }

        if($_POST['status'] == 1){
            $s = sql_fetch_by_key($db, 'scholarship', 'id', $id);
            //check if paid off and mark status as paid
            $due = $p['price'] - $c['pledged'] - $s['amount'];
            if($due < 1 && $c['status']!='Paid'){
                sql_upsert($db, 'campers',  array('status'),  array('status'=>'Paid'), 'id', $c['id']);
            }
        }

        m("Saved at " . $config['date']);
        if($_POST['id']){
            hle($table_name . ".php");
        }else {
            hle($table_name . "_.php?id=".$id);
        }
        break;
    case 'camper-manual-payment':
        $table_name = 'orders';
        $fields = array('id_camper','order_grand_total','purchase_type','bi_firstname','bi_lastname','notes','receipt','order_date','ordernumber');
        $_POST['receipt'] = '<h1>Manual Payment, no receipt</h1>';
        $_POST['order_date'] = date('Y-m-d H:i:s');
        $_POST['ordernumber'] = 'Manual Payment';
        if($_POST['checknumber']){
            $_POST['purchase_type'] .= ' #'.$_POST['checknumber'];
        }

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', '');

        //update camper info to credit the new payment
        $r = sql_fetch_by_key($db, 'campers', "id", $_POST['id_camper']);
        $p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);
        $s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);

        $pledged = $r['pledged'] + $_POST['order_grand_total'];

        $fields2 = array('pledged');
        $values2 = array('pledged'=>$pledged);
        if($pledged+$s['amount'] >= $p['price']){
            $fields2[] = 'status';
            $values2['status'] = 'Paid';
        }
        $id = sql_upsert($db, 'campers', $fields2, $values2, 'id', $r['id']);


        m("Payment Submitted " . $config['date']);
        hle("campers_.php?id=".$_POST['id_camper']);
        break;

    case 'users-save':
        $table_name = 'users';

        $fields = array('user_name', 'user_email', 'display_name');


        $query = $db->prepare("SELECT * FROM users WHERE user_name = ? AND id != ?");
        $query->execute(array($_POST['user_name'], $_GET['id']));
        $rr = $query->fetchAll();
        if (count($rr)>0) {
            e('Selected user name already exits');
            $_POST['user_name'] = $_POST['user_name'].randstring(5);
        }

        $query = $db->prepare("SELECT * FROM users WHERE user_email = ? AND id != ?");
        $query->execute(array($_POST['user_email'], $_GET['id']));
        $rr = $query->fetchAll();
        if (count($rr)>0) {
            e('Selected email address is used by another account');
            $_POST['user_email'] = '';
        }

        if(!$_POST['id']){
            $fields[] = 'user_registered';
            $_POST['user_registered'] = date('Y-m-d H:i:s');
        }

        if ($_POST['user_pass']) {
            $fields[] = 'user_pass';
            $hasher = new Hautelook\Phpass\PasswordHash(8, false);
            $_POST['user_pass'] = $hasher->HashPassword($_POST['user_pass']);
        }

        $values = sql_organize_values($fields, $_POST);
        $id = sql_upsert($db, $table_name, $fields, $values, 'id', $_POST['id']);
        m("Saved at " . $config['date']);
        hle($table_name . "_.php?id=$id");
        break;

    case 'user-approve':
        $u = sql_fetch_by_key($db, 'users', 'id', $_GET['id']);

        $message_approve = 'Welcome to the Espresso Service Network "'.$u['businessname'].'",'.$message_approve;

        $email_body = $message_approve; //set at top of this doc

        $subject = 'Account Approval';
        smtp_mail($config['email_sender'], $u['user_email'], $subject, formattedemail($email_body));

        $id = sql_upsert($db, 'users', array('user_status'), array('user_status'=>1), 'id', $_GET['id']);
        m("User approved at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'user-disable':
        $u = sql_fetch_by_key($db, 'users', 'id', $_GET['id']);

        $id = sql_upsert($db, 'users', array('user_status'), array('user_status'=>-1), 'id', $_GET['id']);
        m("User approved at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'user-deny':
        $u = sql_fetch_by_key($db, 'users', 'id', $_GET['id']);

        $email_body = $message_deny; //set at top of this doc

        $subject = 'Account Denial';
        smtp_mail($config['email_sender'], $u['user_email'], $subject, formattedemail($email_body));

        $id = sql_upsert($db, 'users', array('user_status'), array('user_status'=>-1), 'id', $_GET['id']);
        m("User approved at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'user-bulk':
        if($_POST['action']=="approve"){
            $email_body = $message_approve; //set at top of this doc
            $subject = 'Account Approval';
            $user_status = '1';
        }elseif($_POST['action']=="deny"){
            $email_body = $message_deny; //set at top of this doc
            $subject = 'Account Denial';
            $user_status = '-1';
        }
        $email_body = formattedemail($email_body);
        foreach($_POST['user'] as $uid){

            $u = sql_fetch_by_key($db, 'users', 'id', $uid);
            sql_upsert($db, 'users', array('user_status'), array('user_status'=>$user_status), 'id', $uid);
            smtp_mail($config['email_sender'], $u['user_email'], $subject, 'Welcome to the Espresso Service Network "'.$u['businessname'].'",'.$email_body);

        }

        m("User status changed at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'thankyou-bulk':
        foreach($_POST['scholarship'] as $id){
            $u = sql_fetch_by_key($db, 'scholarship', 'id', $id);
            sql_upsert($db, 'scholarship', array('thankyousent'), array('thankyousent'=>1), 'id', $id);
        }

        m("Thank You status changed at " . $config['date']);
        hle($_SERVER['HTTP_REFERER']);
        break;
    case 'export-campers':
        $p = sql_fetch_by_key($db, 'products', 'id', $_GET['pid']);

        switch($_GET['status']){
            case "Paid":
                $filename = 'Paid_List-'.safename($p['title']);
                $where = "pid=? AND status=?";
                $wherearray = array($_GET['pid'],"Paid");
                break;
            case "Registered":
                $filename = 'Unpaid_List-'.safename($p['title']);
                $where = "pid=? AND status=?";
                $wherearray = array($_GET['pid'],"Registered");
                break;
            default:
                $filename = 'Full_List-'.safename($p['title']);
                $where = "pid=? AND (status='Registered' OR status='Paid')";
                $wherearray = array($_GET['pid']);
                break;
        }

        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Camper_List-'.safename($p['title']).'.csv');


        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $removethese = array('pid', 'uid');

        if($p['signuptype'] == 1){
            $camperfields = $camperfieldsstix;
        }elseif($p['signuptype'] == 2){
            $camperfields = $camperfieldstwigs;
        }else{
            $camperfields = $camperfieldsbasic;
        }

        $fields = array_diff($camperfields, $removethese);


        // output the column headings
        fputcsv($output, array_merge($fields,array('Age','Date Added','Approved Scholarship')));

        // fetch the data
        $query = $db->prepare("SELECT ".implode(',',$fields).", id, TIMESTAMPDIFF(YEAR, bdate, CURDATE()) AS age, dateadded FROM campers WHERE $where ORDER BY dateadded DESC");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
            $s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);
            unset($r['id']);
            if($s){
                $r[] = $s['amount'];
            }else{
                $r[] = 0;
            }
            fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
    case 'export-volunteers':
        $p = sql_fetch_by_key($db, 'products', 'id', $_GET['pid']);

        switch($_GET['status']){
            case "Approved":
                $filename = 'Approved_List-'.safename($p['title']);
                $where = "pid=? AND status=?";
                $wherearray = array($_GET['pid'],"Approved");
                break;
            case "Pending":
                $filename = 'Pending_List-'.safename($p['title']);
                $where = "pid=? AND status=?";
                $wherearray = array($_GET['pid'],"Pending");
                break;
            default:
                $filename = 'Full_List-'.safename($p['title']);
                $where = "pid=? AND (status='Registered' OR status='Paid')";
                $wherearray = array($_GET['pid']);
                break;
        }

        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Volunteer_List-'.safename($p['title']).'.csv');


        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $removethese = array('pid', 'uid');

        if($p['signuptype'] == 1){
            $volunteerfields = $volunteerfieldsstix;
        }elseif($p['signuptype'] == 2){
            $volunteerfields = $volunteerfieldstwigs;
        }else{
            $volunteerfields = $volunteerfieldsbasic;
        }

        $fields = array_diff($volunteerfields, $removethese);


        // output the column headings
        fputcsv($output, array_merge($fields,array('Age','Date Added')));

        // fetch the data
        $query = $db->prepare("SELECT ".implode(',',$fields).", id, TIMESTAMPDIFF(YEAR, bdate, CURDATE()) AS age, dateadded FROM volunteers WHERE $where ORDER BY dateadded DESC");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
            unset($r['id']);
            fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
    case 'export-orders':
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Orders-'.date('Y-m-d').'.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $where = 'id';
        $wherearray = array();
        if($_POST['startdate'] && $_POST['enddate']){
            $where .= " AND order_date >= ? AND order_date <= ?";
            $wherearray[] = date('Y-m-d H:i:s',strtotime($_POST['startdate']));
            $wherearray[] = date('Y-m-d H:i:s',strtotime($_POST['enddate']));
        }

        $fields = array(
            'purchase_type',
            'order_grand_total',
            'bi_firstname',
            'bi_lastname',
            'bi_address',
            'bi_city',
            'bi_state',
            'bi_zip',
            'bi_email',
            'bi_phone',
            'order_date',
            'transaction_id',
            'ordernumber',
            'notes'
        );

        // output the column headings
        fputcsv($output, $fields);

        // fetch the data
        $query = $db->prepare("SELECT ".implode(',',$fields)." FROM orders WHERE $where ORDER BY order_date DESC");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
            fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
    case 'export-scholarships':
        $p = sql_fetch_by_key($db, 'products', 'id', $_POST['camp']);
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Scholarships-'.safename($p['title']).'-'.date('Y-m-d').'.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $where = ' c.pid = ? ';
        $wherearray = array($_POST['camp']);

        //special Thank You's not sent export
        if($_POST['status']==99) {
            $where .= " AND s.status = ?";
            $wherearray[] = 1;
            $where .= " AND s.thankyousent = ?";
            $wherearray[] = 0;

        }elseif($_POST['status']!==''){
            $where .= " AND s.status = ?";
            $wherearray[] = $_POST['status'];
        }

        $fields = array(
            'request',
            'amount',
            'parent_name',
            'parent_email',
            'parent_address',
            'parent_city',
            'parent_state',
            'parent_zip',
            'how_many_adults',
            'how_many_children',
            'ages_of_children',
            'school_lunch',
            'school_lunch_other',
            'annual_income',
            'content',
            'note',
            'thankyousent',
            'requestdate'
        );

        // output the column headings
        fputcsv($output, array_merge(array('camper_firstname','camper_lastname'),$fields));

        // fetch the data
        $query = $db->prepare("SELECT c.firstname, c.lastname, s.".implode(', s.',$fields)." FROM scholarship s, campers c WHERE s.cid = c.id AND $where ORDER BY s.requestdate DESC");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
            fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
    case 'export-cabinmate':
        $p = sql_fetch_by_key($db, 'products', 'id', $_GET['camp']);
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=cabinmate-'.safename($p['title']).'-'.date('Y-m-d').'.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $where = ' c.pid = ? ';
        $wherearray = array($_GET['camp']);


        $fields = array(
            'firstname1',
            'lastname1',
            'age1',
            'gender1',
            'firstname2',
            'lastname2',
            'age2',
            'gender2',
            'requestdate'
        );

        // output the column headings
        fputcsv($output, array_merge(array('requested by'),$fields));
        // fetch the data
        $query = $db->prepare("SELECT CONCAT(c.firstname, ' ', c.lastname) AS requested, cm.".implode(', cm.',$fields)." FROM cabinmate cm, campers c WHERE cm.cid = c.id AND $where ORDER BY cm.requestdate DESC");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
        	fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
    case 'export-tentorrv':
        $p = sql_fetch_by_key($db, 'products', 'id', $_GET['camp']);
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=tentorrv-'.safename($p['title']).'-'.date('Y-m-d').'.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        $where = ' c.pid = ? ';
        $wherearray = array($_GET['camp']);


        $fields = array(
            'lodging',
            'lodging_other',
            'owner',
            'owner_other',
            'stayingwith',
            'rvhookups',
            'rvhookups_other'
        );

        // output the column headings
        fputcsv($output, array_merge(array('requested by'),$fields));
        // fetch the data
        $query = $db->prepare("SELECT CONCAT(c.firstname, ' ', c.lastname) AS requested, cm.".implode(', cm.',$fields)." FROM tentorrv cm, volunteers c WHERE cm.vid = c.id AND $where");
        $query->execute($wherearray);
        $rr = $query->fetchAll();
        foreach($rr as $r){
        	fputcsv($output, $r);
        }
        // loop over the rows, outputting them
//        while ($row = mysql_fetch_assoc($rows)) fputcsv($output, $row);
        break;
}
