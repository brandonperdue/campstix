<?
$nav = array(
	array(array('products.php', 'products_.php'), 'Products', 'products.php'),
	array(array('product_categories.php', 'product_categories_.php'), 'Categories', 'product_categories.php'),
);
foreach ($nav as $nav_element) {
	list($matches, $title, $url) = $nav_element;
	if (in_array(basename($_SERVER['PHP_SELF']), $matches)) {
		echo ' '.htmlspecialchars($title);
	} else {
		?> <a href="<?= $url ?>"><?= htmlspecialchars($title) ?></a><?
	}
}