<p>
	<a href="pages.php">Pages</a> &gt;
	<?
	if ($parent_page) {
		?>
		<a href="pages.php?root=<?= htmlspecialchars($parent_page['id']) ?>">Pages under <i><?= htmlspecialchars($parent_page['title']) ?></i></a> &gt;
		<?
	}
	?>
	<?= $r['id'] ? htmlspecialchars($r['title']) : 'New' ?>
</p>
<?
if ($num_subpages > 0) {
	?>
	<div style="float: right"><a class="btn btn-default" href="pages.php?root=<?= htmlspecialchars($r['id']) ?>">View <?= htmlspecialchars($num_subpages) ?> Subpages</a></div>
	<?
}
?>


