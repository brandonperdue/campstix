<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?= htmlspecialchars($config['company_name']) ?> Backroom</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>

    <link type="text/css" rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css"/>
    <link type="text/css" rel="stylesheet" href="/lib/jquery-notify/ui.notify.css"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="/lib/bower_components/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="/lib/dropzone-master/downloads/css/dropzone.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="global.css?5"/>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });

        var user_id = <?= $_SESSION['admin']['id'] ?>;
        <?
        if (is_array($_SESSION['selectors_to_highlight'])) {
        ?>
        var selectors_to_highlight = <?= json_encode(implode(",", $_SESSION['selectors_to_highlight'])) ?>;
        <?
        unset($_SESSION['selectors_to_highlight']);
        }
        ?>
    </script>
    <script type="text/javascript" src="script.js?3"></script>
    <script type="text/javascript" src="/lib/ckeditor/ckeditor.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/lib/jquery-notify/src/jquery.notify.js" type="text/javascript"></script>
    <script type="text/javascript" src="/lib/bower_components/nestedSortable/jquery.ui.nestedSortable.js"></script>
    <script type="text/javascript" src="/lib/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <script src="/lib/dropzone-master/downloads/dropzone.min.js"></script>
    <?= $header_extra ?>
</head>
<body>

<div id="notification_container" style="display:none">
    <div id="messages">
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
    <div id="sticky">
        <a class="ui-notify-close ui-notify-cross" href="#">x</a>
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
    <div id="errors" class="ui-state-error no-auto-close">
        <a class="ui-notify-close" href="#"><span class="ui-icon ui-icon-close" style="float:right"></span></a>
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
    <div id="withIcon">
        <a class="ui-notify-close ui-notify-cross" href="#">x</a>
        <h1>#{title}</h1>
        <p>#{text}</p>
    </div>
    <div id="buttons">
        <h1>#{title}</h1>
        <p>#{text}</p>
        <p style="margin-top:10px;text-align:center">
            <input type="button" class="confirm" value="Close Dialog"/>
        </p>
    </div>
</div>


<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="/backroom/" class="navbar-brand" id="banner" title="Home">
                <strong>Administration area</strong>
                (<?= htmlspecialchars($_SESSION['admin']['username']) ?>)
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/" target="_blank">View Site</a></li>
                <li><a <?= $_SERVER['REQUEST_URI'] == '/backroom/account.php' ? 'class="current"' : '' ?> href="account.php">Account</a></li>
                <li><a href="a.php?a=logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <ul class="nav nav-sidebar">
                <?
                $backroom_nav = $config['admin_conf']['nav'];
                $current = basename($_SERVER['PHP_SELF']);
                if (!is_array($backroom_nav)) {
                    exit('No admin nav type selected.');
                }
                foreach ($backroom_nav as $n) {
                    $current = preg_match("@{$n[2]}@", basename($_SERVER['PHP_SELF']));
                    ?>
                    <li <?= $current ? 'class="current"' : '' ?>><a href="<?= $n[0] ?>"><?= $n[1] ?></a></li><?
                }
                ?>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <!--toggle sidebar button-->
            <p class="visible-xs">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas"><i class="glyphicon glyphicon-chevron-left"></i></button>
            </p>


            <?
            if ($_SESSION['m'] or $_SESSION['e']){
            ?>
            <div class="feedback_container"><?
                // print errors if any
                foreach (array('m' => 'messages', 'e' => 'errors') as $n => $message_type) {
                    if (is_array($_SESSION[$n])) {
                        ?>
                        <ul class="<?= $message_type ?>" style="display: none ;"><?
                        foreach ($_SESSION[$n] as $message) {
                            ?>
                            <li><?= $message ?></li><?
                        }
                        ?></ul><?
                    }
                    unset($_SESSION[$n]);
                }
                unset($_SESSION['e']);
                ?>
            </div><?
}
