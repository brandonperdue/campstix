<?

include '../common/config_global.php' ;
include '../common/mod_cms.php' ;
include '../common/mod_form.php' ;
include '../common/mod_format.php' ;
include '../common/mod_sql.php' ;
include '../common/mod_util.php' ;
include '../common/mod_chas.php' ;

require_once(__DIR__ . "/../../../vendor/autoload.php");

$basedir_template_files = '../upload/template_files' ;
$basedir_photos         = '../upload/photos' ;
$basedir_products       = '../upload/products' ;
$basedir_pages          = '../upload/pages' ;
$basedir_banners        = '../upload/banners' ;
$basedir_callouts       = '../upload/callouts' ;
$basedir_hp       		= '../upload/homepage' ;
$basedir_provider  		= '../upload/providers' ;
$basedir_news     		= '../upload/newscat' ;
$basedir_gallery     	= '../upload/gallery' ;
$basedir_documents     	= '../upload/documents' ;
$basedir_welcome     	= '../upload/hellogoodbye' ;

// log in for admin
if( false!==strpos($_SERVER['PHP_SELF'],'backroom/') ){
	if( 'admin_login' == $_POST['action'] ){
		$_SESSION['admin'] = admin_login($db, $_POST['username'], $_POST['password']) ;
		if( $_SESSION['admin'] ){
			hle($g_admin_types[ $_SESSION['admin']['type'] ]['home_url']) ;
		}
	}
	if( ! $_SESSION['admin'] and 'index.php'!=basename($_SERVER['PHP_SELF']) ){
		hle('./') ;
	}
}

