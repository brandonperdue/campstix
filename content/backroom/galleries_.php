<?
include '../common/config.php';

$table_name = 'galleries';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
$query = $db->prepare("SELECT COUNT(*) AS photo_count FROM photos WHERE type = 'galleries' AND id_parent = :gallery_id");
$query->execute(array(':gallery_id' => $r['id']));
$count_row = $query->fetch();
$num_images = $count_row['photo_count'];

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form">
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls();

                if ($r) {
                    ?><a class="btn btn-default" href="photos.php?id=<?= htmlspecialchars($r['id']) ?>"><i
                        class="fa fa-fw fa-picture-o"></i>
                    Manage <?= $num_images ?: '' ?>
                    photos</a><?
                }
                ?>

                <hr>
                <? button('back', "galleries.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Galleries</h1>

                <p>
                    <a href="galleries.php">Galleries</a> &gt;
                    <?= $r['id'] ? htmlspecialchars($r['title']) : 'New' ?>
                </p>

                <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

                <p>
                    Title<br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                           style="width: 600px;"/>
                </p>
                <div class="formfield">
                    Description<br/>
                    <textarea name="description" style="width: 100% ; height: 200px;"
                              class="rich_editor basic"><?= htmlspecialchars($r['description']) ?></textarea>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

