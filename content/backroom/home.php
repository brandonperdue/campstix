<?

include '../common/config.php' ;
include 'common/header.php' ;

$query = $db->prepare("SELECT 
		i.title, i.id
	FROM permissions e, pages i
	WHERE
		e.id_admins = ? AND
		e.item_type = 'pages' AND
		i.id = e.id_items
	ORDER BY i.title ASC
	") ;
$query->execute(array($_SESSION['admin']['id']));
$editable_pages = $query->fetchAll();
if( $editable_pages ){
	$i = 0 ;
	?>
	
		<h2>Available Pages</h2>
		
			<table class="list">
				<?
				foreach( $editable_pages as $r ){
					$class = ++$i&1 ? 'odd':'even' ;
					?><tr class="<?= $class ?>">
						<td><?= $r['title'] ?></td>
						<td><? edit_button("pages_.php?id={$r['id']}") ; ?></td>
					</tr><?
				}
			?></table>
	<?
}

$query = $db->prepare("SELECT * FROM pages WHERE id IN (
	SELECT id_items FROM permissions WHERE item_type = 'pages-tree' AND id_admins = ?
	)");
$query->execute(array($_SESSION['admin']['id']));
$parent_pages = $query->fetchAll();
if ($parent_pages) {
	?>
	<h2>Available Sub-Pages</h2>
	<?
	foreach ($parent_pages as $parent_page) {
		cms_show_subpages($db, $parent_page['id']) ;
	}
	
}

include 'common/footer.php' ;

