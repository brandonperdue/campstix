<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}

$page = sql_fetch_by_key($db, 'pages', 'id', $_GET['id']) ;
$query = $db->prepare("SELECT * FROM pages_vars WHERE id_pages = :id_pages ORDER BY title ASC") ;
$query->execute(array(':id_pages'=>$_GET['id'])) ;
$page_vars = $query->fetchAll() ;

include 'common/header.php' ;
?>
<p>Variables for page "<?= htmlspecialchars($page['title']) ?>".</p>
<?
if( ! $query->rowCount() ){
	echo("No variables found") ;
}else{
	?>
	<table class="list">
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Value</th>
			<th>Created</th>
			<th>Updated</th>
		</tr>
		<?
		foreach( $page_vars as $pv ){
			$admin_created = sql_fetch_by_key($db, 'admins', 'id', $pv['id_admins_created']) ;
			$admin_created = sql_fetch_by_key($db, 'admins', 'id', $pv['id_admins_updated']) ;
			?>
			<tr>
				<td><?= $pv['id'] ?></td>
				<td><?= $pv['title'] ?></td>
				<td><?= htmlspecialchars($pv['value']) ?></td>
				<td><small><?= $pv['date_created'] ?><br/>by <?= $admin_created['username'] ?></small></td>
				<td><small><?= $pv['date_updated'] ?><br/>by <?= $admin_updated['username'] ?></small></td>
			</tr>
			<?
		}
		?>
	</table>
	<?
}

include 'common/footer.php' ;

