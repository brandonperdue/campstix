<?
include '../common/config.php';

$page_window = 25;


include 'common/header.php';

?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
<!--            --><?// button('add', 'calendars_.php', 'Add Calendar'); ?>
            <? if ($_GET['past']) { ?>
                <a class="btn btn-default" href="calendars.php"><i class="fa fa-calendar"></i> View Current Events</a>
            <? } else { ?>
                <a class="btn btn-default" href="calendars.php?past=true"><i class="fa fa-calendar"></i> View Past Events</a>
            <? } ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Calendars</h1>

    <style type="text/css">
        .search_form {
            margin: 10px 0;
            border: 1px solid #eee;
            padding: 10px;
        }

        .paginator a {
            display: inline-block;
            padding: 5px 0;
            width: 25px;
            text-align: center;
            border: 1px solid #eee;
            text-decoration: none;
        }

        .paginator .current, .paginator a:hover {
            background-color: #eee;
        }

        #add_to_group_form {
            padding: 10px 0;
            display: none;
        }

        #add_to_group_wrapper {
            padding: 1px 0;
        }

        input[name=add_to_group_search] {
            width: 250px;
        }
        .nobs-row{
            background: #efefef;
        }
        .nobs-row:nth-child(odd){
            background:#f9f9f9;
        }
        .nobs-row:nth-child(odd) .buttons{
            background:#f9f9f9;
        }
    </style>

<?

$rr = $db->query("SELECT * FROM  calendars");

if (!$rr) {
    show_nothing();
} else {
    ?>
    <form action="a.php?a=mass-delete&amp;t=events&amp;return_url=calendars.php<?= $_GET['past'] ? '&amp;past=true' : '' ?>" method="post">
        <ol class="treeview list root">
            <?
            foreach ($rr as $r) {
                ?>
                <li style="display:block; background:#f9f9f9; border:solid 1px #ddd; margin-bottom:15px; padding:3px;">
                    <div class="nobs-row" style="padding:5px;">
                        <div class="cell" style="font-weight:bold; color:#<?= trim($r['color'], '#') ?>"><?= htmlspecialchars($r['title']) ?></div>
                        <div class="cell dead"><?= $r['google'] ?></div>
                        <div class="buttons">
<!--                            <div class="cell">--><?// edit_button("calendars_.php?id={$r['id']}") ?><!--</div>-->
                            <div class="cell"><? add_button("events_.php?cal={$r['id']}") ?></div>
<!--                            <div class="cell">--><?// delete_button('calendars', $r['id']) ?><!--</div>-->
                        </div>
                    </div>
                    <?
                    $page_number = $_GET['page'] ? $_GET['page'] : 1;
                    $where = $_GET['past'] ? 'AND eventdate <= NOW()' : 'AND eventdate > NOW()';

                    $limit = ($page_number - 1) * $page_window . ", " . $page_window;

                    $temp = $db->prepare("SELECT * FROM events WHERE cid = :cal $where ORDER BY eventdate ASC");
                    $temp->execute(array(':cal' => $r['id']));
                    $event_count = $db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);

                    $query2 = $db->prepare("SELECT * FROM events WHERE cid = :cal $where ORDER BY eventdate ASC LIMIT $limit");
                    $query2->execute(array(':cal' => $r['id']));
                    $rr2 = $query2->fetchAll();

                    // create paginator html
                    $paginator_replaced_base = preg_replace('@&?page=\d+@', '', basename($_SERVER['REQUEST_URI']));
                    $paginator = mc_get_paginator(array(
                        'baselink'    => $paginator_replaced_base . (false !== strpos($paginator_replaced_base, '?') ? '&amp;' : '?') . 'page=',
                        'num_tabs'    => 20,
                        'window'      => $page_window,
                        'count'       => $event_count,
                        'link_suffix' => '',
                        'cur_page'    => $page_number,
                    ));

                    if ($rr2) {
                        ?>
                        <div class="paginator">
                            <b><?= $paginator ?>
                            &nbsp; <?= $event_count ?> total events found</b>
                        </div>
                        <?
                        foreach ($rr2 as $r2) {
                            ?>
                            <div class="nobs-row static_entries">
                                <div class="cell dead">
                                    <?= date('M j, Y',strtotime($r2['eventdate'])) ?>
                                </div>
                                <div class="cell">
                                    <?= $r2['title'] ?>
                                </div>
                                <div class="buttons">
                                    <div class="cell">
                                        <a href="<?= "events_.php?id={$r2['id']}&amp;cal={$r['id']}&amp;a=copy" ?>" class="button button_copy"
                                           title="Copy event"></a>
                                    </div>
                                    <div class="cell"><? edit_button("events_.php?id={$r2['id']}&cal={$r['id']}"); ?></div>
                                    <div class="cell">
                                        <?
                                        $delete_url = "a.php?a=delete&amp;t=events&amp;return_url=" . urlencode($_SERVER['REQUEST_URI']) . "&amp;id=".$r2['id'];
                                        ?>
                                        <span class="delete_container">
                                            <a title="Delete" href="#" onclick="if(window.confirm('Really delete?')){ window.location='<?= $delete_url ?>'; }"
                                                    class="delete_first btn btn-danger btn-xs">
                                                <span class="fa fa-trash-o"></span>
                                            </a>
                                        </span>
                                    </div>
                                    <div class="cell">
                                        <input type="checkbox" name="fields[]" value="<?= $r2['id'] ?>"/>
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                    }
                    ?>
                </li>
                <?
            }
            ?>
        </ol>
        <div class="checked_action_links">
            <input type="submit" name="submit" onclick="return confirm('Are you sure?')" value="Delete Selected"/>
            <input type="button" class="select_all" onclick="checkUncheckAllfields(this)" value="Select All"/>
        </div>
    </form>
    <?
}
?>
        </div>
    </div>

<?

include 'common/footer.php';

