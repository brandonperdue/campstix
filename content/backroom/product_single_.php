<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'products';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r) {
    $r['taxable'] = 1;
    $r['description'] = "<h4>In order to purchase your custom product please add the item to your cart</h4><p>&nbsp;</p>";
}

include 'common/header.php';
?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#reset_form_link').click(function () {
                $('#reset_form').slideToggle();
            });
        });
    </script>

    <form action="a.php?a=product_single-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="id_categories" value="0"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "product_single.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Product</h1>

                <div class="row">
                    <div class="col-md-12">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Partnumber</b><br/>
                            <input type="text" name="partnumber" value="<?= htmlspecialchars($r['partnumber']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Price</b><br/>
                            $<input type="text" name="price" value="<?= htmlspecialchars($r['price']) ?>"/>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>X Size</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="xsize" value="<?= htmlspecialchars($r['xsize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>Y Size</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="ysize" value="<?= htmlspecialchars($r['ysize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="formfield">
                                    <b>Depth</b> <span class="note">in inches</span><br/>
                                    <input type="text" name="zsize" value="<?= htmlspecialchars($r['zsize']) ?>"
                                           style="width:100%"/>
                                </div>
                            </div>
                        </div>

                        <div class="formfield">
                            <b>Weight</b> <span class="note">in lbs</span><br/>
                            <input type="text" name="weight" value="<?= htmlspecialchars($r['weight']) ?>"
                                   style="width:100%"/>
                        </div>
                        <div class="formfield">
                            <b>Product Settings</b><br>
                            <label>
                                <input type="checkbox" name="taxable" <?= $r['taxable'] ? 'checked="checked"' : '' ?>/>
                                Taxable
                            </label><br/>
                        </div>
                        <div class="formfield">
                            <b>Description</b><br/>
                            <textarea name="description" class="rich_editor basic"
                                      style="width: 100%; height: 200px ;"><?= $r['description'] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

