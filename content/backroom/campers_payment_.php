<?
include '../common/config.php';

$table_name = 'campers';


$r = sql_fetch_by_key($db, 'campers', "id", $_GET['id']);

$p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);
$f = sql_fetch_by_key($db, 'funding', 'cid', $r['id']);
$s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);

$due = $p['price'] - $r['pledged'] - $s['amount'];

include 'common/header.php';


?>
    <form action="a.php?a=camper-manual-payment" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id_camper" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <button type="submit" class="btn btn-primary" name="submit" value="save"><i class="fa fa-fw fa-floppy-o"></i> Submit Payment</button>
                <? button('back', "campers_.php?id=" . $r['id'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Submit Payment for <?=$r['firstname'] ?> <?=$r['lastname'] ?></h1>
                <p>$<?= number_format($due, 2) ?> of $<?= number_format($p['price'], 2) ?> due</p>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Payment Details</h2>

                        <div class="formfield">
                            <b>Payment Amount</b><br/>
                            <input type="text" name="order_grand_total" value=""/>
                        </div>
                        <div class="formfield">
                            <b>Type</b><br/>
                            <select name="purchase_type" id="">
                                <option value="Check">Check</option>
                                <option value="Cash">Cash</option>
                            </select>
                        </div>

                        <div class="formfield">
                            <b>Check Number</b> <span>This gets added to the "Type" field for easier tracking</span><br/>
                            <input type="text" name="checknumber" value=""/>
                        </div>
                        <div class="formfield">
                            <b>Payer Firstname</b><br/>
                            <input type="text" name="bi_firstname" value=""/>
                        </div>
                        <div class="formfield">
                            <b>Payer Lastname</b><br/>
                            <input type="text" name="bi_lastname" value=""/>
                        </div>
                        <div class="formfield">
                            <b>Notes</b><br/>
                            <textarea name="notes" rows="10" style="width:100%;"></textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </form>
<?

include 'common/footer.php';

