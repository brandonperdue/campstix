<?
include '../common/config.php';


$table_name = 'option_categories';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r) {
    $r['enabled'] = 1;
    $r['id_parent'] = $_GET['id_parent'];
}

include 'common/header.php';
?>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Option Category</h1>

                <div class="formfield">
                    <b>Title</b><br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"/>
                </div>
                <div class="formfield">
                    <b>Only show when the following is selected</b><br/>
                    <?
                    write_select(array(
                        'rows' => $db->query("SELECT * FROM options WHERE  id_categories!=".($r['id']?addslashes($r['id']):0)." ORDER BY id_categories ASC"),
                        'value' => 'id',
                        'label' => 'title',
                        'name' => 'option_id',
                        'current' => $r['option_id'],
                        'default' => 0,
                        'display_length' => 35,
                    ));
                    ?>
                </div>
                <div class="formfield">
                    Disclaimers/Information<br/>
                    <textarea name="description" style="width: 100% ; height: 200px;"
                              class="rich_editor basic"><?= htmlspecialchars($r['description']) ?></textarea>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
