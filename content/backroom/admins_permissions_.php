<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}
$table_name = 'permissions' ;

$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']) ;
if( $_GET['id_admins'] ){
	$r['id_admins'] = $_GET['id_admins'] ;
}
$admin_record = sql_fetch_by_key($db, 'admins', "id", $r['id_admins']) ;

include 'common/header.php' ;
?>
<script type="text/javascript">
$(document).ready(function(){
	$('[name=item_type]').change(update_permission_item_selector) ;
	update_permission_item_selector() ;
}) ;
function update_permission_item_selector(){
	var type = $('[name=item_type]').val() ;
	var current = $('[name=id_current_item]').val() ;
	var url = 'a.php?a=get_permission_item_selector&item_type='+type+'&current='+current ;
	$('.permission_item_selector').load(url) ;
}
</script>

<div class="breadcrumbs">
	<a href="admins.php">Admins</a> &gt;
	<a href="admins_permissions.php?id=<?= $admin_record['id'] ?>">Permissions for <?= $admin_record['username'] ?></a>
</div>

<form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<?= $r['id'] ?>"/>
	<input type="hidden" name="id_admins" value="<?= $r['id_admins'] ?>"/>
	<input type="hidden" name="id_current_item" value="<?= $r['id_items'] ?>"/>
	<? print_controls(array('back'=>"admins_permissions.php?id={$r['id_admins']}")) ; ?>
	<table class="editor">
		<tr>
			<td>Type<br/>
				<?
				write_select(array(
					'db'=>$db,
					'rows'=>$db->query("SELECT * FROM permissions_item_types ORDER BY title ASC"),
					'value'=>'keyword',
					'label'=>'title',
					'current'=>$r['item_type'],
					'name'=>'item_type',
					)) ;
				?>
			</td>
			<td class="permission_item_selector"></td>
		</tr>
	</table>
</form>
<?
include 'common/footer.php' ;

