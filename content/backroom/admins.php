<?
include '../common/config.php';
if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}
$rr = $db->query("SELECT * FROM admins ORDER BY username ASC");
include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Controls</h4>
        <a href="admins_.php" class="btn btn-success">Add Admin</a>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Admins</h1>
<?
if (!$rr) {
    show_nothing();
} else {
    ?>
    <table class="table" style="width: auto;">
    <?
    foreach ($rr as $r) {
        $class = ++$i & 1 ? 'odd' : 'even';
        ?>
        <tr class="<?= $class ?>">
        <td><?= $r['username'] ?></td>
        <td><? edit_button("admins_.php?id={$r['id']}"); ?></td>
        <td><? delete_button('admins', $r['id']); ?></td>
        </tr><?
    }
    ?></table><?
}
?>

        </div>
    </div>
<?
include 'common/footer.php';

