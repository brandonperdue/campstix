<?
include '../common/config.php';

//code to add keyword saved for keyword pattern
//$query = $db->prepare("SELECT * FROM directory WHERE NOT keyword ORDER BY id DESC");
//$query->execute();
//$tt = $query->fetchAll();
//foreach ($tt as $t) {
//
//    $fields = array('keyword');
//    $values = array('keyword' => trim(getKeyword('directory', $t['name'].'-'.$t['city'].'-'.$t['state'],'-'), $t['id']));
//    $did = sql_upsert($db, 'directory', $fields, $values, 'id', $t['id']);
//}


//Code to bulk generate coordinates

//$query = $db->prepare("SELECT * FROM directory WHERE NOT coordinates ORDER BY id DESC");
//$query->execute();
//$tt = $query->fetchAll();
//foreach ($tt as $t) {
//    $fulladdress = ($t['address'] ? $t['address'] . ' ' : '') .
//        ($t['address_2'] ? $t['address_2'] . ' ' : '') .
//        ($t['city'] ? $t['city'] . ' ' : '') .
//        ($t['state'] ? $t['state'] . ' ' : '') .
//        $t['zip'];
//
//
//    $coordinatesarray = get_coords($fulladdress);
//    if(is_numeric($coordinatesarray['lat'])){
//        $coordinates = $coordinatesarray['lat'].', '.$coordinatesarray['lng'];
//
//        $fields = array('coordinates');
//        $values = array('coordinates' => $coordinates);
//        $id = sql_upsert($db, 'directory', $fields, $values, 'id', $t['id']);
//    }
//}


include 'common/header.php';

?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'directory_.php', 'Add Entry'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">

            <?

            $query = $db->prepare("SELECT * FROM directory WHERE requestdelete ORDER BY id DESC");
            $query->execute();
            $rr = $query->fetchAll();
            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <h1>Directory Deletion Requests</h1>
                <ol class="treeview list root" cmsTable="directory" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['id_user']);
                    $class = ++$i & 1 ? 'odd' : 'even';
                    if ($r['hidden']) {
                        $class .= ' dead';
                    }
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <? if($u){ ?>
                                <div class="cell">
                                    <a href="users_.php?id=<?=$u['id'] ?>" class="btn btn-default btn-xs" title="Member">
                                        <span class="fa fa-user"></span>
                                    </a>
                                </div>
                            <? } ?>
                            <div class="cell"><? delete_button('directory', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="directory_.php?id=<?= $r['id'] ?>">
                            <?= htmlspecialchars($r['name']) ?>
                            <span class="dead"><?= $r['city'] ?> <?= $r['state'] ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>


            <h1>Directory Listings</h1>

            <?

            $query = $db->prepare("SELECT * FROM directory ORDER BY id DESC");
            $query->execute();
            $rr = $query->fetchAll();
            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <ol class="treeview list root" cmsTable="directory" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['id_user']);
                    $class = ++$i & 1 ? 'odd' : 'even';
                    if ($r['hidden']) {
                        $class .= ' dead';
                    }
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <? if($u){ ?>
                                <div class="cell">
                                    <a href="users_.php?id=<?=$u['id'] ?>" class="btn btn-default btn-xs" title="Member">
                                        <span class="fa fa-user"></span>
                                    </a>
                                </div>
                            <? } ?>
                            <div class="cell"><? delete_button('directory', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="directory_.php?id=<?= $r['id'] ?>">
                            <?= htmlspecialchars($r['name']) ?>
                            <span class="dead"><?= $r['city'] ?> <?= $r['state'] ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
        </div>
    </div>
<?
include 'common/footer.php';

