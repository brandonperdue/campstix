<?
include '../common/config.php';

$table_name = 'cabinmate';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$c = sql_fetch_by_key($db, 'campers', 'id', $r['cid']);
$p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
$f = sql_fetch_by_key($db, 'funding', 'cid', $c['id']);
$s = sql_fetch_by_key($db, 'scholarship', 'cid', $c['id']);
$u = sql_fetch_by_key($db, 'users', 'id', $c['uid']);

include 'common/header.php';

$campmatefields = array(
        'firstname1',
        'lastname1',
        'age1',
        'gender1',
        'firstname2',
        'lastname2',
        'age2',
        'gender2',
        'requestdate'
);

?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                    <a href="/a.php?a=login_as&uid=<?= $u['id'] ?>" target="_blank" class="btn btn-default" title="User"><i class="fa fa-fw fa-sign-in"></i> Login In As User</a>
                <? } ?>
                <? if ($c) { ?>
                    <a href="campers_.php?id=<?= $c['id'] ?>" class="btn btn-default" title="Camper"><i class="fa fa-fw fa-user"></i> Camper Info</a>
                <? } ?>

                <? if ($r['status'] != "Paid"){ ?>
                    <a href="cabinmate_payment_.php?id=<?= $r['id'] ?>" class="btn btn-default" title="Add Payment"><i class="fa fa-fw fa-money"></i> Add Payment</a>
                <? } ?>

                <? if($s){ ?>
                    <a href="scholarship_.php?id=<?=$s['id'] ?>" class="btn btn-default" title="Campership Request"><i class="fa fa-fw fa-gift"></i> Campership Request</a>
                <? }else{ ?>
                    <a href="scholarship_.php?cid=<?=$r['id'] ?>" class="btn btn-default" title="Add Campership"><i class="fa fa-fw fa-gift"></i> Add Campership</a>

                <? } ?>
                <? if($f){ ?>
                    <a href="/fund-my-camper/<?=$f['keyword'] ?>" target="_blank" class="btn btn-default" title="Fund My Camper"><i class="fa fa-fw fa-heart"></i> Fund My Camper</a>
                <? } ?>
                <hr>
                <? button('back', "$table_name.php?pid=".$p['id'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Cabinmate Request</h1>

                <div class="row">
                    <div class="col-md-12">
                        <table class="stripe-tbl">
                            <tr>
                                <?
                                $i = 0;
                                foreach($campmatefields as $f){
                                    if($i%2 == 0){
                                        echo '</tr><tr>';
                                    }
                                    echo '<th>'.$f.'</th><td>'.$r[$f].'</td>';
                                    $i++;
                                }
                                ?>
                            </tr>
                        </table>
                        <hr>
                        <p>To edit the full details please
                            <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                               title="Login as <?= $u['user_email'] ?>" target="_blank">Login In As User</a></p>
                    </div>
                </div>
            </div>
        </div>


    </form>
<?

include 'common/footer.php';

