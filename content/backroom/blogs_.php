<?
include '../common/config.php';


$table_name = 'blogs';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if (!$r['index_entries']) {
    $r['index_entries'] = $config['blog_index_entry_limit_default'];
}

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Blogs</h1>

                <div class="formfield">
                    <b>Blog Title</b><br/>
                    <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" id="title" style="width:290px;"/>
                </div>
                <div class="formfield">
                    <b>Entries on Index Page</b><br/>
                    <input type="text" name="index_entries" value="<?= $r['index_entries'] ?>"/>
                </div>

                <div class="formfield">
                    <b>Location</b> <span class="note">Location should match the keyword of the page that serves as the blog's index.</span><br/>
                    <?= $config['site_address'] ?>/
                    <input type="text" name="keyword" class="autokeyword" rel="title"
                           autokeywordbase="<?= htmlspecialchars($location_path_base) ?>"
                           value="<?= $r['keyword'] ?>" style="width:75%;"/>
                </div>

            </div>
        </div>
    </form>
<?

include 'common/footer.php';

