<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'scholarship';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

if(!$r['cid']){
    $r['cid'] = $_GET['cid'];
}

$c = sql_fetch_by_key($db, 'campers', 'id', $r['cid']);
$p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);

//if($r['status'] == 0){
//    $r['amount'] = $r['request'];
//}


include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="cid" value="<?= $r['cid'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <button type="submit" class="btn btn-primary" name="submit" >
                    <i class="fa fa-fw fa-floppy-o"></i>
                    <? if(!$_GET['id']){?>
                        Save Request
                    <?}else{?>
                        Submit Response
                    <?}?>
                </button>
                <a href="campers_.php?id=<?=$c['id'] ?>" class="btn btn-default" title="View Camper"><i class="fa fa-fw fa-user"></i> Camper</a>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Scholarship Request Response</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Approved/Denied</b><br/>
                            <select name="status">
                                <option value="0">Pending</option>
                                <option value="1" <? if($r['status']==1){ ?>selected="selected"<? } ?>>Approved</option>
                                <option value="2" <? if($r['status']==2){ ?>selected="selected"<? } ?>>Denied</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Thank You Sent?</b><br/>
                            <select name="thankyousent">
                                <option value="0">No</option>
                                <option value="1" <? if($r['thankyousent']==1){ ?>selected="selected"<? } ?>>Yes</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Approved Amount</b><br/>
                            <input type="text" name="amount" value="<?= htmlspecialchars($r['amount']) ?>"/>
                        </div>
                        <div class="formfield">
                            <label><b>Email Account Holder?</b> <span class="note">This will only send if approved or denied</span><br/>
                            <input type="checkbox" name="emailcamper" <? if($r['status']==0){ ?>checked="checked"<? } ?> />
                            </label>
                        </div>

                        <div class="formfield">
                            <b>Comments for account holder email</b><br/>
                            <textarea name="comments" style="width:100%; height:100px;"></textarea>
                        </div>
                        <div class="formfield">
                            <b>Notes for Administration</b> <span class="note">Account holder will not see this</span><br/>
                            <textarea name="note" style="width:100%; height:100px;"><?=$r['note'] ?></textarea>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <h2>Request Details</h2>

                        <p>
                            <b>Camper:</b> <?=$c['firstname'] ?> <?=$c['lastname'] ?>
                        </p>
                        <p>
                            <b>Camp:</b> <?=$p['title'] ?> (<?= date("M jS Y",strtotime($p['startdate'])) ?>)
                        </p>
                        <p>
                            <b>Request Date:</b> <?= date("M jS Y",strtotime($r['requestdate'])) ?>
                        </p>

                        <div class="formfield">
                            <b>Requested Amount</b><br/>
                            <input type="text" name="request" value="<?= htmlspecialchars($r['request']) ?>"/>
                        </div>
                        <h3>Parent or Guardian's Contact Information</h3>
                        <div class="formfield">
                            <b>Name</b><br/>
                            <input type="text" name="parent_name" value="<?= htmlspecialchars($r['parent_name']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Email</b><br/>
                            <input type="text" name="parent_email" value="<?= htmlspecialchars($r['parent_email']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Address</b><br/>
                            <input type="text" name="parent_address" value="<?= htmlspecialchars($r['parent_address']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>City</b><br/>
                            <input type="text" name="parent_city" value="<?= htmlspecialchars($r['parent_city']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>State</b><br/>
                            <input type="text" name="parent_state" value="<?= htmlspecialchars($r['parent_state']) ?>"/>
                        </div>
                        <h3>Household Information</h3>
                        <div class="formfield">
                            <b>How Many Adults Are In Your Family</b><br/>
                            <input type="text" name="how_many_adults" value="<?= htmlspecialchars($r['how_many_adults']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>How Many Children Are In Your Family</b><br/>
                            <input type="text" name="how_many_children" value="<?= htmlspecialchars($r['how_many_children']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>What are the ages of the children in your family?</b><br/>
                            <input type="text" name="ages_of_children" value="<?= htmlspecialchars($r['ages_of_children']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>School Lunch Eligibility?</b><br/>
                            <select name="school_lunch" id="">
                                <option value=""></option>
                                <option value="Camper qualifies for free school lunch" <? if($r['school_lunch'] == "Camper qualifies for free school lunch"){ ?>selected="selected"<? } ?>>Camper qualifies for free school lunch</option>
                                <option value="Camper qualifies for reduced school lunch" <? if($r['school_lunch'] == "Camper qualifies for reduced school lunch"){ ?>selected="selected"<? } ?>>Camper qualifies for reduced school lunch</option>
                                <option value="Camper does not qualify for reduced or free lunch"  <? if($r['school_lunch'] == "Camper does not qualify for reduced or free lunch"){ ?>selected="selected"<? } ?>>Camper does not qualify for reduced or free lunch</option>
                                <option value="Other" <? if($r['school_lunch'] == "Other"){ ?>selected="selected"<? } ?>>Other</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>If "Other"</b><br/>
                            <input type="text" name="school_lunch_other" value="<?= htmlspecialchars($r['school_lunch_other']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>What is your Annual Household Income</b><br/>
                            <input type="text" name="annual_income" value="<?= htmlspecialchars($r['annual_income']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Parent Comments</b><br/>
                            <textarea name="content" style="width:100%;height:120px"><?=$r['content'] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';
