<?
include '../common/config.php';

if ($_GET['id'] and !admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
    exit('Permission denied');
}


$query = $db->prepare("SELECT * FROM pages WHERE id = ?");
$query->execute(array($_GET['id']));
$r = $query->fetch();

if (!$r) {
    $r['comments_disabled'] = true;
    $r['template'] = $config['default_template'];
    $r['id_parent'] = $_GET['id_parent'];
}
// get the full navigation chain so we can suggest a full path for this page's location
$nav_chain = find_nav_chain($db, $r);

$parent_page = get_page($db, array('id' => $r['id_parent']));

$query = $db->prepare("SELECT COUNT(id) FROM pages WHERE id_parent = :id");
$query->execute(array(':id' => $r['id']));
$num_subpages = $query->fetchColumn();

array_pop($nav_chain);
$location_path_base = "";
foreach ($nav_chain as $page) {
    $exploded_url_keyword = explode("/", $page['keyword']);
    $location_path_base .= end($exploded_url_keyword) . "/";
}

include 'common/header.php';

if ($r['editing_template']) {
    // non-standard edit page - e.g. for the home page
    include $r['editing_template'];

} else {

    ?>
    <form action="a.php?a=pages-config" method="post" class="editor-form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <?
                if ($_GET['id'] and $r['template']) {
                    ?><a class="btn btn-default" href="pages_.php?id=<?= $_GET['id'] ?>"><i
                            class="fa fa-fw fa-edit"></i> Edit Contents</a><?
                }
                ?>
                <hr>
                <? button('back', "pages.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Pages</h1>
                <? include 'common/page_edit_header.php'; ?>

                <div class="row">
                    <div class="col-md-6">
                        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>


                        <div class="formfield"><b>Title</b> <span class="note">(breadcrumbs, title tag)</span><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width:75%;" id="title"/>
                        </div>

                        <div class="formfield">
                            <b>Location</b> <span class="note">(Home page must use "home")</span> <?= $g_site_address ?>
                            /<br/>
                            <input type="text" name="keyword" class="autokeyword" rel="title"
                                   autokeywordbase="<?= htmlspecialchars($location_path_base) ?>"
                                   value="<?= $r['keyword'] ?>" style="width:75%;"/>
                        </div>

                        <div class="formfield"><b>Parent Page</b><br/>
                            <?
                            $rows = cms_get_sub_items_list($db, 'pages', 0);
                            foreach ($rows as $key => $row) {
                                $rows[$key]['title'] = str_repeat("&nbsp;", 3 * $row['depth']) . $row['title'];
                            }
                            unset($rows[$r['id']]);
                            write_select(array(
                                'db' => $db,
                                'rows' => $rows,
                                'label' => 'title',
                                'value' => 'id',
                                'current' => $r['id_parent'],
                                'name' => 'id_parent',
                            ));
                            ?>
                        </div>

                        <div class="formfield"><b>Template</b><br/><?
                            write_select(array(
                                'rows' => $config['templates'],
                                'name' => 'template',
                                'current' => $r['template'],
                                'default' => 'page',
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Meta Title</b><br/>
                            <input type="text" name="meta_title" value="<?= htmlspecialchars($r['meta_title']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Meta Description</b><br/>
                            <textarea style="width:99%;"
                                      name="meta_description"><?= htmlspecialchars($r['meta_description']) ?></textarea>
                        </div>

                        <div class="formfield">
                            <b>Options</b><br>
                            <label><input type="checkbox"
                                          name="navhide" <?= $r['navhide'] ? 'checked="checked"' : '' ?>/> Hide from
                                navigation</label><br/>
                            <?
                            if ($g_page_comments_possible) {
                                ?><label><input type="checkbox"
                                                name="comments_disabled" <?= $r['comments_disabled'] ? 'checked="checked"' : '' ?>/>
                                Disable comments </label><?
                            }
                            ?>
                        </div>

                        <div class="editor type_section"
                             id="type_special" <?= 'special' != $r['type'] ? 'style="display:none;"' : '' ?>>
                            <div class="formfield">Special Address<br/><input type="text" name="frontend_include"
                                                                              value="<?= $r['frontend_include'] ?>"/>
                            </div>
                        </div>
                        <div class="editor type_section"
                             id="type_link" <?= 'link' != $r['type'] ? 'style="display:none;"' : '' ?>>
                            <div class="formfield"><input type="text" name="link" value="<?= $r['link'] ?>"
                                                          style="width: 600px ;"/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?
}

include 'common/footer.php';

