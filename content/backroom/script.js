function removeSubsection(e){
    $(e).closest('.subsection').remove();
    return false;
}

jQuery.fn.passwordStrength = function (options) {
    var strengths = {
        '0': 'Weak',
        '1': 'Weak',
        '2': 'Fair',
        '3': 'Good',
        '4': 'Strong'
    };
    var $password_field = $(this);
    var update_strength = function () {
        var strength = zxcvbn($password_field.val(), options['user_inputs']);
        $(options['strength_text']).text(strengths[strength.score]);
    }
    $password_field.bind('keyup change', update_strength);
};
jQuery.fn.generatePassword = function (options, callback) {
    $(this).click(function () {
        var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-,.?!@#$%&*";
        var length = 8 + Math.floor(Math.random() * 3);
        var password = '';
        for (var i = 0; i < length; i++) {
            password += alphabet[Math.floor(Math.random() * (alphabet.length))];
        }
        $(options['fill_password']).html(password);
        console.log(password);
        $(options['unhide']).show();
        if (typeof(callback) == "function") {
            callback();
        }
    });
};
$(document).ready(function () {
    $("table.list tr").mouseover(function () {
        $(this).addClass('rowhover');
    });
    $("table.list tr").mouseout(function () {
        $(this).removeClass('rowhover');
    });
    $('.errors,.messages').slideDown();
});
function handle_complete_upload() {
}
function update_page_type_options() {
    var type_select = document.getElementById('type_select');
    var type = type_select[type_select.selectedIndex].value;
    $('.type_section').hide();
    $('#type_' + type).show();
}
function delete_showform(btn) {
    $(btn).hide();
    $(btn).parent().children('.delete_form').show();
}
function delete_cancel(btn) {
    $(btn).parent().parent().children('.delete_form').hide();
    $(btn).parent().parent().children('.delete_first').show();
}
function iwcl(message, url) {
    if (window.confirm(message)) {
        window.location = url;
    }
}

function SetUrl(url, width, height, alt) {
    urlobj.value = url;
    oWindow = null;
}


$(function () {
    $('.autokeyword').each(function () {
        var $keyword = $(this);
        var $input = $('#' + $keyword.attr('rel'));
        var auto_keyword_base = $keyword.attr('autokeywordbase') ? $keyword.attr('autokeywordbase') : '';
        $input.attr('last_value', $input.val());
        $input.bind('change, keyup', function () {
            var keyword_for_old_value = generate_keyword($input.attr('last_value'));
            if (keyword_for_old_value == $keyword.val() || $.trim($keyword.val()) == '') {
                // only auto-update keyword if keyword is currently set to auto value
                var new_keyword = generate_keyword($input.val());
                $keyword.val(new_keyword);
            }
            $input.attr('last_value', $input.val());
        });
        $keyword.after($('<span class="auto_generate_keyword btn btn-default btn-xs">Auto Generate</span>').click(function () {
            $keyword.val(generate_keyword($input.val()));
        }));
        function generate_keyword(title) {
            var keyword = auto_keyword_base + title.replace(/[^\w]+/g, '-').toLowerCase();
            return keyword;
        }
    });
    $('.filepicker').click(function () {
        urlobj = document.getElementsByName($(this).attr('rel'))[0];
        var url = '/lib/Filemanager/index.html';
        var width = screen.width * 0.7;
        var height = screen.height * 0.7;
        var iLeft = (screen.width - width) / 2;
        var iTop = (screen.height - height) / 2;
        var sOptions = "toolbar=no,status=no,resizable=yes,dependent=yes";
        sOptions += ",width=" + width;
        sOptions += ",height=" + height;
        sOptions += ",left=" + iLeft;
        sOptions += ",top=" + iTop;
        oWindow = window.open(url, "BrowseWindow", sOptions);

    });

    user_id = user_id;
    function setup_toggle() {
        $('.row').each(function (index, element) {
            if ($(element).siblings('ol').children().length) {
                var count = $(element).siblings('ol').find('.row').length;
                $(element).prepend('<div class="cell"><span title="' + count + ' sub elements" class="toggle_children btn btn-xs" href="#"><span class="fa fa-chevron-down"></span></span></div>');
            }
        });
        $('.toggle_children').click(function () {
            var $ol = $(this).closest('.row').siblings('ol');
            var num_children = $ol.children().length;
            $ol.toggleClass('hidden_list');
            $ol.slideToggle('fast');
            save_toggle_states();
            return false;
        });
    }

    function load_toggle_states() {
        var json = localStorage['hidden_pages_user_' + user_id];
        var root_id = $('.treeview').attr('rootElementId');
        if (!json) {
            return;
        }
        var hidden_ids = JSON.parse(json);

        // if the loaded tree view is set to start from a root id which is currently hidden, remove that id from the list before rendering
        if (hidden_ids.indexOf(root_id) >= 0) {
            hidden_ids.splice(hidden_ids.indexOf(root_id), 1);
        }
        for (var i = 0, l = hidden_ids.length; i < l; i++) {
            var id = hidden_ids[i];
            $('ol[RootElementId=' + id + ']').hide().addClass('hidden_list');
        }
    }

    function save_toggle_states() {
        var hidden_ids = [];
        $('ol.hidden_list').each(function (index, element) {
            hidden_ids.push($(element).attr('RootElementId'));
        });
        var json = JSON.stringify(hidden_ids);
        localStorage['hidden_pages_user_' + user_id] = json;
    }

    setup_toggle();
    load_toggle_states();

    $('.rearrange-enable').click(function () {
        $('hide-while-rearranging').hide();
        $('ol.root .row .buttons').hide();
        $('ol.root .toolbar').hide();
        $('.rearrange-enable').addClass('btn-hover');
        $('.rearrange-finished').show();
        $('.rearrange-cancel').show();
        $('.rearrange-finished').click(function () {
            $(this).prop('disabled', true);
            $(this).text('Saving...');
            $sortable = $('.sortable');
            var cmstable = $sortable.attr('cmsTable');
            var mode = $sortable.attr('cmsHierarchyMode');
            var root_element_id = $sortable.attr('RootElementId');
            var positions = '';
            if ($sortable.hasClass('sortablegrid')) {
                //force old "sortable" style jquery into new "nestedSortable" style jquery
                var positionsstring = $sortable.sortable('toArray');
                $.each(positionsstring, function (index, value) {
                    var varr = value.split('-');
                    positions += 'priorityitems[' + varr[1] + ']=' + root_element_id + '&';
                });
            } else {
                positions = $sortable.sortable('serialize');
            }
            $.post('a.php?a=prioritize',
                'table=' + cmstable + '&mode=' + mode + '&root_element_id=' + root_element_id + '&' + positions,
                function (response) {
                    window.location.reload();
                }
            );
        });
        $('.rearrange-enable').click(function () {
            window.location.reload();
        });
        $('.rearrange-cancel').click(function () {
            window.location.reload();
        });
        $('ol.root').addClass('sortable');
        $('.edit-link').removeAttr('href');

        $('ol.sortable:not(.sortablegrid)').nestedSortable({
            disableNesting: 'no-nest',
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li',
            maxLevels: 3,
            opacity: 0.6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            start: function (e, ui) {
                ui.placeholder.height('37px');
            }
        });
        $('ol.sortablegrid').sortable();
    });
    $('.advanced-toggle').click(function () {
        $('.cell.advanced').toggle();
    });

    $('.rich_editor').each(function (index, element) {
        initialCKeditor(index, element);
    });

    $notifications = $("#notification_container").notify();
    $('.feedback_container').children().each(function (index, element) {
        template_id = $(element).attr('class');
        $(element).children().each(function (index, element) {
            options = {queue: 5};
            if (template_id == 'errors') {
                options['custom'] = true;
                options['expires'] = false;
            }
            $notifications.notify("create", template_id, {text: $(element).text()}, options);
        });
    });
    if (typeof selectors_to_highlight != 'undefined') {
        $(selectors_to_highlight).effect('highlight', 5000);
    }
});

function initialCKeditor(index, element){

    var toolbar = [
        ['Source', 'Maximize'],
        ['Paste'],
        ['Undo', 'Redo', '-', 'Find', 'Replace'],
        ['Format', 'Bold', 'Italic', 'RemoveFormat'],
        ['FontSize'],
        ['NumberedList', 'BulletedList', 'Blockquote'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
        ['Link', 'Unlink', 'Anchor'],
        ['AccordionList','CollapsibleItem'],
        ['Image', 'ds_gallery', 'add_columns', 'Table', 'MediaEmbed']
    ];
    if ($(element).hasClass('basic')) {
        var toolbar = [
            ['Source', 'Maximize'],
            ['Paste'],
            ['Undo', 'Redo'], ['Find', 'Replace'],
            ['Format', 'Bold', 'Italic', 'RemoveFormat'],
            ['NumberedList', 'BulletedList', 'Blockquote'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
            ['Link', 'Unlink', 'Anchor'],
            ['AccordionList','CollapsibleItem'],
            ['Image', 'ds_gallery', 'add_columns', 'Table', 'MediaEmbed']
        ];
    }
    CKEDITOR.replace(element, {
        filebrowserBrowseUrl: '../lib/elfinder/elfinder.html',
        contentsCss: ['/lib/bootstrap-3.1.1/css/bootstrap.min.css', '/global.css?1', '/backroom/editor_settings/ckeditor.css'],
        bodyId: 'ckeditor',
        height: element.style.height,
        width: element.style.width,
        toolbar: toolbar,
        forcePasteAsPlainText: true,
        scayt_autoStartup: true,
        allowedContent: true
    });
    CKEDITOR.plugins.addExternal('iframedialog', '/backroom/editor_settings/iframedialog/', 'plugin.js');
    CKEDITOR.plugins.addExternal('mediaembed', '/backroom/editor_settings/mediaembed/', 'plugin.js');
    CKEDITOR.plugins.addExternal('ds_gallery', '/backroom/editor_settings/ds_gallery/', 'plugin.js');
    CKEDITOR.plugins.addExternal('justify', '/backroom/editor_settings/justify/', 'plugin.js');
    CKEDITOR.plugins.addExternal('add_columns', '/backroom/editor_settings/add_columns/', 'plugin.js');

    CKEDITOR.config.extraPlugins = 'iframedialog,mediaembed,ds_gallery,add_columns,justify';
    //CKEDITOR.config.extraPlugins = 'iframedialog,mediaembed,codemirror,ds_gallery,add_columns,widget,button,notification,toolbar,lineutils,clipboard,widgetselection,collapsibleItem,accordionList';
    CKEDITOR.dtd.$removeEmpty['i'] = false;
}