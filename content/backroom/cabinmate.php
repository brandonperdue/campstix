<?
include '../common/config.php';
include 'common/header.php';
if ($_GET['pid']) {
    $p = sql_fetch_by_key($db, 'products', 'id', $_GET['pid']);
    ?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <a href="cabinmate.php" class="btn btn-default"><i class="fa fa-fw fa-tree"></i> Select Camp</a>
            <a href="a.php?a=export-cabinmate&camp=<?= $_GET['pid'] ?>" class="btn btn-default"><i class="fa fa-fw fa-download"></i> Export All</a>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Cabinmate Requests</h1>
            <?
            $query = $db->prepare("SELECT cm.*, c.firstname, c.lastname FROM cabinmate cm INNER JOIN campers c ON cm.cid = c.id INNER JOIN products p ON c.pid = p.id WHERE p.id = ? ORDER BY c.firstname DESC");
            $query->execute(array($_GET['pid']));
            $rr = $query->fetchAll();
            if (!$rr) {
                echo 'None Found';
            } else {
                ?>
                <ol class="treeview list root" cmsTable="cabinmate" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="campers_.php?id=<?= $r['cid'] ?>"
                                   title=" | Click to see Camper info">
                                    <span class="fa fa-user fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell"><? delete_button('cabinmate', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="cabinmate_.php?id=<?= $r['id'] ?>">
                            <b><?= htmlspecialchars($r['firstname'] . ' ' . $r['lastname']) ?></b>
                             - requests <?= htmlspecialchars($r['firstname1'] . ' ' . $r['lastname1']) ?>
                            or <?= htmlspecialchars($r['firstname2'] . ' ' . $r['lastname2']) ?>
                            <span class="rightcontent">Req: <?= date('M jS Y', strtotime($r['requestdate'])) ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
            <br>
        </div>
    </div>
    <?

} else {
    $query = $db->prepare("SELECT * FROM products WHERE signuptype ORDER BY startdate DESC");
    $query->execute();
    $rr = $query->fetchAll();
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h1>Select Camp</h1>
            <ol class="treeview list root">
                <?php
                foreach ($rr as $r) {
                    $query = $db->prepare("SELECT * FROM campers WHERE status != 'Unpaid' AND pid = ? ORDER BY dateadded DESC");
                    $query->execute(array($r['id']));
                    $cc = $query->fetchAll();
                    ?>
                    <li class="no-nest">
                        <div class="row">
                            <a class="cell edit-link" href="cabinmate.php?pid=<?= $r['id'] ?>">
                                <?= htmlspecialchars($r['title']) ?> <b>(<?= count($cc) ?> Campers)</b>
                            </a>
                        </div>
                    </li>
                <? } ?>
            </ol>
        </div>
    </div>
    <?
}
include 'common/footer.php';

