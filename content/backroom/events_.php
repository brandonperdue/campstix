<?
include '../common/config.php';
$table_name = 'events';
$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);
if ($_GET['a'] == 'copy') {
    unset($r['id']);
}
if (!$r) {
    $r['eventdate'] = date('Y-m-d');
    $r['allday'] = 1;
} else {
    list($r['eventdate'], $r['eventtime']) = explode(' ', $r['eventdate']);
    $r['eventtime'] = date("g:i a", strtotime($r['eventtime']));
}

if($r['allday']){
    $r['eventtime'] = '';
}

if(!$r['startdate']){
    $r['startdate'] = $r['eventdate'];
}

$header_extra = '<script src="/lib/jquery-ui-timepicker-addon.js" type="text/javascript"></script>';
include 'common/header.php';
?>
    <style type="text/css">
        /* css for timepicker */
        .ui-timepicker-div .ui-widget-header {
            margin-bottom: 8px;
        }

        .ui-timepicker-div dl {
            text-align: left;
        }

        .ui-timepicker-div dl dt {
            float: left;
            clear: left;
            padding: 0 0 0 5px;
        }

        .ui-timepicker-div dl dd {
            margin: 0 10px 10px 45%;
        }

        .ui-timepicker-div td {
            font-size: 90%;
        }

        .ui-tpicker-grid-label {
            background: none;
            border: none;
            margin: 0;
            padding: 0;
        }

        .ui-timepicker-rtl {
            direction: rtl;
        }

        .ui-timepicker-rtl dl {
            text-align: right;
            padding: 0 5px 0 0;
        }

        .ui-timepicker-rtl dl dt {
            float: right;
            clear: right;
        }

        .ui-timepicker-rtl dl dd {
            margin: 0 45% 10px 10px;
        }
    </style>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="cid" value="<?= $_GET['cal'] ?>"/>
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "calendars.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Events</h1>
                <table class="editor">
                    <tr>
                        <td colspan="3">
                            <div class="title" style="margin-right:10px;">Title</div>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" style="width:100%;"/>
                        </td>
                        </tr>
                    <tr>
                        <td style="width:30%; vertical-align:top;">
                            <div class="title">Start Date <span class="note">time is for sorting only</span></div>
                            <input type="text" class="date_single" size="9" name="startdate" value="<?= htmlspecialchars($r['startdate']) ?>"/>
                            <input type="text" class="time_single" size="6" name="eventtime" value="<?= htmlspecialchars($r['eventtime']) ?>"/>
                            <label><input type="checkbox" id="allday" name="allday" <? if ($r['allday']) echo 'checked="checked"'; ?> />All
                                day?</label>
                        </td>
                        <td style="width:30%; vertical-align:top;">
                            <div class="title">End Date</div>
                            <input type="text" class="date_single" size="9" name="eventdate" value="<?= htmlspecialchars($r['eventdate']) ?>"/>

                            <? /*
                            <? if (!$r['id']) { ?>
                                <button class="add-date">Add Another Date</button><? } ?>
                            <div class="extra-dates"></div>
                        */ ?>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('.time_single').timepicker({
                                        timeFormat: "h:mm tt"
                                    });
                                    $('#allday').change(function () {
                                        checktime();
                                    });
                                    $(".date_single").datepicker({
                                        defaultDate: "d",
                                        changeMonth: true,
                                        dateFormat: "yy-mm-dd",
                                    });
                                    checktime();
                                    $(".add-date").click(function () {
                                        $(".extra-dates").after('<input type="text" class="date_single" size="9" name="eventdate[]" value="<?= htmlspecialchars($r['eventdate']) ?>"/><br>');
                                        $(".date_single").datepicker({
                                            defaultDate: "d",
                                            changeMonth: true,
                                            dateFormat: "yy-mm-dd",
                                        });
                                        return false;
                                    });
                                });
                                function checktime() {
                                    if ($('#allday').attr('checked')) {
                                        $('.time_single').attr("disabled", "disabled");
                                    } else {
                                        $('.time_single').removeAttr("disabled");
                                    }
                                }
                            </script>
                        </td>
                        <td style="width:40%; vertical-align:top;">
                            <div class="title" style="margin-right:10px;">Time Desc <span class="note">(I.E. "10am to Noon")</span></div>
                            <input type="text" name="timetext" value="<?= htmlspecialchars($r['timetext']) ?>" style="width:250px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="title">Synopsis <span class="note">This should be 1-2 sentences that describe the event.  If nothing is entered text will be taken from the main content.</span></div>
                            <textarea name="synopsis" class="rich_editor basic" style="width: 99% ; height: 400px ;"><?= $r['synopsis'] ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="title">Content</div>
                            <textarea name="content" class="rich_editor basic" style="width: 99% ; height: 400px ;"><?= $r['content'] ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

