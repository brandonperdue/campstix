<?
include 'common/config.php' ;

$r = sql_one('orders',$_GET['id']) ;

include 'common/header.php' ;

?>

<form action="a.php?a=orders-save" method="post" class="editor-form">
	<input type="hidden" name="id" value="<?= $r['id'] ?>"/>
	<? print_controls(array('back'=>"orders.php",)) ; ?>
	<table class="editor">
		<tr>
			<td style="width: 130px ;"><b>Date</b><br/>
				<?= date('Y-m-d h:i a',strtotime($r['order_date'])) ?></td>
			<td style="width: 140px ;"><b>Order Number</b><br/>
				<?= $r['ordernumber'] ?></td>
			<td style="width: 100px ;"><b>Type</b><br/>
				<?= $r['purchase_type'] ?></td>
			<td style="width: 120px ;"><b>Transaction ID</b><br/>
				<?= $r['transaction_id'] ?></td>
			<td>
				<label><input type="checkbox" name="refunded" <?= $r['refunded'] ? 'checked="checked"' : '' ?>/> Refunded</label>
				<?= $r['refunded'] ? ("(on ".date('Y-m-d h:i a',strtotime($r['order_date'])).")") : '' ?>
			</td>
		</tr>
		<tr>
			<td colspan="99">Notes<br/>
				<textarea name="notes" style="width: 340px ; height: 100px ;"><?= $r['notes'] ?></textarea></td>
		</tr>
	</table>
</form>

<div class="section">
	<h2>Resend receipt</h2>
	<form action="a.php?a=orders-resend-receipt" method="post">
		<input type="hidden" name="id_orders" value="<?= $r['id'] ?>"/>
		<input type="text" name="email" value="<?= htmlspecialchars($r['bi_email']) ?>"/>
		<input type="submit" value="Send"/>
	</form>
</div>

<div class="section">
	<h2>Shipping Records</h2>
	<?
	$shipments = sql_all('order_shipments', "id_orders = '{$r['id']}'", "date_shipped DESC") ;
	if( $shipments ){
		?><table class="list">
			<tr>
				<th>Shipped</th>
				<th>Carrier</th>
				<th>Email</th>
				<th>Link</th>
			</tr><?
			foreach( $shipments as $shipment ){
				?><tr>
					<td><?= date('Y-m-d', strtotime($shipment['date_shipped'])) ?></td>
					<td><?= $shipment['carrier_title'] ?></td>
					<td><?= $shipment['email_to'] ?></td>
					<td><a href="<?= $shipment['tracking_link'] ?>">Link</a></td>
				</tr><?
				if( $shipment['notes'] ){
					?><tr><td colspan="99" style="padding-bottom: 15px ; background-color: #f2f2f2 ;"><?= nl2br(htmlspecialchars($shipment['notes'])) ?></td></tr><?
				}
			}
		?></table><?
	}
	?>
	<p><a href="#" onclick="$('.shipping_form').slideToggle('fast'); return false;">New Record</a></p>
	<div class="shipping_form" style="display: none ; border:">
		<form action="a.php?a=orders-send-tracking-info" method="post" class="editor-form" style="margin: 0 ;">
			<input type="hidden" name="id_orders" value="<?= $r['id'] ?>"/>
			<table class="editor">
				<tr>
					<td>Date Shipped<br/>
						<? write_select_date(array('name'=>'date_shipped','current'=>$r['date_shipped'])) ; ?></td>
					<td>Recipient Email<br/>
						<input type="text" name="email" value="<?= htmlspecialchars($r['bi_email']) ?>"/></td>
					<td>Carrier<br/><?
						write_select(array(
							'sql'=>"SELECT id, title FROM shipping_carriers ORDER BY title ASC",
							'label'=>'title',
							'value'=>'id',
							'name'=>'id_shipping_carriers',
							)) ;
					?></td>
					<td>Tracking Number<br/>
						<input type="text" name="tracking_number"/>
					</td>
				</tr>
				<tr>
					<td colspan="99">Notes<br/>
						<textarea name="notes" style="width: 340px ; height: 100px ;"><?= $r['notes'] ?></textarea></td>
				</tr>
				<tr><td colspan="99"><input type="submit" value="Save and Send"/></td></tr>
			</table>
		</form>
	</div>
</div>


<style type="text/css">
.receipt table{ border-spacing: 4px ; border-collapse: separate ; }
</style>
<div class="section receipt">
	<h2>Receipt</h2>
	<?= $r['receipt'] ?>
</div>
<?
include 'common/footer.php' ;

