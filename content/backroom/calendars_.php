<?
include '../common/config.php';

$table_name = 'calendars';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

$header_extra = '<script src="/lib/colpick/js/colpick.js" type="text/javascript"></script><link rel="stylesheet" href="/lib/colpick/css/colpick.css" type="text/css"/>';

include 'common/header.php';
?>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Calendar</h1>
        <table class="editor">
            <tr>
                <td colspan="3">
                    <div class="title">Calendar Name</div>
                    <input type="text" name="title" value="<?= $r['title'] ?>" style="width: 99%;"/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="title">Color</div>
                    <style type="text/css">
                        #colpick {
                            margin: 0;
                            padding: 0;
                            border: 0;
                            width: 70px;
                            height: 20px;
                            border-right: 20px solid <?=($r['color']?'#'.trim($r['color'],'#'):'#000') ?>;
                            line-height: 20px;
                        }
                    </style>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#colpick').colpick({
                                layout: 'hex',
                                submit: 0,
                                colorScheme: 'dark',
                                onChange: function (hsb, hex, rgb, el, bySetColor) {
                                    $(el).css('border-color', '#' + hex);
                                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                                    if (!bySetColor) $(el).val(hex);
                                }
                            }).keyup(function () {
                                $(this).colpickSetColor(this.value);
                            });
                        });
                    </script>
                    <input type="text" name="color" value="<?= $r['color'] ?>" id="colpick"/>
                </td>
            </tr>
        </table>
            </div>
        </div>

    </form>
<?
include 'common/footer.php';

