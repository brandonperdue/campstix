<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}

$query = $db->prepare("SELECT t.title, p.id, p.item_type, p.id_items, t.table
	FROM permissions p, permissions_item_types t
	WHERE
		p.id_admins = ? AND
		p.item_type = t.keyword
	ORDER BY t.title ASC
	") ;
$query->execute(array($_GET['id']));
$rr = $query->fetchAll();

$admin_record = sql_fetch_by_key($db, 'admins', 'id', $_GET['id']);

include 'common/header.php' ;
?>

<div class="breadcrumbs">
	<a href="admins.php">Admins</a> &gt;
	Permissions for <?= htmlspecialchars($admin_record['username']) ?>
</div>

<div class="controls">
	<a href="<?= 'admins_permissions_.php?id_admins='.$_GET['id'] ?>" class="btn btn-success">Add Permission</a>
</div>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
	<table class="list">
		<tr><th>Type</th><th>Title</th></tr><?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			$permitted_item = sql_fetch_by_key($db, $r['table'], 'id', $r['id_items']) ;
			?><tr class="<?= $class ?>">
				<td><?= htmlspecialchars($r['title']) ?></td>
				<td><?= htmlspecialchars($permitted_item['title']) ?></td>
				<td><? edit_button("admins_permissions_.php?id={$r['id']}") ; ?></td>
				<td><? delete_button('permissions',$r['id']) ; ?></td>
			</tr><?
		}
	?></table><?
}

include 'common/footer.php' ;

