/*
* Embed Media Dialog based on http://www.fluidbyte.net/embed-youtube-vimeo-etc-into-ckeditor
*
* Plugin name:      mediaembed
* Menu button name: MediaEmbed
*
* Youtube Editor Icon
* http://paulrobertlloyd.com/
*
* @author Fabian Vogelsteller [frozeman.de]
* @version 0.4
*/
var iframeWindow = null;
( function() {
   CKEDITOR.plugins.add('ds_gallery', {
		requires: ['iframedialog'],
		init: function(a) {
        	var me = this;
			CKEDITOR.dialog.add( 'gallery_dialog', function (){
              return {
                 title : 'Gallery Selector',
                 minWidth : 550,
                 minHeight : 200,
                 contents :
                       [
                          {
                             id : 'iframe',
                             label : 'Insert Gallery',
                             expand : true,
                             elements :
                                   [
                                      {
                                         type : 'iframe',
                                         src : me.path + 'list_galleries.php',
                                         width : '100%',
                                         height : 200,
                                         onContentLoad : function() {
											var iframe = document.getElementById(this._.frameId);
											iframeWindow = iframe.contentDocument || iframe.contentWindow.document;
                                         }
                                      }
                                   ]
                          }
                       ],
                 onOk : function(){
					var e = iframeWindow.getElementById("gallery");
					this._.editor.insertHtml('<div><div class="ds_gallery">[gallery-'+e.options[e.selectedIndex].value+']</div></div>');
                 }
              };
           } );
			
			a.addCommand('ds_gallery', {
				exec: function(e) {
					e.openDialog('gallery_dialog');
				}
			});
			a.ui.addButton('ds_gallery', {
				label: 'Insert Gallery',
				command: 'ds_gallery',
				icon: this.path + 'images/icon.png'
			});
		}
	});
} )();