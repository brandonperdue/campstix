<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$table_name = 'options';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(0); ?>
                <? if($r['id_product']){ ?>
                    <a href="products_.php?id=<?=$r['id_product'] ?>" class="btn btn-default" title="Product">
                        <i class="fa fa-fw fa-shopping-cart"></i> Product
                    </a>
                <? } ?>
                <hr>
                <? button('back', "option_categories.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Options</h1>
                <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width: 100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Category</b><br/><?
                            $rows = $db->query("SELECT * FROM option_categories ORDER BY priority ASC")
                            ?>
                            <select name="id_categories">
                                <option value=""></option>
                                <?
                                $show_svgs = false;
                                foreach( $rows as $row ){
                                    if($row['svg_cat'] && $row['id']==$r['id_categories']){
                                        $show_svgs = true;
                                    }
                                    ?>
                                    <option <?=$row['svg_cat']?'svg="true"':'' ?> value="<?= $row['id'] ?>" <?= $row['id']==$r['id_categories'] ? 'selected="selected"':'' ?>>
                                    <?= $row['title'] ?>
                                    </option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                    <h2>Hide the Following when selected</h2>
                    <?
                    $cc = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");
                    foreach ($cc as $c) {

                        $query = $db->prepare("SELECT * FROM options WHERE id_categories = ? ORDER BY priority DESC");
                        $query->execute(array($c['id']));
                        $oo = $query->fetchAll();
                        if (count($oo)) {
                            ?>
                            <h3><?= $c['title'] ?></h3>
                            <?
                            foreach ($oo as $o) {
                                $query = $db->prepare("SELECT * FROM  option_hide WHERE hide_oid = ? AND oid = ?");
                                $query->execute(array($o['id'], $r['id']));
                                $po = $query->fetch();
                                ?>
                                <div class="col-md-3">
                                    <label>
                                        <input type="checkbox"
                                               name="option[]"
                                               value="<?= $o['id'] ?>" <? if ($po['id']) echo 'checked="checked"'; ?> />
                                        <?= $o['title'] ?>
                                    </label>
                                </div>
                            <?
                            }
                            ?>
                            <div class="checkbox-menu">
                                <button type="button" class="checkallbelow btn btn-primary"><i
                                        class="fa fa-check-square-o"></i> All
                                </button>
                                <button type="button" class="checknonebelow btn btn-cancel"><i
                                        class="fa fa-check-square-o"></i> None
                                </button>
                            </div>
                            <hr>
                        <?
                        }
                    }
                    ?>
                </div>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

