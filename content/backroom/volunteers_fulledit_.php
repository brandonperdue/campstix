<?
include '../common/config.php';

$table_name = 'volunteers';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);

include 'common/header.php';


?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="pid" value="<?= $r['pid'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                <? } ?>

                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Volunteer</h1>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Status</h2>

                        <div class="formfield">
                            <b>Status</b><br/>

                            <select name="status">
                                <option value=""></option>
                                <option <? if ($r['status'] == "Pending"){ ?>selected="selected"<? } ?> value="Pending">Pending</option>
                                <option <? if ($r['status'] == "Approved"){ ?>selected="selected"<? } ?> value="Approved">Approved</option>
                            </select>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <h2>Registration Information</h2>
                        <div class="formfield">
                            <b>Parent User</b><br/><?
                            write_select(array(
                                'db'      => $db,
                                'rows'    => $db->query("SELECT * FROM users ORDER BY user_email ASC"),
                                'label'   => 'display_name',
                                'value'   => 'id',
                                'current' => $r['uid'],
                                'name'    => 'uid',
                            ));
                            ?>
                        </div>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="firstname" value="<?= $r['firstname'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="lastname" value="<?= $r['lastname'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Date of Birth</b><br/>
                            <input type="text" name="bdate" class="date_single" value="<?= $r['bdate'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Gender</b><br/>

                            <select name="gender">
                                <option value=""></option>
                                <option <? if ($r['gender'] == "Male"){ ?>selected="selected"<? } ?> value="Male">Male</option>
                                <option <? if ($r['gender'] == "Female"){ ?>selected="selected"<? } ?> value="Female">Female</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>T-Shirt Size</b><br/>

                            <select name="tshirt" id="">
                                <option value=""></option>
                                <option <? if ($r['tshirt'] == "Youth X-Small"){ ?>selected="selected"<? } ?> value="Youth X-Small">Youth X-Small
                                </option>
                                <option <? if ($r['tshirt'] == "Youth Small"){ ?>selected="selected"<? } ?> value="Youth Small">Youth Small</option>
                                <option <? if ($r['tshirt'] == "Youth Medium"){ ?>selected="selected"<? } ?> value="Youth Medium">Youth Medium
                                </option>
                                <option <? if ($r['tshirt'] == "Youth Large"){ ?>selected="selected"<? } ?> value="Youth Large">Youth Large</option>
                                <option <? if ($r['tshirt'] == "Youth X-Large"){ ?>selected="selected"<? } ?> value="Youth X-Large">Youth X-Large
                                </option>
                                <option <? if ($r['tshirt'] == "Adult Small"){ ?>selected="selected"<? } ?> value="Adult Small">Adult Small</option>
                                <option <? if ($r['tshirt'] == "Adult Medium"){ ?>selected="selected"<? } ?> value="Adult Medium">Adult Medium
                                </option>
                                <option <? if ($r['tshirt'] == "Adult Large"){ ?>selected="selected"<? } ?> value="Adult Large">Adult Large</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>New to Camp STIX/TWIGS?</b><br/>

                            <select name="new_to_camp">
                                <option value=""></option>
                                <option <? if ($r['new_to_camp'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['new_to_camp'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>If the camper has not attended previously, how did you hear about camp?</b><br/>

                            <input type="text" name="heard_about_from" value="<?= $r['heard_about_from'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Does this camper have diabetes?</b><br/>

                            <select name="has_diabetes" class="shownextonyes" rel="has_diabetesmore">
                                <option value=""></option>
                                <option <? if ($r['has_diabetes'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['has_diabetes'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="has_diabetesmore">
                                Age at Diagnosis<br>
                                <select name="age_of_diagnosis">
                                    <option <? if ($r['age_of_diagnosis'] == "0"){ ?>selected="selected"<? } ?> value="0">0</option>
                                    <option <? if ($r['age_of_diagnosis'] == "1"){ ?>selected="selected"<? } ?> value="1">1</option>
                                    <option <? if ($r['age_of_diagnosis'] == "2"){ ?>selected="selected"<? } ?> value="2">2</option>
                                    <option <? if ($r['age_of_diagnosis'] == "3"){ ?>selected="selected"<? } ?> value="3">3</option>
                                    <option <? if ($r['age_of_diagnosis'] == "4"){ ?>selected="selected"<? } ?> value="4">4</option>
                                    <option <? if ($r['age_of_diagnosis'] == "5"){ ?>selected="selected"<? } ?> value="5">5</option>
                                    <option <? if ($r['age_of_diagnosis'] == "6"){ ?>selected="selected"<? } ?> value="6">6</option>
                                    <option <? if ($r['age_of_diagnosis'] == "7"){ ?>selected="selected"<? } ?> value="7">7</option>
                                    <option <? if ($r['age_of_diagnosis'] == "8"){ ?>selected="selected"<? } ?> value="8">8</option>
                                    <option <? if ($r['age_of_diagnosis'] == "9"){ ?>selected="selected"<? } ?> value="9">9</option>
                                </select>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Marital status of camper's parent(s)/guardian(s)</b><br/>

                            <textarea name="parent_status"><?= $r['parent_status'] ?></textarea>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Who has legal custody of the camper?
                                <span
                                    class="note">Please list the first and last name of the individuals that have legal custody of this camper here</span>
                                <br/>

                                <textarea name="legal_custody"><?= $r['legal_custody'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>Family History
                                <span class="note">To help best serve the child, please provide any notes regarding the family (i.e. recent divorce, remarriage, death, or other pertinent event)</span>
                                <br/>

                                <textarea name="family_history"><?= $r['family_history'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <h2>Information Collected for Fundraising and Grant Writing</h2>
                        </div>
                        <div class="formfield">
                            <b>
                                Camper's Ethnicity
                                <br/>

                                <select name="ethnicity">
                                    <option value=""></option>
                                    <option <? if ($r['ethnicity'] == "American Indian or Alaska Native"){ ?>selected="selected"<? } ?>
                                            value="American Indian or Alaska Native">American Indian or Alaska Native
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Asian"){ ?>selected="selected"<? } ?> value="Asian">Asian</option>
                                    <option <? if ($r['ethnicity'] == "Black or African American"){ ?>selected="selected"<? } ?>
                                            value="Black or African American">Black
                                        or African American
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Hispanic or Latino"){ ?>selected="selected"<? } ?> value="Hispanic or Latino">
                                        Hispanic or Latino
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Native Hawaiian or Other Pacific Islander"){ ?>selected="selected"<? } ?>
                                            value="Native Hawaiian or Other Pacific Islander">Native Hawaiian or Other Pacific Islander
                                    </option>
                                    <option <? if ($r['ethnicity'] == "White"){ ?>selected="selected"<? } ?> value="White">White</option>
                                    <option <? if ($r['ethnicity'] == "Undisclosed"){ ?>selected="selected"<? } ?> value="Undisclosed">Undisclosed
                                    </option>
                                </select>
                                <br/>
                        </div>

                        <h2>Medical and Activity Information</h2>

                        <div class="formfield">
                            <div class="note">
                                Please provide us with as detailed information as possible regarding the medical and dietary information requested
                                below. It is your
                                responsibility to be sure any reported allergies or medically prescribed diets are also indicated on the physician
                                release forms.
                            </div>
                        </div>
                        <div class="formfield">
                            <b>Insurance Group</b><br/>

                            <input type="text" name="insurance_group" value="<?= $r['insurance_group'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Policy Number</b><br/>

                            <input type="text" name="policy_number" value="<?= $r['policy_number'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Name of Diabetes Healthcare Provider</b><br/>

                            <input type="text" name="diabetes_provider" value="<?= $r['diabetes_provider'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Diabetes Provider's Phone Number</b><br/>

                            <input type="text" name="provider_phone" value="<?= $r['provider_phone'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Name of Primary Care Provider/Pediatrician</b><br/>

                            <input type="text" name="primary_care_name" value="<?= $r['primary_care_name'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Primary Care Provider's Phone Number</b><br/>

                            <input type="text" name="primary_provider_phone" value="<?= $r['primary_provider_phone'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Diabetes Care Opportunities
                                <span class="note">With what aspect(s) of diabetes care would you like help for your child during camp? (Examples include rotating pump sites, trying new injection areas, learning to count carbs, calculating insulin , doing their pump site by themselves, etc.)</span>
                                <br/>

                                <textarea name="care_opportunities"><?= $r['care_opportunities'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Besides diabetes, does your child experience any physical, emotional, behavioral, learning, or social challenges?<span
                                    class="reqstar">*</span> </b><br/>

                            <select name="other_challenges" class="shownextonyes" rel="other_challenges_more">
                                <option value=""></option>
                                <option <? if ($r['other_challenges'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['other_challenges'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="other_challenges_more">
                                If "Yes", please explain:<br>
                                <textarea name="other_challenges_more"><?= $r['other_challenges_more'] ?></textarea>
                <span class="note">
                    Camp STIX/TWIGS volunteers strive to meet the individual needs of children with various challenges and want to make camp a positive experience for all campers. If you answered “yes” to this question we will contact you before camp to discuss your child’s individual needs and determine if they can be appropriately supported at camp. We work hard to try to accommodate children with social, emotional and behavioral difficulties, but due to the nature of camp (physical environment, staff to camper ratio, experience of volunteer staff), some challenges are not manageable in the camp environment. If you have any questions or additional information you feel we should know, please email the Camp Director at camptwigs@campstix.org.
                </span>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Are there any activities in which the camper should not participate?</b><br/>

                            <select name="other_activities" class="shownextonyes" rel="other_activities_more">
                                <option value=""></option>
                                <option <? if ($r['other_activities'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['other_activities'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="other_activities_more">
                                If "Yes", please list the activities:<br>
                                <textarea name="other_activities_more"
                                          placeholder="I.E.: rope course, swimming, climbing wall, etc."><?= $r['other_activities_more'] ?></textarea>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Does your camper have any medical allergies (I.E.: medications, latex, bee stings, etc)?</b><br/>

                            <select name="allergies" class="shownextonyes" rel="allergies_more">
                                <option value=""></option>
                                <option <? if ($r['allergies'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['allergies'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="allergies_more">
                                If "Yes", please list all allergies and the type of reaction to them:<br>
                                <textarea name="allergies_more"
                                          placeholder="I.E.: penicillin causes rash or bee stings cause anaphylaxis"><?= $r['allergies_more'] ?></textarea>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is your camper allergic to any foods (I.E.: peanuts, shellfish, dairy, eggs, soy, gluten/wheat, nuts, fruit,
                                etc.)?
                                <div class="note">
                                    This information is ahead of camp to allow for the planning of the diets and ordering of the necessary specialty
                                    diet items.
                                </div>
                                <br/>


                                <select name="allergies_food" class="shownextonyes" rel="allergies_food_more">
                                    <option value=""></option>
                                    <option <? if ($r['allergies_food'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['allergies_food'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="allergies_food_more">
                                    If "Yes", please list all food allergies and the type of reaction to them:<br>
                                    <textarea name="allergies_food_more"
                                              placeholder="I.E.: penicillin causes rash or bee stings cause anaphylaxis"><?= $r['allergies_food_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is your camper on a medically prescribed diet (I.E.: gluten free, dairy free)?
                                <br/>

                                <select name="medical_diet" class="shownextonyes" rel="medical_diet_more">
                                    <option value=""></option>
                                    <option <? if ($r['medical_diet'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['medical_diet'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="medical_diet_more">
                                    If "Yes", please list:<br>
                                    <textarea name="medical_diet_more"><?= $r['medical_diet_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is there any reason that your camper would carry, or have a need for, an Epi-Pen?
                                <br/>

                                <select name="epipen" class="shownextonyes" rel="epipen_more">
                                    <option value=""></option>
                                    <option <? if ($r['epipen'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['epipen'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="epipen_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="epipen_more"><?= $r['epipen_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Does your camper have any history of asthma or other respiratory issues?
                                <br/>

                                <select name="asthma">
                                    <option value=""></option>
                                    <option <? if ($r['asthma'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['asthma'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="asthma_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="asthma_more"><?= $r['asthma_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is there any other medical or dietary information you feel it would be important for us to be aware of during your
                                camper’s time at camp?
                                <br/>

                                <select name="important_medical_info">
                                    <option value=""></option>
                                    <option <? if ($r['important_medical_info'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['important_medical_info'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="important_medical_info_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="important_medical_info_more"><?= $r['important_medical_info_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Are immunizations up to date for your camper?
                                <br/>

                                <select name="immunizations_up_to_date">
                                    <option value=""></option>
                                    <option <? if ($r['immunizations_up_to_date'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['immunizations_up_to_date'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                    <option <? if ($r['immunizations_up_to_date'] == "We do not immunize"){ ?>selected="selected"<? } ?>
                                            value="We do not immunize">We do not immunize
                                    </option>
                                </select>
                                <div id="immunizations_date">
                                    Please enter the date of the camper's last tetanus shot:<br>
                                    <input type="text" name="immunizations_date" class="date_single" value="<?= $r['immunizations_date'] ?>"/>
                                </div>
                                <div id="immunizations_up_to_date_more">
                                    If your family chooses NOT to immunize, we require that you state your reason in the following comments field
                                    (medical, personal/philosophical or religious reasons):<br>
                                    <textarea name="immunizations_up_to_date_more"><?= $r['immunizations_up_to_date_more'] ?></textarea>
                                </div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <h2>Parent/Guardian Information</h2>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="parent_fname" value="<?= $r['parent_fname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="parent_lname" value="<?= $r['parent_lname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Relationship to Camper</b><br/>
                            <select name="guardian_relation" id="">
                                <option value=""></option>
                                <option <? if ($r['guardian_relation'] == "Mother"){ ?>selected="selected"<? } ?> value="Mother">Mother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Father"){ ?>selected="selected"<? } ?> value="Father">Father
                                </option>
                                <option <? if ($r['guardian_relation'] == "Grandmother"){ ?>selected="selected"<? } ?> value="Grandmother">
                                    Grandmother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Grandfather"){ ?>selected="selected"<? } ?> value="Grandfather">
                                    Grandfather
                                </option>
                                <option <? if ($r['guardian_relation'] == "Stepfather"){ ?>selected="selected"<? } ?> value="Stepfather">
                                    Stepfather
                                </option>
                                <option <? if ($r['guardian_relation'] == "Stepmother"){ ?>selected="selected"<? } ?> value="Stepmother">
                                    Stepmother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Email Address</b><br/>

                            <input type="text" name="parent_email" value="<?= $r['parent_email'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Would you like to receive updates from Camp STIX/TWIGS about camper registration announcements and important dates?</b><br/>
                            <select name="newsletter_signup">
                                <option value=""></option>
                                <option <? if ($r['newsletter_signup'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['newsletter_signup'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Preferred Phone Number</b><br/>

                            <input type="text" name="parent_phone" value="<?= $r['parent_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>May we send text messages to the number above to contact you regarding your camper?</b><br/>
                            <select name="can_we_text_you">
                                <option value=""></option>
                                <option <? if ($r['can_we_text_you'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['can_we_text_you'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Alternate Phone Number</b><br/>

                            <input type="text" name="parent_alt_phone" value="<?= $r['parent_alt_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Address</b><br/>

                            <input type="text" name="parent_address" value="<?= $r['parent_address'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>City</b><br/>

                            <input type="text" name="parent_city" value="<?= $r['parent_city'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>State</b><br/>

                            <select name="parent_state">
                                <option value=""></option>
                                <? foreach ($config['us_states'] as $abr => $full) { ?>
                                    <option <? if ($r['parent_state'] == $abr){ ?>selected="selected"<? } ?> value="<?= $abr ?>"><?= $full ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Zip</b><br/>

                            <input type="text" name="parent_zip" value="<?= $r['parent_zip'] ?>"/>
                        </div>


                        <h2>Secondary Emergency Contact Information</h2>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="emergencycontact_fname" value="<?= $r['emergencycontact_fname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="emergencycontact_lname" value="<?= $r['emergencycontact_lname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Relationship to Camper</b><br/>
                            <select name="emergencycontact_relation" id="">
                                <option value=""></option>
                                <option <? if ($r['emergencycontact_relation'] == "Mother"){ ?>selected="selected"<? } ?> value="Mother">Mother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Father"){ ?>selected="selected"<? } ?> value="Father">Father
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Grandmother"){ ?>selected="selected"<? } ?> value="Grandmother">
                                    Grandmother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Grandfather"){ ?>selected="selected"<? } ?> value="Grandfather">
                                    Grandfather
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Stepfather"){ ?>selected="selected"<? } ?> value="Stepfather">
                                    Stepfather
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Stepmother"){ ?>selected="selected"<? } ?> value="Stepmother">
                                    Stepmother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Preferred Phone Number</b><br/>
                            <input type="text" name="emergencycontact_phone" value="<?= $r['emergencycontact_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Alternate Phone Number</b><br/>
                            <input type="text" name="emergencycontact_alt_phone" value="<?= $r['emergencycontact_alt_phone'] ?>"/>
                        </div>



                        <h2>Camp STIX Specific Fields</h2>
                        <div class="formfield">
                            <b>Preferred Name</b><br/>
                            <input type="text" name="preferred_name" value="<?= $r['preferred_name'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Do you live within 500 miles of Spokane, WA?</b><br/>
                            <select name="within_500_miles">
                                <option value=""></option>
                                <option <? if ($r['within_500_miles'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['within_500_miles'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Will this be your child’s first time away from home?</b><br/>
                            <select name="first_time_away_from_home">
                                <option value=""></option>
                                <option <? if ($r['first_time_away_from_home'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['first_time_away_from_home'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Does your camper wet the bed?</b><br/>
                            <select name="bedwetter">
                                <option value=""></option>
                                <option <? if ($r['bedwetter'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['bedwetter'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="important_medical_info_more">
                                If "Yes", please describe strategies, preferences, etc.:<br>
                                <textarea name="bedwetter_more"><?= $r['bedwetter_more'] ?></textarea>
                            </div>
                        </div>
                        <div class="formfield">
                            <b>Parents Employer</b><br/>
                            <input type="text" name="parent_employer" value="<?= $r['parent_employer'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Permission is given to Camp STIX to transport my camper(s) to and from camp activities sponsored by the camp and/or to a medical facility for emergency care.</b><br/>
                            <input type="text" name="release_transport" value="<?= $r['release_transport'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Permission is given to Camp STIX to use appropriate images of my camper(s) in any photos or videos that will be approved by the Camp STIX Board of Directors to be displayed in promotional and entertainment media which is created to benefit the work of Camp STIX. I understand that there will be no compensation made in exchange for use of any photos or videos as outlined above.</b><br/>
                            <input type="text" name="release_images" value="<?= $r['release_images'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Dress Code and understand that my camper(s) will be held accountable to it while at camp.</b><br/>
                            <input type="text" name="release_dresscode" value="<?= $r['release_dresscode'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Camper Expectations and understand that my camper(s) will be held accountable to them while at camp.</b><br/>
                            <input type="text" name="release_expectations" value="<?= $r['release_expectations'] ?>" style="width:100px;" />
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Code and understand that my camper(s) will be held accountable to it while at camp.</b><br/>
                            <input type="text" name="release_code" value="<?= $r['release_code'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Riverview Bible Camp Release and Arbitration Agreement carefully and understand it.</b><br/>
                            <input type="text" name="release_biblecamp" value="<?= $r['release_biblecamp'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Parents Full Legal Name</b><br/>
                            <input type="text" name="signature_full_legal_name" value="<?= $r['signature_full_legal_name'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Date Signed</b><br/>
                            <input type="text" class="date_single" name="signature_date" value="<?= $r['signature_date'] ?>"/>
                        </div>
                    </div>
                </div>
                <table class="ecom_tbl">
                    <tr>
                        <th colspan="2">Registration Information</th>
                    </tr>
                    <tr>
                        <td class="labeltd" style="width: 50%;">First Name</td>
                        <td>
                            <input type="text" name="firstname" value="{{ v.firstname }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Last Name</td>
                        <td>
                            <input type="text" name="lastname" value="{{ v.lastname }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Date of Birth</td>
                        <td>
                            <input type="text" name="bdate" class="date_single" value="{{ v.bdate }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Gender</td>
                        <td>
                            <select name="gender" required>
                                <option value=""></option>
                                <option <? if( v.gender == "Male"){ ?>selected="selected"<? } ?> value="Male">Male</option>
                                <option <? if( v.gender == "Female"){ ?>selected="selected"<? } ?> value="Female">Female</option>
                                <option <? if( v.gender == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">T-Shirt Size</td>
                        <td>
                            <select name="tshirt" id="" required>
                                <option value=""></option>
                                <option <? if( v.tshirt=="Adult XS"){ ?>selected="selected"<? } ?> value="Adult XS">Adult XS</option>
                                <option <? if( v.tshirt=="Adult Small"){ ?>selected="selected"<? } ?> value="Adult Small">Adult Small</option>
                                <option <? if( v.tshirt=="Adult Medium"){ ?>selected="selected"<? } ?> value="Adult Medium">Adult Medium</option>
                                <option <? if( v.tshirt=="Adult Large"){ ?>selected="selected"<? } ?> value="Adult Large">Adult Large</option>
                                <option <? if( v.tshirt=="Adult XL"){ ?>selected="selected"<? } ?> value="Adult XL">Adult XL</option>
                                <option <? if( v.tshirt=="Adult 2XL"){ ?>selected="selected"<? } ?> value="Adult 2XL">Adult 2XL</option>
                                <option <? if( v.tshirt=="Adult 3XL"){ ?>selected="selected"<? } ?> value="Adult 3XL">Adult 3XL</option>
                                <option <? if( v.tshirt=="Adult 4XL"){ ?>selected="selected"<? } ?> value="Adult 4XL">Adult 4XL</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Is this your first year at Camp STIX/TWIGS?</td>
                        <td>
                            <select name="newtocamp" class="shownextonyes" show="Yes" hide="returncamp" rel="newcamp" required>
                                <option value=""></option>
                                <option <? if( v.newtocamp == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.newtocamp == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>

                            <div id="returncamp" style="display: none;">
                                Camp Name?:<br>
                                <input type="text" name="campname_return" value="{{ v.campname_return }}" />
                                Which team were you on the last time you volunteered at Camp STIX?:<br>
                                <select name="last_volunteer_team">
                                    <option value=""></option>
                                    <option <? if( v.last_volunteer_team == "DISH" ?>selected="selected"<? } ?> value="DISH">DISH</option>
                                    <option <? if( v.last_volunteer_team == "MASH" ?>selected="selected"<? } ?> value="MASH">MASH</option>
                                    <option <? if( v.last_volunteer_team == "Program" ?>selected="selected"<? } ?> value="Program">Program</option>
                                    <option <? if( v.last_volunteer_team == "Support" ?>selected="selected"<? } ?> value="Support">Support</option>
                                </select>
                                What year was the last time you volunteered at Camp STIX?:<br>
                                <input type="text" name="last_volunteer_year" value="{{ v.last_volunteer_year }}" />
                            </div>
                            <div id="newcamp" style="display: none;">
                                Have you ever been a camper at Camp Stix:<br>
                                <select name="previouscamper">
                                    <option value=""></option>
                                    <option <? if( v.previouscamper == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if( v.previouscamper == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                How did you hear about Camp STIX?:<br>
                                <input type="text" name="heardfrom" value="{{ v.heardfrom }}" />
                                Do you have a Camp Name?:<br>
                                <div class="note">
                                    If you have volunteered at a summer camp before and received a camp name, please enter it here. If not, no worries! You will receive a name at camp! Just remember, you don't get to name yourself!
                                </div>
                                <input type="text" name="campname_new" class="neverreq" value="{{ v.campname_new }}" />
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <th colspan="2">Your Contact Information</th>
                    </tr>
                    <tr>
                        <td class="labeltd">Email Address</td>
                        <td>
                            <input type="text" name="contact_email" value="{{ v.contact_email }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Email Address
                            <div class="note">Please confirm your email address as this is our primary method of communication.</div>
                        </td>
                        <td>
                            <input type="text" name="confirm_contact_email" value="{{ v.confirm_contact_email }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Phone Number</td>
                        <td>
                            <input type="text" name="contact_phone" value="{{ v.contact_phone }}" placeholder="888 888 8888" onkeyup="this.value=addSpaces(this.value);" pattern="[0-9]{3} [0-9]{3} [0-9]{4}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Can you recieve texts at this number?
                            <div class="note">If possible, please put a number that we can text for your phone number.</div>
                        </td>
                        <td>
                            <select name="can_we_text_you" required>
                                <option value=""></option>
                                <option <? if( v.can_we_text_you == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.can_we_text_you == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">City of Residence</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                Some departments hold pre-camp meetings in different cities. This helps us coordinate those meetings.
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">City</td>
                        <td>
                            <input type="text" name="contact_city" value="{{ v.contact_city }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">State</td>
                        <td>
                            <select name="contact_state" required>
                                <option value=""></option>
                                <? for abr, full in config.us_states ?>
                                <option <? if( v.contact_state == abr){ ?>selected="selected"<? } ?> value="{{ abr }}">{{ full }}</option>
                                <? endfor ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2">Emergency Contact Information</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                In case of an emergency involving you, who should we contact?
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd" style="width: 50%;">First Name</td>
                        <td>
                            <input type="text" name="emergencycontact_fname" value="{{ v.emergencycontact_fname }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Last Name</td>
                        <td>
                            <input type="text" name="emergencycontact_lname" value="{{ v.emergencycontact_lname }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Relationship to You</td>
                        <td>
                            <input type="text" name="emergencycontact_relation" value="{{ v.emergencycontact_relation }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Phone Number</td>
                        <td>
                            <input type="text" name="emergencycontact_phone" value="{{ v.emergencycontact_phone }}" placeholder="888 888 8888" onkeyup="this.value=addSpaces(this.value);" pattern="[0-9]{3} [0-9]{3} [0-9]{4}" required />
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Medical Information</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                This information is to help us provide a great camp experience for you! All of your information will remain confidential.
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Do you have Type 1 diabetes?</td>
                        <td>
                            <select name="hasdiabetes" required>
                                <option value=""></option>
                                <option <? if( v.hasdiabetes == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.hasdiabetes == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Do you have any medical allergies?
                            <div class="note">(Medications, latex, bee stings, etc)</div>
                        </td>
                        <td>
                            <select name="allergies" class="shownextonyes" rel="allergiesmore" required>
                                <option value=""></option>
                                <option <? if( v.allergies == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.allergies == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="allergiesmore" class="showmore" style="display: none;">
                                If "Yes", please list all medical allergies and the type of reaction to them:<br>
                                <textarea name="allergiesmore" placeholder="I.E.: penicillin causes rash or bee stings cause anaphylaxis">{{ v.allergiesmore }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Are you allergic to any foods?
                            <div class="note">(Peanuts, shellfish, dairy, eggs, soy, gluten/wheat, nuts, fruit, etc.)</div>
                        </td>

                        <td>
                            <select name="allergiesfood" class="shownextonyes" rel="allergiesfoodmore" required>
                                <option value=""></option>
                                <option <? if( v.allergiesfood == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.allergiesfood == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="allergiesfoodmore" style="display: none;">
                                If "Yes", please list all food allergies and the type of reaction to them:<br>
                                <textarea name="allergiesfoodmore">{{ v.allergiesfoodmore }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Are you on a medically prescribed diet?
                            <div class="note">(Gluten free, dairy free, etc.)</div>
                        </td>
                        <td>
                            <select name="medicaldiet" class="shownextonyes" rel="medicaldietmore" required>
                                <option value=""></option>
                                <option <? if( v.medicaldiet == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.medicaldiet == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="medicaldietmore" style="display: none;">
                                If "Yes", please list:<br>
                                <textarea name="medicaldietmore" >{{ v.medicaldietmore }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Are there any other dietary needs that we should be aware of? </td>
                        <td>
                            <select name="otherdiataryneeds" class="shownextonyes" rel="otherdiataryneeds_more" required>
                                <option value=""></option>
                                <option <? if( v.otherdiataryneeds == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.otherdiataryneeds == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="otherdiataryneeds_more" class="showmore" style="display: none;">
                                If "Yes", please list:<br>
                                <textarea name="otherdiataryneeds_more">{{ v.otherdiataryneeds_more }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Do you have any disabilities for which you'd like Camp STIX/TWIGS to make provisions, to help you perform the essential functions of the position you are seeking?</td>
                        <td>
                            <select name="otherdisabilities" class="shownextonyes" rel="otherdisabilities_more" required>
                                <option value=""></option>
                                <option <? if( v.otherdisabilities == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.otherdisabilities == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="otherdisabilities_more" class="showmore" style="display: none;">
                                If "Yes", please list the disabilites:<br>
                                <textarea name="otherdisabilities_more">{{ v.otherdisabilities_more }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Would you be able to fulfill your required duties on limited sleep?</td>
                        <td>
                            <select name="limitedsleep" class="shownextonyes" rel="limitedsleep_more" required>
                                <option value=""></option>
                                <option <? if( v.limitedsleep == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.limitedsleep == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="limitedsleep_more" class="showmore" style="display: none;">
                                If "Yes", please list the disabilites:<br>
                                <textarea name="limitedsleep_more">{{ v.limitedsleep_more }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Do you have any other medical conditions we should be aware of?
                        </td>
                        <td>
                            <select name="otherconditions" class="shownextonyes" rel="otherconditions_more" required>
                                <option value=""></option>
                                <option <? if( v.otherconditions == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.otherconditions == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="otherconditions" style="display: none;">
                                If "Yes", please explain:<br>
                                <textarea name="otherconditions_more" >{{ v.otherconditions_more }}</textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Your Experience</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">A chance for you to tell us a little bit more about yourself to help us get you know you better</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">What is the highest educational or training degree you have accomplished?</td>
                        <td>
                            <input type="text" name="highestdegree" value="{{ v.highestdegree }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">How many years have you volunteered as staff at a diabetes summer camp?</td>
                        <td>
                            <input type="text" name="yearsvolunteered" value="{{ v.yearsvolunteered }}" required />
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">Do you have a preference for the age of camper you work with?
                            <div class="note">If No, leave blank</div>
                        </td>
                        <td>
                            <input type="text" name="preferred_camper_age" value="{{ v.preferred_camper_age }}" />
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Department Selection</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                <p>At Camp STIX/TWIGS we have four main departments:</p>

                                <p>DISH (Kitchen)</p>
                                <p>MASH (Medical)</p>
                                <p>Program (Counselors, Junior Counselors)</p>
                                <p>Support (Photography, Education, Arts and Crafts, Administrative, Camper Advocate)</p>

                                <p>You can apply for all departments or just one. This application will ask you about each department one at a time starting with DISH below. At the end of the application you will be able to list the order of your preferences of departments that you would like to be considered for selection.</p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">DISH</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                Put a smile on everyone's faces by helping prepare and serve the wonderful meals carefully crafted by our DISH team! All you need is a food handler's permit and a great attitude!
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Would you like to be considered for DISH?
                        </td>
                        <td>
                            <select name="dish" class="shownextonyes" rel="dish_more" required>
                                <option value=""></option>
                                <option <? if( v.dish == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.dish == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="dish_more">
                            <table>
                                <tr>
                                    <td class="labeltd" style="width:50%; border-top:0;">
                                        When are you available?
                                        <div class="note">
                                            DISH team members will arrive at camp as early as 2 PM on Friday, June 22nd, 2018 and stay through the afternoon of Saturday, June 30th, 2018.
                                        </div>
                                    </td>
                                    <td style="border-top:0;">
                                        <input type="text" name="dish_availabletime" value="{{ v.dish_availabletime }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Do you currently have your Washington State Food Worker Card?</td>
                                    <td>
                                        <select name="dish_current_foodscard">
                                            <option value=""></option>
                                            <option <? if( v.dish_current_foodscard == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.dish_current_foodscard == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Current employment or training situation</td>
                                    <td>
                                        <input type="text" name="dish_currentemployment" value="{{ v.dish_currentemployment }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What, if any, culinary skills or background do you have?</td>
                                    <td>
                                        <input type="text" name="dish_skills" value="{{ v.dish_skills }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What, if any, restaurant management skills or background do you have?</td>
                                    <td>
                                        <input type="text" name="dish_management_skills" value="{{ v.dish_management_skills }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What, if any, dietary/nutritional credentials do you have?</td>
                                    <td>
                                        <input type="text" name="dish_dietary_credentials" value="{{ v.dish_dietary_credentials }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What, if any, specialty diet skills or background do you have?</td>
                                    <td>
                                        <input type="text" name="dish_specialty_diet_skills" value="{{ v.dish_specialty_diet_skills }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Please list any job preferences or talents that you may have in the kitchen arena?
                                    </td>
                                    <td>
                                        <input type="text" name="dish_job_preference" value="{{ v.dish_job_preference }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        What are your expectations for working in DISH?
                                    </td>
                                    <td>
                                        <input type="text" name="dish_expectations" value="{{ v.dish_expectations }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        What is your best quality you can offer the DISH department?
                                    </td>
                                    <td>
                                        <input type="text" name="dish_best_quality" value="{{ v.dish_best_quality }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        How could we best utilize your talents/skills in DISH?
                                    </td>
                                    <td>
                                        <input type="text" name="dish_best_utilize" value="{{ v.dish_best_utilize }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Are there any considerations you may need?
                                        <div class="note">The DISH department is a fast-paced environment that will be physically demanding at times. It requires teamwork and the ability to work in close-quarters with one another. Will you be comfortable working in this environment?</div>
                                    </td>
                                    <td>
                                        <input type="text" name="dish_any_considerations" value="{{ v.dish_any_considerations }}" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">MASH</th>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Would you like to be considered for MASH (Medical)?
                        </td>
                        <td>
                            <select name="mash" class="shownextonyes" rel="mash_more" required>
                                <option value=""></option>
                                <option <? if( v.mash == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.mash == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="mash_more">
                            <table>
                                <tr>
                                    <td class="labeltd" style="width:50%; border-top:0;">
                                        What is your anticipated arrival time and departure?
                                    </td>
                                    <td style="border-top:0;">
                                        <input type="text" name="mash_availabletime" value="{{ v.mash_availabletime }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What is your Designation or Title?</td>
                                    <td>
                                        <input type="text" name="mash_title" value="{{ v.mash_title }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What current professional license do you hold?</td>
                                    <td>
                                        <input type="text" name="mash_license" value="{{ v.mash_license }}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Has your license ever been revoked?</td>
                                    <td>
                                        <select name="mash_licenserevoked" class="shownextonyes" rel="mash_licenserevoked_more">
                                            <option value=""></option>
                                            <option <? if( v.mash_licenserevoked == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.mash_licenserevoked == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                        <div id="mash_licenserevoked_more" class="showmore" style="display: none;">
                                            If "Yes", please explain:<br>
                                            <textarea name="mash_licenserevoked_more">{{ v.mash_licenserevoked_more }}</textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Have you ever been accused of, convicted of, or had a deferred adjudication of medical malpractice?</td>
                                    <td>
                                        <select name="mash_malpractice" class="shownextonyes" rel="mash_malpractice_more">
                                            <option value=""></option>
                                            <option <? if( v.mash_malpractice == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.mash_malpractice == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                        <div id="mash_malpractice_more" class="showmore" style="display: none;">
                                            If "Yes", please explain:<br>
                                            <textarea name="mash_malpractice_more">{{ v.mash_malpractice_more }}</textarea>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What specific expectations of your experience and role on the MASH team do you have while at Camp TWIGS/STIX?</td>
                                    <td>
                                        <input type="text" name="mash_expectation" value="{{ v.mash_expectation }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What is one of the toughest challenges you anticipate as a potential member of the MASH team?</td>
                                    <td>
                                        <input type="text" name="mash_toughestchallenges" value="{{ v.mash_toughestchallenges }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Camp requires long hours and times of little sleep. What challenges do you anticipate?</td>
                                    <td>
                                        <input type="text" name="mash_sleepchallenges" value="{{ v.mash_sleepchallenges }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        For returning staff; do you wish to be a team leader in training? If so, what goals do you have for growth over this year at camp?
                                        <div class="note">If you are not returning or do not wish to be a team leader, please leave blank.</div>
                                    </td>
                                    <td>
                                        <input type="text" name="mash_growth_goals" class="neverreq" value="{{ v.mash_growth_goals }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Describe one way you could improve the TWIGS/STIX/TWIGS experience for the campers as a member of the MASH team:</td>
                                    <td>
                                        <input type="text" name="mash_improveexperience" value="{{ v.mash_improveexperience }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What are some hobbies and interests?</td>
                                    <td>
                                        <input type="text" name="mash_hobbies" value="{{ v.mash_hobbies }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">If you are a returning member of MASH, what motivates you to return?
                                        <div class="note">If you are not a returning member of MASH, enter "N/A"</div>
                                    </td>
                                    <td>
                                        <input type="text" name="mash_return_motivation" value="{{ v.mash_return_motivation }}" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Program</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                Crazy campfire skits and wacky dance parties and enthusiastic campers, oh my! Take lead of a cabin of kids who will steal your heart, and maybe your afternoon snack, as a Counselor or Junior Counselor!
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Would you like to be considered for Program?
                            <div class="note">(Counselor or Junior Counselor)</div>
                        </td>
                        <td>
                            <select name="program" class="shownextonyes" rel="program_more" required>
                                <option value=""></option>
                                <option <? if( v.program == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.program == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="program_more">
                            <table>
                                <tr>
                                    <td class="labeltd" style="width:50%; border-top:0;">Program team members will arrive at camp at 4:00 PM on Friday, June 22nd, 2018 and stay through the afternoon of Saturday, June 30th, 2018. Are you available for that time?</td>
                                    <td style="border-top:0;">
                                        <input type="text" name="program_availabletime" value="{{ v.program_availabletime }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Which specific Program position are you applying for?</td>
                                    <td>
                                        <select name="program_position">
                                            <option value=""></option>
                                            <option <? if( v.program_position == "Counselor (age 18+)"){ ?>selected="selected"<? } ?> value="Counselor (age 18+)">Counselor (age 18+)</option>
                                            <option <? if( v.program_position == "Junior Counselor (age 16+)"){ ?>selected="selected"<? } ?> value="Junior Counselor (age 16+)">Junior Counselor (age 16+)</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        If you are not selected for the position you are applying for, would you like to be considered for another Program position?
                                    </td>
                                    <td>
                                        <select name="consider_another_program" required>
                                            <option value=""></option>
                                            <option <? if( v.consider_another_program == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.consider_another_program == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        What types of activities do you enjoy and feel you could lead?
                                    </td>
                                    <td>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="40-ft Slide" <? if( "40-ft Slide" in v.program_lead ?>checked="checked"<? } ?>> 40-ft Slide</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="60-ft Vertical Playground" <? if( "60-ft Vertical Playground" in v.program_lead ?>checked="checked"<? } ?>> 60-ft Vertical Playground</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Archery" <? if( "Archery" in v.program_lead ?>checked="checked"<? } ?>> Archery</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Basketball" <? if( "Basketball" in v.program_lead ?>checked="checked"<? } ?>> Basketball</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Canoes" <? if( "Canoes" in v.program_lead ?>checked="checked"<? } ?>> Canoes</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Crafts" <? if( "Crafts" in v.program_lead ?>checked="checked"<? } ?>> Crafts</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Gaga (dodge) Ball" <? if( "Gaga (dodge) Ball" in v.program_lead ?>checked="checked"<? } ?>> Gaga (dodge) Ball</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="High Ropes Course" <? if( "High Ropes Course" in v.program_lead ?>checked="checked"<? } ?>> High Ropes Course Wall</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Low Ropes Course" <? if( "Low Ropes Course" in v.program_lead ?>checked="checked"<? } ?>> Low Ropes Course Wall</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Pool Swim" <? if( "Pool Swim" in v.program_lead ?>checked="checked"<? } ?>> Pool Swim</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Rappelling Tower" <? if( "Rappelling Tower" in v.program_lead ?>checked="checked"<? } ?>> Rappelling Tower</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Riverfront Swim" <? if( "Riverfront Swim" in v.program_lead ?>checked="checked"<? } ?>> Riverfront Swim</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Volleyball" <? if( "Volleyball" in v.program_lead ?>checked="checked"<? } ?>> Volleyball</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Water Balloon War Zone" <? if( "Water Balloon War Zone" in v.program_lead ?>checked="checked"<? } ?>> Water Balloon War Zone</label><br>
                                        <label><input type="checkbox" class="neverreq" name="program_lead[]" value="Zipline" <? if( "Zipline" in v.program_lead ?>checked="checked"<? } ?>> Zipline</label><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Do you feel comfortable speaking to an audience, sharing your personal experiences, or giving a brief inspirational talk?
                                    </td>
                                    <td>
                                        <select name="program_publicspeaking">
                                            <option value=""></option>
                                            <option <? if( v.program_publicspeaking == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.program_publicspeaking == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What contributions have you made at Camp STIX/TWIGS or at another similar camp (if you haven't been a part of Camp STIX/TWIGS before)?</td>
                                    <td>
                                        <input type="text" name="program_othercontributions" value="{{ v.program_othercontributions }}" class="neverreq" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What current certification, license, and/or training do you have which might be helpful in the position you seek?
                                    </td>
                                    <td>
                                        <input type="text" name="program_certifications" value="{{ v.program_certifications }}" class="neverreq" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What are your expectations for Camp STIX/TWIGS as a member of the Program team?</td>
                                    <td>
                                        <input type="text" name="program_expectations" value="{{ v.program_expectations }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What are some things you could do during camp to elevate your role as a leader for the team?</td>
                                    <td>
                                        <input type="text" name="program_elevateleader" value="{{ v.program_elevateleader }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What is one of the toughest challenges you anticipate as a potential member of the team?</td>
                                    <td>
                                        <input type="text" name="program_toughestchallenges" value="{{ v.program_toughestchallenges }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Describe one way you could improve the TWIGS/STIX/TWIGS experience for the campers as a member of the Program team:</td>
                                    <td>
                                        <input type="text" name="program_improveexperience" value="{{ v.program_improveexperience }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What is one thing about you that makes you stand out as a potential member of the Program team?</td>
                                    <td>
                                        <input type="text" name="program_standout" value="{{ v.program_standout }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        If you are younger than 18 and applying for a Junior Counselor position, would you like to attend as a camper, if not selected to be a Junior Counselor?
                                        
                                    </td>
                                    <td>
                                        <select name="program_under18_camper">
                                            <option value=""></option>
                                            <option <? if( v.program_under18_camper == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.program_under18_camper == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                            <option <? if( v.program_under18_camper == "I will be over 18 at the time of camp"){ ?>selected="selected"<? } ?> value="I will be over 18 at the time of camp">I will be over 18 at the time of camp</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2">Support</th>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Would you like to be considered for Support?
                        </td>
                        <td>
                            <select name="support" class="shownextonyes" rel="support_more" required>
                                <option value=""></option>
                                <option <? if( v.program == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.program == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="support_more">
                            <table>
                                <tr>
                                    <td class="labeltd" style="width:50%; border-top:0;">Support team members will arrive at camp at 4:00 PM on Friday, June 22nd, 2018 and stay through the afternoon of Saturday, June 30th, 2018. What of that time are you available for?</td>
                                    <td style="border-top:0;">
                                        <input type="text" name="support_availabletime" value="{{ v.support_availabletime }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Which specific Support position are you applying for?</td>
                                    <td>
                                        <select name="support_position">
                                            <option value=""></option>
                                            <option <? if( v.support_position == "Arts & Crafts"){ ?>selected="selected"<? } ?> value="Arts & Crafts">Arts & Crafts (Assist the Arts & Crafts Director with planned activities.)</option>
                                            <option <? if( v.support_position == "Photography"){ ?>selected="selected"<? } ?> value="Photography">Photography (Capture photos and video of camp activities.)</option>
                                            <option <? if( v.support_position == "Administrative"){ ?>selected="selected"<? } ?> value="Administrative">Administrative (Provide administrative support to the Camp Director. More fun than it sounds!)</option>
                                            <option <? if( v.support_position == "Camper Advocate"){ ?>selected="selected"<? } ?> value="Camper Advocate">Camper Advocate (Help monitor and maintain camper welfare. Applicants with counseling or social work experience are preferred.)</option>
                                            <option <? if( v.support_position == "Education"){ ?>selected="selected"<? } ?> value="Education">Education (Assist the Education Director with planned activities and skits.)</option>
                                            <option <? if( v.support_position == "Photography"){ ?>selected="selected"<? } ?> value="Photography">Photography (Capture photos and video of camp activities.)</option>
                                            <option <? if( v.support_position == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                                        </select>
                                        If Other:
                                        <input type="text" name="support_positionother" value="{{ v.support_positionother }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        If you are not selected for the position you are applying for, would you like to be considered for another Support team position?
                                    </td>
                                    <td>
                                        <select name="support_consider_another">
                                            <option value=""></option>
                                            <option <? if( v.support_consider_another == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.support_consider_another == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What contributions have you made at Camp STIX/TWIGS or at another similar camp (if you haven't been a part of Camp STIX/TWIGS before)? </td>
                                    <td>
                                        <input type="text" name="support_contributions" value="{{ v.support_contributions }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What are your expectations for Camp STIX/TWIGS as a member of the Support team?</td>
                                    <td>
                                        <input type="text" name="support_expectations" value="{{ v.support_expectations }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">Describe one way you could improve the STIX/TWIGS experience for the campers as a member of the Support team?</td>
                                    <td>
                                        <input type="text" name="support_improve" value="{{ v.support_improve }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">What is one thing about you that makes you stand out as a potential member of the Support team?</td>
                                    <td>
                                        <input type="text" name="support_standout" value="{{ v.support_standout }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        What specific experience do you have that applies to the role you are applying for?
                                    </td>
                                    <td>
                                        <input type="text" name="support_experience" value="{{ v.support_experience }}" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <th colspan="2">Department Selection</th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="note">
                                Please be sure that you have completed the application section for each of the departments that you would like to be considered for.
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Which department is your first preference for placement?
                        </td>
                        <td>
                            <select name="departmentpreference_1" required>
                                <option value=""></option>
                                <option <? if( v.departmentpreference_1 == "DISH"){ ?>selected="selected"<? } ?> value="DISH">DISH</option>
                                <option <? if( v.departmentpreference_1 == "MASH"){ ?>selected="selected"<? } ?> value="MASH">MASH</option>
                                <option <? if( v.departmentpreference_1 == "Program"){ ?>selected="selected"<? } ?> value="Program">Program</option>
                                <option <? if( v.departmentpreference_1 == "Support"){ ?>selected="selected"<? } ?> value="Support">Support</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Which department is your second preference for placement?
                            <div class="note">Respond below only if you wish to be considered for and have applied for more than one department. You can use the back button below to apply for another department at this time if you like.</div>
                        </td>
                        <td>
                            <select name="departmentpreference_2">
                                <option value=""></option>
                                <option <? if( v.departmentpreference_2 == "DISH"){ ?>selected="selected"<? } ?> value="DISH">DISH</option>
                                <option <? if( v.departmentpreference_2 == "MASH"){ ?>selected="selected"<? } ?> value="MASH">MASH</option>
                                <option <? if( v.departmentpreference_2 == "Program"){ ?>selected="selected"<? } ?> value="Program">Program</option>
                                <option <? if( v.departmentpreference_2 == "Support"){ ?>selected="selected"<? } ?> value="Support">Support</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2">Age Verification</th>
                    </tr>
                    <tr>
                        <td class="labeltd">
                            Are you 18 years of age or older as of completing this application?
                        </td>
                        <td>
                            <select name="olderthan18" class="shownextonyes" show="Yes" hide="youngerthan18_more" rel="olderthan18_more" required>
                                <option value=""></option>
                                <option <? if( v.olderthan18 == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.olderthan18 == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" id="olderthan18_more" style="display: none;">
                            <table>
                                <tr>
                                    <th colspan="2">Parent/Guardian of Camper</th>
                                </tr>
                                <tr>
                                    <td class="labeltd" style="width:50%; border-top:0;">
                                        Are you the parent/guardian of a child planning to attend camp as a camper this year?
                                    </td>
                                    <td style="border-top:0;">
                                        <select name="guardian_of_camper" class="shownextonyes" rel="guardian_of_camper_more">
                                            <option value=""></option>
                                            <option <? if( v.guardian_of_camper == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.guardian_of_camper == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" id="guardian_of_camper_more" style="display: none;">
                                        <b>Camp STIX/TWIGS Children of Volunteers Policy</b><br>
                                        <b>Have you read the Children of Volunteers Policy above and agree to discuss it with your camper and agree to abide by it?</b>
                                        <select name="read_COV_policy">
                                            <option value=""></option>
                                            <option <? if( v.read_COV_policy == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.read_COV_policy == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>

                                        Please list the full name(s) of all your camper(s) you plan to register for camp this year:<br>
                                        <input type="text" name="children_fullnames" value="{{ v.children_fullnames }}" />
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2">Volunteer Releases</th>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Have you read the above releases carefully and understand and agree to them?
                                    </td>
                                    <td>
                                        <select name="release_agree">
                                            <option value=""></option>
                                            <option <? if( v.release_agree == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.release_agree == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        I have read this waiver and release carefully, and understand it.
                                    </td>
                                    <td>
                                        <select name="camp_waiver_agree">
                                            <option value=""></option>
                                            <option <? if( v.camp_waiver_agree == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                            <option <? if( v.camp_waiver_agree == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Parent/Guardian Phone Number
                                        <div class="note">We will use this number to contact you if we have any questions in regard to this application. Please enter in the format: 123-123-1234</div>
                                    </td>
                                    <td>
                                        <input type="text" name="parent_vol_phone" value="{{ v.parent_vol_phone }}" />
                                    </td>
                                </tr>

                                <tr>
                                    <th colspan="2">Volunteers under 18</th>
                                </tr>
                                <tr>
                                    <th colspan="2">Parent/Guardian of Minor Volunteer Form</th>
                                </tr>
                                <tr>
                                    <td class="labeltd" style="width:50%;">
                                        Parent/Guardian Name
                                        <div class="note">By typing your full legal name below, you certify that you have read and agree to the Camp TWIGS Medical Release, Transportation Release, and Media Release for your child. You also are giving permission for your child to volunteer at Camp TWIGS.</div>
                                    </td>
                                    <td>
                                        <input type="text" name="parentname" value="{{ v.parentname }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Parent/Guardian Email Address
                                        <div class="note">We will send you an email to verify that you authorized this form.</div>
                                    </td>
                                    <td>
                                        <input type="text" name="parentemail" value="{{ v.parentemail }}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labeltd">
                                        Parent/Guardian Phone Number
                                        <div class="note">We will use this number to contact you if we have any questions in regard to this application. Please enter in the format: 123-123-1234</div>
                                    </td>
                                    <td>
                                        <input type="text" name="parentphone" value="{{ v.parentphone }}" />
                                    </td>
                                </tr>
                    <tr>
                        <th colspan="2">Camp STIX/TWIGS Policies</th>
                    </tr>
                    <tr>
                        <td class="labeltd" style="width: 50%;">
                            Have you read the Camp STIX/TWIGS Dress Code and agree to abide by the expectations set forth with the understanding that failure to comply may result in dismissal from camp? 
                        </td>
                        <td>
                            <select name="dresscode_agree">
                                <option value=""></option>
                                <option <? if( v.dresscode_agree == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.dresscode_agree == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="labeltd" style="width: 50%;">
                            Have you read the Camp STIX/TWIGS Code and agree to abide by the expectations set forth with the understanding that failure to comply may result in dismissal from camp? 
                        </td>
                        <td>
                            <select name="code_agree">
                                <option value=""></option>
                                <option <? if( v.code_agree == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if( v.code_agree == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </td>
                    </tr>
                    </tr>

                </table>
            </div>
        </div>


    </form>
<?

include 'common/footer.php';

