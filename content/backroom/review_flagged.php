<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-12">
            <?

            $query = $db->prepare("SELECT * FROM review WHERE (verify_text != '' OR verify_file != '') AND verified = 0 ORDER BY entered DESC");
            $query->execute();
            $rr = $query->fetchAll();

            if (!$rr) {
                //do nothing
            } else {
                ?>
                <h1>Verification Requests</h1>
                <?

                foreach ($rr as $r) {
                    $directory = sql_fetch_by_key($db, 'directory', 'id', $r['directory_id']);
                    ?>
                    <div class="review-item">
                        <b>Review For:</b><br>
                        <a href="directory.php?id=<?= $directory['id'] ?>" target="_blank"><?= $directory['business_name'] ?></a><br>
                        <div class="menu">
                            <button onclick="if(window.confirm('Really verify?')){ window.location='a.php?a=verify-review&amp;id=<?= $r['id'] ?>'; }"
                                    class="btn btn-primary">Verify Review
                            </button>
                            <button onclick="if(window.confirm('Really deny?')){ window.location='a.php?a=deny-review&amp;id=<?= $r['id'] ?>'; }"
                                    class="btn btn-primary">Deny Verification
                            </button>
                        </div>
                        <div class="review-subs">
                            <div class="review-item">
                                <div class="review-rate starrating"><?= starRateDisplay($r['rate']) ?></div>
                                <div class="review-title"><?= $r['title'] ?></div>
                                <div class="review-entered"><?= $r['entered'] ?></div>
                                <div class="review-content"><?= $r['content'] ?></div>
                                <div class="review-name">by: <?= $r['name'] ?></div>
                                <?
                                if ($r['user_id']) {

                                    $user = sql_fetch_by_key($db, 'users', 'id', $r['user_id']);
                                    ?>
                                    <br>
                                    <i>
                                        Submitted by:
                                        <a href="users.php?id=<?= $r['user_id'] ?>"><?= $user['display_name'] ?></a>
                                    </i>
                                <? } ?>
                                <br><br>
                                <b>Verification:</b><br>
                                <?= $r['verify_text'] ?>
                                <?
                                if ($r['verify_file']) {
                                    echo '<br><a href="/upload/review/'.$r['verify_file'].'" target="_blank">View File</a>';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?
                }
            }
            ?>


            <h1>Reported Reviews</h1>

            <?

            $query = $db->prepare("SELECT * FROM review WHERE report ORDER BY entered DESC");
            $query->execute();
            $rr = $query->fetchAll();

            if (!$rr) {
                echo('No reviews have been reported');
            } else {

                foreach ($rr as $r) {
                    $origin = sql_fetch_by_key($db, 'review', 'id', $r['parent']);
                    ?>
                    <div class="review-item">
                        <div class="menu">
                            <button onclick="if(window.confirm('Really delete?')){ window.location='a.php?a=delete-review&amp;id=<?= $r['id'] ?>'; }"
                                    class="btn btn-primary">Delete Review
                            </button>
                            <button
                                onclick="if(window.confirm('Really approve?')){ window.location='a.php?a=approve-review&amp;id=<?= $r['id'] ?>'; }"
                                class="btn btn-primary">Approve Review (remove report)
                            </button>
                        </div>
                        <div class="review-rate starrating"><?= starRateDisplay($origin['rate']) ?></div>
                        <div class="review-title"><?= $origin['title'] ?></div>
                        <div class="review-entered"><?= $origin['entered'] ?></div>
                        <div class="review-content"><?= $origin['content'] ?></div>
                        <div class="review-subs">
                            <div class="review-item">
                                <div class="review-title"><?= $r['title'] ?></div>
                                <div class="review-entered"><?= $r['entered'] ?></div>
                                <div class="review-content"><?= $r['content'] ?></div>
                                <div class="review-name">by: <?= $r['name'] ?></div>
                                <?
                                if ($r['user_id']) {

                                    $user = sql_fetch_by_key($db, 'users', 'id', $r['user_id']);
                                    ?>
                                    <br>
                                    <i>
                                        Submitted by:
                                        <a href="users.php?id=<?= $r['user_id'] ?>"><?= $user['display_name'] ?></a>
                                    </i>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <?
                }
            }
            ?>


        </div>
    </div>
<?
include 'common/footer.php';
?>