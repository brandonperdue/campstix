<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$rr = $db->query("SELECT * FROM product_categories ORDER BY priority ASC");

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Products</h4>
            <? button('add', 'products_.php', 'Add Product'); ?>
            <? button('details', 'option_categories.php', 'Edit Options'); ?>
            <button class="btn btn-default rearrange-enable"><i class="fa fa-fw fa-arrows"></i> Rearrange</button>
            <hr>
            <h4>Categories</h4>
            <? button('add', 'product_categories_.php', 'Add Category'); ?>
            <? cms_show_sub_items($db, 'product_categories', 0, [], false, true); ?>
            <a href="product_categories_arrange.php">Rearrange Categories</a>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Products</h1>

            <?
            $rr = $db->query("
	SELECT p.*, c.title AS category_title
	FROM products AS p LEFT OUTER JOIN product_categories AS c
		ON p.id_categories = c.id WHERE p.id_categories
	ORDER BY p.priority ASC");
            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <? print_rearrange_controls(); ?>
                <ol class="treeview list root" cmsTable="products" cmsHierarchyMode="flat">
                    <?
                    foreach ($rr as $r) {
                        ?>
                        <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                            <div class="row">
                                <div class="buttons">
                                    <div class="cell">
                                        <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=display&amp;t=products&amp;id=<?=$r['id'] ?>" title="Click to <?=$r['navhide']?'show':'hide' ?> in nav">
                                            <?
                                            if (!$r['display']) {
                                                ?><span class="fa fa-eye-slash"></span><?
                                            } else {
                                                ?><span class="fa fa-eye"></span><?
                                            }
                                            ?>
                                        </a>
                                    </div>
<!--                                    <div class="cell">--><?// duplicate_button('products', $r['id']); ?><!--</div>-->
                                    <div class="cell"><? delete_button('products', $r['id']) ?></div>
                                </div>
                                <a class="cell edit-link <?= !$r['display']?' dead':'' ?>" href="products_.php?id=<?= $r['id'] ?>">
                                    <span class="dead"><?= htmlspecialchars($r['category_title']) ?></span>
                                    <?= htmlspecialchars($r['title']) ?>
                                    $<?= number_format($r['price'], 2) ?>
                                </a>
                            </div>
                        </li>
                    <?
                    }
                    ?>
                </ol>
            <?
            }
            ?>
        </div>
    </div>
<?
include 'common/footer.php';
?>