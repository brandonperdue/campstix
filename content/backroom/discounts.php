<?
include '../common/config.php';

$rr = $db->query("SELECT * FROM discounts ORDER BY expires DESC");

if( ! admin_perm_check($db, $_SESSION['admin'], 'discounts', $news['id']) ){
	exit('Permission denied') ;
}

include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <? button('add', 'discounts_.php', 'Add Coupon Code') ; ?>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1>Coupon Codes</h1>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
	<table class="list">
		<tr>
			<th>Title</th>
			<th>Code</th>
			<th>Percent off</th>
			<th>Dollar off</th>
			<th>Expires</th>
		</tr>
		<?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
			?><tr class="<?= $class ?>">
				<td><?= htmlspecialchars($r['title']) ?></td>
				<td><?= htmlspecialchars($r['code']) ?></td>
				<td><?= htmlspecialchars($r['percent']) ?>%</td>
				<td>$<?= htmlspecialchars($r['dollaroff']) ?></td>
                <td><?= htmlspecialchars(date('F j, Y', strtotime($r['expires']))) ?></td>
				<td><? edit_button("discounts_.php?id={$r['id']}") ; ?></td>
				<td><? delete_button('discounts',$r['id']) ; ?></td>
			</tr><?
		}
	?></table><?
}

include 'common/footer.php' ;

