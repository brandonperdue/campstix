<?
include '../common/config.php' ;

$query = $db->prepare("SELECT * FROM gallery_tags ORDER BY cat DESC") ;
$query->execute(array()) ;
$rr = $query->fetchAll() ;

include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <? button('add', 'gallery_tags_.php', 'Add tag') ; ?>
        <hr>
        <h4>Categories</h4>
        <? button('add', 'gallery_tag_cats_.php', 'Add Category'); ?>
        <? cms_show_sub_items($db, 'gallery_tag_cats', 0, [], false, true); ?>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1>Gallery Tags</h1>

<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
        <ol class="treeview list root" cmsTable="products" cmsHierarchyMode="flat">
            <?
		foreach( $rr as $r ){
			$class = ++$i&1 ? 'odd':'even' ;
            $c = sql_fetch_by_key($db, "gallery_tag_cats", "id", $r['cat']) ;
			?>
            <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                <div class="row">
                    <div class="buttons">
                        <div class="cell">
                            <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=display&amp;t=gallery_tags&amp;id=<?=$r['id'] ?>" title="Click to <?=$r['navhide']?'show':'hide' ?> in nav">
                                <?
                                if (!$r['display']) {
                                    ?><span class="fa fa-eye-slash"></span><?
                                } else {
                                    ?><span class="fa fa-eye"></span><?
                                }
                                ?>
                            </a>
                        </div>
                        <div class="cell"><? delete_button('gallery_tags', $r['id']) ?></div>
                    </div>
                    <a class="cell edit-link <?= !$r['display']?' dead':'' ?>" href="gallery_tags_.php?id=<?= $r['id'] ?>">
                        <?= htmlspecialchars($r['title']) ?>
                        <span class="dead"><?=$c['title'] ?></span>
                    </a>
                </div>
			</li><?
		}
	?></ol><?
}
?>
    </div>
    </div>
<?
include 'common/footer.php' ;

