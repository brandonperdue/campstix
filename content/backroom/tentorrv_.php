<?
include '../common/config.php';

$table_name = 'tentorrv';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$v = sql_fetch_by_key($db, 'volunteers', 'id', $r['vid']);
$p = sql_fetch_by_key($db, 'products', 'id', $v['pid']);
$u = sql_fetch_by_key($db, 'users', 'id', $v['uid']);

include 'common/header.php';

$tentorrvfields = array(
	'lodging',
	'lodging_other',
	'owner',
	'owner_other',
	'stagingwith',
	'rvhookups',
	'rvhookups_other'
);

?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
				<? print_controls(); ?>
				<? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                    <a href="/a.php?a=login_as&uid=<?= $u['id'] ?>" target="_blank" class="btn btn-default" title="User"><i class="fa fa-fw fa-sign-in"></i> Login In As User</a>
				<? } ?>
				<? if ($v) { ?>
                    <a href="volunteers_.php?id=<?= $v['id'] ?>" class="btn btn-default" title="Camper"><i class="fa fa-fw fa-user"></i> Volunteer Info</a>
				<? } ?>
                <hr>
				<? button('back', "$table_name.php?pid=".$p['id'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Tent/RV Request</h1>

                <div class="row">
                    <div class="col-md-12">
                        <table class="stripe-tbl">
                            <tr>
								<?
								$i = 0;
								foreach($tentorrvfields as $f){
									if($i%2 == 0){
										echo '</tr><tr>';
									}
									echo '<th>'.$f.'</th><td>'.$r[$f].'</td>';
									$i++;
								}
								?>
                            </tr>
                        </table>
                        <hr>
                        <p>To edit the full details please
                            <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                               title="Login as <?= $u['user_email'] ?>" target="_blank">Login In As User</a></p>
                    </div>
                </div>
            </div>
        </div>


    </form>
<?

include 'common/footer.php';

