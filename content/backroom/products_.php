<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
	exit('Permission denied');
}

$table_name = 'products';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>

    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
				<? print_controls(0); ?>
                <hr>
				<? button('back', "products.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Camps</h1>

                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Title</b><br/>
                            <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>"
                                   style="width: 100%;" id="title"/>
                        </div>
                        <div class="formfield">
                            <b>Location</b> <?= $g_site_address ?>
                            /<br/>
                            <input type="text" name="keyword" class="autokeyword" rel="title"
                                   autokeywordbase="<?= htmlspecialchars($location_path_base) ?>"
                                   value="<?= $r['keyword'] ?>" style="width:75%;"/>
                        </div>
                        <div class="formfield">
                            <b>Start Date</b><br/>
                            <input type="text" class="datepicker" style="width:290px;" name="startdate"
                                   value="<?= htmlspecialchars($r['startdate']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Allow Signup After Date</b> <span class="note">This camp won't be selectable for signup until this date is reached</span><br/>
                            <input type="text" class="datepicker" style="width:290px;" name="showdate"
                                   value="<?= htmlspecialchars($r['showdate']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Allow Volunteer Signup After Date</b> <span class="note">Same as the above date but for volunteer signups</span><br/>
                            <input type="text" class="datepicker" style="width:290px;" name="showdate_volunteer"
                                   value="<?= htmlspecialchars($r['showdate_volunteer']) ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Camp Category</b> <span class="note">These are based off of the pages with "Camp" set for their template type</span><br/><?
							write_select(array(
								'rows' => $db->query("SELECT * FROM pages WHERE template='camp' ORDER BY priority ASC"),
								'value' => 'id',
								'label' => 'title',
								'name' => 'id_categories',
								'current' => $r['id_categories'],
								'default' => $_GET['id_categories'],
								'display_length' => 35,
								'blank_display_text' => 'Not a Camp'
							));
							?>
                        </div>
                        <div class="formfield">
                            <b>Signup Type</b> <span
                                    class="note">This effects the what questions registrants are asked</span><br/>
                            <select name="signuptype" id="">
                                <option value="1">STIX Camp</option>
                                <option value="2" <? if ($r['signuptype'] == 2){ ?>selected="selected"<? } ?>>TWIGS
                                    Camp
                                </option>
                                <option value="3" <? if ($r['signuptype'] == 3){ ?>selected="selected"<? } ?>>Basic
                                    Event
                                </option>
                                <option value="0" <? if ($r['signuptype'] === 0){ ?>selected="selected"<? } ?>>No
                                    Signup
                                </option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Price</b><br/>
                            $<input type="text" name="price" value="<?= htmlspecialchars($r['price']) ?>"
                                    style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Minimum Down</b><br/>
                            $<input type="text" name="minimumdown" value="<?= htmlspecialchars($r['minimumdown']) ?>"
                                    style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Financial Need Code</b> <span class="note">This code is case sensitive and is used to bypass the minimum down amount at registration.</span><br/>
                            <input type="text" name="financialneedcode"
                                   value="<?= htmlspecialchars($r['financialneedcode']) ?>" style="width:100%;"/>
                        </div>
                        <div class="formfield">
                            <b>Email Contact</b> <span
                                    class="note">This is where receipts for these orders are sent.</span><br/>
                            <input type="text" name="contact_email" value="<?= htmlspecialchars($r['contact_email']) ?>"
                                   style="width:100%;"/>
                        </div>

						<?php
						if($r['id']){
							?>
                            <hr>
                            <p>
                                <b>Testing camper signup link: </b><br>
                                &nbsp;&nbsp;
                                <a href="/checkout_process.php?a=cart-add&id=<?=$r['id'] ?>&qty=1">
                                    https://www.campstix.org/checkout_process.php?a=cart-add&id=<?=$r['id'] ?>&qty=1
                                </a>
                            </p>
                            <p>
                                <b>Testing volunteer signup link: </b><br>
                                &nbsp;&nbsp;
                                <a href="/volunteer-signup/?c=1<?=$r['id'] ?>">
                                    https://www.campstix.org/volunteer-signup/?c=<?=$r['id'] ?>
                                </a>
                            </p>
							<?php
						}
						?>
                    </div>
                    <div class="col-md-6">

                        <div class="formfield">
                            <b>Thank You Page Text</b><br/>
                            <textarea name="thankyou_text" class="rich_editor basic"
                                      style="width: 100% ; height: 250px ;"><?= $r['thankyou_text'] ?></textarea>
                        </div>
                        <div class="formfield">
                            <b>Email Receipt Text</b><br/>
                            <textarea name="reciept_text" class="rich_editor"
                                      style="width: 100% ; height: 450px ;"><?= $r['reciept_text'] ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('.datepicker').datepicker({
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
    </form>
<?
include 'common/footer.php';

