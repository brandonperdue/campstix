<?
include '../common/config.php';

$table_name = 'directory';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

if(!$r['id_user'] && $_GET['id_user']){
    $r['id_user'] = $_GET['id_user'];
}

$u = sql_fetch_by_key($db, 'users', 'id', $r['id_user']);

include 'common/header.php';


?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if($u){ ?>
                    <a href="users_.php?id=<?=$u['id'] ?>" class="btn btn-default" title="Back"><i class="fa fa-fw fa-user"></i> User Info</a>
                <? } ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Directory</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>User</b><br/><?
                            write_select(array(
                                'db' => $db,
                                'rows' => $db->query("SELECT * FROM users ORDER BY user_email ASC"),
                                'label' => 'display_name',
                                'value' => 'id',
                                'current' => $u['id'],
                                'name' => 'id_user',
                            ));
                            ?>
                        </div>
                        <div class="formfield">
                            <b>Description</b><br/>
                            <textarea name="description" class="rich_editor basic"
                              style="width: 100% ; height: 150px ;"><?= $r['description'] ?></textarea>
                        </div>
                        <div class="formfield">
                            <b>Name</b><br/>
                            <input type="text" name="name" value="<?= htmlspecialchars($r['name']) ?>" id="name" />
                        </div>
                        <div class="formfield">
                            <b>Website Url</b><br/>
                            <input type="text" name="url" value="<?= htmlspecialchars($r['url']) ?>" id="url"/>
                        </div>
                        <div class="formfield">
                            <b>Service Phone</b><br/>
                            <input type="text" name="phone" value="<?= htmlspecialchars($r['phone']) ?>" id="phone"/>
                        </div>
                        <div class="formfield">
                            <b>Email Contact</b><br/>
                            <input type="text" name="email" value="<?= htmlspecialchars($r['email']) ?>" id="email"/>
                        </div>

                        <div class="formfield">
                            <b>Location Hours</b><br/>
                            <input type="text" name="hours_location" value="<?= htmlspecialchars($r['hours_location']) ?>" id="hours_location"/>
                        </div>
                        <div class="formfield">
                            <b>On-Site Hours</b><br/>
                            <input type="text" name="hours_dispatch" value="<?= htmlspecialchars($r['hours_dispatch']) ?>" id="hours_dispatch"/>
                        </div>
                        <div class="formfield">
                            <b>Preferred Manufacturers</b><br/>
                            <input type="text" name="preferredmanufacturers" value="<?= htmlspecialchars($r['preferredmanufacturers']) ?>" id="hours_dispatch"/>
                        </div>
                        <div class="formfield">
                            <b>Are you insured?</b><br>
                            <select name="insured" id="">
                                <option value="0">No</option>
                                <option value="1" <? if($r['insured']) echo 'selected="selected"'; ?>>Yes</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Additional Info</b><br/>
                            <textarea name="additionalinfo" class=""
                                      style="width: 100% ; height: 150px ;"><?= $r['additionalinfo'] ?></textarea>
                        </div>

                        <div class="formfield">
                            <b>Logo</b><br/>
                            <?
                            if ($r['image']) {
                                ?>
                                <small>
                                    <img style="border: 0 ;" src="<?= "../upload/directory/thumb/{$r['image']}" ?>"
                                    <br/>
                                    <input type="checkbox" name="image_clear" id="image_clear"/> <label
                                        for="image_clear">Clear
                                        image</label>
                                </small>
                                <br/>
                            <?
                            }
                            ?>
                            <input type="file" name="image"/>
                        </div>
                        <br>
                        <div class="formfield">
                            <b>Warranty File (must be a .pdf)</b><br/>
                            <?
                            if ($r['warranty_file']) {
                                ?>
                                <small>
                                    <a href="/upload/directory/file/<?=$r['warranty_file']?>" target="_blank"><?=$r['warranty_file'] ?></a>
                                    <br/>
                                    <input type="checkbox" name="warranty_clear" id="warranty_clear"/> <label
                                        for="warranty_clear">Clear
                                        warranty</label>
                                </small>
                                <br/>
                            <?
                            }
                            ?>
                            <input type="file" name="warranty_file"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h2>Location Address</h2>
                        <div class="formfield">
                            <b>Address</b><br/>
                            <input type="text" name="address" value="<?= htmlspecialchars($r['address']) ?>" id="address"/>
                        </div>
                        <div class="formfield">
                            <b>Address Line 2</b><br/>
                            <input type="text" name="address_2" value="<?= htmlspecialchars($r['address_2']) ?>" id="address_2"/>
                        </div>
                        <div class="formfield">
                            <b>City</b><br/>
                            <input type="text" name="city" value="<?= htmlspecialchars($r['city']) ?>" id="city"/>
                        </div>
                        <div class="formfield">
                            <b>State</b><br/>
                            <select name="state" id="state">
                                <?
                                    foreach($config['us_states'] as $k => $v){
                                        $s = '';
                                        if($k == $r['state']){
                                            $s = 'selected="selected"';
                                        }
                                        ?>
                                        <option value="<?=$k ?>" <?=$s ?>><?=ucwords(strtolower($v)) ?></option>
                                        <?
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Zip</b><br/>
                            <input type="text" name="zip" value="<?= htmlspecialchars($r['zip']) ?>" id="zip"/>
                        </div>
                        <div class="formfield">
                            <b>Coordinates</b> <span class="note">(Used for distance calculations.  Should be in "lat, long" format)</span><br/>
                            <input type="text" name="coordinates" class="autocoordinates" id="coordinates" value="<?= $r['coordinates'] ?>"/>
                            <button id="generate_coordinates" class="btn btn-primary" style="margin-top:6px;">
                                Generate From Address
                            </button>
                        </div>

                        <h2>Mailing Address</h2>
                        <div class="formfield">
                            <b>Address</b><br/>
                            <input type="text" name="loc_address" value="<?= htmlspecialchars($r['loc_address']) ?>" id="address"/>
                        </div>
                        <div class="formfield">
                            <b>City</b><br/>
                            <input type="text" name="loc_city" value="<?= htmlspecialchars($r['loc_city']) ?>" id="city"/>
                        </div>
                        <div class="formfield">
                            <b>State</b><br/>
                            <select name="loc_state" id="loc_state">
                                <?
                                foreach($config['us_states'] as $k => $v){
                                    $s = '';
                                    if($k == $r['loc_state']){
                                        $s = 'selected="selected"';
                                    }
                                    ?>
                                    <option value="<?=$k ?>" <?=$s ?>><?=ucwords(strtolower($v)) ?></option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Zip</b><br/>
                            <input type="text" name="loc_zip" value="<?= htmlspecialchars($r['loc_zip']) ?>" id="zip"/>
                        </div>
                    </div>
                </div>
                <script>

                    $(function() {

                        $('#generate_coordinates').click(function(event) {
                            event.preventDefault();
                            $(this).html('Generating <i class="fa fa-cog fa-spin"></a>');
                            $.ajax({
                                type: 'POST',
                                url: "a_ajax.php",
                                data: {
                                    'a': 'get_coordinates',
                                    'address': $('#address').val(),
                                    'address-2': $('#address-2').val(),
                                    'city': $('#city').val(),
                                    'state': $('#state').val(),
                                    'zip': $('#zip').val()
                                }
                            }).done(function(content){
                                $('#coordinates').val(content);
                                $('#generate_coordinates').html('Coordinates Generated');
                            });
                        });
                    });
                </script>

            </div>
        </div>
    </form>
<?

include 'common/footer.php';

