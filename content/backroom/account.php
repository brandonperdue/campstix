<?
include '../common/config.php';
include 'common/header.php';
?>
<script src="/lib/bower_components/zxcvbn/zxcvbn-async.js"></script>
<script>
	$(function(){
		$('[name=new_password]').passwordStrength({
			strength_text: '#score',
			user_inputs: ['<?= htmlspecialchars($_SESSION['admin']['username']) ?>']
		});
		$('#generate').generatePassword({
			fill_password: '.fill_password',
			unhide: '#password_unhide'
		}, function() {
			$('[name=new_password]').trigger('change');
		});
	});
</script>
<h2>Update Password</h2>
<form action="a.php?a=account-password" method="post">
	<p>
		New Password<br/>
		<input type="password" class="fill_password" name="new_password"/>
		<input type="text" class="fill_password" id="password_unhide" style="display: none;" disabled/>
		<span id="score"></span><br/>
		<input id="generate" type="button" value="Generate" class="btn btn-default"/>
	</p>
	<p>
		<input type="submit" value="Save" class="btn btn-primary"/>
	</p>
</form>

<?
include 'common/footer.php';
?>
