<?
include '../common/config.php';

$parent_table = 'galleries';
$basedir = "../upload/photos";

$r = sql_fetch_by_key($db, 'photos', 'id', $_GET['id']);

if (!$r['id_parent']) {
    $r['id_parent'] = $_GET['id_parent'];
}

$editing_item = sql_fetch_by_key($db, $parent_table, 'id', $r['id_parent']);


$urladdon = '';
$backlink = 'galleries.php';
$backlinktxt = 'Galleries';

if($_GET['pid']) {
    $urladdon = 'pid='.$_GET['pid'].'&';
    $backlink = 'products_.php?id='.$_GET['pid'];
    $backlinktxt = 'Products';
}

include 'common/header.php';

?>
    <form action="a.php?<?=$urladdon ?>a=photos-save" method="post" class="editor-form" enctype="multipart/form-data" id="editor_form">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="id_parent" value="<?= $r['id_parent'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <button class="btn btn-default" name="submit" type="submit" value="Save and add another">
                    <i class="fa fa-fw fa-plus"></i> Save and add another
                </button>
                <hr>
                <? button('back', "photos.php?{$urladdon}id=" . $r['id_parent'], 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Photos</h1>

                <a href="<?=$backlink?>"><?=$backlinktxt ?></a> &gt;
                <a href="galleries_.php?<?=$urladdon ?>id=<?= htmlspecialchars($editing_item['id']) ?>"><?= htmlspecialchars($editing_item['title']) ?></a>
                &gt;
                <a href="photos.php?<?=$urladdon ?>id=<?= htmlspecialchars($editing_item['id']) ?>">Manage photos</a> &gt;
                Edit

                <div class="formfield">
                    <?= cms_photo_selector(
                        'Image',
                        'file',
                        $r['image'],
                        $basedir . '/400/',
                        $basedir . '/800/'
                    )
                    ?>
                </div>
                <div class="formfield">
                    Title<br/>
                    <input type="text" name="title" style="width: 100%;" value="<?= htmlspecialchars($r['title']) ?>"/>
                </div>
                <div class="formfield">
                    Description <span class="note">This should be a single sentence that goes below the title</span><br/>
                    <textarea name="content" style="width: 100% ; height: 200px;"
                              class="rich_editor basic"><?= htmlspecialchars($r['content']) ?></textarea>
                </div>
                <div class="formfield">
                    Alt Text <span class="note">Used for SEO and displayed if image doesn't load.</span> <br/>
                    <input type="text" name="alt" style="width: 100%;" value="<?= htmlspecialchars($r['alt']) ?>"/>
                </div>
            </div>
        </div>
    </form>
<?
include 'common/footer.php';

