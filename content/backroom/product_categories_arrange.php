<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$rr = $db->query("SELECT * FROM product_categories ORDER BY priority ASC");

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Categories</h4>
            <? button('add', 'product_categories_.php', 'Add Category'); ?>
            <? button('back', "product_categories.php", 'Back'); ?>
            <button class="btn btn-default rearrange-enable"><i class="fa fa-fw fa-arrows"></i> Rearrange</button>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Categories</h1>
                <? print_rearrange_controls(); ?>
                <? cms_show_sub_items($db, 'product_categories', 0, [], true, true); ?>
        </div>
    </div>
<?
include 'common/footer.php';
?>