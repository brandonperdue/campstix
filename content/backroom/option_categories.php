<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}

$rr = $db->query("SELECT * FROM option_categories ORDER BY priority ASC");

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'options_.php', 'Add Option'); ?>
            <button class="btn btn-default rearrange-enable"><i class="fa fa-fw fa-arrows"></i> Rearrange</button>
            <hr>
            <h4>Categories</h4>
            <? button('add', 'option_categories_.php', 'Add Category'); ?>
            <? cms_show_sub_items($db, 'option_categories', 0, [], false, true); ?>
            <a href="option_categories_rearrange.php">Rearrange Categories</a>
            <hr>
            <? button('back', "product_categories.php", 'Back to Products'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Options</h1>

            <?
            $rr = $db->query("
                SELECT p.*, c.title AS category_title
                FROM options AS p LEFT OUTER JOIN option_categories AS c
                    ON p.id_categories = c.id
                WHERE c.id
                ORDER BY p.priority ASC");
            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <? print_rearrange_controls(); ?>
                <ol class="treeview list root" cmsTable="options" cmsHierarchyMode="flat">
                    <?
                    foreach ($rr as $r) {
                        ?>
                        <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                            <div class="row">
                                <div class="buttons">
                                    <div class="cell">
                                        <a class="btn btn-default btn-xs" href="a.php?a=flag-switch&amp;f=display&amp;t=options&amp;id=<?=$r['id'] ?>" title="Click to <?=$r['navhide']?'show':'hide' ?> in nav">
                                            <?
                                            if (!$r['display']) {
                                                ?><span class="fa fa-eye-slash"></span><?
                                            } else {
                                                ?><span class="fa fa-eye"></span><?
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="cell"><? duplicate_button('options', $r['id']); ?></div>
                                    <div class="cell"><? delete_button('options', $r['id']) ?></div>
                                </div>
                                <a class="cell edit-link <?= !$r['display']?' dead':'' ?>" href="options_.php?id=<?= $r['id'] ?>">
                                    <span class="dead"><?= htmlspecialchars($r['category_title']) ?></span>
                                    <?= htmlspecialchars($r['title']) ?>
                                    <? if($p){ echo '('.$p['name'].')'; } ?>
                                    <?php

                                    $query = $db->prepare("SELECT * FROM product_options WHERE oid = ?");
                                    $query->execute(array($r['id']));
                                    $poptions = $query->fetchAll();
                                    if($poptions){
                                        $list = '';
                                        foreach ($poptions as $po) {
                                            $product = sql_fetch_by_key($db, 'products', 'id', $po['pid']);
                                            if($product['title']) {
                                                $list .= ' | ' . $product['title'];
                                            }
                                        }
                                        if($list){
                                            echo '<span href="#" title="Used In: '.trim($list,' |').'" class="fa fa-info-circle fa-fw"></span>';
                                        }
                                    }
                                    ?>
                                </a>
                            </div>
                        </li>
                    <?
                    }
                    ?>
                </ol>
            <?
            }
            ?>
        </div>
    </div>
<?
include 'common/footer.php';
?>