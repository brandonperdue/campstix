<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}
$table_name = 'settings';

$query = $db->prepare("SELECT * FROM `$table_name` ORDER BY title ASC");
$query->execute(array(':id_users'=>$user['id']));
$rr = $query->fetchAll();

include 'common/header.php' ;
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Setting Editor</h1>
<?

if( ! $rr ){
	show_nothing() ;
}else{
	?>
        <table class="list table" style="width: auto;">
            <?
            foreach( $rr as $r ){
                $class = ++$i&1 ? 'odd':'even' ;
                ?><tr class="<?= $class ?>">
                    <td class="vert-align"><?= htmlspecialchars($r['title']) ?></td>
                    <td class="vert-align"><?= htmlspecialchars(fmt_ellipsis($r['value'], 50)) ?></td>
                    <td class="vert-align"><? edit_button($table_name."_.php?id={$r['id']}") ?></td>
                </tr><?
            }
            ?>
        </table>
	<?
}
?>

    </div>
</div>
<?

include 'common/footer.php' ;

