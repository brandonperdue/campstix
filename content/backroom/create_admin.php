<?
error_reporting(E_ALL & ~E_NOTICE);
if ($_POST) {
	try {
		$db_name     = $_POST['db_name'];
		$db_host     = $_POST['db_host'];
		$db_username = $_POST['db_username'];
		$db_password = $_POST['db_password'];

		$db = new PDO("mysql:dbname=" . $db_name . ";host=" . $db_host, $db_username, $db_password);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$db->query("SET sql_mode=''");
	} catch (PDOException $e) {
		die("Problem connecting to database.");
	}
	$query = $db->prepare("SELECT * FROM admins WHERE username = ?");
	$query->execute([$_POST['new_admin_username']]);
	if ($query->rowCount()) {
		$existing_user = $query->fetch();
		die("User ".htmlspecialchars($_POST['new_admin_username'])." already exists with id ".htmlspecialchars($existing_user['id']));
	}
	try {
		require_once(__DIR__ . "/../../vendor/autoload.php");
		$hasher = new Hautelook\Phpass\PasswordHash(8, false);
		$hash = $hasher->HashPassword($_POST['new_admin_password']);
		
		$query = $db->prepare("INSERT INTO admins SET
			username = :username,
			hashed_password = :password,
			enabled = 1");
		$query->execute([':username'=>$_POST['new_admin_username'], ':password'=>$hash]);
		$id = $db->lastInsertId();
		echo "<p>User was created with ID $id</p>";
	} catch (PDOException $e) {
		die("Couldn't create user.");
	}
	try {
		$query = $db->prepare("INSERT INTO permissions SET
			id_admins = :id_admins,
			item_type = 'super'");
		$query->execute([':id_admins'=>$id]);
		echo '
			Super permission was created.
				<form style="display: inline;" method="post" action="./">
					<input type="hidden" name="username" value="'.htmlspecialchars($_POST['new_admin_username']).'"/>
					<input type="hidden" name="password" value="'.htmlspecialchars($_POST['new_admin_password']).'"/>
					<input type="hidden" name="action" value="admin_login"/>
					<input type="submit" value="Login"/>
				</form>
			
		';
	} catch (PDOException $e) {
		die("Couldn't create permissions.");
	}
}
?>
<form action="" method="post">
	<p>
		db name<br/>
		<input type="text" name="db_name" value="<?= htmlspecialchars($_POST['db_name']) ?>"/>
	</p>
	<p>
		db host<br/>
		<input type="text" name="db_host" value="<?= htmlspecialchars($_POST['db_host']) ?>"/>
	</p>
	<p>
		db username<br/>
		<input type="text" name="db_username" value="<?= htmlspecialchars($_POST['db_username']) ?>"/>
	</p>
	<p>
		db password<br/>
		<input type="text" name="db_password" value="<?= htmlspecialchars($_POST['db_password']) ?>"/>
	</p>
	<hr>
	<p>
		new admin username<br/>
		<input type="text" name="new_admin_username" value="<?= htmlspecialchars($_POST['new_admin_username']) ?>"/>
	</p>
	<p>
		new admin password<br/>
		<input type="text" name="new_admin_password" value="<?= htmlspecialchars($_POST['new_admin_password']) ?>"/>
	</p>
	<p>
		<input type="submit" value="Create user"/>
	</p>
</form>
