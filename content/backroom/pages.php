<?
include '../common/config.php' ;

if( ! admin_perm_check($db, $_SESSION['admin'], 'super') ){
	exit('Permission denied') ;
}
$root_id = 0;
if (is_numeric($_GET['root'])) {
	$root_id = $_GET['root'];
}

$root_page = get_page($db, array('id' => $root_id));

include 'common/header.php' ;
?>

<div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <a class="btn btn-success" href="pages_config_.php?id_parent=<?= htmlspecialchars($root_id) ?>"><i class="fa fa-fw fa-plus"></i> Add Page</a>
        <button class="btn btn-default rearrange-enable"><i class="fa fa-fw fa-arrows"></i> Rearrange</button>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
        <h1>Pages</h1>
        <?
        if ($root_id != 0) {
            ?>
            <p>
                <a href="pages.php">Pages</a> &gt;
                Pages under <a href="pages_.php?id=<?= htmlspecialchars($root_page['id']) ?>"><i><?= htmlspecialchars($root_page['title']) ?></a></i>
            </p>
        <?
        }

        print_rearrange_controls();

        cms_show_subpages($db, $root_id, [], true) ;

        // show orphans if any

        $orphans = $db->query("SELECT p1.* FROM pages AS p1 LEFT OUTER JOIN pages AS p2
            ON p1.id_parent = p2.id
            WHERE p1.id_parent != 0
                AND p2.id IS NULL
            GROUP BY p1.id_parent
            ORDER BY p1.priority ASC") ;

        if( $orphans->rowCount() ){
            ?>
            <p><b>Orphaned Pages</b></p>
            <?
            foreach( $orphans as $orphan ){
                cms_show_subpages($db, $orphan['id_parent']) ;
            }
        }
    ?>
    </div>
</div>
<?
include 'common/footer.php' ;

