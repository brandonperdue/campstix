<?
include '../common/config.php';
include 'common/header.php';
if ($_GET['pid']) {
    $p = sql_fetch_by_key($db, 'products', 'id', $_GET['pid']);
    ?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'campers_.php', 'Add Camper'); ?>
            <a href="campers.php" class="btn btn-default"><i class="fa fa-fw fa-tree"></i> Select Camp</a>
            <a href="a.php?a=export-campers&pid=<?= $_GET['pid'] ?>" class="btn btn-default"><i class="fa fa-fw fa-download"></i> Export All</a>
            <?
            if ($_GET['order'] == 'name') {
                $order = 'firstname';
                ?>
                <a href="campers.php?pid=<?= $_GET['pid'] ?>" class="btn btn-default"><i class="fa fa-fw fa-sort"></i> Order By Date</a>
                <?
            } else {
                $order = 'dateadded';
                ?>
                <a href="campers.php?pid=<?= $_GET['pid'] ?>&order=name" class="btn btn-default"><i class="fa fa-fw fa-sort"></i> Order By Name</a>
                <?
            }
            ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1><?= $p['title'] ?> Campers</h1>
            <a href="a.php?a=export-campers&pid=<?= $_GET['pid'] ?>&status=Paid" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Fully Paid</a>
            <h2>Fully Paid</h2>
            <?
            $where = 'status = "Paid" AND pid = ?';
            $wherearray = array($_GET['pid']);
            $query = $db->prepare("SELECT * FROM campers WHERE $where ORDER BY $order DESC");
            $query->execute($wherearray);
            $rr = $query->fetchAll();
            if (!$rr) {
                echo 'None Found';
            } else {
                ?>
                <ol class="treeview list root" cmsTable="campers" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);
                    $f = sql_fetch_by_key($db, 'funding', 'cid', $r['id']);
                    $s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <? if($f['id']){ ?>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="/fund-my-camper/<?=$f['keyword'] ?>"
                                   title="Funding page" target="_blank">
                                    <span class="fa fa-heart fa-fw"></span>
                                </a>
                            </div>
                            <? } ?>
                            <? if($s['id']){ ?>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="scholarship_.php?id=<?=$s['id'] ?>"
                                   title="Scholarship Request">
                                    <span class="fa fa-gift fa-fw"></span>
                                </a>
                            </div>
                            <? } ?>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="users_.php?id=<?= $u['id'] ?>"
                                   title="<?= $u['user_email'] ?> | Click to see User info">
                                    <span class="fa fa-user fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                                   title="Login as <?= $u['user_email'] ?>" target="_blank">
                                    <span class="fa fa-sign-in fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell"><? delete_button('campers', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="campers_.php?id=<?= $r['id'] ?>">
                            <b><?= htmlspecialchars($r['firstname'] . ' ' . $r['lastname']) ?></b>
                            <span class="rightcontent">Reg: <?= $u['user_email'] ?> | <?= date('M jS Y', strtotime($r['dateadded'])) ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
            <a href="a.php?a=export-campers&pid=<?= $_GET['pid'] ?>&status=Paid" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Fully Paid</a>
            <br>
            <br>
            <br>
            <hr>
            <br>
            <a href="a.php?a=export-campers&pid=<?= $_GET['pid'] ?>&status=Registered" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Partially Paid</a>
            <h2>Partially Paid</h2>
            <?
            $where = 'status = "Registered" AND pid = ?';
            $wherearray = array($_GET['pid']);
            $query = $db->prepare("SELECT * FROM campers WHERE $where ORDER BY $order DESC");
            $query->execute($wherearray);
            $rr = $query->fetchAll();
            if (!$rr) {
                'None Found';
            } else {
                ?>
                <ol class="treeview list root" cmsTable="campers" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);
                    $f = sql_fetch_by_key($db, 'funding', 'cid', $r['id']);
                    $u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);
                    $due = $p['price'] - $r['pledged'] - $s['amount'];
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <? if($f['id']){ ?>
                                <div class="cell">
                                    <a class="btn btn-default btn-xs" href="/fund-my-camper/<?=$f['keyword'] ?>"
                                       title="Funding page" target="_blank">
                                        <span class="fa fa-heart fa-fw"></span>
                                    </a>
                                </div>
                            <? } ?>
                            <? if($s['id']){ ?>
                                <div class="cell">
                                    <a class="btn btn-default btn-xs" href="scholarship_.php?id=<?=$s['id'] ?>"
                                       title="Scholarship Request">
                                        <span class="fa fa-gift fa-fw"></span>
                                    </a>
                                </div>
                            <? } ?>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="campers_payment_.php?id=<?= $r['id'] ?>"
                                   title="Enter Manual Payment for Camper">
                                    <span class="fa fa-money fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="users_.php?id=<?= $u['id'] ?>"
                                   title="<?= $u['user_email'] ?> | Click to see User info">
                                    <span class="fa fa-user fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $u['id'] ?>"
                                   title="Login as <?= $u['user_email'] ?>" target="_blank">
                                    <span class="fa fa-sign-in fa-fw"></span>
                                </a>
                            </div>
                            <div class="cell"><? delete_button('campers', $r['id']) ?></div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="campers_.php?id=<?= $r['id'] ?>">
                            <b><?= htmlspecialchars($r['firstname'] . ' ' . $r['lastname']) ?></b>
                            <span class="dead">$<?= number_format($due, 2) ?> of $<?= number_format($p['price'], 2) ?> due</span>
                            <span class="rightcontent">Reg: <?= $u['user_email'] ?> | <?= date('M jS Y', strtotime($r['dateadded'])) ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
            <br>
            <a href="a.php?a=export-campers&pid=<?= $_GET['pid'] ?>&status=Registered" style="float: right; margin-top:10px;" class="btn btn-primary"><i
                    class="fa fa-download fa-fw"></i> Export Partially Paid</a>
        </div>
    </div>
    <?

} else {
    $query = $db->prepare("SELECT * FROM products WHERE signuptype ORDER BY startdate DESC");
    $query->execute();
    $rr = $query->fetchAll();
    ?>
    <div class="row">
        <div class="col-lg-12">
            <h1>Select Camp</h1>
            <ol class="treeview list root">
                <?php
                foreach ($rr as $r) {
                    $query = $db->prepare("SELECT * FROM campers WHERE status != 'Unpaid' AND pid = ? ORDER BY dateadded DESC");
                    $query->execute(array($r['id']));
                    $cc = $query->fetchAll();
                    ?>
                    <li class="no-nest">
                        <div class="row">
                            <a class="cell edit-link" href="campers.php?pid=<?= $r['id'] ?>">
                                <?= htmlspecialchars($r['title']) ?> <b>(<?= count($cc) ?> Campers)</b>
                            </a>
                        </div>
                    </li>
                <? } ?>
            </ol>
        </div>
    </div>
    <?
}
include 'common/footer.php';

