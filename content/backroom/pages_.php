<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'pages', $_GET['id'])) {
    exit('Permission denied');
}

$version = isset($_GET['version']) ? $_GET['version'] : '';

$r = get_page($db, array('id' => $_GET['id']), $version);
$page_vars = $r['page_vars'];

$parent_page = get_page($db, array('id' => $r['id_parent']));

$query = $db->prepare("SELECT COUNT(id) FROM pages WHERE id_parent = :id");
$query->execute(array(':id' => $r['id']));
$num_subpages = $query->fetchColumn();

// find out if a draft exists, and what it's number is
$query = $db->prepare("SELECT * FROM pages_vars WHERE id_pages = ? AND version != ''");
$query->execute(array($r['id']));
$versioned_page_var = $query->fetch();
if ($versioned_page_var['version']) {
    $page_has_draft = true;
    $page_draft_edit_url = "pages_.php?id={$r['id']}&version={$versioned_page_var['version']}";
} else {
    $page_has_draft = false;
}

$template_keyword = $r['template'];
$template_save_url = "a.php?a=pages-save&template=$template_keyword";
$template_path = "../templates/$template_keyword" . "_edit.php";
$template_title = $config['templates'][$template_keyword];

if (!$r) {
    exit('Page not found in database');
}
if (!$template_keyword) {
    exit('No template specified');
}
if (!$config['templates'][$template_keyword]) {
    exit('Invalid template specified');
}

$front_end_link = "../{$r['keyword']}" . ($version ? "?cmsversion=$version" : '');

include 'common/header.php';

?>
    <form action="<?= $template_save_url ?>" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="version" value="<?= $version ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <?
                if ($version) {
                    ?>
                    <button type="submit" class="btn btn-primary" name="publish_draft" value="Save as Live"><i
                            class="fa fa-fw fa-floppy-o"></i> Save as Live
                    </button>
                    <a class="btn btn-default" href="pages_.php?id=<?= $r['id'] ?>"><i class="fa fa-fw fa-eye"></i>
                        Switch to Live Version</a>
                    <button type="submit" class="btn btn-default" name="save_draft" value="Save Draft"><i
                            class="fa fa-fw fa-files-o"></i> Save Draft
                    </button>
                    <button type="button" class="btn btn-cancel" name="delete" value="Delete Draft"
                            onclick="if(window.confirm('Really delete this draft?'))window.location='a.php?a=drafts-delete&id=<?= $r['id'] ?>'">
                        <i class="fa fa-fw fa-times"></i> Delete Draft
                    </button>

                <?
                }
                if (!$o['no_save'] and !$version) {
                    if ($page_has_draft) {
                        ?>
                        <button type="submit" class="btn btn-primary" name="Save" value="Save"><i
                                class="fa fa-fw fa-floppy-o"></i> Save
                        </button>
                        <a class="btn btn-default" href="<?= $page_draft_edit_url ?>"><i class="fa fa-fw fa-eye"></i>
                            View Draft</a>
                    <?
                    } else {
                        ?>
                        <button type="submit" class="btn btn-primary" name="Save" value="Save"><i
                                class="fa fa-fw fa-floppy-o"></i> Save
                        </button>
                        <button type="submit" class="btn btn-default" name="save_draft" value="Save Draft"><i
                                class="fa fa-fw fa-files-o"></i> Save Draft
                        </button>
                    <?
                    }
                }
                ?>
                <a class="btn btn-default" href="pages_config_.php?id=<?= $_GET['id'] ?>"><i
                        class="fa fa-fw fa-gear"></i> Configure Page</a>
                <hr>
                <? button('back', "pages.php", 'Back'); ?>
                <a class="btn btn-default" href="<?= $front_end_link ?>" target="_blank"
                   title="View the page on the site's front end.  Opens new window."><span
                        class="fa fa-fw fa-external-link"></span> View in Front End</a>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Pages</h1>
                <?

                if (admin_perm_check($db, $_SESSION['admin'], 'super')) {
                    include 'common/page_edit_header.php';
                }
                if (is_file($template_path)) {
                    include $template_path;
                } else {
                    ?>
                    <p>There aren't any editing options for this template type (<?= htmlspecialchars($template_title) ?>
                        ).</p>
                <?
                }
                ?>
            </div>
        </div>
    </form>
<?

include 'common/footer.php';

