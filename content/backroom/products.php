<?
include '../common/config.php';
if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
    exit('Permission denied');
}
$rr = $db->query("SELECT * FROM products ORDER BY priority ASC");

include 'common/header.php';
?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'products_.php', 'Add Camp Product'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Camps</h1>


<?
if (!$rr) {
    show_nothing();
} else {
    ?>
    <ol class="treeview list root" cmsTable="manufacturer" cmsHierarchyMode="flat">
        <?
        foreach ($rr as $r) {
            ?>
            <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                <div class="row">
                    <div class="buttons">
                        <div class="cell"><? delete_button('products', $r['id']) ?></div>
                    </div>
                    <a class="cell edit-link <?= !$r['display']?' dead':'' ?>" href="products_.php?id=<?= $r['id'] ?>">
                        <?= htmlspecialchars($r['title']) ?>
                        <span class="dead">$<?= number_format($r['price'], 2) ?></span>
                    </a>
                </div>
            </li>
            <?
        }
        ?>
    </ol>
    <?
}
?>
</div>
</div>
<?php
include 'common/footer.php';

