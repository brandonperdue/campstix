<?
include '../common/config.php';

$table_name = 'volunteers';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);
$u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);

include 'common/header.php';


?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>
        <input type="hidden" name="pid" value="<?= $r['pid'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                    <a href="/a.php?a=login_as&uid=<?= $r['uid'] ?>" target="_blank" class="btn btn-default" title="User"><i class="fa fa-fw fa-sign-in"></i> Login In As User</a>
                <? } ?>

                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Volunteer</h1>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Status</h2>

                        <div class="formfield">
                            <b>Status</b><br/>

                            <select name="status">
                                <option value=""></option>
                                <option <? if ($r['status'] == "Pending"){ ?>selected="selected"<? } ?> value="Pending">Pending</option>
                                <option <? if ($r['status'] == "Approved"){ ?>selected="selected"<? } ?> value="Approved">Approved</option>
                            </select>
                            <br/>

                        <h2>Registration Information</h2>
                        <div class="formfield">
                            <b>Parent User</b><br/><?
                            write_select(array(
                                'db'      => $db,
                                'rows'    => $db->query("SELECT * FROM users ORDER BY user_email ASC"),
                                'label'   => 'display_name',
                                'value'   => 'id',
                                'current' => $r['uid'],
                                'name'    => 'uid',
                            ));
                            ?>
                        </div>
                            <hr>
                            <table class="stripe-tbl">
                                <tr>
                            <?
                            $i = 0;
                            foreach($volunteerfieldsstix as $f){
                                if($i%2 == 0){
                                    echo '</tr><tr>';
                                }
                                echo '<th>'.$f.'</th><td>'.$r[$f].'</td>';
                                $i++;
                            }
                            ?>
                                </tr>
                            </table>

                            <hr>
                            <p>To edit the full details please
                                <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $r['uid'] ?>"
                                   title="Login as <?= $u['user_email'] ?>" target="_blank">Login In As User</a></p>
                    </div>

                </div>
        </div>


    </form>
<?

include 'common/footer.php';

