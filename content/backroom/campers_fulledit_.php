<?
include '../common/config.php';

$table_name = 'campers';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);
$f = sql_fetch_by_key($db, 'funding', 'cid', $r['id']);
$s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);

include 'common/header.php';


?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                <? } ?>

                <? if ($r['status'] != "Paid"){ ?>
                    <a href="campers_payment_.php?id=<?= $r['id'] ?>" class="btn btn-default" title="Add Payment"><i class="fa fa-fw fa-money"></i> Add Payment</a>
                <? } ?>

                <? if($s){ ?>
                    <a href="scholarship_.php?id=<?=$s['id'] ?>" class="btn btn-default" title="Campership Request"><i class="fa fa-fw fa-gift"></i> Campership Request</a>
                <? }else{ ?>
                    <a href="scholarship_.php?cid=<?=$r['id'] ?>" class="btn btn-default" title="Add Campership"><i class="fa fa-fw fa-gift"></i> Add Campership</a>

                <? } ?>
                <? if($f){ ?>
                    <a href="/fund-my-camper/<?=$f['keyword'] ?>" target="_blank" class="btn btn-default" title="Fund My Camper"><i class="fa fa-fw fa-heart"></i> Fund My Camper</a>
                <? } ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Camper</h1>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Status</h2>

                        <? if ($r['status'] != "Paid"){ ?>
                            <div class="formfield">
                                <b>Amount Paid</b><br/>
                                <input type="text" name="pledged" value="<?= $r['pledged'] ?>"/>
                                <br/>
                            </div>
                        <? }else{ ?>
                            <input type="hidden" name="pledged" value="<?= $r['pledged'] ?>"/>
                        <? }?>
                        <div class="formfield">
                            <b>Status</b><br/>

                            <select name="status">
                                <option value=""></option>
                                <option <? if ($r['status'] == "Paid"){ ?>selected="selected"<? } ?> value="Paid">Paid</option>
                                <option <? if ($r['status'] == "Registered"){ ?>selected="selected"<? } ?> value="Registered">Registered</option>
                            </select>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <h2>Registration Information</h2>
                        <div class="formfield">
                            <b>Parent User</b><br/><?
                            write_select(array(
                                'db'      => $db,
                                'rows'    => $db->query("SELECT * FROM users ORDER BY user_email ASC"),
                                'label'   => 'display_name',
                                'value'   => 'id',
                                'current' => $r['uid'],
                                'name'    => 'uid',
                            ));
                            ?>
                        </div>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="firstname" value="<?= $r['firstname'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="lastname" value="<?= $r['lastname'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Date of Birth</b><br/>
                            <input type="text" name="bdate" class="date_single" value="<?= $r['bdate'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Gender</b><br/>

                            <select name="gender">
                                <option value=""></option>
                                <option <? if ($r['gender'] == "Male"){ ?>selected="selected"<? } ?> value="Male">Male</option>
                                <option <? if ($r['gender'] == "Female"){ ?>selected="selected"<? } ?> value="Female">Female</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>T-Shirt Size</b><br/>

                            <select name="tshirt" id="">
                                <option value=""></option>
                                <option <? if ($r['tshirt'] == "Youth X-Small"){ ?>selected="selected"<? } ?> value="Youth X-Small">Youth X-Small
                                </option>
                                <option <? if ($r['tshirt'] == "Youth Small"){ ?>selected="selected"<? } ?> value="Youth Small">Youth Small</option>
                                <option <? if ($r['tshirt'] == "Youth Medium"){ ?>selected="selected"<? } ?> value="Youth Medium">Youth Medium
                                </option>
                                <option <? if ($r['tshirt'] == "Youth Large"){ ?>selected="selected"<? } ?> value="Youth Large">Youth Large</option>
                                <option <? if ($r['tshirt'] == "Youth X-Large"){ ?>selected="selected"<? } ?> value="Youth X-Large">Youth X-Large
                                </option>
                                <option <? if ($r['tshirt'] == "Adult Small"){ ?>selected="selected"<? } ?> value="Adult Small">Adult Small</option>
                                <option <? if ($r['tshirt'] == "Adult Medium"){ ?>selected="selected"<? } ?> value="Adult Medium">Adult Medium
                                </option>
                                <option <? if ($r['tshirt'] == "Adult Large"){ ?>selected="selected"<? } ?> value="Adult Large">Adult Large</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>New to Camp STIX/TWIGS?</b><br/>

                            <select name="new_to_camp">
                                <option value=""></option>
                                <option <? if ($r['new_to_camp'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['new_to_camp'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>If the camper has not attended previously, how did you hear about camp?</b><br/>

                            <input type="text" name="heard_about_from" value="<?= $r['heard_about_from'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Does this camper have diabetes?</b><br/>

                            <select name="has_diabetes" class="shownextonyes" rel="has_diabetesmore">
                                <option value=""></option>
                                <option <? if ($r['has_diabetes'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['has_diabetes'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="has_diabetesmore">
                                Age at Diagnosis<br>
                                <select name="age_of_diagnosis">
                                    <option <? if ($r['age_of_diagnosis'] == "0"){ ?>selected="selected"<? } ?> value="0">0</option>
                                    <option <? if ($r['age_of_diagnosis'] == "1"){ ?>selected="selected"<? } ?> value="1">1</option>
                                    <option <? if ($r['age_of_diagnosis'] == "2"){ ?>selected="selected"<? } ?> value="2">2</option>
                                    <option <? if ($r['age_of_diagnosis'] == "3"){ ?>selected="selected"<? } ?> value="3">3</option>
                                    <option <? if ($r['age_of_diagnosis'] == "4"){ ?>selected="selected"<? } ?> value="4">4</option>
                                    <option <? if ($r['age_of_diagnosis'] == "5"){ ?>selected="selected"<? } ?> value="5">5</option>
                                    <option <? if ($r['age_of_diagnosis'] == "6"){ ?>selected="selected"<? } ?> value="6">6</option>
                                    <option <? if ($r['age_of_diagnosis'] == "7"){ ?>selected="selected"<? } ?> value="7">7</option>
                                    <option <? if ($r['age_of_diagnosis'] == "8"){ ?>selected="selected"<? } ?> value="8">8</option>
                                    <option <? if ($r['age_of_diagnosis'] == "9"){ ?>selected="selected"<? } ?> value="9">9</option>
                                </select>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Marital status of camper's parent(s)/guardian(s)</b><br/>

                            <textarea name="parent_status"><?= $r['parent_status'] ?></textarea>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Who has legal custody of the camper?
                                <span
                                    class="note">Please list the first and last name of the individuals that have legal custody of this camper here</span>
                                <br/>

                                <textarea name="legal_custody"><?= $r['legal_custody'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>Family History
                                <span class="note">To help best serve the child, please provide any notes regarding the family (i.e. recent divorce, remarriage, death, or other pertinent event)</span>
                                <br/>

                                <textarea name="family_history"><?= $r['family_history'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <h2>Information Collected for Fundraising and Grant Writing</h2>
                        </div>
                        <div class="formfield">
                            <b>
                                Camper's Ethnicity
                                <br/>

                                <select name="ethnicity">
                                    <option value=""></option>
                                    <option <? if ($r['ethnicity'] == "American Indian or Alaska Native"){ ?>selected="selected"<? } ?>
                                            value="American Indian or Alaska Native">American Indian or Alaska Native
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Asian"){ ?>selected="selected"<? } ?> value="Asian">Asian</option>
                                    <option <? if ($r['ethnicity'] == "Black or African American"){ ?>selected="selected"<? } ?>
                                            value="Black or African American">Black
                                        or African American
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Hispanic or Latino"){ ?>selected="selected"<? } ?> value="Hispanic or Latino">
                                        Hispanic or Latino
                                    </option>
                                    <option <? if ($r['ethnicity'] == "Native Hawaiian or Other Pacific Islander"){ ?>selected="selected"<? } ?>
                                            value="Native Hawaiian or Other Pacific Islander">Native Hawaiian or Other Pacific Islander
                                    </option>
                                    <option <? if ($r['ethnicity'] == "White"){ ?>selected="selected"<? } ?> value="White">White</option>
                                    <option <? if ($r['ethnicity'] == "Undisclosed"){ ?>selected="selected"<? } ?> value="Undisclosed">Undisclosed
                                    </option>
                                </select>
                                <br/>
                        </div>

                        <h2>Medical and Activity Information</h2>

                        <div class="formfield">
                            <div class="note">
                                Please provide us with as detailed information as possible regarding the medical and dietary information requested
                                below. It is your
                                responsibility to be sure any reported allergies or medically prescribed diets are also indicated on the physician
                                release forms.
                            </div>
                        </div>
                        <div class="formfield">
                            <b>Insurance Group</b><br/>

                            <input type="text" name="insurance_group" value="<?= $r['insurance_group'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Policy Number</b><br/>

                            <input type="text" name="policy_number" value="<?= $r['policy_number'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Name of Diabetes Healthcare Provider</b><br/>

                            <input type="text" name="diabetes_provider" value="<?= $r['diabetes_provider'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Diabetes Provider's Phone Number</b><br/>

                            <input type="text" name="provider_phone" value="<?= $r['provider_phone'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Name of Primary Care Provider/Pediatrician</b><br/>

                            <input type="text" name="primary_care_name" value="<?= $r['primary_care_name'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Primary Care Provider's Phone Number</b><br/>

                            <input type="text" name="primary_provider_phone" value="<?= $r['primary_provider_phone'] ?>"/>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Diabetes Care Opportunities
                                <span class="note">With what aspect(s) of diabetes care would you like help for your child during camp? (Examples include rotating pump sites, trying new injection areas, learning to count carbs, calculating insulin , doing their pump site by themselves, etc.)</span>
                                <br/>

                                <textarea name="care_opportunities"><?= $r['care_opportunities'] ?></textarea>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Besides diabetes, does your child experience any physical, emotional, behavioral, learning, or social challenges?<span
                                    class="reqstar">*</span> </b><br/>

                            <select name="other_challenges" class="shownextonyes" rel="other_challenges_more">
                                <option value=""></option>
                                <option <? if ($r['other_challenges'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['other_challenges'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="other_challenges_more">
                                If "Yes", please explain:<br>
                                <textarea name="other_challenges_more"><?= $r['other_challenges_more'] ?></textarea>
                <span class="note">
                    Camp STIX/TWIGS volunteers strive to meet the individual needs of children with various challenges and want to make camp a positive experience for all campers. If you answered “yes” to this question we will contact you before camp to discuss your child’s individual needs and determine if they can be appropriately supported at camp. We work hard to try to accommodate children with social, emotional and behavioral difficulties, but due to the nature of camp (physical environment, staff to camper ratio, experience of volunteer staff), some challenges are not manageable in the camp environment. If you have any questions or additional information you feel we should know, please email the Camp Director at camptwigs@campstix.org.
                </span>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>Are there any activities in which the camper should not participate?</b><br/>

                            <select name="other_activities" class="shownextonyes" rel="other_activities_more">
                                <option value=""></option>
                                <option <? if ($r['other_activities'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['other_activities'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="other_activities_more">
                                If "Yes", please list the activities:<br>
                                <textarea name="other_activities_more"
                                          placeholder="I.E.: rope course, swimming, climbing wall, etc."><?= $r['other_activities_more'] ?></textarea>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Does your camper have any medical allergies (I.E.: medications, latex, bee stings, etc)?</b><br/>

                            <select name="allergies" class="shownextonyes" rel="allergies_more">
                                <option value=""></option>
                                <option <? if ($r['allergies'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['allergies'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="allergies_more">
                                If "Yes", please list all allergies and the type of reaction to them:<br>
                                <textarea name="allergies_more"
                                          placeholder="I.E.: penicillin causes rash or bee stings cause anaphylaxis"><?= $r['allergies_more'] ?></textarea>
                            </div>
                            <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is your camper allergic to any foods (I.E.: peanuts, shellfish, dairy, eggs, soy, gluten/wheat, nuts, fruit,
                                etc.)?
                                <div class="note">
                                    This information is ahead of camp to allow for the planning of the diets and ordering of the necessary specialty
                                    diet items.
                                </div>
                                <br/>


                                <select name="allergies_food" class="shownextonyes" rel="allergies_food_more">
                                    <option value=""></option>
                                    <option <? if ($r['allergies_food'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['allergies_food'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="allergies_food_more">
                                    If "Yes", please list all food allergies and the type of reaction to them:<br>
                                    <textarea name="allergies_food_more"
                                              placeholder="I.E.: penicillin causes rash or bee stings cause anaphylaxis"><?= $r['allergies_food_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is your camper on a medically prescribed diet (I.E.: gluten free, dairy free)?
                                <br/>

                                <select name="medical_diet" class="shownextonyes" rel="medical_diet_more">
                                    <option value=""></option>
                                    <option <? if ($r['medical_diet'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['medical_diet'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="medical_diet_more">
                                    If "Yes", please list:<br>
                                    <textarea name="medical_diet_more"><?= $r['medical_diet_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is there any reason that your camper would carry, or have a need for, an Epi-Pen?
                                <br/>

                                <select name="epipen" class="shownextonyes" rel="epipen_more">
                                    <option value=""></option>
                                    <option <? if ($r['epipen'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['epipen'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="epipen_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="epipen_more"><?= $r['epipen_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Does your camper have any history of asthma or other respiratory issues?
                                <br/>

                                <select name="asthma">
                                    <option value=""></option>
                                    <option <? if ($r['asthma'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['asthma'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="asthma_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="asthma_more"><?= $r['asthma_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Is there any other medical or dietary information you feel it would be important for us to be aware of during your
                                camper’s time at camp?
                                <br/>

                                <select name="important_medical_info">
                                    <option value=""></option>
                                    <option <? if ($r['important_medical_info'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['important_medical_info'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                </select>
                                <div id="important_medical_info_more">
                                    If "Yes", please explain:<br>
                                    <textarea name="important_medical_info_more"><?= $r['important_medical_info_more'] ?></textarea>
                                </div>
                                <br/>
                        </div>
                        <div class="formfield">
                            <b>
                                Are immunizations up to date for your camper?
                                <br/>

                                <select name="immunizations_up_to_date">
                                    <option value=""></option>
                                    <option <? if ($r['immunizations_up_to_date'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                    <option <? if ($r['immunizations_up_to_date'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                                    <option <? if ($r['immunizations_up_to_date'] == "We do not immunize"){ ?>selected="selected"<? } ?>
                                            value="We do not immunize">We do not immunize
                                    </option>
                                </select>
                                <div id="immunizations_date">
                                    Please enter the date of the camper's last tetanus shot:<br>
                                    <input type="text" name="immunizations_date" class="date_single" value="<?= $r['immunizations_date'] ?>"/>
                                </div>
                                <div id="immunizations_up_to_date_more">
                                    If your family chooses NOT to immunize, we require that you state your reason in the following comments field
                                    (medical, personal/philosophical or religious reasons):<br>
                                    <textarea name="immunizations_up_to_date_more"><?= $r['immunizations_up_to_date_more'] ?></textarea>
                                </div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <h2>Parent/Guardian Information</h2>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="parent_fname" value="<?= $r['parent_fname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="parent_lname" value="<?= $r['parent_lname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Relationship to Camper</b><br/>
                            <select name="guardian_relation" id="">
                                <option value=""></option>
                                <option <? if ($r['guardian_relation'] == "Mother"){ ?>selected="selected"<? } ?> value="Mother">Mother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Father"){ ?>selected="selected"<? } ?> value="Father">Father
                                </option>
                                <option <? if ($r['guardian_relation'] == "Grandmother"){ ?>selected="selected"<? } ?> value="Grandmother">
                                    Grandmother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Grandfather"){ ?>selected="selected"<? } ?> value="Grandfather">
                                    Grandfather
                                </option>
                                <option <? if ($r['guardian_relation'] == "Stepfather"){ ?>selected="selected"<? } ?> value="Stepfather">
                                    Stepfather
                                </option>
                                <option <? if ($r['guardian_relation'] == "Stepmother"){ ?>selected="selected"<? } ?> value="Stepmother">
                                    Stepmother
                                </option>
                                <option <? if ($r['guardian_relation'] == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Email Address</b><br/>

                            <input type="text" name="parent_email" value="<?= $r['parent_email'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Would you like to receive updates from Camp STIX/TWIGS about camper registration announcements and important dates?</b><br/>
                            <select name="newsletter_signup">
                                <option value=""></option>
                                <option <? if ($r['newsletter_signup'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['newsletter_signup'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Preferred Phone Number</b><br/>

                            <input type="text" name="parent_phone" value="<?= $r['parent_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>May we send text messages to the number above to contact you regarding your camper?</b><br/>
                            <select name="can_we_text_you">
                                <option value=""></option>
                                <option <? if ($r['can_we_text_you'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['can_we_text_you'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Alternate Phone Number</b><br/>

                            <input type="text" name="parent_alt_phone" value="<?= $r['parent_alt_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Address</b><br/>

                            <input type="text" name="parent_address" value="<?= $r['parent_address'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>City</b><br/>

                            <input type="text" name="parent_city" value="<?= $r['parent_city'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>State</b><br/>

                            <select name="parent_state">
                                <option value=""></option>
                                <? foreach ($config['us_states'] as $abr => $full) { ?>
                                    <option <? if ($r['parent_state'] == $abr){ ?>selected="selected"<? } ?> value="<?= $abr ?>"><?= $full ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Zip</b><br/>

                            <input type="text" name="parent_zip" value="<?= $r['parent_zip'] ?>"/>
                        </div>


                        <h2>Secondary Emergency Contact Information</h2>
                        <div class="formfield">
                            <b>First Name</b><br/>

                            <input type="text" name="emergencycontact_fname" value="<?= $r['emergencycontact_fname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Last Name</b><br/>

                            <input type="text" name="emergencycontact_lname" value="<?= $r['emergencycontact_lname'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Relationship to Camper</b><br/>
                            <select name="emergencycontact_relation" id="">
                                <option value=""></option>
                                <option <? if ($r['emergencycontact_relation'] == "Mother"){ ?>selected="selected"<? } ?> value="Mother">Mother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Father"){ ?>selected="selected"<? } ?> value="Father">Father
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Grandmother"){ ?>selected="selected"<? } ?> value="Grandmother">
                                    Grandmother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Grandfather"){ ?>selected="selected"<? } ?> value="Grandfather">
                                    Grandfather
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Stepfather"){ ?>selected="selected"<? } ?> value="Stepfather">
                                    Stepfather
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Stepmother"){ ?>selected="selected"<? } ?> value="Stepmother">
                                    Stepmother
                                </option>
                                <option <? if ($r['emergencycontact_relation'] == "Other"){ ?>selected="selected"<? } ?> value="Other">Other</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Preferred Phone Number</b><br/>
                            <input type="text" name="emergencycontact_phone" value="<?= $r['emergencycontact_phone'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Alternate Phone Number</b><br/>
                            <input type="text" name="emergencycontact_alt_phone" value="<?= $r['emergencycontact_alt_phone'] ?>"/>
                        </div>



                        <h2>Camp STIX Specific Fields</h2>
                        <div class="formfield">
                            <b>Preferred Name</b><br/>
                            <input type="text" name="preferred_name" value="<?= $r['preferred_name'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Do you live within 500 miles of Spokane, WA?</b><br/>
                            <select name="within_500_miles">
                                <option value=""></option>
                                <option <? if ($r['within_500_miles'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['within_500_miles'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Will this be your child’s first time away from home?</b><br/>
                            <select name="first_time_away_from_home">
                                <option value=""></option>
                                <option <? if ($r['first_time_away_from_home'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['first_time_away_from_home'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                        </div>
                        <div class="formfield">
                            <b>Does your camper wet the bed?</b><br/>
                            <select name="bedwetter">
                                <option value=""></option>
                                <option <? if ($r['bedwetter'] == "Yes"){ ?>selected="selected"<? } ?> value="Yes">Yes</option>
                                <option <? if ($r['bedwetter'] == "No"){ ?>selected="selected"<? } ?> value="No">No</option>
                            </select>
                            <div id="important_medical_info_more">
                                If "Yes", please describe strategies, preferences, etc.:<br>
                                <textarea name="bedwetter_more"><?= $r['bedwetter_more'] ?></textarea>
                            </div>
                        </div>
                        <div class="formfield">
                            <b>Parents Employer</b><br/>
                            <input type="text" name="parent_employer" value="<?= $r['parent_employer'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Permission is given to Camp STIX to transport my camper(s) to and from camp activities sponsored by the camp and/or to a medical facility for emergency care.</b><br/>
                            <input type="text" name="release_transport" value="<?= $r['release_transport'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Permission is given to Camp STIX to use appropriate images of my camper(s) in any photos or videos that will be approved by the Camp STIX Board of Directors to be displayed in promotional and entertainment media which is created to benefit the work of Camp STIX. I understand that there will be no compensation made in exchange for use of any photos or videos as outlined above.</b><br/>
                            <input type="text" name="release_images" value="<?= $r['release_images'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Dress Code and understand that my camper(s) will be held accountable to it while at camp.</b><br/>
                            <input type="text" name="release_dresscode" value="<?= $r['release_dresscode'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Camper Expectations and understand that my camper(s) will be held accountable to them while at camp.</b><br/>
                            <input type="text" name="release_expectations" value="<?= $r['release_expectations'] ?>" style="width:100px;" />
                        </div>
                        <div class="formfield">
                            <b>I have read the Camp STIX Code and understand that my camper(s) will be held accountable to it while at camp.</b><br/>
                            <input type="text" name="release_code" value="<?= $r['release_code'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>I have read the Riverview Bible Camp Release and Arbitration Agreement carefully and understand it.</b><br/>
                            <input type="text" name="release_biblecamp" value="<?= $r['release_biblecamp'] ?>" style="width:100px;"/>
                        </div>
                        <div class="formfield">
                            <b>Parents Full Legal Name</b><br/>
                            <input type="text" name="signature_full_legal_name" value="<?= $r['signature_full_legal_name'] ?>"/>
                        </div>
                        <div class="formfield">
                            <b>Date Signed</b><br/>
                            <input type="text" class="date_single" name="signature_date" value="<?= $r['signature_date'] ?>"/>
                        </div>
                    </div>
                </div>

                <? if($r['id']){?>
                    <br>
                    <hr>
                    <br>
                    <div class="row">
                    <div class="col-md-12">
                        <h2>Payment History</h2>
                        <?php
                        $query = $db->prepare("SELECT *
                            FROM orders
                            WHERE id_camper = ?
                            ORDER BY order_date DESC");
                        $query->execute(array($r['id']));
                        $oo = $query->fetchAll();
                        if(!$oo){
                            ?>
                            <b>No payments made</b>
                            <?
                        }else{
                            ?>
                            <table class="list" style="clear: both ;">
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Number</th>
                                    <th>Type</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                </tr>
                                <?
                                $i = 0;
                                foreach ($oo as $o) {
                                    $i = $i + 1;


                                    ?>
                                <tr class="alt2 <?= $o['refunded'] ? 'dead' : '' ?> order" rel="<?= $o['id'] ?>">
                                    <td class="r"><?= $i ?>.</td>
                                    <td class="dead r"><?= fmt_date($o['order_date']) ?></td>
                                    <td><?= $o['ordernumber'] ?></td>
                                    <td><?= $o['purchase_type'] ?></td>
                                    <td><?= htmlspecialchars($o['bi_firstname'] . ' ' . $o['bi_lastname']) ?></td>
                                    <td class="r"><?= fmt_price($o['order_grand_total']) ?></td>
                                    <td>
                                        <?= edit_button("orders_.php?id={$o['id']}") ?>
                                        <? delete_button('orders', $r['id']) ?>
                                    </td>
                                    </tr><?
                                }
                                ?>
                            </table>
                            <?
                        }
                        ?>

                    </div>
                </div>
                <? } ?>
            </div>
        </div>


    </form>
<?

include 'common/footer.php';

