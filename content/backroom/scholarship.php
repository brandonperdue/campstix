<?
include '../common/config.php';

if(!$_GET['status']){
    $_GET['status'] = 0;
    $title = "Pending";
}elseif($_GET['status'] == 1){
    $title = "Approved";
}else{
    $title = "Denied";
}

$query = $db->prepare("SELECT * FROM scholarship WHERE `status` = ? ORDER BY requestdate");
$query->execute(array($_GET['status']));
$rr = $query->fetchAll();


if( ! admin_perm_check($db, $_SESSION['admin'], 'scholarship', $news['id']) ){
    exit('Permission denied') ;
}


//$query = $db->prepare("SELECT * FROM campers");
//$query->execute();
//$cc = $query->fetchAll();
//foreach( $cc as $c ){
//    $p = sql_fetch_by_key($db, 'products', 'id', $c['pid']);
//    $s = sql_fetch_by_key($db, 'scholarship', 'cid', $c['id']);
//    //check if paid off and mark status as paid
//    $due = $p['price'] - $c['pledged'] - $s['amount'];
//    if($due < 1 && $c['status']!='Paid'){
//        sql_upsert($db, 'campers',  array('status'),  array('status'=>'Paid'), 'id', $c['id']);
//    }
//}


include 'common/header.php' ;
?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>View Requests</h4>
        <? if($_GET['status']){ ?>
            <a href="scholarship.php" class="btn btn-default"><i class="fa fa-hourglass fa-fw"></i> Pending Requests</a>
        <? } ?>
        <? if($_GET['status'] != 1){ ?>
            <a href="scholarship.php?status=1" class="btn btn-default"><i class="fa fa-thumbs-up fa-fw"></i> Accepted Requests</a>
        <? } ?>
        <? if($_GET['status'] != 2){ ?>
            <a href="scholarship.php?status=2" class="btn btn-default"><i class="fa fa-thumbs-down fa-fw"></i> Denied Requests</a>
        <? } ?>
        <hr>

        <b>Export</b>
        <div>
            <form method="post" action="a.php?a=export-scholarships">
                <p>
                <select name="camp" id="" style="width:100%;">
                    <?php
                        $query = $db->prepare("SELECT * FROM products");
                        $query->execute(array());
                        $camps = $query->fetchAll();
                        foreach($camps as $c){
                    ?>
                    <option value="<?=$c['id'] ?>"><?=$c['title'] ?></option>
                    <?php
                    }
                    ?>
                </select>
                </p>
                <p>
                <select name="status" id="" style="width:100%;">
                    <option value="">All</option>
                    <option value="1">Approved</option>
                    <option value="2">Denied</option>
                    <option value="0">Pending</option>
                    <option value="99">Approved (No Thank You)</option>
                </select>
                </p>
                <button class="btn btn-default"><i class="fa fa-fw fa-download"></i> Export Scholarships</button>
            </form>
        </div>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1><?=$title?> Scholarship Requests</h1>
<?

if( ! $rr ){
    show_nothing() ;
}else{
    ?>
    <form action="a.php?a=thankyou-bulk" method="post">
    <ol class="treeview list root" cmsTable="scholarship" cmsHierarchyMode="flat">
        <?
        foreach( $rr as $r ){
            $c = sql_fetch_by_key($db, 'campers', 'id', $r['cid']);
            ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <? if($r['status']){ ?>
                            <div class="buttons">
                                <? if($r['thankyousent']){ ?>
                                    <b>Thank You Complete</b>
                                <? }else{ ?>
                                    <label style="padding:0;margin:0;">
                                    <input type="checkbox" name="scholarship[]" value="<?=$r['id'] ?>" />
                                    Thank You Sent?
                                    </label>
                                <? } ?>
                            </div>
                        <? } ?>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="scholarship_.php?id=<?= $r['id'] ?>">
                            <b><?= $c['firstname'] ?> <?= $c['lastname'] ?></b>
                            (<?= date("M jS Y",strtotime($r['requestdate'])) ?>)
                            <? if($r['status']){ ?>
                                <span class="dead">Amount Approved: $<?= $r['amount'] ?></span>
                            <? }else{ ?>
                                <span class="dead">Request: $<?= $r['request'] ?></span>
                            <? } ?>
                        </a>
                    </div>
                </li>
            <?
        }
    ?></ol>
    <div class="text-right">
        <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-fw fa-floppy-o"></i> Update Thank You's</button>
    </div>
    </form><?
}

include 'common/footer.php' ;

