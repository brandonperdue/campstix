<?
include '../common/config.php';

$table_name = 'users';

$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

include 'common/header.php';
?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(0); ?>
                <hr>
                <? button('back', "$table_name.php".($r['user_status']==-1?'?denied=true':''), 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">
                <h1>Member</h1>
                <div class="row">
                    <div class="col-md-6">
                        <div class="formfield">
                            <b>Password</b> <span class="note">(Leave blank if unchanged)</span><br/>
                            <input type="password" name="user_pass" value="" id="user_pass" />
                        </div>
                        <div class="formfield">
                            <b>Display Name</b><br/>
                            <input type="text" name="display_name" value="<?= htmlspecialchars($r['display_name']) ?>" id="display_name"/>
                        </div>
                        <div class="formfield">
                            <b>Email</b><br/>
                            <input type="text" name="user_email" value="<?= htmlspecialchars($r['user_email']) ?>" id="user_email"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <? if($r['id']){ ?>
                            <h2>Campers</h2>
                            <?

                            $query = $db->prepare("SELECT * FROM campers WHERE uid = ? && status != 'Unpaid' ORDER BY id DESC");
                            $query->execute(array($r['id']));
                            $dd = $query->fetchAll();
                            if (!$dd) {
                                show_nothing();
                            } else {
                                foreach ($dd as $d) {
                                    ?>
                                    <p>
                                        <a class="btn btn-default" href="campers_.php?id=<?=$d['id'] ?>">
                                            <b><?=$d['firstname'] ?> <?=$d['lastname'] ?></b> | <?=$d['status']?> -
                                            <?= date('M j Y',strtotime($d['dateadded'])) ?>
                                        </a>
                                    </p>
                                    <?
                                }
                            }
                            ?>
                        <? } ?>
                    </div>
                </div>

            </div>
        </div>
    </form>
<?

include 'common/footer.php';

