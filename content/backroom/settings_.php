<?
include '../common/config.php';

if (!admin_perm_check($db, $_SESSION['admin'], 'super')) {
	exit('Permission denied');
}

$table_name = 'settings';

$r = sql_fetch_by_key($db, $table_name, 'id', $_GET['id']);

include 'common/header.php';
?>
<form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form">
	<input type="hidden" name="id" value="<?= $r['id'] ?>"/>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Controls</h4>
	        <? print_controls(array('back' => "$table_name.php")); ?>
            <hr>
            <? button('back', "$table_name.php", 'Back'); ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">
            <h1>Setting Editor</h1>
            <table class="editor">
                <tr>
                    <td style="white-space: normal;">
                        <?
                        if ($_SESSION['test']) {
                            ?>
                            <p>
                                Name <span class="note">(of the variable)</span><br/>
                                <input type="name" name="name" value="<?= htmlspecialchars($r['name']) ?>" style="width: 600px;"/>
                            </p>
                            <p>
                                Title<br/>
                                <input type="text" name="title" value="<?= htmlspecialchars($r['title']) ?>" style="width: 600px;"/>
                            </p>
                            <p>
                                Description<br/>
                                <textarea name="description" style="width: 600px; height: 100px;"><?= htmlspecialchars($r['description']) ?></textarea>
                            </p>
                            <?
                        } else {
                            ?>
                            <b><?= htmlspecialchars($r['title']) ?></b>
                            <p><?= htmlspecialchars($r['description']) ?></p>
                            <?
                        }
                        ?>
                        <p>
                            <textarea style="width: 100%; height: 300px;" <? if($r['allowhtml']) echo ' class="rich_editor basic"'; ?> name="value"><?= htmlspecialchars($r['value']) ?></textarea>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form>
<?
include 'common/footer.php';

