<?
include '../common/config.php';

$table_name = 'campers';


$r = sql_fetch_by_key($db, $table_name, "id", $_GET['id']);

$p = sql_fetch_by_key($db, 'products', 'id', $r['pid']);
$f = sql_fetch_by_key($db, 'funding', 'cid', $r['id']);
$s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);
$u = sql_fetch_by_key($db, 'users', 'id', $r['uid']);

include 'common/header.php';


?>
    <form action="a.php?a=<?= $table_name ?>-save" method="post" class="editor-form" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $r['id'] ?>"/>

        <div class="row">
            <div class="col-lg-2 controls-panel">
                <h4>Controls</h4>
                <? print_controls(); ?>
                <? if ($u) { ?>
                    <a href="users_.php?id=<?= $u['id'] ?>" class="btn btn-default" title="User"><i class="fa fa-fw fa-user"></i> User Info</a>
                    <a href="/a.php?a=login_as&uid=<?= $r['uid'] ?>" target="_blank" class="btn btn-default" title="User"><i class="fa fa-fw fa-sign-in"></i> Login In As User</a>
                <? } ?>

                <? if ($r['status'] != "Paid"){ ?>
                    <a href="campers_payment_.php?id=<?= $r['id'] ?>" class="btn btn-default" title="Add Payment"><i class="fa fa-fw fa-money"></i> Add Payment</a>
                <? } ?>

                <? if($s){ ?>
                    <a href="scholarship_.php?id=<?=$s['id'] ?>" class="btn btn-default" title="Campership Request"><i class="fa fa-fw fa-gift"></i> Campership Request</a>
                <? }else{ ?>
                    <a href="scholarship_.php?cid=<?=$r['id'] ?>" class="btn btn-default" title="Add Campership"><i class="fa fa-fw fa-gift"></i> Add Campership</a>

                <? } ?>
                <? if($f){ ?>
                    <a href="/fund-my-camper/<?=$f['keyword'] ?>" target="_blank" class="btn btn-default" title="Fund My Camper"><i class="fa fa-fw fa-heart"></i> Fund My Camper</a>
                <? } ?>
                <hr>
                <? button('back', "$table_name.php", 'Back'); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-2">

                <h1>Camper</h1>

                <div class="row">
                    <div class="col-md-12">
                        <h2>Status</h2>

                        <? if ($r['status'] != "Paid"){ ?>
                            <div class="formfield">
                                <b>Amount Paid</b><br/>
                                <input type="text" name="pledged" value="<?= $r['pledged'] ?>"/>
                                <br/>
                            </div>
                        <? }else{ ?>
                            <input type="hidden" name="pledged" value="<?= $r['pledged'] ?>"/>
                        <? }?>
                        <div class="formfield">
                            <b>Status</b><br/>

                            <select name="status">
                                <option value=""></option>
                                <option <? if ($r['status'] == "Paid"){ ?>selected="selected"<? } ?> value="Paid">Paid</option>
                                <option <? if ($r['status'] == "Registered"){ ?>selected="selected"<? } ?> value="Registered">Registered</option>
                            </select>
                            <br/>
                        </div>

                        <h2>Registration Information</h2>
                        <div class="formfield">
                            <b>Parent User</b><br/><?
                            write_select(array(
                                'db'      => $db,
                                'rows'    => $db->query("SELECT * FROM users ORDER BY user_email ASC"),
                                'label'   => 'display_name',
                                'value'   => 'id',
                                'current' => $r['uid'],
                                'name'    => 'uid',
                            ));
                            ?>
                        </div>
                        <hr>
                        <table class="stripe-tbl">
                            <tr>
                                <?
                                $i = 0;
                                foreach($camperfieldsstix as $f){
                                    if($i%2 == 0){
                                        echo '</tr><tr>';
                                    }
                                    echo '<th>'.$f.'</th><td>'.$r[$f].'</td>';
                                    $i++;
                                }
                                ?>
                            </tr>
                        </table>
                        <hr>
                        <p>To edit the full details please
                            <a class="btn btn-default btn-xs" href="/a.php?a=login_as&uid=<?= $r['uid'] ?>"
                               title="Login as <?= $u['user_email'] ?>" target="_blank">Login In As User</a></p>
                    </div>
                </div>
                <? if($r['id']){?>
                    <br>
                    <hr>
                    <br>
                    <div class="row">
                    <div class="col-md-12">
                        <h2>Payment History</h2>
                        <?php

                        $s = sql_fetch_by_key($db, 'scholarship', 'cid', $r['id']);

                        $query = $db->prepare("SELECT *
                            FROM orders
                            WHERE id_camper = ?
                            ORDER BY order_date DESC");
                        $query->execute(array($r['id']));
                        $oo = $query->fetchAll();
                        if(!$oo && !$s['amount'] && !$s['amount'] != 1){
                            ?>
                            <b>No payments made</b>
                            <?
                        }else{
                            ?>
                            <table class="list" style="clear: both ;">
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Number</th>
                                    <th>Type</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                </tr>
                                <?
                                $i = 0;
                                foreach ($oo as $o) {
                                    $i = $i + 1;


                                    ?>
                                <tr class="alt2 <?= $o['refunded'] ? 'dead' : '' ?> order" rel="<?= $o['id'] ?>">
                                    <td class="r"><?= $i ?>.</td>
                                    <td class="dead r"><?= fmt_date($o['order_date']) ?></td>
                                    <td><?= $o['ordernumber'] ?></td>
                                    <td><?= $o['purchase_type'] ?></td>
                                    <td><?= htmlspecialchars($o['bi_firstname'] . ' ' . $o['bi_lastname']) ?></td>
                                    <td class="r"><?= fmt_price($o['order_grand_total']) ?></td>
                                    <td>
                                        <?= edit_button("orders_.php?id={$o['id']}") ?>
                                        <? delete_button('orders', $r['id']) ?>
                                    </td>
                                    </tr><?
                                }
                                if($s['amount']){
                                    ?>
                                <tr class="alt2 <?= $o['refunded'] ? 'dead' : '' ?> order" rel="<?= $o['id'] ?>">
                                    <td class="r"><?= $i ?>.</td>
                                    <td class="dead r"><?= fmt_date($s['requestdate']) ?></td>
                                    <td>-</td>
                                    <td>Campership</td>
                                    <td>-</td>
                                    <td class="r"><?= fmt_price($s['amount']) ?></td>
                                    <td>
                                        <?= edit_button("scholarship_.php?id={$s['id']}") ?>
                                    </td>
                                    </tr><?
                                }
                                ?>
                            </table>
                            <?
                        }
                        ?>

                    </div>
                </div>
                <? } ?>
            </div>
        </div>


    </form>
<?

include 'common/footer.php';

