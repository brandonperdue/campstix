a<?

include '../common/config.php';

//$query = $db->prepare("SELECT * FROM orders ORDER BY order_date DESC");
//$query->execute();
//$rr = $query->fetchAll();


// page selection feature
$page_window = 40;
$page_number = $_GET['page'] ? $_GET['page'] : 1;
$sql_limit = ($page_number - 1) * $page_window . ", " . $page_window;
$sql_where = " 1 ";

// search
if (isset($_GET['q']) and $_GET['q']) {
    $search_fields = array('bi_firstname', 'bi_lastname', 'ordernumber', 'bi_email');
    $search_words = preg_split('@[^a-z0-9]+@i', $_GET['q']);
    $search_string = "CONCAT_WS(' ', " . implode(', ', $search_fields) . ")";
    foreach ($search_words as $word) {
        $sql_where .= " AND $search_string LIKE '%$word%'";
    }
}

$query = $db->prepare("SELECT SQL_CALC_FOUND_ROWS o.*, COUNT(os.id) AS order_shipments_count
	FROM orders o LEFT OUTER JOIN order_shipments os
		ON os.id_orders = o.id
	WHERE $sql_where
	GROUP BY o.id
	ORDER BY order_date DESC
	LIMIT $sql_limit");
$query->execute();
$rr = $query->fetchAll();

$count = $db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);

// create paginator html
$paginator = get_paginator(array(
    'baselink'    => $page_base . '?q=' . $_GET['q'] . '&amp;page=',
    'num_tabs'    => 20,
    'window'      => 40,
    'count'       => $count,
    'link_suffix' => '',
    'cur_page'    => $page_number,
    'show_arrows' => true,
));


include 'common/header.php';

if (!$rr) {
    show_nothing();
} else {
    ?>
    <div class="row">
    <div class="col-lg-2 controls-panel">
        <h4>Options</h4>
        <b>Export</b>
        <div>
            <script>
                $(function(){
                    $('.datepicker').datepicker({
                        dateFormat : 'yy-mm-dd'
                    });
                });
            </script>
            <form method="post" action="a.php?a=export-orders">
                Start Date
                <input type="text" class="datepicker" style="width:100%" name="startdate" value="<?=date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) ); ?>" />
                End Date
                <input type="text" class="datepicker" style="width:100%" name="enddate" value="<?=date('Y-m-d') ?>" />
                <button class="btn btn-default"><i class="fa fa-fw fa-download"></i> Export Orders</button>
            </form>
        </div>
        <hr>
        <b>Search</b>
        <div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("[name=q]").focus();
                });
            </script>
            <?
            if ($_GET['q'] || $_GET['aff']) {
                ?><a href="<?= $page_base ?>?reset_view=1"><i class="fa fa-times"></i> Clear Search</a><?
            }
            ?>
            <form method="get" action="">
                <input type="hidden" name="page" value=""/>
                <input type="text" placeholder="search text" style="width:100%" name="q" value="<?= htmlspecialchars($_GET['q']) ?>" accesskey="s"/>
                <button class="btn btn-default"><i class="fa fa-fw fa-search"></i> Search</button>
            </form>
        </div>
    </div>
    <div class="col-lg-10 col-lg-offset-2">
    <h1>Orders</h1>
    <div style="width: 100% ; overflow: hidden ;">
        <div class="paginator" style="float: left ;">
            <?= $paginator ?>
        </div>
    </div>
    <table class="list" style="clear: both ;">
        <tr>
            <th>#</th>
            <th>Date</th>
            <th>Number</th>
            <th>Type</th>
            <th>Customer</th>
            <th>Total</th>
            <!--            <th>Shipping</th>-->
        </tr>
        <?
        $i = 0;
        foreach ($rr as $r) {
            $i = $i + 1;


            ?>
        <tr class="alt2 <?= $r['refunded'] ? 'dead' : '' ?> order" rel="<?= $r['id'] ?>">
            <td class="r"><?= $i ?>.</td>
            <td class="dead r"><?= fmt_date($r['order_date']) ?></td>
            <td><?= $r['ordernumber'] ?></td>
            <td><?= $r['purchase_type'] ?></td>
            <td><?= htmlspecialchars($r['bi_firstname'] . ' ' . $r['bi_lastname']) ?></td>
            <td class="r"><?= fmt_price($r['order_grand_total']) ?></td>
            <!--                <td style="text-align: center;">-->
            <!--                    --><?//
            //                    if ($r['order_shipments_count'] > 1) {
            //                        echo '<i class="fa fa-truck"></i>('.$r['order_shipments_count'].')';
            //                    } elseif ($r['order_shipments_count'] == 1) {
            //                        echo '<i class="fa fa-truck"></i>';
            //                    } else {
            //                        // nothing
            //                    }
            //
            ?>
            <!--                </td>-->
            <td>
                <?= edit_button("orders_.php?id={$r['id']}") ?>
                <? delete_button('orders', $r['id']) ?>
            </td>
            </tr><?
        }
        ?>
    </table>
    </div>
    </div>
    <?
}
include 'common/footer.php';
