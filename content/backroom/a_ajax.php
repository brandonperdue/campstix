<?
require '../common/config.php';

switch ($_POST['a']) {
    case 'delete_option_image':
        $basepath = '../upload/patterns/' . $_POST['oid'] . '/';
        $f = $basepath . $_POST['f'];
        if (file_exists($f)) {
            unlink($f);
            echo '<i class="fa fa-trash-o fa-5x fa-fw"></i><br />File Deleted';
        }
        break;
    case 'get_coordinates':
        $fulladdress = ($_POST['address'] ? $_POST['address'] . ' ' : '') .
            ($_POST['address_2'] ? $_POST['address_2'] . ' ' : '') .
            ($_POST['city'] ? $_POST['city'] . ' ' : '') .
            ($_POST['state'] ? $_POST['state'] . ' ' : '') .
            $_POST['zip'];
        $coords = get_coords($fulladdress);


        echo implode(',', $coords);

        break;
    case 'edit_subsection':
        $query = $db->prepare("SELECT * FROM pages_sections WHERE id = ?");
        $query->execute(array($_POST['id']));
        $r = $query->fetch();

        $basedir = '../upload/page_images';
        ?>
        <a class="remove_subsection btn btn-warning" title="Remove Sub Section" href="#" onclick="return removeSubsection(this)">X</a>
        <div class="row">
            <div class="col-md-6">
                <input type="hidden" name="ss_id[]" value="<?=$r['id'] ?>">
                <input type="hidden" name="ss_count[]" value="<?=$_POST['count'] ?>">
                <div class="formfield">
                    <?= cms_photo_selector(
                        'Section Image (2000 pixels wide)',
                        'ss_image[]',
                        $r['image'],
                        $basedir . '/thumb/',
                        $basedir . '/1000/'
                    )
                    ?>
                </div>
                <div class="formfield">
                    <b>Heading</b><br/>
                    <input type="text" name="ss_header[]" value="<?= $r['header'] ?>" style="width: 100% ;"/>
                </div>
                <div class="formfield">
                    <b>Sub Heading</b><br/>
                    <input type="text" name="ss_subheader[]" value="<?= $r['subheader'] ?>" style="width: 100% ;"/>
                </div>
                <div class="formfield">
                    <b>Priority</b> <span class="note">The order in which the boxes show (low to high)</span><br/>
                    <select name="ss_priority[]">
                        <? for($i=0;$i<10;$i++){ ?>
                        <option value="<?=$i ?>" <? if($i == $r['priority']){ ?>selected="selected"<? } ?>><?=$i ?></option>
                        <? } ?>
                    </select>
                </div>
                <div class="formfield">
                    <b>Image Gallery</b> <span class="note">Gallery used for image grid.  Should have 3, 5, or 7 images.</span><br/>
                    <select name="ss_gallery[]">
                        <option value="0">No Gallery</option>
                        <?
                        $galleries = $db->query("SELECT *, title AS formatted_title FROM galleries WHERE NOT projectid ORDER BY title ASC");
                        foreach ($galleries as $g) {
                            $query = $db->prepare("SELECT * FROM photos WHERE id_parent = :id_parent ORDER BY priority");
                            $query->execute(array(
                                ':id_parent' => $g['id']
                            ));
                            $p = $query->fetchAll();
                            ?>
                            <option value="<?= $g['id'] ?>" <? if($g['id'] == $r['gallery']){ ?>selected="selected"<? } ?>>
                                <?= $g['formatted_title'] ?> (<?=count($p) ?> images)
                            </option>
                            <?
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="formfield">
                    <b>Text</b><br/>
                    <textarea name="ss_content[]" class="rich_editor basic" style="width: 98% ; height: 150px ;"><?= $r['content'] ?></textarea>
                </div>
            </div>
        </div>
        <?
        break;
}