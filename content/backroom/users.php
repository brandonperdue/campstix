<?
include '../common/config.php';

$query = $db->prepare("SELECT * FROM users ORDER BY id DESC");
$query->execute();
$rr = $query->fetchAll();


include 'common/header.php';

if($_GET['denied']){
    $status = -1;
    $btn = '<a href="users.php" class="btn btn-default" title="View Approved"><i class="fa fa-fw fa-eye"></i> View Approved Users</a>';
    $title = 'Denied Users';
}else{
    $status = 1;
    $btn = '<a href="users.php?denied=true" class="btn btn-default" title="View Denied"><i class="fa fa-fw fa-eye"></i> View Denied Users</a>';
    $title = 'Users';
}

?>
    <div class="row">
        <div class="col-lg-2 controls-panel">
            <h4>Options</h4>
            <? button('add', 'users_.php', 'Add User'); ?>
            <?//=$btn ?>
        </div>
        <div class="col-lg-10 col-lg-offset-2">

            <?

            $query = $db->prepare("SELECT * FROM users WHERE NOT user_status ORDER BY id DESC");
            $query->execute();
            $rr = $query->fetchAll();

            if ($rr && $status>0) {
                ?>
                <h1>Users</h1>
                <form action="a.php?a=user-bulk" method="post">
                    <ol class="treeview list root" cmsTable="directory" cmsHierarchyMode="flat">
                        <?
                        foreach ($rr as $r) {

                            $class = ++$i & 1 ? 'odd' : 'even';
                            if ($r['hidden']) {
                                $class .= ' dead';
                            }
                            ?>
                        <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                            <div class="row">
                                <div class="buttons">
                                    <div class="cell"> <input type="checkbox" name="user[]" value="<?=$r['id'] ?>" /> </div>
                                    <div class="cell">
                                <span class="delete_container">
                                    <a title="Delete" href="#" onclick="if(window.confirm('Really delete?')){ window.location='a.php?a=delete&amp;t=users&amp;return_url=%2Fbackroom%2Fusers.php&amp;id=<?=$r['id'] ?>'; }" class="delete_first btn btn-danger btn-xs">
                                        <span class="fa fa-trash-o"></span>
                                    </a>
                                </span>
                                    </div>
                                </div>
                                <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="users_.php?id=<?= $r['id'] ?>">
                                    <b><?= htmlspecialchars($r['display_name']) ?>
                                        <?= htmlspecialchars($r['display_lastname']) ?></b>
                                    (<?= date("M jS Y",strtotime($r['user_registered'])) ?>)
                                    <span class="dead"><?= $r['user_email'] ?></span>
                                </a>
                            </div>
                            </li><?
                        }
                        ?></ol>
                    <div class="checkbox-menu">
                        <button type="button" class="checkallbelow btn btn-primary"><i
                                class="fa fa-check-square-o"></i> Check All
                        </button>
                        <button type="button" class="checknonebelow btn btn-cancel"><i
                                class="fa fa-check-square-o"></i> Check None
                        </button>
                        <br>
                        <div class="note"><br>Note: Submitting will approve or deny all selected users and send each of them an automated email.</div>
                        <br>
                        <select name="action">
                            <option value="approve">Approve</option>
                            <option value="deny">Deny</option>
                        </select>

                        <button type="submit" class="btn btn-primary" name="submit" value="save"><i class="fa fa-fw fa-floppy-o"></i> Submit</button>
                    </div>
                    <script type="text/javascript">
                        function check_option_box() {
                            console.log(parseFloat($(this).val()));
                            if (parseFloat($(this).val()) > 0) {
                                $(this).parent().parent().parent().find('input[type="checkbox"]').prop('checked', true);
                            }
                        }
                        $(document).ready(function () {
                            $('.checkallbelow').click(function () {
                                $(this).parent().prevUntil('h3').each(function () {
                                    $(this).find('input[type="checkbox"]').prop('checked', true);
                                });
                            });
                            $('.checknonebelow').click(function () {
                                $(this).parent().prevUntil('h3').each(function () {
                                    $(this).find('input[type="checkbox"]').prop('checked', false);
                                });
                            });
                        });
                    </script>
                </form>
                <?
            }
            ?>
            <h1><?=$title ?></h1>
            <?

            $query = $db->prepare("SELECT * FROM users WHERE user_status=".$status." ORDER BY id DESC");
            $query->execute();
            $rr = $query->fetchAll();

            if (!$rr) {
                show_nothing();
            } else {
                ?>
                <ol class="treeview list root" cmsTable="directory" cmsHierarchyMode="flat">
                <?
                foreach ($rr as $r) {
                    $class = ++$i & 1 ? 'odd' : 'even';
                    if ($r['hidden']) {
                        $class .= ' dead';
                    }
                    ?>
                <li class="no-nest" id="priorityitems-<?= $r['id'] ?>">
                    <div class="row">
                        <div class="buttons">
                            <div class="cell"><? delete_button('users', $r['id']) ?></div>
                            <div class="cell">
                                <a class="btn btn-default btn-xs" href="a.php?a=user-disable&amp;id=<?=$r['id'] ?>" title="Disable"><span class="fa fa-times"></span></a>
                            </div>
                        </div>
                        <a class="cell edit-link <?= $r['hidden'] ? ' dead' : '' ?>" href="users_.php?id=<?= $r['id'] ?>">
                            <?= htmlspecialchars($r['display_name']) ?>
                            <span class="dead"><?= $r['user_email'] ?></span>
                        </a>
                    </div>
                    </li><?
                }
                ?></ol><?
            }
            ?>
        </div>
    </div>
<?
include 'common/footer.php';
